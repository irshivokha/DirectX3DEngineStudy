Texture2D TexColor : register(t0); //Texture역시 texturearray또는 Atlas로 묶은뒤 해당하는 index의 셰이더를 불러와서 부르는게 매번 Set하는 것보다 훨씬 drawCall을 줄이는 방법일 것이다.
SamplerState SamplerDefault : register(s0);


struct PS_INPUT
{
	float4 vPosition : SV_POSITION;
	float3 Color : COLOR;
};


float4 PS(PS_INPUT input) : SV_Target
{
	
	
	float4 color = float4(input.Color, 1.0f);
	return color;

}

