cbuffer cbWorld : register(b0)
{
	matrix World;
	matrix InverseWorld;
	int4 IsSkinned;
	matrix SkinnedMatrix[6];
	matrix NormalMatrix[6];
	float4 TesselationFactor;
};

// 제어점을 입력합니다.
struct VS_CONTROL_POINT_OUTPUT
{
	float3 vPosition : WORLDPOS;
	float3 Normal : NORMAL;
	float2 Tex : TEXCOORD0;
	// TODO: 다른 내용을 변경/추가합니다.
};

// 제어점을 출력합니다.
struct HS_CONTROL_POINT_OUTPUT
{
	float3 vPosition : WORLDPOS;
	float3 Normal : NORMAL;
	float2 Tex : TEXCOORD0;
	
};

// 패치 상수 데이터를 출력합니다.
struct HS_CONSTANT_DATA_OUTPUT
{
	float EdgeTessFactor[3]			: SV_TessFactor; // 예를 들어 쿼드 도메인에 대해 [4]입니다.
	float InsideTessFactor			: SV_InsideTessFactor; // 예를 들어 쿼드 도메인에 대해 Inside[2]입니다.
	// TODO: 다른 내용을 변경/추가합니다.
};

#define NUM_CONTROL_POINTS 3

// 패치 상수 함수
HS_CONSTANT_DATA_OUTPUT CalcHSPatchConstants(
	InputPatch<VS_CONTROL_POINT_OUTPUT, NUM_CONTROL_POINTS> ip,
	uint PatchID : SV_PrimitiveID)
{
	HS_CONSTANT_DATA_OUTPUT Output;

	// 여기에 출력을 계산할 코드를 삽입합니다.
	Output.EdgeTessFactor[0] = 
		Output.EdgeTessFactor[1] = 
		Output.EdgeTessFactor[2] = 
		Output.InsideTessFactor = 16.0f; // 예를 들어 대신 동적 공간 분할(tessellation) 인수를 계산할 수 있습니다.

	return Output;
}

[domain("tri")]
[partitioning("fractional_even")]
[outputtopology("triangle_cw")]
[outputcontrolpoints(3)]
[patchconstantfunc("CalcHSPatchConstants")]
HS_CONTROL_POINT_OUTPUT HS( 
	InputPatch<VS_CONTROL_POINT_OUTPUT, NUM_CONTROL_POINTS> ip, 
	uint i : SV_OutputControlPointID,
	uint PatchID : SV_PrimitiveID )
{
	HS_CONTROL_POINT_OUTPUT Output;

	// 여기에 출력을 계산할 코드를 삽입합니다.
	Output.vPosition = ip[i].vPosition;
	
	Output.Tex = ip[i].Tex;
	Output.Normal = ip[i].Normal;
	
	return Output;
}
