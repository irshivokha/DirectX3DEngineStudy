Texture2D txDiffuse : register(t0);
SamplerState samLinear : register(s0);

cbuffer PixelBuffer :register(b0)
{
	float4 pixelColor;
}

struct VS_OUTPUT
{
	float4 Position : SV_POSITION;
	float2 Tex : TEXCOORD0;
};

float4 main(VS_OUTPUT Input) : SV_TARGET
{
	// Sample the texture pixel at this location.
	float4 color = txDiffuse.Sample(samLinear, Input.Tex);

	// If the color is black on the texture then treat this pixel as transparent.
	if (color.r == 0.0f)
	{
		color.a = 0.0f;
	}

	// If the color is other than black on the texture then this is a pixel in the font so draw it using the font pixel color.
	else
	{
		color.a = 1.0f;
		color = color * pixelColor;
	}

	return color;
}