cbuffer cbEveryFrameBuffer :register(b0)
{
	matrix WVO;
};

struct VS_INPUT
{
	float4 Position : POSITION;
	float2 Tex : TEXCOORD0;
};

struct VS_OUTPUT
{
	float4 Position : SV_POSITION;
	float2 Tex : TEXCOORD0;
};

VS_OUTPUT main(VS_INPUT Input)
{
	VS_OUTPUT Output;
	Output.Position = mul(Input.Position, WVO);
	Output.Tex = Input.Tex;

	return Output;
}