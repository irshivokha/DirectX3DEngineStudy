cbuffer ScreenSizeBuffer:register(b0)
{
	float screenWidth;
	//float sampleWeight[9];
	float blurAmount;
	float2 padding;
};


struct VS_INPUT
{
	float4 Pos : POSITION;
	float2 Tex : TEXCOORD0;
};

struct PS_INPUT
{
	float4 Pos : SV_POSITION;
	float2 Tex : TEXCOORD0;
	float2 TexCoord1 : TEXCOORD1;
	float2 TexCoord2 : TEXCOORD2;
	float2 TexCoord3 : TEXCOORD3;
	float2 TexCoord4 : TEXCOORD4;
	float2 TexCoord5 : TEXCOORD5;
	float2 TexCoord6 : TEXCOORD6;
	float2 TexCoord7 : TEXCOORD7;
	float2 TexCoord8 : TEXCOORD8;
	float2 TexCoord9 : TEXCOORD9;
	float BlurAmount : TEXCOORD10;
};


PS_INPUT VS(VS_INPUT input)
{
	PS_INPUT output;
	float texelSize = 1.0f / screenWidth;

	output.Pos = input.Pos;
	output.Tex = input.Tex;
	output.TexCoord1 = input.Tex + float2(texelSize * -4.0f, 0.0f);
	output.TexCoord2 = input.Tex + float2(texelSize * -3.0f, 0.0f);
	output.TexCoord3 = input.Tex + float2(texelSize * -2.0f, 0.0f);
	output.TexCoord4 = input.Tex + float2(texelSize * -1.0f, 0.0f);
	output.TexCoord5 = input.Tex + float2(texelSize *  0.0f, 0.0f);
	output.TexCoord6 = input.Tex + float2(texelSize *  1.0f, 0.0f);
	output.TexCoord7 = input.Tex + float2(texelSize *  2.0f, 0.0f);
	output.TexCoord8 = input.Tex + float2(texelSize *  3.0f, 0.0f);
	output.TexCoord9 = input.Tex + float2(texelSize *  4.0f, 0.0f);
	output.BlurAmount = blurAmount;

	return output;
}