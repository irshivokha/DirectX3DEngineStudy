cbuffer cbEveryFrameBuffer :register(b0)
{
	matrix WVP;
	matrix World;
};

struct VS_INPUT
{
	float4 Position : POSITION;
	float3 Normal : NORMAL;
	float2 Tex : TEXCOORD0;
};

struct VS_OUTPUT
{
	float4 Position : SV_POSITION;
	float3 Normal : NORMAL;
	float2 Tex : TEXCOORD0;
};

VS_OUTPUT main(VS_INPUT Input)
{
	VS_OUTPUT Output;

	Output.Position = mul(Input.Position, WVP);
	Output.Normal = mul(Input.Normal, (float3x3)World);
	Output.Normal  = normalize(Output.Normal);
	Output.Tex = Input.Tex;

	return Output;
}