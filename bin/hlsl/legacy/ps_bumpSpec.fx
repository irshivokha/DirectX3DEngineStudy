Texture2D txDiffuse : register(t0);
Texture2D txNormal : register(t1);
Texture2D txSpec : register(t2);
SamplerState samLinear : register(s0);


struct PS_INPUT
{
	float4 Pos : SV_POSITION;
	float2 mUV : TEXCOORD0;
	float3 N : NORMAL;
	float3 T : TANGENT;
	float3 B : BINORMAL;
	float3 LightDir : TEXCOORD1;
	float3 ViewDir : TEXCOORD2;
};

float4 PS(PS_INPUT Input) : SV_Target
{
float3 gLightColor;
gLightColor.rgb = 1.0f;

float3 tangentNormal = txNormal.Sample(samLinear , Input.mUV).xyz;
tangentNormal = normalize(tangentNormal * 2 - 1); //convert 0~1 to -1~+1.

												  //read from vertex shader
float3x3 TBN = float3x3(normalize(Input.T), normalize(Input.B),
	normalize(Input.N)); //transforms world=>tangent space

TBN = transpose(TBN); //transform tangent space=>world

float3 worldNormal = mul(TBN, tangentNormal); //note: mat * scalar
											  //(since TBN is row matrix)

float3 lightDir = normalize(Input.LightDir);
float3 diffuse = saturate(dot(worldNormal, -lightDir));

//float4 albedo = tex2D(samLinear, Input.mUV);
float4 albedo = txDiffuse.Sample(samLinear, Input.mUV);

diffuse = gLightColor * albedo.rgb * diffuse;

float3 specular = 0;
if (diffuse.x > 0)
{
	float3 reflection = reflect(lightDir, worldNormal);
	float3 viewDir = normalize(Input.ViewDir);

	specular = saturate(dot(reflection, -viewDir));
	specular = pow(specular, 20.0f);

	//further adjustments to specular (since texture is 2D)
	//float specularIntensity = tex2D(SpecularSampler, Input.mUV);
	float specularIntensity = txSpec.Sample(samLinear, Input.mUV).xyz;
	specular *= specularIntensity * gLightColor;
}

float3 ambient = float3(0.1f, 0.1f, 0.1f) * albedo;

return float4(ambient + diffuse + specular, 1);
}









