cbuffer cbEveryFrameBuffer : register(b0)
{
	matrix World;
	matrix View;
	matrix Projection;
};

cbuffer FogBuffer
{
	float fogStart;
	float fogEnd;
}


struct VS_INPUT
{
	float4 Position : POSITION;
	float2 Tex : TEXCOORD0;
};

struct VS_OUTPUT
{
	float4 Position : SV_POSITION;
	float2 Tex : TEXCOORD0;
	float fogFactor : FOG;
};

VS_OUTPUT main(VS_INPUT Input)
{
	VS_OUTPUT Output;
	float4 cameraPosition;
	
	Input.Position.w = 1.0;
	

    Output.Position = mul(Input.Position, World);
    Output.Position = mul(Output.Position, View);
    Output.Position = mul(Output.Position, Projection);

	Output.Tex = Input.Tex;

    cameraPosition = mul(Input.Position, World);
    cameraPosition = mul(Input.Position, View);

	//Caculate linear fog
	Output.fogFactor = saturate((fogEnd - cameraPosition.z)/(fogEnd-fogStart));

	return Output;
}