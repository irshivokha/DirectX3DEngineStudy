Texture2D txDiffuse : register(t0);
SamplerState samLinear : register(s0);


struct PS_INPUT
{
	float4 Pos : SV_POSITION;
	float2 Tex : TEXCOORD0;
	float2 TexCoord1 : TEXCOORD1;
	float2 TexCoord2 : TEXCOORD2;
	float2 TexCoord3 : TEXCOORD3;
	float2 TexCoord4 : TEXCOORD4;
	float2 TexCoord5 : TEXCOORD5;
	float2 TexCoord6 : TEXCOORD6;
	float2 TexCoord7 : TEXCOORD7;
	float2 TexCoord8 : TEXCOORD8;
	float2 TexCoord9 : TEXCOORD9;
	float BlurAmount : BlurAmount;
};

float4 PS(PS_INPUT input) : SV_Target
{
	float weight0, weight1, weight2, weight3, weight4;
	float normalization;
	float4 color;
	//k
	//weight0 = 1.0f;
	//weight1 = 0.9f;
	//weight2 = 0.55f;
	//weight3 = 0.18f;
	//weight4 = 0.1f;

	weight0 = (exp(-(0 * 0) / (2 * input.BlurAmount * input.BlurAmount)));
	weight1 = (exp(-(1 * 1) / (2 * input.BlurAmount * input.BlurAmount)));
	weight2 = (exp(-(2 * 2) / (2 * input.BlurAmount * input.BlurAmount)));
	weight3 = (exp(-(3 * 3) / (2 * input.BlurAmount * input.BlurAmount)));
	weight4 = (exp(-(4 * 4) / (2 * input.BlurAmount * input.BlurAmount)));


	normalization = (weight0 + 2.0f * (weight1 + weight2 + weight3 + weight4));

	// Normalize the weights.
	weight0 = weight0 / normalization;
	weight1 = weight1 / normalization;
	weight2 = weight2 / normalization;
	weight3 = weight3 / normalization;
	weight4 = weight4 / normalization;
	

	// Initialize the color to black.
	color = float4(0.0f, 0.0f, 0.0f, 0.0f);

	// Add the nine horizontal pixels to the color by the specific weight of each.
		color += txDiffuse.Sample(samLinear, input.TexCoord1) * weight4;
		color += txDiffuse.Sample(samLinear, input.TexCoord2) * weight3;
		color += txDiffuse.Sample(samLinear, input.TexCoord3) * weight2;
		color += txDiffuse.Sample(samLinear, input.TexCoord4) * weight1;
		color += txDiffuse.Sample(samLinear, input.TexCoord5) * weight0;
		color += txDiffuse.Sample(samLinear, input.TexCoord6) * weight1;
		color += txDiffuse.Sample(samLinear, input.TexCoord7) * weight2;
		color += txDiffuse.Sample(samLinear, input.TexCoord8) * weight3;
		color += txDiffuse.Sample(samLinear, input.TexCoord9) * weight4;


	//color = txDiffuse.Sample(samLinear, input.Tex);

	//Finally we set the alpha value.

	// Set the alpha channel to one.
	color.a = 1.0f;

	return color;

}








