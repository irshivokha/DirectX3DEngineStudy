/////////////
// GLOBALS //
/////////////

Texture2D shaderTextures[2] : register(t0);
SamplerState SampleType : register(s0);


//////////////
// TYPEDEFS //
//////////////
struct PS_INPUT
{
	float4 Position : SV_POSITION;
	float2 Tex : TEXCOORD0;
};


////////////////////////////////////////////////////////////////////////////////
// Pixel Shader
////////////////////////////////////////////////////////////////////////////////
float4 main(PS_INPUT Input) : SV_TARGET
{
float4 color1;
float4 color2;
float4 blendColor;


// Get the pixel color from the first texture.
color1 = shaderTextures[0].Sample(SampleType, Input.Tex);

// Get the pixel color from the second texture.
color2 = shaderTextures[1].Sample(SampleType, Input.Tex);

// Combine the two textures based on the alpha value.
blendColor = color1*color2;

// Saturate the final color value.
//blendColor = saturate(blendColor);

return blendColor;
}
