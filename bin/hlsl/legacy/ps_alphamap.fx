/////////////
// GLOBALS //
/////////////

Texture2D shaderTextures[3] : register(t0);
SamplerState SampleType : register(s0);


//////////////
// TYPEDEFS //
//////////////
struct PS_INPUT
{
    float4 Position : SV_POSITION;
    float2 Tex : TEXCOORD0;
};
//In the alpha map pixel shader we first take a sample of the pixel from the two color textures and alpha texture. Then we multiply the alpha value by the base color to get the pixel value for the base texture. After that we multiply the inverse of the alpha (1.0 - alpha) by the second color texture to get the pixel value for the second texture. 
//We then add the two pixel values together and saturate to produce the final blended pixel.

////////////////////////////////////////////////////////////////////////////////
// Pixel Shader
////////////////////////////////////////////////////////////////////////////////
float4 PS(PS_INPUT Input) : SV_TARGET
{
    float4 color1;
    float4 color2;
    float4 alphaValue;
    float4 blendColor;


    // Get the pixel color from the first texture.
    color1 = shaderTextures[0].Sample(SampleType, Input.Tex);

    // Get the pixel color from the second texture.
    color2 = shaderTextures[1].Sample(SampleType, Input.Tex);

    // Get the alpha value from the alpha map texture.
    alphaValue = shaderTextures[2].Sample(SampleType, Input.Tex);
	
    // Combine the two textures based on the alpha value.
    blendColor = (alphaValue * color1) + ((1.0 - alphaValue) * color2);
    
    // Saturate the final color value.
    blendColor = saturate(blendColor);

    return blendColor;
}
