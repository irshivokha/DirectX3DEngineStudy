Texture2D txDiffuse : register( t0 );
Texture2D txNormal : register(t1);
SamplerState samLinear : register( s0 );

cbuffer cbChangesEveryFrame : register( b0 )
{
 //   float4 vMeshColor;
    float4 vLightDir;
};

struct PS_INPUT
{
	float4 Pos : SV_POSITION;
	float2 Tex : TEXCOORD0;
	float3 Norm : NORMAL;
	float3 Tangent : TANGENT;
	float3 Binormal : BINORMAL;
};

float4 PS( PS_INPUT input) : SV_Target
{
	//read normal from tex
	float3 tangentNormal = txNormal.Sample(samLinear , input.Tex).xyz;
	tangentNormal = normalize(tangentNormal * 2 - 1); //convert 0~1 to -1~+1.

	float3x3 TBN = float3x3(input.Tangent, input.Binormal , input.Norm); //transforms world=>tangent space
	TBN = transpose(TBN); //transform tangent space=>world
	float3 worldNormal = mul(TBN, tangentNormal); //note: mat * scalar
												  //(since TBN is row matrix)
	float3 lightDir = vLightDir.xyz;
	float3 diffuse = saturate(dot(worldNormal, -lightDir));

	float4 albedo = txDiffuse.Sample(samLinear, input.Tex);
	diffuse = albedo.rgb * diffuse;

	float3 ambient = float3(0.3f, 0.3f, 0.3f) * albedo.rgb;

	return float4(ambient + diffuse, 1);
}









