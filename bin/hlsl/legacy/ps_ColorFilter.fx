Texture2D txDiffuse : register( t0 );
SamplerState samLinear : register( s0 );

Texture2D txDepth : register(t1);
SamplerState DepthSamLinear : register(s1);


cbuffer cbChangesEveryFrame : register(b0)
{
	//float4x4 Value;
	int FilterSwitch;
	float3 padding;
};


static const float4x4 ColorFilter = { 1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1 };

static const float4x4 InverseFilter = { -1, 0, 0, 0,
0, -1, 0, 0,
0, 0, -1, 0,
1, 1, 1, 1 };

static const float3 GrayScaleIntensity = { 0.299f, 0.587f, 0.114f };

static const float3x3 SepiaFilter = { 0.393f, 0.349f, 0.272f,
0.769f, 0.686f, 0.534f,
0.189f, 0.168f, 0.131f };

struct PS_INPUT
{
	float4 Pos : SV_POSITION;
	float2 Tex : TEXCOORD0;
};

float ConvertToLinear(float InDepth)

{

	float FarZ = 100.0f;
	float NearZ = 1.0f;

	float PROJECTION_A = FarZ / (FarZ - NearZ);

	float PROJECTION_B = (-NearZ) / (FarZ - NearZ);

	return PROJECTION_B / (InDepth - PROJECTION_A);

}

float4 PS( PS_INPUT input) : SV_Target
{
	float4 color = txDiffuse.Sample(samLinear, input.Tex);
	float4 DepthColor = txDepth.Sample(DepthSamLinear, input.Tex);
	float4 finalColor;

	if (FilterSwitch == 0)
	{
		finalColor = color;
		return finalColor;
	}

	else if (FilterSwitch == 1)
	{
		finalColor = float4(mul(color, InverseFilter).rgb, color.a);
	}

	else if (FilterSwitch ==2)
	{
		finalColor = float4(mul(color.rgb, SepiaFilter), color.a);
	}

	else if (FilterSwitch == 3)
	{
		float intensity = dot(color.rgb, GrayScaleIntensity);
		finalColor = float4(intensity.rrr, color.a);
	}
	
	return finalColor;

}









