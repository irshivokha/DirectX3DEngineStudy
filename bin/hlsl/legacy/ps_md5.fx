Texture2D txDiffuse : register(t0);
Texture2D txNormal : register(t1);
SamplerState samLinear : register(s0);

struct VS_OUTPUT
{
	float4 Pos : SV_POSITION;
	float2 TexCoord : TEXCOORD;
	float3 normal : NORMAL;
	float3 tangent : TANGENT;
	float4 LightDir : TEXCOORD1;
	float4 LightColor : TEXCOORD2;
};



float4 PS(VS_OUTPUT input) : SV_TARGET
{
input.normal = normalize(input.normal);

//If material has a diffuse texture map, set it now
float4 diffuse = txDiffuse.Sample(samLinear, input.TexCoord);

//If material has a normal map, we can set it now
//Load normal from normal map
float4 normalMap = txNormal.Sample(samLinear, input.TexCoord);

//Change normal map range from [0, 1] to [-1, 1]
normalMap = (2.0f*normalMap) - 1.0f;

//Make sure tangent is completely orthogonal to normal
input.tangent = normalize(input.tangent - dot(input.tangent, input.normal)*input.normal);

//Create the biTangent
float3 biTangent = cross(input.normal, input.tangent);

//Create the "Texture Space"
float3x3 texSpace = float3x3(input.tangent, biTangent, input.normal);

//Convert normal from normal map to texture space and store in input.normal
input.normal = normalize(mul(normalMap, texSpace));


float3 finalColor;

finalColor = diffuse * input.LightColor;
finalColor += saturate(dot(input.LightDir, input.normal) * input.LightColor * diffuse);

return float4(finalColor, diffuse.a);
}