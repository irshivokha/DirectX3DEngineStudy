
cbuffer cbChangesEveryFrame : register( b0 )
{
	matrix World;
	matrix WVP;
};

struct VS_INPUT
{
    float4 Pos : POSITION;
    float2 Tex : TEXCOORD0;
	float3 Norm : NORMAL;
	float3 Tangent : TANGENT;
	float3 Binormal : BINORMAL;
};

struct PS_INPUT
{
    float4 Pos : SV_POSITION;
	float2 Tex : TEXCOORD0;
    float3 Norm : NORMAL;
	float3 Tangent : TANGENT;
	float3 Binormal : BINORMAL;
};

PS_INPUT VS( VS_INPUT input )
{
    PS_INPUT output = (PS_INPUT)0;
    output.Pos = mul( input.Pos, WVP);
    output.Tex = input.Tex;
    output.Norm = mul( input.Norm , World);
	output.Tangent = mul(input.Tangent, World);
	output.Binormal = mul(input.Binormal, World);
    return output;
}


