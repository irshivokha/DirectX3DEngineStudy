
Texture2D txDiffuse : register(t0);
SamplerState SamplerType : register(s0);


cbuffer TexTransbuffer : register(b0)
{
    float4 UVTranslate;
};

struct PS_INPUT
{
    float4 position : SV_POSITION;
    float2 tex : TEXCOORD0;
    //float clip : SV_ClipDistance0;
};

float4 main(PS_INPUT Input) : SV_TARGET
{
    return txDiffuse.Sample(SamplerType, Input.tex + UVTranslate.xy);
}
	
