
cbuffer cbChangesEveryFrame : register(b0)
{
	matrix WVO;
};

struct VS_INPUT
{
	float4 Pos : POSITION;
	float2 Tex : TEXCOORD;
};

struct PS_INPUT
{
	float4 Pos : SV_POSITION;
	float2 Tex : TEXCOORD0;
};

PS_INPUT main(VS_INPUT input)
{
	PS_INPUT output = (PS_INPUT)0;

	output.Pos.w = 1.0f;
	output.Pos = mul(input.Pos, WVO);
	output.Tex = input.Tex;
	return output;
}