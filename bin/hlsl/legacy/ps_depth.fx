Texture2D txDiffuse : register( t0 );
SamplerState samLinear : register( s0 );

cbuffer distanceBuffer:register(b0)
{
	float far_clamp;
	float d_far;
	float d_focus;
	float d_near;
};

struct PS_INPUT
{
	float4 Position : SV_POSITION;
	float4 DepthPos : TEXCOORD0;
};


float ComputeDepthBlur(float depth)
{
	float f;

	if (depth < d_focus)
	{
		// scale depth value between near blur distance and focal distance to [-1, 0] range
		f = (depth - d_focus) / (d_focus - d_near);
	}
	else
	{
		// scale depth value between focal distance and far blur
		// distance to [0, 1] range
		f = (depth - d_focus) / (d_far - d_focus);

		// clamp the far blur to a maximum blurriness
		f = clamp(f, 0, far_clamp);
	}

	// scale and bias into [0, 1] range
	return f * 0.5f + 0.5f;
}

float4 PS( PS_INPUT input) : SV_Target
{
	float depthValue;
	float4 color;
	depthValue = input.Position.z / input.Position.w;

	// First 10% of the depth buffer color red.
	if (depthValue < 0.9f)
	{
		color = float4(1.0, 0.0f, 0.0f, 1.0f);
	}

	// The next 0.025% portion of the depth buffer color green.
	if (depthValue > 0.9f)
	{
		color = float4(0.0, 1.0f, 0.0f, 1.0f);
	}

	// The remainder of the depth buffer color blue.
	if (depthValue > 0.925f)
	{
		color = float4(0.0, 0.0f, 1.0f, 1.0f);
	}

	return Color;

}









