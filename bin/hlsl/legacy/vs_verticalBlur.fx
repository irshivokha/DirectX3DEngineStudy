cbuffer ScreenSizeBuffer:register(b0)
{
	float screenHeight;
	float blurAmount;
	//float sampleWeight[9];
	float2 padding;
};



//////////////
// TYPEDEFS //
//////////////
struct VS_INPUT
{
	float4 position : POSITION;
	float2 Tex : TEXCOORD0;
};

struct PS_INPUT
{
	float4 position : SV_POSITION;
	float2 Tex : TEXCOORD0;
	float2 TexCoord1 : TEXCOORD1;
	float2 TexCoord2 : TEXCOORD2;
	float2 TexCoord3 : TEXCOORD3;
	float2 TexCoord4 : TEXCOORD4;
	float2 TexCoord5 : TEXCOORD5;
	float2 TexCoord6 : TEXCOORD6;
	float2 TexCoord7 : TEXCOORD7;
	float2 TexCoord8 : TEXCOORD8;
	float2 TexCoord9 : TEXCOORD9;
	float BlurAmount : TEXCOORD10;
};


////////////////////////////////////////////////////////////////////////////////
// Vertex Shader
////////////////////////////////////////////////////////////////////////////////
PS_INPUT VS(VS_INPUT input)
{
	PS_INPUT output;
	float texelSize;


	//The texel size is based on the height value.
	// Determine the floating point size of a texel for a screen with this specific height.
	texelSize = 1.0f / screenHeight;

//The offsets are modified by only the height value, the width stays at its current value.
// Create UV coordinates for the pixel and its four vertical neighbors on either side.
	output.position = input.position;
	output.Tex = input.Tex;
	output.TexCoord1 = input.Tex + float2(0.0f, texelSize * -4.0f);
	output.TexCoord2 = input.Tex + float2(0.0f, texelSize * -3.0f);
	output.TexCoord3 = input.Tex + float2(0.0f, texelSize * -2.0f);
	output.TexCoord4 = input.Tex + float2(0.0f, texelSize * -1.0f);
	output.TexCoord5 = input.Tex + float2(0.0f, texelSize *  0.0f);
	output.TexCoord6 = input.Tex + float2(0.0f, texelSize *  1.0f);
	output.TexCoord7 = input.Tex + float2(0.0f, texelSize *  2.0f);
	output.TexCoord8 = input.Tex + float2(0.0f, texelSize *  3.0f);
	output.TexCoord9 = input.Tex + float2(0.0f, texelSize *  4.0f);
	output.BlurAmount = blurAmount;

	return output;
}
