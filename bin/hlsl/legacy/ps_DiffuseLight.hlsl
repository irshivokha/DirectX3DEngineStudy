Texture2D txDiffuse : register(t0);
SamplerState samLinear : register(s0);

cbuffer LightBuffer :register(b0)
{
	float4 DiffuseColor;
	float4 AmbientColor;
	float4 LightDir;
	float4 SpecularPower;
	float4 ReturnPosNormal;
};

struct VS_OUTPUT
{
	float4 Position : SV_POSITION;
	float3 Normal : NORMAL;
	float2 Tex : TEXCOORD0;
	float3 ViewDirection : TEXCOORD1;


};

float4 main(VS_OUTPUT Input) : SV_TARGET
{

	float3 LightDirection = -LightDir.xyz;
	float LightIntensity = saturate(dot(Input.Normal, LightDirection));

	float4 TexColor = txDiffuse.Sample(samLinear, Input.Tex);
	float4 Color = AmbientColor;
	float3 reflection = normalize(reflect(LightDir,Input.Normal));
	float4 specular = float4(0.0f, 0.0f, 0.0f, 0.0f);

	if (LightIntensity > 0.0f)
	{
		Color += (LightIntensity*DiffuseColor);
		saturate(Color);
		specular = pow(saturate(dot(reflection, -Input.ViewDirection)), SpecularPower.x);
	}
	
	Color += specular;

	saturate(Color);

	Color = Color * TexColor;

	if (ReturnPosNormal.x == 1.0f&&ReturnPosNormal.y == 0.0f)			//Pos를 반환한다
		Color = Input.Position;
	else if (ReturnPosNormal.x == 0.0f&&ReturnPosNormal.y == 1.0f)		//Normal을 반환한다
		Color = float4(Input.Normal,1.0f);
	return Color;

}