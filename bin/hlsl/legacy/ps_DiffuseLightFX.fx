Texture2D txDiffuse : register(t0);
SamplerState samLinear : register(s0);

cbuffer LightBuffer :register(b0)
{
	float4 DiffuseColor;
	float4 AmbientColor;
	float4 LightDir;
};

struct VS_OUTPUT
{
	float4 Position : SV_POSITION;
	float3 Normal : NORMAL;
	float2 Tex : TEXCOORD0;
	
	
};

float4 main(VS_OUTPUT Input) : SV_TARGET
{
	
	float3 LightDirection = -LightDir.xyz;
	float LightIntensity = saturate(dot(Input.Normal, LightDirection));
	
	float4 TexColor = txDiffuse.Sample(samLinear, Input.Tex);
	float4 Color = AmbientColor;

	if (LightIntensity > 0.0f)
		Color += (LightIntensity*DiffuseColor);

	saturate(Color);

	Color = Color * TexColor;

	Color.a = 1.0f;
	return Color;
	
}