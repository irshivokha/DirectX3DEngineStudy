cbuffer MatrixBuffer
{
    float4x4 World;
    float4x4 View;
    float4x4 Projection;
};

cbuffer ClipPlaneBuffer
{
    float4 clipPlane;
};

struct VS_INPUT
{
    float4 position : POSITION;
    float2 tex : TEXCOORD0;
};

struct PS_INPUT
{
    float4 position : SV_POSITION;
    float2 tex : TEXCOORD0;
    float clip : SV_ClipDistance0;
};






PS_INPUT main(VS_INPUT Input)
{
    PS_INPUT Output;
    Output.position = mul(Input.position, World);
    Output.position = mul(Output.position, View);
    Output.position = mul(Output.position, Projection);

    Output.tex = Input.tex;

    //Set the clipping plane
    Output.clip = dot(mul(Input.position, World), clipPlane);

	return Output;
}