Texture2D txDiffuse : register(t0);
SamplerState samLinear : register(s0);

struct VS_OUTPUT
{
	float4 Position : SV_POSITION;
	float2 Tex : TEXCOORD0;
	float fogFactor : FOG;
};


float4 main(VS_OUTPUT Input) : SV_TARGET
{
    float4 texColor;
    float4 fogColor;
    float4 finalColor;

    texColor = txDiffuse.Sample(samLinear, Input.Tex);

    fogColor = float4(0.5, 0.5, 0.5, 1);

    finalColor = Input.fogFactor * texColor + (1.0 - Input.fogFactor) * fogColor;

    return finalColor;

}