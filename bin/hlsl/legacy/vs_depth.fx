cbuffer MatrixBuffer:register(b0)
{
	matrix WVP;
};



//////////////
// TYPEDEFS //
//////////////
struct VS_INPUT
{
	float4 Position : POSITION;
};

struct PS_INPUT
{
	float4 Position : SV_POSITION;
	float4 DepthPos : TEXCOORD0;
};


////////////////////////////////////////////////////////////////////////////////
// Vertex Shader
////////////////////////////////////////////////////////////////////////////////
PS_INPUT VS(VS_INPUT input)
{
	PS_INPUT output;

	input.Position.w = 1.0f;

	output.Position = mul(input.Position, WVP);
	output.DepthPos = output.Position;



	return output;
}
