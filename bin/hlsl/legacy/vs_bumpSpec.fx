
cbuffer cbChangesEveryFrame : register( b0 )
{
	matrix World;
	matrix View;
	matrix Proj;
	float4 vLightDir;
	float4 f4ViewDirection;
};

struct VS_INPUT
{
    float4 Pos : POSITION;
    float2 Tex : TEXCOORD0;
	float3 Norm : NORMAL;
	float3 Tangent : TANGENT;
	float3 Binormal : BINORMAL;
};

struct PS_INPUT
{
    float4 Pos : SV_POSITION;
	float2 Tex : TEXCOORD0;
    float3 Norm : NORMAL;
	float3 Tangent : TANGENT;
	float3 Binormal : BINORMAL;
	float3 LightDir : TEXCOORD1;
	float3 ViewDir : TEXCOORD2;
};

PS_INPUT VS( VS_INPUT input )
{
    PS_INPUT output = (PS_INPUT)0;
    output.Pos = mul( input.Pos, World);
	output.Pos = mul(output.Pos, View);
	output.Pos = mul(output.Pos, Proj);
    output.Tex = input.Tex;
    output.Norm = mul( input.Norm , World);
	output.Tangent = mul(input.Tangent, World);
	output.Binormal = mul(input.Binormal, World);
	output.LightDir = vLightDir;
	output.ViewDir = f4ViewDirection;

    return output;
}


