cbuffer cbChangesEveryFrame: register(b0)
{
	matrix WVP;
	matrix World;
	float4 lightDir;
	float4 lightColor;
};

struct VS_INPUT
{
    float4 Pos : POSITION;
    float2 Tex : TEXCOORD0;
    float3 Norm : NORMAL;
    float3 Tangent : TANGENT;
};

struct VS_OUTPUT
{
	float4 Pos : SV_POSITION;
	float2 TexCoord : TEXCOORD;
	float3 normal : NORMAL;
	float3 tangent : TANGENT;
	float4 LightDir : TEXCOORD1;
	float4 LightColor : TEXCOORD2;
};



VS_OUTPUT VS(VS_INPUT input)
{
	VS_OUTPUT output;

	output.Pos = mul(input.Pos, WVP);
	output.normal = mul(input.Norm, World);
	output.tangent = mul(input.Tangent, World);
	output.TexCoord = input.Tex;
	output.LightDir = lightDir;
	output.LightColor = lightColor;

	return output;
}
