/////////////
// GLOBALS //
/////////////

Texture2D shaderTextures : register(t0);
SamplerState SampleType : register(s0);


cbuffer TransparentBuffer:register(b0)
{
	float4 BlendAmount;
}

//////////////
// TYPEDEFS //
//////////////
struct PS_INPUT
{
    float4 Position : SV_POSITION;
    float2 Tex : TEXCOORD0;
	float Depth : TEXCOORD1;
};
//In the alpha map pixel shader we first take a sample of the pixel from the two color textures and alpha texture. Then we multiply the alpha value by the base color to get the pixel value for the base texture. After that we multiply the inverse of the alpha (1.0 - alpha) by the second color texture to get the pixel value for the second texture. 
//We then add the two pixel values together and saturate to produce the final blended pixel.

////////////////////////////////////////////////////////////////////////////////
// Pixel Shader
////////////////////////////////////////////////////////////////////////////////


float4 PS(PS_INPUT Input) : SV_TARGET
{
	float4 Color;

    Color = shaderTextures.Sample(SampleType, Input.Tex);
	Color.a = BlendAmount;
			
    return Color;
}
