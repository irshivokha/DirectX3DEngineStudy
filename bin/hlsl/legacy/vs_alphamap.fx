cbuffer MatrixBuffer:register(b0)
{
	matrix World;
    matrix View;
    matrix Proj;
};



//////////////
// TYPEDEFS //
//////////////
struct VS_INPUT
{
	float4 Position : POSITION;
	float2 Tex : TEXCOORD0;
};

struct PS_INPUT
{
	float4 Position : SV_POSITION;
	float2 Tex : TEXCOORD0;
};


////////////////////////////////////////////////////////////////////////////////
// Vertex Shader
////////////////////////////////////////////////////////////////////////////////
PS_INPUT VS(VS_INPUT input)
{
	PS_INPUT output;

	input.Position.w = 1.0f;

	output.Position = mul(input.Position, World);
    output.Position = mul(output.Position, View);
    output.Position = mul(output.Position, Proj);
	output.Tex = input.Tex;

	return output;
}
