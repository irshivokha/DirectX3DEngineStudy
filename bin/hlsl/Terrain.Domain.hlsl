Texture2D TexHeight : register(t0); 
SamplerState SamplerHeight : register(s2);


cbuffer cbWorld : register(b0)
{
	matrix World;
	matrix InverseWorld;
	int4 IsSkinned;
	matrix SkinnedMatrix[6];
	matrix NormalMatrix[6];
	float4 TesselationFactor;
	
	
};

cbuffer cbCamera : register(b1) //바인딩은 한번 하면 계속감. 내용이 바뀔때만 map 호출하고 내용도 업데이트 하여 memcpy하기.
{
	matrix View;
	matrix Proj;
	float4 CamPos;
	float4 CamDir;
	float4 minMaxDistance;
	float4 minMaxLOD;
};

cbuffer ShadingValue : register(b2) 
{
	float Diffuse;
	float Specular;
	float Ambient;
	float padding;
	float4 HeightMapDimensions; //xy : 픽셀 크기 zw : 기하구조 크기	
};


struct HS_CONTROL_POINT_OUTPUT
{
	float3 vPosition : CONTROL_POINT_POSITION;
	float2 Tex : CONTROL_POINT_TEXCOORD;
};


struct HS_PER_PATCH_OUTPUT
{
	float EdgeTessFactor[4]		: SV_TessFactor; // 예를 들어 쿼드 도메인에 대해 [4]입니다.
	float InsideTessFactor[2]	: SV_InsideTessFactor; // 예를 들어 쿼드 도메인에 대해 Inside[2]입니다.
	// TODO: 다른 내용을 변경/추가합니다.
};

struct DS_OUTPUT
{
	float4 vPosition : SV_POSITION;
	float3 Color : COLOR;
	// TODO: 다른 내용을 변경/추가합니다.
};


float SampleHeightMap(float2 uv)
{
	//밉맵 수준을 직접 지정해야 하므로 반드시 SampleLevel()을 사용해야 한다.
	
	//비례계수 3.0은 그냥 시행착오로 정한 그럴듯한 값
	
	const float SCALE = 0.3f;
	return SCALE * TexHeight.SampleLevel(SamplerHeight, uv, 0.0f).r;
}


float3 Sobel(float2 tc)
{
	//자주 쓰이는 값을 미리 계산해둠
	float2 pxSz = float2(1.0 / HeightMapDimensions.x, 1.0f / HeightMapDimensions.y);
	
	//필요한 오프셋들을 계산함
	float2 o00 = tc + float2(-pxSz.x, -pxSz.y);
	float2 o10 = tc + float2(0.0f, -pxSz.y);
	float2 o20 = tc + float2(pxSz.x, -pxSz.y);
	
	float2 o01 = tc + float2(-pxSz.x, 0.0f);
	float2 o21 = tc + float2(pxSz.x, 0.0f);
	
	float2 o02 = tc + float2(-pxSz.x, pxSz.y);
	float2 o12 = tc + float2(0.0f, pxSz.y);
	float2 o22 = tc + float2(pxSz.x, pxSz.y);
	
	//소벨 필터를 적용하려면 현재 픽셀 주위의 8픽셀이 필요하다
	
	float h00 = SampleHeightMap(o00);
	float h10 = SampleHeightMap(o10);
	float h20 = SampleHeightMap(o20);
	
	float h01 = SampleHeightMap(o01);
	float h21 = SampleHeightMap(o21);	
	
	float h02 = SampleHeightMap(o02);
	float h12 = SampleHeightMap(o12);
	float h22 = SampleHeightMap(o22);
	
	//소벨 필터를 평가한다
	float Gx = h00 - h20 + 2.0f * h01 - 2.0f * h21 + h02 - h22;
	float Gy = h00 + 2.0f * h10 + h20 - h02 - 2.0f * h12 - h22;
	
	//빠진 z성분을 생성한다.	
	float Gz = 0.01f * sqrt(max(0.0f, 1.0f - Gx * Gx - Gy * Gy));
	
	//법선이 단위 길이가 되도록 정규화 한후 반환한다.	
	return normalize(float3(2.0f * Gx, Gz, 2.0f * Gy));
	
	
}


[domain("quad")]
DS_OUTPUT DS(
	HS_PER_PATCH_OUTPUT input,
	float2 domain : SV_DomainLocation,
	const OutputPatch<HS_CONTROL_POINT_OUTPUT, 4> patch)
{
	DS_OUTPUT Output = (DS_OUTPUT)0;
	
	//patch[]에서 뽑은 세계 공간 좌표 성분 세개와 uvw(무게중심좌표)의 보간 계수들을 이용하여 위치를 적절히 보간
	

	float3 finalVertexCoord = float3(0.0f, 0.0f, 0.0f);
	
	//u,v
	
	//0,0 : patch[0].position
	//1,0 : patch[1].position
	//2,0 : patch[2].position
	//3,0 : patch[3].position
	
	/*
	0--1
	  /
	 /
	2--3
	*/
	
	
	//사각형 내에서 보간하는 공식. 직접 구해보니 이렇게 하면 제대로 결과값 나옴.
	finalVertexCoord.xz = patch[0].vPosition.xz * (1.0f - domain.x) * (1.0f - domain.y)
	+ patch[1].vPosition.xz * domain.x * (1.0f - domain.y)
	+ patch[2].vPosition.xz * (1.0f - domain.x) * domain.y
	+ patch[3].vPosition.xz * domain.x * domain.y;
	
	
	float2 texcoord = patch[0].Tex * (1.0f - domain.x) * (1.0f - domain.y)
	+ patch[1].Tex * domain.x * (1.0f - domain.y)
	+ patch[2].Tex * (1.0f - domain.x) * domain.y
	+ patch[3].Tex * domain.x * domain.y;
	
	
	//높이 맵 텍스쳐에서 높이 값을 뽑는다
	finalVertexCoord.y = SampleHeightMap(texcoord);
	
	//세계 공간 위치를 래스터화기가 사용할 투영공간좌표계(클립좌표)계로 변환시켜 출력해야 함. 기하 셰이더 뒤로 미룰수도 잇지만 기하 셰이더안사용하니까 여기사 사용.
	Output.vPosition = mul(float4(finalVertexCoord, 1.0f), View);
	Output.vPosition = mul(Output.vPosition, Proj);
	
	//높이 맵에 소벨 필터를 적용하여 적절한 법선 벡터 가져오기.
	//소벨필터를 사용하여 법선을 계산하므로 성능 상승. 노멀 텍스쳐를 사용할경우 품질은 증가해도 샘플링 비용이 있음. (법선맵을 쓰는거랑 뭔차이가 있는지 이해가 잘안간다.)
	float3 normal = Sobel(texcoord);
	normal = normalize(mul(float4(normal, 1.0), InverseWorld).xyz);
	Output.Color = min(0.75f, max(0.0f, dot(normal, float3(0.0f, 1.0f, 0.0f))));
	
	
	return Output;
}
