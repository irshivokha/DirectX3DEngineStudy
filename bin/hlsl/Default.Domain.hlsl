Texture2D TexHeight : register(t0); //Texture역시 texturearray또는 Atlas로 묶은뒤 해당하는 index의 셰이더를 불러와서 부르는게 매번 Set하는 것보다 훨씬 drawCall을 줄이는 방법일 것이다.
SamplerState SamplerDefault : register(s0);

cbuffer cbViewProj : register(b1) //바인딩은 한번 하면 계속감.(검증필요) 내용이 바뀔때만 map 호출하고 내용도 업데이트 하여 memcpy하기.
{ 
	matrix View;
	matrix Proj;
	float4 CamPos;
	float4 CamDir;
	float4 minMaxDistance;
	float4 minMaxLOD;
};


struct DS_OUTPUT
{
	float4 vPosition  : SV_POSITION;
	float3 Normal : NORMAL;
	float2 Tex : TEXCOORD0;	
};

// 제어점을 출력합니다.
struct HS_CONTROL_POINT_OUTPUT
{
	float3 vPosition : WORLDPOS; 
	float3 Normal : NORMAL;
	float2 Tex : TEXCOORD0;
};

// 패치 상수 데이터를 출력합니다.
struct HS_CONSTANT_DATA_OUTPUT
{
	float EdgeTessFactor[3]			: SV_TessFactor; // 예를 들어 쿼드 도메인에 대해 [4]입니다.
	float InsideTessFactor			: SV_InsideTessFactor; // 예를 들어 쿼드 도메인에 대해 Inside[2]입니다.
	// TODO: 다른 내용을 변경/추가합니다.
};

#define NUM_CONTROL_POINTS 3

[domain("tri")]
DS_OUTPUT DS(
	HS_CONSTANT_DATA_OUTPUT input,
	float3 domain : SV_DomainLocation,
	const OutputPatch<HS_CONTROL_POINT_OUTPUT, NUM_CONTROL_POINTS> patch)
{
	DS_OUTPUT Output;
	
	float3 vWorldPos = patch[0].vPosition * domain.x + patch[1].vPosition * domain.y + patch[2].vPosition * domain.z;

	
	Output.Tex = patch[0].Tex * domain.x + patch[1].Tex * domain.y + patch[2].Tex * domain.z;
	Output.Normal = patch[0].Normal * domain.x + patch[1].Normal * domain.y + patch[2].Normal * domain.z;
	Output.Normal = normalize(Output.Normal);
	
	float fHeightMapMIPLevel = clamp((distance(vWorldPos.xyz, CamPos.xyz) - 100.0f) / 100.0f, 0, 3);	//두번째 100이 near?를 결정함
	
	float4 texHeight = TexHeight.SampleLevel(SamplerDefault, Output.Tex, fHeightMapMIPLevel);
	
	const float fScale = 0.5f;
	vWorldPos.xyz = vWorldPos.xyz + Output.Normal * texHeight.r * 0.01f;
				
	Output.vPosition = mul(float4(vWorldPos, 1), View);
	Output.vPosition = mul(Output.vPosition, Proj);

	return Output;
}
