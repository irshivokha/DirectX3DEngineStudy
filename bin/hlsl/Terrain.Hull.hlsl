cbuffer cbWorld : register(b0)
{
	matrix World;
	matrix InverseWorld;
	int4 IsSkinned;
	matrix SkinnedMatrix[6];
	matrix NormalMatrix[6];
	float4 TesselationFactor;
};

cbuffer cbCamera : register(b1) //바인딩은 한번 하면 계속감. 내용이 바뀔때만 map 호출하고 내용도 업데이트 하여 memcpy하기.
{
	matrix View;
	matrix Proj;
	float4 CamPos;
	float4 CamDir;
	float4 minMaxDistance;
	float4 minMaxLOD;
};

cbuffer HeightMapParams : register(b2)
{
	// xy = 텍스쳐의 크기
	// zw = geometry dimension
	float4 heightMapDimensions;	
};

Texture2D TexHeightMap : register(t0);
Texture2D TexLodLookup : register(t1);
SamplerState SamplerHeightMap : register(s0);

// 제어점을 입력합니다.
struct VS_CONTROL_POINT_OUTPUT
{
	float3 vPosition : WORLDPOS;
	float3 Normal : NORMAL;
	float2 Tex : TEXCOORD0;
	// TODO: 다른 내용을 변경/추가합니다.
};

// 제어점을 출력합니다.
struct HS_CONTROL_POINT_OUTPUT
{
	float3 vPosition : CONTROL_POINT_POSITION;
	//float3 Normal : NORMAL;
	float2 Tex : CONTROL_POINT_TEXCOORD;
	
};

// 패치 상수 데이터를 출력합니다.
struct HS_PER_PATCH_OUTPUT
{
	float EdgeTessFactor[4] : SV_TessFactor; // 예를 들어 쿼드 도메인에 대해 [4]입니다.
	float InsideTessFactor[2] : SV_InsideTessFactor; // 예를 들어 쿼드 도메인에 대해 Inside[2]입니다.
	// TODO: 다른 내용을 변경/추가합니다.
};


float3 ComputePatchMidPoint(float3 cp0, float3 cp1, float3 cp2, float3 cp3)
{
	return (cp0 + cp1 + cp2 + cp3) / 4.0f;

}

float ComputeScaledDistance(float3 from, float3 to)
{
	//카메라와 타일 중점의 거리 계산
	float d = distance(from, to);
	
	//그 거리를 0.0~1.0으로 비례하기
	
	return (d - minMaxDistance.x) / (minMaxDistance.y - minMaxDistance.x);
}

float ComputePatchLOD(float3 midPoint)
{
	//비례된 거리를 계산한다.	
	float d = ComputeScaledDistance(CamPos.xyz, midPoint);
	
	//이 [0.0,1.0]구간 거리를 원하는 LOD 구간으로 비례시킨다.
	//거리가 가까울수록 높은 LOD가 나와야 하므로 거리 값을 뒤집어야 한다.
	return lerp(minMaxLOD.x, minMaxLOD.y, 1.0f - d);
}

float4 ReadLookup(uint2 idx)
{
	return TexLodLookup.Load(uint3(idx, 0));
}

uint2 ComputeLookupIndex(uint patch, int xOffset, int zOffset)
{
	// For a 32x32 grid there will be 1024 patches
	// thus 'patch' is 0-1023, we need to decode
	// this into an XY coordinate
	uint2 p;
	
	p.x = patch % (uint) heightMapDimensions.z;
	p.y = patch / (uint) heightMapDimensions.w;
	
	// With the XY coordinate for the patch being rendered we
	// then need to offset it according to the parameters
	p.x = clamp(p.x + xOffset, 0, ((uint) heightMapDimensions.z) - 1);
	p.y = clamp(p.y + zOffset, 0, ((uint) heightMapDimensions.w) - 1);
	
	return p;
}

float ComputePatchLODUsingLookup(float3 midPoint, float4 lookup)
{
	// Compute the scaled distance
	float d = ComputeScaledDistance(CamPos.xyz, midPoint);
	
	// lookup:
	//	x,y,z = patch normal
	//	w = standard deviation from patch plane
	
	// It's quite rare to get a std.dev of 1.0, or much above 0.75
	// so we apply a small fudge-factor here. Can be tweaked or removed
	// according to your chosen dataset...
	float sd = lookup.w / 0.75;
	
	// The final LOD is the per-patch Std.Dev. scaled by the distance
	// from the viewer.
	// -> This computation won't ever increase LOD *above* that of the
	//    naieve linear form (see ComputePatchLOD) but it will work
	//    to ensure extra triangles aren't generated for patches that
	//    don't need them.
	float lod = sd * (1.0f - d);
	
	// Finally, transform this value into an actual LOD
	return lerp(minMaxLOD.x, minMaxLOD.y, clamp(lod, 0.0f, 1.0f));
}

// 패치 상수 함수
HS_PER_PATCH_OUTPUT hsPerPatch(
	InputPatch<VS_CONTROL_POINT_OUTPUT, 12> ip,
	uint PatchID : SV_PrimitiveID)
{
	HS_PER_PATCH_OUTPUT Output = (HS_PER_PATCH_OUTPUT) 0;

/*	
			  6-----7
			  l	    l
			  l	    l	
		8-----1-----3-----4
		l	  l	    l	  l
		l	  l	    l	  l
		9-----0-----2-----5			  
			  l	    l
			  l	    l	
			  10----11
*/
	
	float3 midPoints[] =
	{
		//inputPatch는 indexBuffer기준으로 들어오는것으로 추정
		
		//주 사각형
		ComputePatchMidPoint(ip[0].vPosition, ip[1].vPosition, ip[2].vPosition, ip[3].vPosition)
		
		//+x 방향 이웃
		, ComputePatchMidPoint(ip[2].vPosition, ip[3].vPosition, ip[4].vPosition, ip[5].vPosition)
		
		//+z 방향 이웃
		, ComputePatchMidPoint(ip[1].vPosition, ip[3].vPosition, ip[6].vPosition, ip[7].vPosition)
		
		//-x 방향 이웃
		, ComputePatchMidPoint(ip[0].vPosition, ip[1].vPosition, ip[8].vPosition, ip[9].vPosition)

		//-z 방향 이웃		
		, ComputePatchMidPoint(ip[0].vPosition, ip[2].vPosition, ip[10].vPosition, ip[11].vPosition)
		
	};
	
	//각 타일의 적절한 LOD를 결정하기
	
	float dist[] =
	{
		//현재 타일 사각형
		ComputePatchLOD(midPoints[0])
		
		//+x방향 이웃
		, ComputePatchLOD(midPoints[1])
		
		//+z방향 이웃
		, ComputePatchLOD(midPoints[2])
		
		//-x방향 이웃
		, ComputePatchLOD(midPoints[3])
		
		//-z방향 이웃
		, ComputePatchLOD(midPoints[4])
	};
	
	//현재 타일 내부조각의 LOD는 항상 타일 자체의 LOD와 같다.
	
	Output.InsideTessFactor[0] = dist[0];
	Output.InsideTessFactor[1] = dist[0];
	
			
	
	/*
	가장자리 조각의 LOD구현 하는 규칙
	
	-이웃 타일의 LOD가 더 낮으면 그 LOD를 가장자리 조각의 LOD로 설정
	
	-이웃 타일의 LOD가 더 높다면 현재 타일의 LOD 그대로 사용.(그 이웃타일이 현재 타일에 맞게
	자신의 가장자리 LOD를 낮춘다
	*/
	
	Output.EdgeTessFactor[0] = min(dist[0], dist[4]);
	Output.EdgeTessFactor[1] = min(dist[0], dist[3]);
	Output.EdgeTessFactor[2] = min(dist[0], dist[2]);
	Output.EdgeTessFactor[3] = min(dist[0], dist[1]);
	
	
	return Output;
}

HS_PER_PATCH_OUTPUT hsPerPatchWithLookup(InputPatch<VS_CONTROL_POINT_OUTPUT, 12> ip, uint PatchID : SV_PrimitiveID)
{
	HS_PER_PATCH_OUTPUT output;
	
	float4 tileLookup[] =
	{
		// Main quad
		ReadLookup(ComputeLookupIndex(PatchID, 0, 0)),
		
		// + x 방향 이웃(왜..?)
		ReadLookup(ComputeLookupIndex(PatchID, 0, 1)),
		
		// +z 방향 이웃
		ReadLookup(ComputeLookupIndex(PatchID, 1, 0)),
		
		// -x 방향 이웃
		ReadLookup(ComputeLookupIndex(PatchID, 0, -1)),
		
		// -z 방향 이웃
		ReadLookup(ComputeLookupIndex(PatchID, -1, 0))
	};

/*	
			  6-----7
			  l	    l
			  l	    l	
		8-----1-----3-----4
		l	  l	    l	  l
		l	  l	    l	  l
		9-----0-----2-----5			  
			  l	    l
			  l	    l	
			  10----11
*/	
	float3 midPoints[] =
	{
		//inputPatch는 indexBuffer기준으로 들어오는것으로 추정
		
		//주 사각형
		ComputePatchMidPoint(ip[0].vPosition, ip[1].vPosition, ip[2].vPosition, ip[3].vPosition)
		
		//+x 방향 이웃
		, ComputePatchMidPoint(ip[2].vPosition, ip[3].vPosition, ip[4].vPosition, ip[5].vPosition)
		
		//+z 방향 이웃
		, ComputePatchMidPoint(ip[1].vPosition, ip[3].vPosition, ip[6].vPosition, ip[7].vPosition)
		
		//-x 방향 이웃
		, ComputePatchMidPoint(ip[0].vPosition, ip[1].vPosition, ip[8].vPosition, ip[9].vPosition)

		//-z 방향 이웃		
		, ComputePatchMidPoint(ip[0].vPosition, ip[2].vPosition, ip[10].vPosition, ip[11].vPosition)
		
	};
	
	// Determine the LOD for each patch
	float detail[] =
	{
		// Main quad
		ComputePatchLODUsingLookup(midPoints[0], tileLookup[0])
		
		// +x neighbour
		, ComputePatchLODUsingLookup(midPoints[1], tileLookup[1])
		
		// +z neighbour
		, ComputePatchLODUsingLookup(midPoints[2], tileLookup[2])
		
		// -x neighbour
		, ComputePatchLODUsingLookup(midPoints[3], tileLookup[3])
		
		// -z neighbour
		, ComputePatchLODUsingLookup(midPoints[4], tileLookup[4])
	};
	
	//계수 설정
	output.InsideTessFactor[0] =
	output.InsideTessFactor[1] = detail[0];
	
	output.EdgeTessFactor[0] = detail[4];
	output.EdgeTessFactor[1] = detail[3];
	output.EdgeTessFactor[2] = detail[2];
	output.EdgeTessFactor[3] = detail[1];
	
	return output;
}

[domain("quad")]
[partitioning("fractional_odd")]
[outputtopology("triangle_cw")]
[outputcontrolpoints(4)]
[patchconstantfunc("hsPerPatch")]
HS_CONTROL_POINT_OUTPUT HS(
	InputPatch<VS_CONTROL_POINT_OUTPUT, 12> ip,
	uint i : SV_OutputControlPointID,
	uint PatchID : SV_PrimitiveID)
{
	HS_CONTROL_POINT_OUTPUT Output;

	// 여기에 출력을 계산할 코드를 삽입합니다.
	Output.vPosition = ip[i].vPosition;
	Output.Tex = ip[i].Tex;

	return Output;
}

[domain("quad")]
[partitioning("fractional_odd")]
[outputtopology("triangle_cw")]
[outputcontrolpoints(4)]
[patchconstantfunc("hsPerPatchWithLookup")]
HS_CONTROL_POINT_OUTPUT HsComplex(InputPatch<VS_CONTROL_POINT_OUTPUT, 12> patch,
									uint i : SV_OutputControlPointID)
{
	HS_CONTROL_POINT_OUTPUT o = (HS_CONTROL_POINT_OUTPUT)0;
	
	o.vPosition = patch[i].vPosition;
	o.Tex = patch[i].Tex;
	
	return o;
}
