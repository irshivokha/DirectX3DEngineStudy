Texture2D TexColor : register( t0 );			//Texture역시 texturearray또는 Atlas로 묶은뒤 해당하는 index의 셰이더를 불러와서 부르는게 매번 Set하는 것보다 훨씬 drawCall을 줄이는 방법일 것이다.
SamplerState SamplerDefault : register( s0 );



cbuffer ShadingValue : register(b0)					//만약 가능할경우, 셰이더마다 ShadingValue 값이 다를 텐데, 따라서 상수버퍼를 array로 묶어서 바인딩 한뒤 각 셰이더 코드 안에서 해당 index의 해당하는 상수버퍼 코드를 사용하는걸로 쓰는게 좋을듯
{
	float Diffuse;
	float Specular;
	float Ambient;
	float padding;
	float4 HeightMapDimensions; //xy : 픽셀 크기 zw : 기하구조 크기	
};

cbuffer cbLight : register(b1)
{
	float4 LightPos[100];
	float4 LightDir[100];
	float4 LightColor[100];
	uint4 LightCount;
}

struct PS_INPUT
{
	float4 vPosition : SV_POSITION;
	float3 Normal : NORMAL;
	float2 Tex : TEXCOORD0;
};


float4 PS(PS_INPUT input) : SV_Target
{
	float diffuse = 0;
	for (uint i = 0; i < LightCount.x; i++)
	{
		float3 lightDir = normalize(LightDir[i].xyz);
		diffuse += saturate(dot(-lightDir, input.Normal));
	}
	
	float4 ambient = 0.25;
	float4 color = TexColor.Sample(SamplerDefault, input.Tex) * (diffuse + ambient);
	//float4 color = float4(1,0,0,1);
	return color;

}









