
cbuffer cbWorld : register( b0 )
{
	matrix World;
	matrix InverseWorld;
	int4 IsSkinned;
	matrix SkinnedMatrix[6];
	matrix NormalMatrix[6];
	float4 TesselationFactor;
};



//cbuffer cbViewProj: register(b1)		//바인딩은 한번 하면 계속감.(검증필요) 내용이 바뀔때만 map 호출하고 내용도 업데이트 하여 memcpy하기.
//{										//위에놈이랑 같이 묶어서 바인딩 하기.
//	matrix View;
//	matrix Proj;
//};

struct VS_INPUT
{
	float3 Pos : POSITION;
	int4 Bone : BONEIDS;
	float4 weights : BONEWEIGHTS;
	float3 Normal : NORMAL;
	float2 UV : TEXCOORD;
};

struct VS_CONTROL_POINT_OUTPUT
{
	float3 Pos : WORLDPOS;
	float3 Normal : NORMAL;
    float2 UV : TEXCOORD;		
};

VS_CONTROL_POINT_OUTPUT VS(VS_INPUT input)
{
	VS_CONTROL_POINT_OUTPUT output = (VS_CONTROL_POINT_OUTPUT) 0;
	if(IsSkinned.x==1)
	{
		output.Pos = (mul(float4(input.Pos, 1.0f),SkinnedMatrix[input.Bone.x])*input.weights.x).xyz;
		output.Pos += (mul(float4(input.Pos, 1.0f), SkinnedMatrix[input.Bone.y]) * input.weights.y).xyz;
		output.Pos += (mul(float4(input.Pos, 1.0f), SkinnedMatrix[input.Bone.z]) * input.weights.z).xyz;
		output.Pos += (mul(float4(input.Pos, 1.0f), SkinnedMatrix[input.Bone.w]) * input.weights.w).xyz;
		
		output.Normal = (mul(input.Normal, (float3x3)NormalMatrix[input.Bone.x]) * input.weights.x).xyz;
		output.Normal += (mul(input.Normal, (float3x3) NormalMatrix[input.Bone.y]) * input.weights.y).xyz;
		output.Normal += (mul(input.Normal, (float3x3) NormalMatrix[input.Bone.z]) * input.weights.z).xyz;
		output.Normal += (mul(input.Normal, (float3x3) NormalMatrix[input.Bone.w]) * input.weights.w).xyz;
		
				
		
	}
	else
	{
		output.Pos = mul(float4(input.Pos.xyz, 1.0f), World).xyz;
		output.Normal = mul(float4(input.Normal, 1), World).xyz;
	}
		
	
    output.UV = input.UV.xy;
	
	
	
    return output;
}


