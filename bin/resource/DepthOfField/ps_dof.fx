Texture2D tSource : register( t0 );
Texture2D tSourceLow : register(t1);
Texture2D tDepth : register(t2);

SamplerState samplerSource: register(s0);
SamplerState samplerSourceLow : register(s1);
SamplerState samplerDepth : register(s2);



#define NUM_TAPS 4


cbuffer distanceBuffer:register(b0)
{
	float2 fInverseViewportDimensions;
	// maximum CoC radius and diameter in pixels
	float2 vMaxCoC;

	// scale factor for maximum CoC size on low res. image
	float radiusScale;

	float3 padding;
};

// contains poisson-distributed positions on the unit circle
float2 poisson[8] = {
	float2(0.0,      0.0),
	float2(0.527837,-0.085868),
	float2(-0.040088, 0.536087),
	float2(-0.670445,-0.179949),
	float2(-0.419418,-0.616039),
	float2(0.440453,-0.639399),
	float2(-0.757088, 0.349334),
	float2(0.574619, 0.685879)
};

struct PS_INPUT
{
	float4 Pos : SV_POSITION;
	float2 Tex : TEXCOORD0;
};





float4 PS( PS_INPUT input) : SV_Target
{

	float4 cOut;
	float discRadius, discRadiusLow, centerDepth;
	float4 DepthColor = tDepth.Sample(samplerDepth, input.Tex);

	// pixel size of low resolution image
	float2 pixelSizeLow = 4.0 * fInverseViewportDimensions;

	cOut = tSource.Sample(samplerSource, input.Tex);   // fetch center tap
	cOut.a = DepthColor.a;
	centerDepth = cOut.a;              // save its depth

	// convert depth into blur radius in pixels
	discRadius = abs(cOut.a * vMaxCoC.y - vMaxCoC.x);
	
	// compute disc radius on low-res image
	discRadiusLow = discRadius * radiusScale;
	
	// reuse cOut as an accumulator
	cOut = 0;
	
	for (int t = 0; t < NUM_TAPS; t++)
	{
		// fetch low-res tap
		float2 coordLow = input.Tex + (pixelSizeLow * poisson[t] * discRadiusLow);
		float4 tapLow = tSourceLow.Sample(samplerSourceLow, coordLow);
		tapLow.a = DepthColor.a;

		// fetch high-res tap
		float2 coordHigh = input.Tex + (fInverseViewportDimensions * poisson[t] * discRadius);
		float4 tapHigh = tSource.Sample(samplerSource, coordHigh);
		tapHigh.a = DepthColor.a;

		// put tap blurriness into [0, 1] range
		float tapBlur = abs(tapHigh.a * 2.0 - 1.0);

		// mix low- and hi-res taps based on tap blurriness
		float4 tap = lerp(tapHigh, tapLow, tapBlur);

		// apply leaking reduction: lower weight for taps that are
		// closer than the center tap and in focus
		tap.a = (tap.a >= centerDepth) ? 1.0 : abs(tap.a * 2.0 - 1.0);

		// accumulate
		cOut.rgb += tap.rgb * tap.a;
		cOut.a += tap.a;
	}

	// normalize and return result
	return (cOut / cOut.a);

	//float4 color1 = tSource.Sample(samplerSource, input.Tex);
	//float4 color2 = tSourceLow.Sample(samplerSourceLow, input.Tex);
	////float4 final = lerp(color1, color2, 0.5f);

	//return color1;

}









