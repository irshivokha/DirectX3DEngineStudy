Texture2D txDiffuse : register(t0);
SamplerState samLinear : register( s0 );

cbuffer distanceBuffer:register(b0)
{
	float d_near;
	float d_focus;
	float d_far;
	float far_clamp;	
};


struct PS_INPUT
{
	float4 Position : SV_POSITION;
	float2 Tex : TEXCOORD0;
	float Depth : TEXCOORD1;
};


float ComputeDepthBlur(float depth)
{
	float f;

	if (depth < d_focus)
	{
		// scale depth value between near blur distance and focal distance to [-1, 0] range
		f = (depth - d_focus) / (d_focus - d_near);
	}
	else
	{
		// scale depth value between focal distance and far blur
		// distance to [0, 1] range
		f = (depth - d_focus) / (d_far - d_focus);

		// clamp the far blur to a maximum blurriness
		f = clamp(f, 0, far_clamp);
	}

	// scale and bias into [0, 1] range
	return f * 0.5f + 0.5f;
}

float4 PS( PS_INPUT input) : SV_Target
{
	float4 Color;
	float f = ComputeDepthBlur(input.Depth);
	Color = float4(f, f, f, f);

	
	return Color;

}









