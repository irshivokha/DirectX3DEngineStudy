cbuffer MatrixBuffer:register(b0)
{
	matrix matWVP;
	matrix matView;
};




//////////////
// TYPEDEFS //
//////////////
struct VS_INPUT
{
	float4 Position : POSITION;
	float2 Tex : TEXCOORD0;
};

struct PS_INPUT
{
	float4 Position : SV_POSITION;
	float2 Tex : TEXCOORD0;
	float Depth : TEXCOORD1;
};


////////////////////////////////////////////////////////////////////////////////
// Vertex Shader
////////////////////////////////////////////////////////////////////////////////
PS_INPUT VS(VS_INPUT input)
{
	PS_INPUT output = (PS_INPUT)0;

	input.Position.w = 1.0f;

	output.Position = mul(input.Position, matWVP);
	float3 pView = mul(input.Position, matView);
	output.Depth = pView.z;
	output.Tex = input.Tex;
	return output;
}
