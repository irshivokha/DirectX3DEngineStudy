struct PS_INPUT
{
    float4 Pos : SV_POSITION;
    float4 domePos : TEXCOORD0;
    float4 f4apexColor : TEXCOORD1;
    float4 f4centerColor : TEXCOORD2;	
};


float4 PS(PS_INPUT input) : SV_TARGET
{
    float height;
    float4 outputColor;

    height = input.domePos.y;

    if(height < 0.0f)
    {
      height = 0.0f;
    }
    outputColor = lerp(input.f4centerColor,input.f4apexColor,height);

    return outputColor;
}




