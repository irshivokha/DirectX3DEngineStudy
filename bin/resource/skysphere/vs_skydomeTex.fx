
cbuffer matrixbuffer: register( b0 )
{
	matrix WVP;
};

struct VS_INPUT
{
    float3 Pos : POSITION;
    float2 Tex : TEXCOORD0;
};

struct PS_INPUT
{
    float4 Pos : SV_POSITION;
    float3 Tex : TEXCOORD0;

};

PS_INPUT VS( VS_INPUT input )
{

    PS_INPUT output = (PS_INPUT)0;
    output.Pos = mul(float4(input.Pos,1.0f), WVP).xyww;
    output.Tex = input.Pos;	

    return output;
}


