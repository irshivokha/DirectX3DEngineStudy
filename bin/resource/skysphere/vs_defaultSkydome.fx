
cbuffer matrixbuffer: register( b0 )
{
	matrix WVP;
	float4 apexColor;
	float4 centerColor;
};

struct VS_INPUT
{
    float4 Pos : POSITION;
    float2 Tex : TEXCOORD;
};

struct PS_INPUT
{
    float4 Pos : SV_POSITION;
    float2 Tex : TEXCOORD0;
    float4 domePos : TEXCOORD1;	
};

PS_INPUT VS( VS_INPUT input )
{
    input.Pos.w = 1.0f;

    PS_INPUT output = (PS_INPUT)0;
    output.Pos = mul(input.Pos, WVP);
    output.Tex = input.Tex;	
    output.domePos = input.Pos;

    return output;
}


