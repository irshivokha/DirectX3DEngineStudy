
cbuffer matrixbuffer: register( b0 )
{
	matrix WVP;
	float4 apexColor;
	float4 centerColor;
};

struct VS_INPUT
{
    float4 Pos : POSITION;
};

struct PS_INPUT
{
    float4 Pos : SV_POSITION;
    float4 domePos : TEXCOORD0;
    float4 f4apexColor : TEXCOORD1;
    float4 f4centerColor : TEXCOORD2;	
};

PS_INPUT VS( VS_INPUT input )
{
    input.Pos.w = 1.0f;

    PS_INPUT output = (PS_INPUT)0;
    output.Pos = mul(input.Pos, WVP);
    output.f4apexColor = apexColor;
    output.f4centerColor = centerColor;	
    output.domePos = input.Pos;

    return output;
}


