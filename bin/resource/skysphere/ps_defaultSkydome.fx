Texture2D txDiffuse : register( t0 );
SamplerState samLinear : register( s0 );

struct PS_INPUT
{
    float4 Pos : SV_POSITION;
    float2 Tex : TEXCOORD0;
    float4 domePos : TEXCOORD1;	
};


float4 PS(PS_INPUT input) : SV_TARGET
{
    float height;
    float4 outputColor;
	float4 color;
    height = input.domePos.y;

    if(height < 0.0f)
    {
      height = 0.0f;
    }
    outputColor = lerp(txDiffuse.Sample(samLinear, input.Tex), 1.0f, height);

    return outputColor;
}




