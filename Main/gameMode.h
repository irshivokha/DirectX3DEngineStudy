#pragma once



class C_MYWIN;
class Timer;
class Fps;
class CPU;

namespace Engine
{
	class GraphicsEngine;
}

class GameMode
{
public:
	struct SysInfo
	{
		int Fps;
		int CpuRate;
		float elapsedTime;
	};
private:
	static GameMode *Game;
	C_MYWIN				*Win;
	Engine::GraphicsEngine* GraphicsEngine;
	//Timer	*TimerInstance;
	//Fps					*Fps;
	//CPU					*Cpu;
	SysInfo				m_SysInfo;
	
private:
	GameMode();
	static bool		updateContents();

public:
	static void		CreateInstance();
	static GameMode*  GetInstance();
	static void		ReleaseInstance();
	

	void			Init(HINSTANCE hInstance);
	void			Release();
	void			Update();

	void TestFunc();
	void CallBack();
};