#include "stdafx.h"
#include "../api/myWin.h"
#include "gameMode.h"
#include "../3DGraphicsEngine/GraphicsEngine.h"
#include "../3DGraphicsEngine/GameObject/GameObjectManager.h"
#include "../3DGraphicsEngine/GameObject/GameObject.h"
#include "../3DGraphicsEngine/GameObject/IScript.h"
#include "../Projects/FirstGame/Assets/Scripts/MainScript.h"

//#pragma comment(lib, "../bin/FirstGame.lib")

//Game은 Play화면. EditMode가 없으므로 여기서 hardCode로 대신 object 배치하기.

void Test()
{
	Engine::GameObjectManager& mgr = Engine::GameObjectManager::GetInstance();
	Engine::GameObjPtr scriptObj = mgr.CreateGameObject();


}

GameMode* GameMode::Game = nullptr;

void GameMode::CreateInstance()
{
	if (!Game)
		Game = new GameMode;
}
GameMode* GameMode::GetInstance()
{
	return Game;
}
void GameMode::ReleaseInstance()
{
	if (Game)
	{
		Game->Release();
		delete Game;
		Game = nullptr;
	}
}

GameMode::GameMode() :
	GraphicsEngine(nullptr),
	Win(nullptr),
	m_SysInfo()
{
}

void GameMode::Init(HINSTANCE hInstance)
{
	if (!Win)
	{
		Win = new C_MYWIN;
		Win->Init(hInstance);
	}
	if (!GraphicsEngine)
	{
		GraphicsEngine = new Engine::GraphicsEngine();
		GraphicsEngine->Init(Win->GetHwnd());
	}

	TestFunc();


}

void GameMode::Release()
{
	if (Win)
	{
		delete Win;
		Win = nullptr;
	}
	if (GraphicsEngine)
	{
		GraphicsEngine->Release();
		delete GraphicsEngine;
		GraphicsEngine = nullptr;
	}
	//if (TimerInstance)
	//{
	//	delete TimerInstance;
	//	TimerInstance = nullptr;
	//}

	//if (Fps)
	//{
	//	delete Fps;
	//	Fps = nullptr;
	//}

	//if (Cpu)
	//{
	//	delete Cpu;
	//	Cpu = nullptr;
	//}
}

void GameMode::Update()
{
	if(GraphicsEngine)
		Win->UpdateMsg(updateContents);
}

void GameMode::TestFunc()
{

	Engine::GameObjectManager& mgr = Engine::GameObjectManager::GetInstance();
	Engine::GameObjPtr scriptObj = mgr.CreateGameObject();
	Engine::Script::MainScript* newScript = new Engine::Script::MainScript();	
	scriptObj->SetScriptObj(newScript);
	newScript->Start();	
	//newScript->Test(&Test);
}

void GameMode::CallBack()
{
}

bool GameMode::updateContents()
{
	//Game->m_fElapsedTime += Game->Timer->GetElapsedTime();

	//if (Game->m_fElapsedTime >= 1.0f / (float)100)
	//{
	//	Game->Timer->SetOffsetTime();
	//	Game->GraphicsEngine->UpdateScene(Game->m_fElapsedTime);
	//	Game->m_fElapsedTime = 0.0f;
	//}


	////시간관련 Update
	//Game->m_SysInfo.CpuRate = Game->Cpu->GetCpuPercentage();
	////Game->m_SysInfo.Fps = Game->Fps->GetFps();
	//Game->m_SysInfo.elapsedTime = Game->Timer->GetTime();
	

	Game->GraphicsEngine->UpdateScene();	//&(Game->m_SysInfo)
	
	if (Game->GraphicsEngine->IsExit())
		return false;
	return true;
}







