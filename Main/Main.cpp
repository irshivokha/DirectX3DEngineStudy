#include "stdafx.h"
#include "gameMode.h"

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	GameMode::CreateInstance();
	GameMode::GetInstance()->Init(hInstance);
	GameMode::GetInstance()->Update();
	GameMode::ReleaseInstance();

	return 0;
}