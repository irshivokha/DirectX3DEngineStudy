#include "EngineBaseHeader.h"
#include "defaultShader.h"
#include "Utility/Utility.h"
#include "RenderClasses/DXUtility.h"
#include "RenderClasses/StateObjectManager.h"
#include "RenderClasses/ConstantBufferManager.h"


namespace Engine
{
	void DefaultShader::SetDefault()
	{
		
	}
	DefaultShader::DefaultShader()
	{

	}

	DefaultShader::~DefaultShader()
	{

	}

	void DefaultShader::CreateConstantBuffer()
	{
		ConstantBufferManager::GetInstance().CreateConstantBuffer(ConstantBufferManager::ConstBufferType::perMaterial, sizeof(ConstantBufferManager::DefaultShadingValueFormat));
	}

	bool DefaultShader::CompileShader()
	{
		//ID3DBlob* pVSBlob = nullptr;
		//ID3DBlob* pPSBlob = nullptr;

		D3D11_INPUT_ELEMENT_DESC layout[5] =
		{
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "BONEIDS", 0, DXGI_FORMAT_R32G32B32A32_UINT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "BONEWEIGHTS", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },			
			{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA , 0 }

			//{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			//{ "TEXCOORD", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			//{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			//{ "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			//{ "BINORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		};

		//Utility::CompileShaderFromFile(L"hlsl/vs_default.hlsl", "VS", "vs_4_0", &pVSBlob);
		//Utility::CompileShaderFromFile(L"resource/ps_default.fx", "PS", "ps_4_0", &pPSBlob);


		ID3D10Blob* vertexblob;
		ID3D10Blob* hullblob;
		ID3D10Blob* domainblob;
		ID3D10Blob* pixelblob;

		D3DReadFileToBlob(L"hlsl/Default.Vertex.hlsl.cso", &vertexblob);
		D3DReadFileToBlob(L"hlsl/Default.Hull.hlsl.cso", &hullblob);
		D3DReadFileToBlob(L"hlsl/Default.Domain.hlsl.cso", &domainblob);
		D3DReadFileToBlob(L"hlsl/Default.Pixel.hlsl.cso", &pixelblob);
		//Utility::ReadFileBytes("hlsl/vs_default.hlsl.cso", vertexByteData);
		//Utility::ReadFileBytes("hlsl/ps_default.hlsl.cso", pixelByteData);

		ID3D11Device* device = DXUtility::GetInstance()->GetDevice();
		if (device)
		{			
			device->CreateInputLayout(layout, 5, vertexblob->GetBufferPointer(), vertexblob->GetBufferSize(), &VertexLayout);
			//device->CreateVertexShader(vertexByteData.c_str(), vertexByteData.length(), NULL, &VertexShader);
			//device->CreatePixelShader(pixelByteData.c_str(), pixelByteData.length(), NULL, &PixelShader);
			device->CreateVertexShader(vertexblob->GetBufferPointer(), vertexblob->GetBufferSize(), NULL, &VertexShader);
			device->CreateHullShader(hullblob->GetBufferPointer(), hullblob->GetBufferSize(), NULL, &HullShader);
			device->CreateDomainShader(domainblob->GetBufferPointer(), domainblob->GetBufferSize(), NULL, &DomainShader);
			device->CreatePixelShader(pixelblob->GetBufferPointer(), pixelblob->GetBufferSize(), NULL, &PixelShader);

			vertexblob->Release();
			hullblob->Release();
			domainblob->Release();
			pixelblob->Release();
		}


		//pVSBlob->Release();
		//pPSBlob->Release();

		return true;
	}

	void DefaultShader::UpdateConstantBuffer(void* constBuffPtr)
	{		
		ConstantBufferManager::DefaultShadingValueFormat* buffFormat = static_cast<ConstantBufferManager::DefaultShadingValueFormat*>(constBuffPtr);
		if (!buffFormat)
			return;
		
		ConstantBufferManager::GetInstance().UpdateConstantBuffer(ConstantBufferManager::ConstBufferType::perMaterial, buffFormat, sizeof(ConstantBufferManager::DefaultShadingValueFormat));
	}

	void DefaultShader::LinkToPipeline()
	{
		ID3D11DeviceContext* context = DXUtility::GetInstance()->GetDeviceContext();
		context->IASetInputLayout(VertexLayout);
		context->VSSetShader(VertexShader,nullptr,0);
		context->HSSetShader(HullShader, nullptr, 0);
		context->DSSetShader(DomainShader, nullptr, 0);
		context->PSSetShader(PixelShader, nullptr, 0);
		//SamplerState,DepthStencilState,RasterizerState 세가지.. 따로분리해서 가지고있기?
		context->RSSetState(StateObjectManger::GetInstance()->GetRasterizerSate(StateObjectManger::RasterizingType::Default));
		//context->RSSetState(StateObjectManger::GetInstance()->GetRasterizerSate(StateObjectManger::RasterizingType::Wire));
		context->OMSetDepthStencilState(StateObjectManger::GetInstance()->GetDepthStencilState(StateObjectManger::DepthStencilType::Default), 1);

		//float blendFactor[4] = { 1,1,1,1 };
		//context->OMSetBlendState(StateObjectManger::GetInstance()->GetBlendState(StateObjectManger::BlendingType::Default), blendFactor, 0xffffffff);


	}



}

