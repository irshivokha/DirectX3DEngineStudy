#include "shaderResourcePool.h"

namespace Engine
{
	ShaderResourcePool* ShaderResourcePool::Instance = nullptr;

	ShaderResourcePool::ShaderResourcePool()
	{
		ShaderResourceMapData.clear();
	}

	ShaderResourcePool::~ShaderResourcePool()
	{

	}



	const ShaderResourcePool* const ShaderResourcePool::GetInstance()
	{
		return Instance;
	}

	void ShaderResourcePool::CreateInstance()
	{
		if (!Instance)
			Instance = new ShaderResourcePool();
	}

	void ShaderResourcePool::ReleaseInstance()
	{
		if (!Instance)
		{
			delete Instance;
			Instance = nullptr;
		}
	}

	void ShaderResourcePool::Init()
	{
		ShaderResourceMapData.clear();
	}

	void ShaderResourcePool::Release()
	{
		ShaderResourceMapData.clear();
	}

	void ShaderResourcePool::Register(int UID)
	{

		ShaderValue* pShaderValue = new ShaderValue();

		ShaderResourceMapData.
			insert(std::pair<int, std::tr1::shared_ptr<ShaderValue>>(UID, std::tr1::shared_ptr<ShaderValue>(pShaderValue)));

		return;
	}

	const std::map<int, std::tr1::shared_ptr <ShaderResourcePool::ShaderValue >> ::iterator ShaderResourcePool::GetBegin()
	{
		return ShaderResourceMapData.begin();
	}

	const std::map<int, std::tr1::shared_ptr<ShaderResourcePool::ShaderValue>>::iterator ShaderResourcePool::GetEnd()
	{
		return ShaderResourceMapData.end();
	}

	ShaderResourcePool::ShaderValue::ShaderValue()
		:ScreenWidth(0.0f),
		ScreenHeight(0.0f),
		DofEnabled(false),
		UseReflect(false),
		ColorFilterType(0),
		ReturnPosNormal(),
		blurAmount(0.0f),
		fadeAmount(0.0f),
		SentenceColor(),
		bloomThreshold(0.0f),
		CameraDir(0.0f, 0.0f, 0.0f, 0.0f),
		LightDir(0.0f, 0.0f, 0.0f, 0.0f),
		LightColor(0.0f, 0.0f, 0.0f),
		CameraPos(0.0f, 0.0f, 0.0f, 0.0f),
		LightPos(0.0f, 0.0f, 0.0f, 0.0f),
		World(XMMatrixIdentity()),
		View(XMMatrixIdentity()),
		Proj(XMMatrixIdentity()),
		OrthoMatrix(XMMatrixIdentity()),
		ReflectionMat(XMMatrixIdentity()),
		LightView(XMMatrixIdentity()),
		LightProj(XMMatrixIdentity()),
		ShadowMap(nullptr)
	{

	}

}

