#pragma once
#include "UnCopyable.h"
#include "Engine.h"


namespace Engine
{
	class ShaderObject;
	enum class ShaderType;		//컴파일 다시되는지 실험해보고 상관없으면 include 시키기

	class ShaderManager :public UnCopyable
	{	

	private:
		//Shader* ShaderArray[E_SHADER_MAX];
		std::vector<std::shared_ptr<ShaderObject>> ShaderVector;		//추후에 SingleTon에서 다시 바꿀수도 있음. 이건 고민하는 걸로
		static ShaderManager* pInstance;
		ShaderManager();
		~ShaderManager();

	public:
		static void CreateInstance();
		static void ReleaseInstance();
		static ShaderManager* const GetInstance();
		void LoadShader();
		std::shared_ptr<ShaderObject> const GetShader(ShaderType eShader);
	};
}

