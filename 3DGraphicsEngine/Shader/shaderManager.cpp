#include "EngineBaseHeader.h"
#include "ShaderManager.h"
#include "ShaderObject.h"
#include "DefineValues.h"
#include "Shader/defaultShader.h"
#include "Shader/TerrainShader.h"

namespace Engine
{
	ShaderManager* ShaderManager::pInstance = nullptr;

	ShaderManager::ShaderManager()
	{
		ShaderVector.clear();
	}

	ShaderManager::~ShaderManager()
	{
		ShaderVector.clear();
	}

	void ShaderManager::CreateInstance()
	{
		pInstance = new ShaderManager();
	}

	void ShaderManager::ReleaseInstance()
	{
		if (pInstance)
		{
			delete pInstance;
			pInstance = nullptr;
		}
	}



	ShaderManager* const ShaderManager::GetInstance()
	{
		return pInstance;
	}

	void ShaderManager::LoadShader()
	{
		//ShaderVector.reserve((size_t)ShaderType::Max_Type);
		//ShaderVector.resize((size_t)ShaderType::Max_Type);
		ShaderObject* shaders[(int)ShaderType::Max_Type];
		shaders[(int)ShaderType::Full] = new DefaultShader();
		shaders[(int)ShaderType::Lite] = nullptr;
		shaders[(int)ShaderType::Cartoon] = nullptr;
		shaders[(int)ShaderType::Terrain] = new TerrainShader();
		/*
		cartoon, lite shader set
		*/

		for(int i = (int)ShaderType::Full; i < (int)ShaderType::Max_Type; i++)
		{			
			ShaderObject* shaderPtr = shaders[i];
			std::shared_ptr<ShaderObject> shaderelements(shaderPtr);
			ShaderVector.push_back(shaderelements);
			if (ShaderVector[i])
			{
				ShaderVector[i].get()->CreateConstantBuffer();
				ShaderVector[i].get()->CompileShader();
			}
		}

	}


	std::shared_ptr<ShaderObject> const ShaderManager::GetShader(Engine::ShaderType eShader)
	{
		return ShaderVector[(int)eShader];
	}

}

