#pragma once

#include "EngineBaseHeader.h"
#include "UnCopyable.h"

namespace Engine
{
	class ShaderResourcePool :public UnCopyable
	{

	public:
		struct ShaderValue
		{
			ShaderValue();
			float ScreenWidth;
			float ScreenHeight;
			bool DofEnabled;
			bool UseReflect;
			int ColorFilterType;
			float blurAmount;
			float fadeAmount;
			XMFLOAT4 SentenceColor;
			float bloomThreshold;
			XMFLOAT4 CameraDir;
			XMFLOAT4 LightDir;
			XMFLOAT3 LightColor;
			XMFLOAT4 CameraPos;
			XMFLOAT4 LightPos;
			XMMATRIX World;
			XMMATRIX View;
			XMMATRIX Proj;
			XMMATRIX OrthoMatrix;
			XMMATRIX ReflectionMat;
			XMFLOAT4 ReturnPosNormal;
			XMMATRIX LightView;
			XMMATRIX LightProj;
			ID3D11ShaderResourceView* ShadowMap;
		};

	private:
		explicit ShaderResourcePool();
		~ShaderResourcePool();
		static ShaderResourcePool* Instance;


		std::map<int, std::tr1::shared_ptr<ShaderValue>> ShaderResourceMapData;


	public:
		static const ShaderResourcePool* const GetInstance();
		static void CreateInstance();
		static void ReleaseInstance();
		void Init();
		void Release();
		void Register(int UID);
		const std::map<int, std::tr1::shared_ptr<ShaderValue>>::iterator GetBegin();
		const std::map<int, std::tr1::shared_ptr<ShaderValue>>::iterator GetEnd();

	};
}

