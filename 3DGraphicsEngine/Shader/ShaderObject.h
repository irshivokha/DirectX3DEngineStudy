#pragma once
#include "UnCopyable.h"
#include "DefineValues.h"
namespace Engine
{
	class Property;

	class ShaderObject:public UnCopyable
	{

	protected:
		ID3D11VertexShader* VertexShader;
		ID3D11PixelShader* PixelShader;
		ID3D11HullShader* HullShader;
		ID3D11DomainShader* DomainShader;
		ID3D11ComputeShader* ComputeShader;

		ID3D11InputLayout* VertexLayout;
		ID3D11SamplerState* SamplerState; 

	public:
		ShaderObject();		

		virtual bool CompileShader() = 0;	//이름으로 Shader를 받아서 컴파일 하는 부분은 같음. CompileShader로 바꿔야겠당
		//virtual MakeConstBuffer ()	//Const buffer는 Shader마다 다르게 사용하므로 각 셰이더 클래스에서 재정의. World, View,Projection 등은 공통적으로 전달함/전달하지 않는 셰이더는 없음
		void Release();

		/*ConstantBuffer를 Immutable로 만들수도 있고  Default또는 Dynamic으로 만들수도 있음
		따라서 Map으로 직접 Update하거나 Subresource로 업데이트 해야함
		사실상 constBuffer를 UpdateSubresource로 업데이트 할 일이 있나?*/
		//Object가 가지고 있는 Shader가 다를 수 있으므로 Object를 거칠때마다 shader의 LinkToPipeLine을 호출해주어야함
		//아니면 material에 따라서 sorting을 해야하는데 이는 후에 생각해봐야 할듯
		virtual void LinkToPipeline() = 0;
		virtual void CreateConstantBuffer() = 0;
		virtual void UpdateConstantBuffer(const MaterialPropertyMap& Properties ) = 0;
		//virtual void SetProperty(PropertyType type, const Property& value) = 0;
		
	};
}
