#include "EngineBaseHeader.h"
#include "ShaderObject.h"

namespace Engine
{
	ShaderObject::ShaderObject()
		:VertexShader(nullptr),
		PixelShader(nullptr),
		HullShader(nullptr),
		DomainShader(nullptr),
		ComputeShader(nullptr),
		VertexLayout(nullptr),
		SamplerState(nullptr)		
	{

	}

	void ShaderObject::Release()
	{
		if (VertexShader)
			VertexShader->Release();

		if (PixelShader)
			PixelShader->Release();

		if (HullShader)
			HullShader->Release();

		if (DomainShader)
			DomainShader->Release();

		if (ComputeShader)
			ComputeShader->Release();

		if (VertexLayout)
			VertexLayout->Release();
	}

}


