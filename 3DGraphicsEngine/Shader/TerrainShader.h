#pragma once

#include "../Type/Property.h"
#include "ShaderObject.h"

namespace Engine
{
	class Texture;
	class TerrainShader : public ShaderObject
	{
	private:
		void SetDefault();
		//Property Diffuse;
		//Property Texture;

	public:
		TerrainShader();
		~TerrainShader();

		virtual void CreateConstantBuffer() override;
		virtual bool CompileShader() override;
		virtual void UpdateConstantBuffer(void* constBuffPtr);
		virtual void LinkToPipeline() override;

	private:

	};
}

