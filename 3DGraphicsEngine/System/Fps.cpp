#include "Fps.h"
#include "EngineBaseHeader.h"


Fps::Fps()
{
}


Fps::Fps(const Fps& other)
{
}


Fps::~Fps()
{
}

void Fps::Init()
{
	m_fps = 0;
	m_count = 0;
	m_startTime = timeGetTime();
	return;
}

//The Frame function must be called each frame so that it can increment the frame count by 1. 
//If it finds that one second has elapsed then it will store the frame count in the m_fps variable.It then resets the count and starts the timer again.

void Fps::Frame()
{
	m_count++;

	if (timeGetTime() >= (m_startTime + 1000))
	{
		m_fps = m_count;
		m_count = 0;

		m_startTime = timeGetTime();
	}
}

//GetFps returns the frame per second speed for the last second that just passed.
//This function should be constantly queried so the latest fps can be displayed to the screen.

int Fps::GetFps()
{
	return m_fps;
}
