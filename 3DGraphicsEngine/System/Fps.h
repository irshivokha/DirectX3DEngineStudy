#pragma once

class Fps
{
public:
	Fps();
	Fps(const Fps&);
	~Fps();

	void Init();
	void Frame();
	int GetFps();

private:
	int m_fps, m_count;
	unsigned long m_startTime;
};
