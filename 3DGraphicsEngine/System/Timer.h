#pragma once
#include "UnCopyable.h"
#include <Windows.h>
namespace Engine
{
	class Timer :public UnCopyable
	{
		//private:
		//	DWORD m_dwOffsetTime;
		//public:
		//	C_TIMER();
		//	float GetElapsedTime();
		//	void SetOffsetTime();
	private:
		Timer();
	public:
		static Timer& GetInstance();
		bool Init();
		void Frame();
		double GetElapsedTime();

	private:
		LARGE_INTEGER m_frequency;
		float m_ticksPerMs;
		LARGE_INTEGER m_startTime;
		double m_frameTime;
	};
}


