#include "EngineBaseHeader.h"
#include "Timer.h"


namespace Engine
{
	Timer::Timer() :
		m_frameTime(0),
		m_startTime(),
		m_frequency(),
		m_ticksPerMs(0)
	{

	}


	Timer& Timer::GetInstance()
	{
		static Timer instance;
		return instance;
	}

	bool Timer::Init()
	{
		// Check to see if this system supports high performance Timers.
		QueryPerformanceFrequency(&m_frequency);			//타이머가 지원하는 주파수 1000이면 1초에 1000번의 진동을 지원 = 1/1000초의 정밀도까지 지원
		if (m_frequency.QuadPart == 0)
		{
			return false;
		}

		// Find out how many times the frequency counter ticks every millisecond.
		m_ticksPerMs = (float)(m_frequency.QuadPart);							//후에 시간차를 이 주파수로 나눠주면 단위시간 측정가능. 컴퓨터 별로 성능이 다르기에 반드시 나누어주어야함
																			//그냥 나누면 초단위, 1000으로 나눈것을 나누면 밀리세컨드 단위가 나옴

		QueryPerformanceCounter(&m_startTime);					//현재 CPU의 클럭수

		return true;
	}

	void Timer::Frame()
	{
		LARGE_INTEGER currentTime;

		QueryPerformanceCounter(&currentTime);				//현재 시간을 얻어옴. 현재 CPU의 클럭수임

		//timeDifference = (float)(double)(currentTime.QuadPart - m_startTime.QuadPart);

		m_frameTime = ((double)currentTime.QuadPart - (double)m_startTime.QuadPart) / (double)m_frequency.QuadPart;

		m_startTime = currentTime;

		return;
	}

	//GetTime returns the most recent frame time that was calculated.

	double Timer::GetElapsedTime()
	{
		return m_frameTime;
	}



	//float C_Timer::GetElapsedTime()
	//{
	//	DWORD dwNowTime = timeGetTime();
	//	DWORD dwElapsed = dwNowTime - m_dwOffsetTime;
	//
	//	return (float)dwElapsed * 0.001f;
	//}
	//
	//void C_Timer::SetOffsetTime()
	//{
	//	m_dwOffsetTime = timeGetTime();
	//}
}


