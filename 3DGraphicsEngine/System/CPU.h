#pragma once

#include <pdh.h>

namespace Engine
{
	class CPU
	{
	public:
		CPU();
		CPU(const CPU&);
		~CPU();

		void Init();
		void Release();
		void Frame();
		int GetCpuPercentage();

	private:
		bool m_canReadCpu;
		HQUERY m_queryHandle;
		HCOUNTER m_counterHandle;
		unsigned long m_lastSampleTime;
		long m_cpuUsage;
	};
}



