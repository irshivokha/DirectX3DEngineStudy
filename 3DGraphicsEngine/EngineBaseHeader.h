// stdafx.h : 자주 사용하지만 자주 변경되지는 않는
// 표준 시스템 포함 파일 또는 프로젝트 관련 포함 파일이
// 들어 있는 포함 파일입니다.
//

#pragma once

#include "targetver.h"
#include <Windows.h>
#include <d3d11.h>


#include <d3dcompiler.h>
#include <vector>
#include <list>
#include <map>
#include <memory>

#include <DirectXMath.h>
#include <DirectXPackedVector.h>
#include <fstream>
#include <iostream>
#include <string>
#include <stdlib.h>
#include <sstream>

//모델 임포터
#include <assimp\Importer.hpp>
#include <assimp\scene.h>
#include <assimp\postprocess.h>

#include "EngineNamespace.h" 


using namespace DirectX;
using namespace DirectX::PackedVector;
using namespace std;

#define WIN32_LEAN_AND_MEAN             // 거의 사용되지 않는 내용은 Windows 헤더에서 제외합니다.
