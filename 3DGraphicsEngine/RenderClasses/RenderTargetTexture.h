#pragma once
#include <windows.h>
#include <d3d11.h>
#include <d3dx11.h>
#include <d3dcompiler.h>

class C_DIRECT3D;

class RenderTargetTexture
{
public:
	//FullScreenRenderTarget(Game& game);
	//FullScreenRenderTarget();
	RenderTargetTexture();
	RenderTargetTexture(const RenderTargetTexture& rhs);
	RenderTargetTexture& operator=(const RenderTargetTexture &rhs);
	~RenderTargetTexture();
	ID3D11Texture2D* resolveMultisampledTexture(ID3D11Texture2D *source, unsigned int subresource);
	ID3D11ShaderResourceView* OutputTexture() const;
	ID3D11ShaderResourceView* OutputDepthTexture() const;
	ID3D11DepthStencilState* DepthStencilState;
	ID3D11RenderTargetView* RenderTargetView() const;
	ID3D11DepthStencilView* DepthStencilView() const;
	ID3D11Texture2D* GetTexture2D();

	void Init(ID3D11DeviceContext* pContext, ID3D11Device* pDevice, int width, int height);
	void Begin();
	void End(C_DIRECT3D* pDirect3D);
	void SetDepthStencilState();
	void CheckDepth();
	int GetWidth();	
private:
	C_DIRECT3D* Direct3D;
	int Width;
	int Height;
	ID3D11DeviceContext* DeviceContext;
	ID3D11Device* D11Device;
	ID3D11RenderTargetView* mRenderTargetView;
	ID3D11DepthStencilView* mDepthStencilView;
	ID3D11ShaderResourceView* mOutputTexture;
	ID3D11ShaderResourceView* DepthShaderResourceView;
	D3D11_RASTERIZER_DESC	mRasterDesc;
	ID3D11Texture2D* FullScreenTexture;
	D3D11_VIEWPORT* ViewPort;
	


};