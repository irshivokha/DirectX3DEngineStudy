#include "EngineBaseHeader.h"
#include "DXUtility.h"
#include <d3d11.h>

namespace Engine
{
	DXUtility::DXUtility()
		:DriverType(D3D_DRIVER_TYPE_NULL),
		FeatureLevel(D3D_FEATURE_LEVEL_11_0),
		D3dDevice(nullptr),
		ImmediateContext(nullptr),
		SwapChain(nullptr),
		RenderTargetView(nullptr),
		DepthStencil(nullptr),
		DepthStencilView(nullptr),
		ViewPort(nullptr),
		BackBuffer(nullptr),
		DepthShaderResourceView(nullptr),
		Width(0),
		Height(0),
		Proj(Math::Matrix::Identity()),
		Ortho(Math::Matrix::Identity())
	{
	}

	DXUtility* DXUtility::Instance = nullptr;

	void DXUtility::CreateInstance()
	{
		if (!Instance)
			Instance = new DXUtility();
	}

	void DXUtility::ReleaseInstance()
	{		
		delete Instance;
		Instance = nullptr;
	}

	DXUtility* const DXUtility::GetInstance()
	{
		return Instance;
	}

	HRESULT DXUtility::Init(HWND hWnd, int flags)
	{
		HRESULT hr = S_OK;

		RECT rc;
		GetClientRect(hWnd, &rc);
		UINT width = rc.right - rc.left;
		UINT height = rc.bottom - rc.top;

		Width = rc.right - rc.left;
		Height = rc.bottom - rc.top;

		Width = Width;
		Height = Height;

		UINT createDeviceFlags = 0;
#ifdef _DEBUG
		createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

		D3D_DRIVER_TYPE driverTypes[] =
		{
			D3D_DRIVER_TYPE_HARDWARE,
			D3D_DRIVER_TYPE_WARP,
			D3D_DRIVER_TYPE_REFERENCE,
		};
		UINT numDriverTypes = ARRAYSIZE(driverTypes);

		D3D_FEATURE_LEVEL featureLevels[] =
		{
			D3D_FEATURE_LEVEL_11_0,
			D3D_FEATURE_LEVEL_10_1,
			D3D_FEATURE_LEVEL_10_0,
		};
		UINT numFeatureLevels = ARRAYSIZE(featureLevels);

		//UINT level = 0;
		//UINT sampleCount = 8;

		DXGI_SWAP_CHAIN_DESC sd;
		ZeroMemory(&sd, sizeof(sd));
		sd.BufferCount = 1;
		sd.BufferDesc.Width = width;
		sd.BufferDesc.Height = height;
		sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		sd.BufferDesc.RefreshRate.Numerator = 60;
		sd.BufferDesc.RefreshRate.Denominator = 1;
		sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		sd.OutputWindow = hWnd;
		sd.SampleDesc.Count = 1;
		sd.SampleDesc.Quality = 0;
		sd.Windowed = TRUE;

		for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
		{
			DriverType = driverTypes[driverTypeIndex];
			hr = D3D11CreateDeviceAndSwapChain(nullptr, DriverType, nullptr, createDeviceFlags, featureLevels, numFeatureLevels,
				D3D11_SDK_VERSION, &sd, &SwapChain, &D3dDevice, &FeatureLevel, &ImmediateContext);
			if (SUCCEEDED(hr))
				break;
		}



		if (FAILED(hr))
			return hr;

		// Create a render target view
		//ID3D11Texture2D* pBackBuffer = nullptr;
		hr = SwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&BackBuffer);
		if (FAILED(hr))
			return hr;

		hr = D3dDevice->CreateRenderTargetView(BackBuffer, nullptr, &RenderTargetView);
		//pBackBuffer->Release();
		if (FAILED(hr))
			return hr;

		// Create depth stencil texture
		D3D11_TEXTURE2D_DESC descDepth;
		ZeroMemory(&descDepth, sizeof(descDepth));
		descDepth.Width = width;
		descDepth.Height = height;
		descDepth.MipLevels = 1;
		descDepth.ArraySize = 1;
		//descDepth.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
		descDepth.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
		descDepth.SampleDesc.Count = 1;
		descDepth.SampleDesc.Quality = 0;
		descDepth.Usage = D3D11_USAGE_DEFAULT;
		descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL;
		descDepth.CPUAccessFlags = 0;
		descDepth.MiscFlags = 0;
		hr = D3dDevice->CreateTexture2D(&descDepth, nullptr, &DepthStencil);
		if (FAILED(hr))
			return hr;




		// Create the depth stencil view
		D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
		ZeroMemory(&descDSV, sizeof(descDSV));
		descDSV.Format = descDepth.Format;
		descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
		descDSV.Texture2D.MipSlice = 0;
		hr = D3dDevice->CreateDepthStencilView(DepthStencil, &descDSV, &DepthStencilView);
		if (FAILED(hr))
			return hr;

		D3D11_SHADER_RESOURCE_VIEW_DESC sr_desc;
		sr_desc.Format = DXGI_FORMAT_R24_UNORM_X8_TYPELESS;
		sr_desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		sr_desc.Texture2D.MostDetailedMip = 0;
		sr_desc.Texture2D.MipLevels = -1;
		//D3dDevice->CreateShaderResourceView(DepthStencil, &sr_desc, &DepthShaderResourceView);


		ImmediateContext->OMSetRenderTargets(1, &RenderTargetView, DepthStencilView);

		ViewPort = new D3D11_VIEWPORT();
		// Setup the viewport
		ViewPort->Width = (FLOAT)width;
		ViewPort->Height = (FLOAT)height;
		ViewPort->MinDepth = 0.0f;
		ViewPort->MaxDepth = 1.0f;
		ViewPort->TopLeftX = 0;
		ViewPort->TopLeftY = 0;
		ImmediateContext->RSSetViewports(1, ViewPort);



		// Initialize the projection matrix
		Proj = Math::Matrix::MatrixPerpectiveFovLH(XM_PIDIV4, (float)width / (FLOAT)height, 0.01f, 100.0f);
		Ortho = Math::Matrix::MatrixOrthographicLH((FLOAT)width, (FLOAT)height, 0.01f, 1000.0f);

		return S_OK;
	}

	void DXUtility::Release()
	{
		if (ImmediateContext) ImmediateContext->ClearState();
		if (DepthStencil) DepthStencil->Release();
		if (DepthStencilView) DepthStencilView->Release();
		if (RenderTargetView) RenderTargetView->Release();
		if (SwapChain) SwapChain->Release();
		if (ImmediateContext) ImmediateContext->Release();
		if (D3dDevice) D3dDevice->Release();
		if (ViewPort) delete ViewPort;
		if (BackBuffer) BackBuffer->Release();
		if (DepthShaderResourceView) DepthShaderResourceView->Release();
		{

		}
	}

	void DXUtility::BeginScene()
	{
		float ClearColor[4] = { 0.2f, 0.2f, 1.0f, 1.0f }; // red, green, blue, alpha
		ImmediateContext->ClearRenderTargetView(RenderTargetView, ClearColor);
		// Clear the depth buffer to 1.0 (max depth)
		ImmediateContext->ClearDepthStencilView(DepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);

		//ImmediateContext->PSSetSamplers(0, 1, &SamplerLinear);
	}

	void DXUtility::EndScene()
	{
		SwapChain->Present(0, 0);
	}

	ID3D11Device* const DXUtility::GetDevice()
	{
		return D3dDevice;
	}

	ID3D11DeviceContext* const DXUtility::GetDeviceContext()
	{
		return ImmediateContext;
	}

	const D3D_DRIVER_TYPE DXUtility::GetDriverType() const
	{
		return DriverType;
	}



	void DXUtility::ResetRenderTarget()
	{
		ImmediateContext->OMSetRenderTargets(1, &RenderTargetView, DepthStencilView);
	}

	//void DXUtility::ResetViewPort()
	//{
	//	ImmediateContext->RSSetViewports(1, ViewPort);
	//}


	const ID3D11Texture2D* const DXUtility::GetBackBuffer() const
	{
		return BackBuffer;
	}
}

