#include "EngineBaseHeader.h"
#include "StateObjectManager.h"
#include "Utility/Utility.h"
#include "DXUtility.h"
namespace Engine
{
	StateObjectManger* StateObjectManger::Instance = nullptr;

	StateObjectManger::StateObjectManger()
	{
		for (int i = (int)RasterizingType::Default; i < (int)RasterizingType::Max_RasterzingType; i++)
			RasterizerStates[i] = nullptr;
		for (int i = (int)SamplerType::Default; i < D3D11_COMMONSHADER_SAMPLER_SLOT_COUNT; i++)
			SamplerState[i] = nullptr;
		for (int i = (int)BlendingType::Default; i < (int)BlendingType::Max_BlendingType; i++)
			BlendStates[i] = nullptr;
		for (int i = (int)DepthStencilType::Default; i < (int)DepthStencilType::Max_DepthStencilType; i++)
			DepthStencilStates[i] = nullptr;
	}

	StateObjectManger* const Engine::StateObjectManger::GetInstance()
	{
		if (Instance)
			return Instance;
		else
			return nullptr;
	}

	void StateObjectManger::CreateInstance()
	{
		if (!Instance)
			Instance = new StateObjectManger();
	}

	void StateObjectManger::ReleaseInstance()
	{
		if (Instance)
			Instance->Release();
		Utility::Delete(Instance);
	}

	void StateObjectManger::Init()
	{

		/**********  Make SamplerState  ***********/
		//default Sampler
		D3D11_SAMPLER_DESC samplerDesc;
		samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
		samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
		samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
		samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
		samplerDesc.MipLODBias = 0;
		samplerDesc.MaxAnisotropy = 1;
		samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
		for(int i = 0 ; i <4; i++)
			samplerDesc.BorderColor[i] = 0;
		samplerDesc.MinLOD = 0;
		samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;			//<---- no upperLimit

		DXUtility::GetInstance()->GetDevice()->CreateSamplerState(&samplerDesc, &SamplerState[(int)SamplerType::Default]);
		

		samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
		samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
		samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;

		DXUtility::GetInstance()->GetDevice()->CreateSamplerState(&samplerDesc, &SamplerState[(int)SamplerType::Clamp]);

		//anisotrpoic sampler
		samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
		samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
		samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
		samplerDesc.Filter = D3D11_FILTER_ANISOTROPIC;
		samplerDesc.MaxAnisotropy = 16;
		DXUtility::GetInstance()->GetDevice()->CreateSamplerState(&samplerDesc, &SamplerState[(int)SamplerType::Anisotropic]);

		for (int i = 2; i < D3D11_COMMONSHADER_SAMPLER_SLOT_COUNT; i++)
		{
			SamplerState[i] = nullptr;
		}



		/**********  Make RasterizerState  ***********/
		D3D11_RASTERIZER_DESC rasterizerDesc;
		rasterizerDesc.FillMode = D3D11_FILL_SOLID;
		rasterizerDesc.CullMode = D3D11_CULL_BACK;
		rasterizerDesc.FrontCounterClockwise = FALSE;
		rasterizerDesc.DepthBias = 0;		//그림자  쓸 경우 다시 서술하기
		rasterizerDesc.SlopeScaledDepthBias = 0.0f;
		rasterizerDesc.DepthBiasClamp = 0.0f;
		rasterizerDesc.ScissorEnable = false;
		rasterizerDesc.DepthClipEnable = true;
		rasterizerDesc.AntialiasedLineEnable = false;

		DXUtility::GetInstance()->GetDevice()->CreateRasterizerState(&rasterizerDesc, &RasterizerStates[(int)RasterizingType::Default]);

		rasterizerDesc.CullMode = D3D11_CULL_FRONT;
		DXUtility::GetInstance()->GetDevice()->CreateRasterizerState(&rasterizerDesc, &RasterizerStates[(int)RasterizingType::Default_CullFront]);

		rasterizerDesc.CullMode = D3D11_CULL_NONE;
		DXUtility::GetInstance()->GetDevice()->CreateRasterizerState(&rasterizerDesc, &RasterizerStates[(int)RasterizingType::Default_CullNone]);

		rasterizerDesc.FillMode = D3D11_FILL_WIREFRAME;
		DXUtility::GetInstance()->GetDevice()->CreateRasterizerState(&rasterizerDesc, &RasterizerStates[(int)RasterizingType::Wire]);

		rasterizerDesc.CullMode = D3D11_CULL_FRONT;
		DXUtility::GetInstance()->GetDevice()->CreateRasterizerState(&rasterizerDesc, &RasterizerStates[(int)RasterizingType::Wire_CullFront]);

		rasterizerDesc.CullMode = D3D11_CULL_NONE;
		DXUtility::GetInstance()->GetDevice()->CreateRasterizerState(&rasterizerDesc, &RasterizerStates[(int)RasterizingType::Wire_CullNone]);





		/**********  Make DepthStencilState  ***********/
		D3D11_DEPTH_STENCIL_DESC depthStencilDesc;
		depthStencilDesc.DepthEnable = true;
		depthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
		depthStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;
		depthStencilDesc.StencilEnable = false;				//stencile 내용은 나중에 다시보기
		depthStencilDesc.StencilReadMask = 0xFF;
		depthStencilDesc.StencilWriteMask = 0xFF;

		//Stencil operations if pixel is front-facing.
		depthStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
		depthStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
		depthStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
		depthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

		//Stencil operations if pixel is back-facing.
		depthStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
		depthStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
		depthStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
		depthStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

		DXUtility::GetInstance()->GetDevice()->CreateDepthStencilState(&depthStencilDesc, &DepthStencilStates[(int)DepthStencilType::Default]);


		/**********  Make BlendState  ***********/

		
		D3D11_BLEND_DESC blendDesc;

		//alphablend state
		blendDesc.AlphaToCoverageEnable = false;
		blendDesc.IndependentBlendEnable = false;
		blendDesc.RenderTarget[0].BlendEnable = true;
		blendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
		blendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
		blendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
		blendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
		blendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
		blendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
		blendDesc.RenderTarget[0].RenderTargetWriteMask = 0x0f;
		DXUtility::GetInstance()->GetDevice()->CreateBlendState(&blendDesc, &BlendStates[(int)BlendingType::AlphaBlend]);

		//noBlend
		blendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
		blendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_ZERO;
		DXUtility::GetInstance()->GetDevice()->CreateBlendState(&blendDesc, &BlendStates[(int)BlendingType::Default]);


	}

	ID3D11SamplerState* const *const  StateObjectManger::GetSamplerStates()
	{
		return SamplerState;
	}

	ID3D11BlendState* const StateObjectManger::GetBlendState(StateObjectManger::BlendingType type)
	{
		return BlendStates[(int)type];
	}

	ID3D11RasterizerState* const StateObjectManger::GetRasterizerSate(StateObjectManger::RasterizingType type)
	{
		return RasterizerStates[(int)type];
	}

	ID3D11DepthStencilState* const StateObjectManger::GetDepthStencilState(StateObjectManger::DepthStencilType type)
	{
		return DepthStencilStates[(int)type];
	}

	void StateObjectManger::Release()
	{		
		for (int i = 0; i < (int)SamplerType::Max_SamplerType; i++)
			Utility::Release(SamplerState[i]);
		for (int i = 0; i < (int)RasterizingType::Max_RasterzingType; i++)
			Utility::Release(RasterizerStates[i]);
		for (int i = 0; i < (int)BlendingType::Max_BlendingType; i++)
			Utility::Release(BlendStates[i]);	
		for (int i = 0; i < (int)DepthStencilType::Max_DepthStencilType; i++)
			Utility::Release(DepthStencilStates[i]);

	}


}


