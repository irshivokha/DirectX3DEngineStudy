#include "stdafx.h"

ScreenQuad::ScreenQuad() :
	IndexBuffer(nullptr),
	VertexBuffer(nullptr),
	Shader(nullptr),
	IndexLength(0),
	m_arTexture()
{

}

void ScreenQuad::SetShader(Shader* pShader)
{
	Shader = pShader;
}


HRESULT ScreenQuad::Load(ID3D11Device* pD3dDevice, float screenWidth, float screenHeight, float width, float height)
{


	HRESULT hr = S_OK;

	Width = width;
	Height = height;

	int nVertexLength = 6;
	float offSet = screenWidth / 2.0f;

	VERTEX vertices[] =
	{
		{ XMFLOAT3(-offSet, -offSet, 0.0f), XMFLOAT2(0.0f, 1.0f) },
		{ XMFLOAT3(offSet, -offSet, 0.0f), XMFLOAT2(1.0f, 1.0f) },
		{ XMFLOAT3(offSet, offSet, 0.0f), XMFLOAT2(1.0f, 0.0f) },
		{ XMFLOAT3(-offSet, offSet, 0.0f), XMFLOAT2(0.0f, 0.0f) },
		{ XMFLOAT3(offSet, offSet, 0.0f), XMFLOAT2(1.0f, 0.0f) },
		{ XMFLOAT3(offSet, -offSet, 0.0f), XMFLOAT2(1.0f, 1.0f) },

	};

	hr = RENDER_UTILFUNC::CreateBufferFromData(pD3dDevice, &VertexBuffer, D3D11_BIND_VERTEX_BUFFER,
		vertices,
		sizeof(VERTEX) * 6, D3D11_USAGE_DEFAULT, 0);


	IndexLength = 6;

	WORD indices[] =
	{
		3,1,0,
		5,3,4,


	};

	hr = RENDER_UTILFUNC::CreateBufferFromData(pD3dDevice, &IndexBuffer, D3D11_BIND_INDEX_BUFFER,
		indices, sizeof(WORD) * IndexLength, D3D11_USAGE_DEFAULT, 0);
	

	return hr;

}

HRESULT ScreenQuad::SetTexture(ID3D11ShaderResourceView * pShaderResourceView, ID3D11ShaderResourceView *pDepthShaderresourceView)
{
	m_arTexture[E_DIFFUSE] = pShaderResourceView;
	m_arTexture[E_SECOND] = pDepthShaderresourceView;

	return S_OK;
}

HRESULT ScreenQuad::SetTextuerArray(ID3D11ShaderResourceView ** pShaderResourceView, int count)
{
	for(int i = 0; i <count; i++)
	{
		m_arTexture[i] = pShaderResourceView[i];
	}

	return S_OK;
}

HRESULT ScreenQuad::loadTexture(ID3D11Device * pD3dDevice, const char *pSzModelName)
{
	//OBJLoader *pObjLoader = OBJLoader::GetInstance();
	//HRESULT hr = S_OK;
	//char szTextureName[MAX_PATH] = "";

	//pObjLoader->GetDiffuseTextureName(szTextureName, MAX_PATH, pSzModelName);
	//hr = D3DX11CreateShaderResourceViewFromFileA(pD3dDevice, szTextureName, nullptr, nullptr, &m_arTexture[E_DIFFUSE], nullptr);
	//if (FAILED(hr))
	//	return hr;


	//pObjLoader->GetNormalTextureName(szTextureName, MAX_PATH, pSzModelName);
	//hr = D3DX11CreateShaderResourceViewFromFileA(pD3dDevice, szTextureName, nullptr, nullptr, &m_arTexture[E_NORMAL], nullptr);
	//if (FAILED(hr))
	//	return hr;

	//pObjLoader->GetSpecularTextureName(szTextureName, MAX_PATH, pSzModelName);
	//hr = D3DX11CreateShaderResourceViewFromFileA(pD3dDevice, szTextureName, nullptr, nullptr, &m_arTexture[E_SPEC], nullptr);
	//if (FAILED(hr))
	//	return hr;

	return S_OK;
}





void ScreenQuad::Release()
{
	if (IndexBuffer)
		IndexBuffer->Release();
	if (VertexBuffer)
		VertexBuffer->Release();

	for (int i = 0; i < E_MAX; ++i)
	{
		if (m_arTexture[i])
			m_arTexture[i]->Release();
	}
}


void ScreenQuad::RenderSet(ID3D11DeviceContext*& pDeviceContext)
{
	UINT stride = sizeof(VERTEX);
	UINT offset = 0;
	pDeviceContext->IASetVertexBuffers(0, 1, &VertexBuffer, &stride, &offset);
	// Set index buffer
	pDeviceContext->IASetIndexBuffer(IndexBuffer, DXGI_FORMAT_R16_UINT, 0);

	pDeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	Shader->RenderSet();

	for (int i = 0; i < E_MAX; ++i)
	{
		Shader->SetShaderResource(i, 1, &m_arTexture[i]);
	}


}

void ScreenQuad::Draw(ID3D11DeviceContext*& pDeviceContext)
{
	//Shader->UpdateSubResource(Width, Height,1.5);
	
	Shader->UpdateMappedResource(XMMATRIX(), pDeviceContext, Width, Height);
	Shader->Render();
	pDeviceContext->DrawIndexed(IndexLength, 0, 0);

}

void ScreenQuad::Draw(ID3D11DeviceContext*& pDeviceContext, E_COLORFILTERTYPE filterType)
{
	Shader->UpdateSubResource(filterType);
	Shader->Render();
	pDeviceContext->DrawIndexed(IndexLength, 0, 0);

}

void ScreenQuad::Draw(ID3D11DeviceContext *& pDeviceContext, float width, float height, float blurAmount)
{
	Shader->UpdateSubResource(width,height,blurAmount);
	Shader->Render();
	pDeviceContext->DrawIndexed(IndexLength, 0, 0);
}
