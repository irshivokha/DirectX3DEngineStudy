#pragma once

#include "UnCopyable.h"
#include "DefineValues.h"
#include "DxMath/customMath.h"
#include "Engine.h"
namespace Engine
{
	class ConstantBufferManager:public UnCopyable
	{
	public:
		enum class ConstBufferType
		{
			perObject,
			perCamera,
			perMaterial,
			perLight,
			perFrame,			
			bufferType_max,
		};
		
	public:
		struct ObjectConstBuffFormat
		{
			Math::Matrix World;
			Math::Matrix InverseWorld;
			Math::UInt4 IsSkinned;
			Math::Matrix SkinMatrix[6];
			Math::Matrix NormalMatrix[6];
			Math::Vector4 TessellationFactor;
			

		};

		struct CameraConstBuffFormat
		{
			Math::Matrix View;
			Math::Matrix Proj;
			Math::Vector4 CamPos;
			Math::Vector4 CamDir;
			Math::Vector4 minMaxDistance;
			Math::Vector4 minMaxLOD;
		};

		struct DefaultShadingValueFormat
		{
			float Diffuse;
			float Specular;
			float Ambient;
			float Padding;	//버리는 값
			Math::Vector4 HeightMapInfo;
		};
		
		struct LightBuffFormat
		{	
			Math::Vector4 LightPos[100];
			Math::Vector4 LightDir[100];
			Math::Vector4 LightColor[100];
			Math::UInt4 LightCount;
						
		};

		struct perFrameBuffFormat
		{
		};

	private:
		ID3D11Buffer* ConstBuffer[(int)ConstBufferType::bufferType_max];
		ID3D11Buffer* BuffPerCamera;
		//ID3D11Buffer* BuffPerMaterial[(int)Engine::ShaderType::Max_Type];		//D3D11_COMMONSHADER_CONSTANT_BUFFER_API_SLOT_COUNT
				
		DefaultShadingValueFormat ShadingValueFormat;		
		ConstantBufferManager();

	public:
		/*world를 가지고 있는 버퍼는 포인터 얻어와서 한번 그리기 콜 때 
		매번 memcpy 해주어야 한다.
		view, proj는 각각 view값이 바뀔때와 proj값이바뀌는경우에 Map으로 포인터 얻어와서 viewMatrix와 ProjMatrix 건드리는 쪽에서 바꾸고 UnMap해버리기
		(단 카메라가 여러개여서 viewMatrix가 여러개인 경우는 다시 생각해보기)
		Shader용 Value는 object마다 Shader가 다를 수 있으므로.. 다르다 해도 object는 머티리얼의 포인터를 공유하기에(shading value 값도 공유)
		마우스 event 등으로 수정할때만 map하고 이후 unmap?
		*/
		void CreateConstantBuffer(ConstBufferType type, UINT size);
		//D3D11_MAPPED_SUBRESOURCE* LockConstantBuffer(ConstBufferType type);		//BegineUpdateConstantBuffer
		//void UnLockConstantBuffer(ConstBufferType type);	//EndUpdateConstantBuffer
		
		void UpdateConstantBuffer(ConstBufferType type, void* rowData, UINT size);
		ID3D11Buffer** GetBuffer(ConstBufferType type);

		static ConstantBufferManager& GetInstance();
		void Release();

	};
}

