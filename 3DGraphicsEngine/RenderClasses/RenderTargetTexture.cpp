#include "stdafx.h"

RenderTargetTexture::RenderTargetTexture()
	:mRenderTargetView(nullptr), mDepthStencilView(nullptr),
	DeviceContext(nullptr), Width(0), Height(0), Direct3D(nullptr), D11Device(nullptr),ViewPort(nullptr),FullScreenTexture(nullptr)
{

}

RenderTargetTexture::~RenderTargetTexture()
{
	Direct3D->Release();
	delete Direct3D;
	mOutputTexture->Release();
	delete mOutputTexture;
	mDepthStencilView->Release();
	delete mDepthStencilView;
	mRenderTargetView->Release();
	DepthShaderResourceView->Release();
	delete DepthShaderResourceView;
}

ID3D11ShaderResourceView* RenderTargetTexture::OutputTexture() const
{
	return mOutputTexture;
}

ID3D11ShaderResourceView * RenderTargetTexture::OutputDepthTexture() const
{
	return DepthShaderResourceView;
}

ID3D11RenderTargetView* RenderTargetTexture::RenderTargetView() const
{
	return mRenderTargetView;
}

ID3D11DepthStencilView* RenderTargetTexture::DepthStencilView() const
{
	return mDepthStencilView;
}

ID3D11Texture2D * RenderTargetTexture::GetTexture2D()
{
	return FullScreenTexture;
}

void RenderTargetTexture::Init(ID3D11DeviceContext * pContext, ID3D11Device* pDevice, int width, int height)
{
	DeviceContext = pContext;
	Width = width;
	Height = height;
	D11Device = pDevice;

	D3D11_RENDER_TARGET_VIEW_DESC renderTargetViewDesc;
	D3D11_SHADER_RESOURCE_VIEW_DESC shaderResourceViewDesc;

	UINT level = 0;

	D3D11_TEXTURE2D_DESC fullScreenTextureDesc;
	ZeroMemory(&fullScreenTextureDesc, sizeof
	(fullScreenTextureDesc));
	fullScreenTextureDesc.Width = Width;
	fullScreenTextureDesc.Height = Height;
	fullScreenTextureDesc.MipLevels = 1;
	fullScreenTextureDesc.ArraySize = 1;
	fullScreenTextureDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	fullScreenTextureDesc.SampleDesc.Count = 1;
	fullScreenTextureDesc.SampleDesc.Quality = 0;
	fullScreenTextureDesc.CPUAccessFlags = 0;
	fullScreenTextureDesc.MiscFlags = 0;
	fullScreenTextureDesc.BindFlags = D3D11_BIND_RENDER_TARGET |
		D3D11_BIND_SHADER_RESOURCE;
	fullScreenTextureDesc.Usage = D3D11_USAGE_DEFAULT;

	HRESULT hr;
	//ID3D11Texture2D* fullScreenTexture = nullptr;
	if (FAILED(hr = D11Device->CreateTexture2D
	(&fullScreenTextureDesc, nullptr, &FullScreenTexture)))
	{
		return;
	}

	// Setup the description of the render target view.
	renderTargetViewDesc.Format = fullScreenTextureDesc.Format;
	renderTargetViewDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
	renderTargetViewDesc.Texture2D.MipSlice = 0;

	if (FAILED(hr = D11Device->CreateRenderTargetView
	(FullScreenTexture, &renderTargetViewDesc, &mRenderTargetView)))
	{
		FullScreenTexture->Release();
		return;
	}


	// Setup the description of the shader resource view. 
	shaderResourceViewDesc.Format = fullScreenTextureDesc.Format;
	shaderResourceViewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	shaderResourceViewDesc.Texture2D.MostDetailedMip = 0;
	shaderResourceViewDesc.Texture2D.MipLevels = 1;

	if (FAILED(hr = D11Device->CreateShaderResourceView
	(FullScreenTexture, &shaderResourceViewDesc, &mOutputTexture)))
	{
		FullScreenTexture->Release();
		return;
	}




	FullScreenTexture->Release();

	D3D11_TEXTURE2D_DESC depthStencilDesc;
	ZeroMemory(&depthStencilDesc, sizeof(depthStencilDesc));
	depthStencilDesc.Width = width;
	depthStencilDesc.Height = height;
	depthStencilDesc.MipLevels = 1;
	depthStencilDesc.ArraySize = 1;
	depthStencilDesc.Format = DXGI_FORMAT_R24G8_TYPELESS;
	depthStencilDesc.SampleDesc.Count = 1;
	depthStencilDesc.SampleDesc.Quality = 0;
	depthStencilDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL | 
		D3D11_BIND_SHADER_RESOURCE;
	depthStencilDesc.Usage = D3D11_USAGE_DEFAULT;
	depthStencilDesc.CPUAccessFlags = 0;
	depthStencilDesc.MiscFlags = 0;


	ID3D11Texture2D* depthStencilBuffer = nullptrptr;
	if (FAILED(hr = pDevice->CreateTexture2D
	(&depthStencilDesc, nullptr, &depthStencilBuffer)))
	{
		return;
	}

	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;

	// Set up the depth stencil view description.
	depthStencilViewDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	depthStencilViewDesc.Flags = 0;
	depthStencilViewDesc.Texture2D.MipSlice = 0;

	if (FAILED(hr = pDevice->CreateDepthStencilView
	(depthStencilBuffer, &depthStencilViewDesc, &mDepthStencilView)))
	{
		depthStencilBuffer->Release();
		return;
	}

	D3D11_DEPTH_STENCIL_DESC m_depthStencilDesc;
	// Set up the description of the stencil state.
	m_depthStencilDesc.DepthEnable = true;
	m_depthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	m_depthStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;

	m_depthStencilDesc.StencilEnable = true;
	m_depthStencilDesc.StencilReadMask = 0xFF;
	m_depthStencilDesc.StencilWriteMask = 0xFF;

	// Stencil operations if pixel is front-facing.
	m_depthStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	m_depthStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	m_depthStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	m_depthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	// Stencil operations if pixel is back-facing.
	m_depthStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	m_depthStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	m_depthStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	m_depthStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	// Create the depth stencil state.
	hr = pDevice->CreateDepthStencilState(&m_depthStencilDesc, &DepthStencilState);
	if (FAILED(hr))
	{
		return ;
	}



	D3D11_SHADER_RESOURCE_VIEW_DESC sr_desc;
	sr_desc.Format = DXGI_FORMAT_R24_UNORM_X8_TYPELESS;
	sr_desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	sr_desc.Texture2D.MostDetailedMip = 0;
	sr_desc.Texture2D.MipLevels = -1;
	pDevice->CreateShaderResourceView(depthStencilBuffer, &sr_desc, &DepthShaderResourceView);

	depthStencilBuffer->Release();


	// Setup the viewport
	ViewPort = new D3D11_VIEWPORT();
	ViewPort->Width = (FLOAT)width;
	ViewPort->Height = (FLOAT)height;
	ViewPort->MinDepth = 0.0f;
	ViewPort->MaxDepth = 1.0f;
	ViewPort->TopLeftX = 0;
	ViewPort->TopLeftY = 0;


}

void RenderTargetTexture::Begin()
{
	DeviceContext->OMSetRenderTargets(1,
		&mRenderTargetView, mDepthStencilView);

	float ClearColor[4] = { 0.0f, 1.0f, 0.3f, 1.0f }; // red, green, blue, alpha

	DeviceContext->ClearRenderTargetView(mRenderTargetView, ClearColor);
	// Clear the depth buffer to 1.0 (max depth)
	DeviceContext->ClearDepthStencilView(mDepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);
	DeviceContext->RSSetViewports(1, ViewPort);
}

void RenderTargetTexture::End(C_DIRECT3D* pDirect3D)
{
	pDirect3D->ResetRenderTarget();
}

void RenderTargetTexture::SetDepthStencilState()
{
	DeviceContext->OMSetDepthStencilState(DepthStencilState, 1);
}

void RenderTargetTexture::CheckDepth()
{
	///@brief 임의의 가상 텍스쳐를 만들어 씬을 복사한다
	//ID3D11Resource* pResource;
	//ID3D11Texture2D* tempTexture2D;
	////mDepthStencilView->GetResource(&pResource);
	//DepthShaderResourceView->GetResource(&pResource);

	////D3D11_TEXTURE2D_DESC testDesc;
	////ZeroMemory(&testDesc, sizeof
	////(testDesc));
	////testDesc.Width = Width;
	////testDesc.Height = Height;
	////testDesc.MipLevels = 1;
	////testDesc.ArraySize = 1;
	////testDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	////testDesc.SampleDesc.Count = 1;
	////testDesc.SampleDesc.Quality = 0;
	////testDesc.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
	////testDesc.MiscFlags = 0;
	////testDesc.BindFlags = 0;
	////testDesc.Usage = D3D11_USAGE_STAGING;

	////D11Device->CreateTexture2D(&testDesc, 0, &tempTexture2D);
	////DeviceContext->CopyResource(tempTexture2D, (ID3D11Texture2D*)pResource);

	////D3D11_MAPPED_SUBRESOURCE mappedResource;
	////DeviceContext->Map(tempTexture2D, 0, D3D11_MAP_READ, 0, &mappedResource);

	////struct Color { char r, g, b, a; };
	////Color* obj = (Color*)mappedResource.pData;

	////char r = obj[2].r;
	////char g = obj[233].r;
	////char b = obj[56].r;
	////char a = obj[2].a;

	//D3D11_TEXTURE2D_DESC depthStencilDesc;
	//ZeroMemory(&depthStencilDesc, sizeof(depthStencilDesc));
	//depthStencilDesc.Width = Width;
	//depthStencilDesc.Height = Height;
	//depthStencilDesc.MipLevels = 1;
	//depthStencilDesc.ArraySize = 1;
	//depthStencilDesc.Format = DXGI_FORMAT_R24G8_TYPELESS;
	//depthStencilDesc.SampleDesc.Count = 1;
	//depthStencilDesc.SampleDesc.Quality = 0;
	//depthStencilDesc.BindFlags = 0;
	//depthStencilDesc.Usage = D3D11_USAGE_STAGING;
	//depthStencilDesc.CPUAccessFlags = D3D10_CPU_ACCESS_READ;
	//depthStencilDesc.MiscFlags = 0;

	//D11Device->CreateTexture2D(&depthStencilDesc, 0, &tempTexture2D);
	//DeviceContext->CopyResource(tempTexture2D, (ID3D11Texture2D*)pResource);

	//D3D11_MAPPED_SUBRESOURCE mappedResource;
	//DeviceContext->Map(tempTexture2D, 0, D3D11_MAP_READ, 0, &mappedResource);


	//unsigned int* color = (unsigned int*)mappedResource.pData;


	//bool temp = false;
	//int idx = 0;
	//int count = 0;
	//for(int i = 0 ; i < 130000;i++)
	//{
	//	// Extract 24 depth bits
	//	float depth = static_cast<float>(color[129791] & 0x00FFFFFF);
	//	depth /= 16777215; // divide bei 2^24

	//					   // compute a grayscale value [0;255]
	//	unsigned char colorValue = static_cast<unsigned char>(depth * 255.0f);

	//	if (colorValue != 255)
	//	{
	//		temp = true;
	//		idx = i;
	//		count++;
	//	}
	//}


	return ;
}

int RenderTargetTexture::GetWidth()
{
	return Width;
}

ID3D11Texture2D * RenderTargetTexture::resolveMultisampledTexture(ID3D11Texture2D * source, unsigned int subresource)
{
	D3D11_TEXTURE2D_DESC textureDesc;
	source->GetDesc(&textureDesc);
	if (textureDesc.SampleDesc.Count > 1)
	{
		D3D11_TEXTURE2D_DESC resolveDesc;
		resolveDesc.Width = textureDesc.Width;
		resolveDesc.Height = textureDesc.Height;
		resolveDesc.MipLevels = 1;
		resolveDesc.ArraySize = 1;
		resolveDesc.Format = textureDesc.Format;
		resolveDesc.SampleDesc.Count = 1;
		resolveDesc.SampleDesc.Quality = 0;
		resolveDesc.Usage = textureDesc.Usage;
		resolveDesc.BindFlags = textureDesc.BindFlags;
		resolveDesc.CPUAccessFlags = 0;
		resolveDesc.MiscFlags = 0;
		ID3D11Texture2D *resolveTexture = nullptr;
		HRESULT result = D11Device->CreateTexture2D(&resolveDesc, nullptr, &resolveTexture);
		if (FAILED(result))
		{
			return nullptr;
		}
		DeviceContext->ResolveSubresource(resolveTexture, 0, source, subresource, textureDesc.Format);
		return resolveTexture;
	}
	else
	{
		source->AddRef();
		return source;
	}
}
