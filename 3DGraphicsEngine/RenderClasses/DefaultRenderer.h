#pragma once
#include <vector>
#include "Scene/Camera.h"
#include "Scene/Light.h"
#include "GameObject/GameObjectManager.h"
#include "Engine.h"
namespace Engine
{
	
	

	class DefaultRenderer
	{
		enum RenderingMode
		{
			Foward,
			Deferred,
		};

		enum PostProcessFlag
		{
			Default,
			AntiAliasing,
			ColorFilter,
			Blur,
			Bloom,
			DepthOfField,
			SSAO,
			Max_Count

		};
	private:
		bool PostProcessList[Max_Count];
		std::vector<GameObjPtr> DrawList;	//<--gameObject쪽에서 DrawbleObject가 추가,삭제될때마다 DrawList에 추가해주는게 제일 best 일듯
		Camera DefaultCam;		//나중에 Camera들의 vector로 바꾸기
		Light DefaultLight;
	public:
		DefaultRenderer();
		virtual ~DefaultRenderer();
		void Prepare();
		void Render();
		void SortDrawList();
	};
}

