#pragma once
#include <D3D11.h>

class Shader;
class ScreenQuad
{
public:
	enum E_TEXTURETYPE
	{
		E_DIFFUSE,
		E_SECOND,
		E_THIRD,
		E_MAX,
	};

	enum E_COLORFILTERTYPE
	{
		FILTER_COLOR,
		FILTER_INVERSE,
		FILTER_SEPHIA,
		FILTER_GRAYSCALE,
	};

	struct VERTEX
	{
		XMFLOAT3 Pos;
		XMFLOAT2 Tex;
	};
private:
	ID3D11Buffer*                       VertexBuffer;
	ID3D11Buffer*                       IndexBuffer;
	Shader*							Shader;
	int									 IndexLength;

	int Width;
	int Height;

	ID3D11ShaderResourceView*		m_arTexture[E_MAX];
	HRESULT loadTexture(ID3D11Device* pD3dDevice, const char *pSzModelName);
public:
	ScreenQuad();
	void SetShader(Shader* pShader);
	HRESULT Load(ID3D11Device* pD3dDevice, float screenWidth, float screenHeight, float Width, float Height);
	HRESULT SetTexture(ID3D11ShaderResourceView *pShaderResourceView, ID3D11ShaderResourceView *pDepthShaderresourceView);
	HRESULT SetTextuerArray(ID3D11ShaderResourceView** pShaderResourceView, int count);
	void RenderSet(ID3D11DeviceContext*& pDeviceContext);
	void Draw(ID3D11DeviceContext*& pDeviceContext);
	void Draw(ID3D11DeviceContext*& pDeviceContext, E_COLORFILTERTYPE filterType);
	void Draw(ID3D11DeviceContext*& pDeviceContext, float width, float height, float blurAmount);
	void Release();
};