#pragma once

#include "UnCopyable.h"
#include "DxMath/customMath.h"
#include "Engine.h"

//기본 DX Device 값들,  렌더러 기본 값을(백버퍼,뎁스스텐실)을 제공하는 클래스

namespace Engine
{

	class DXUtility :public UnCopyable
	{
	private:
		static DXUtility* Instance;

		D3D_DRIVER_TYPE                     DriverType;
		D3D_FEATURE_LEVEL                   FeatureLevel;
		ID3D11Device* D3dDevice;
		ID3D11DeviceContext* ImmediateContext;
		IDXGISwapChain* SwapChain;
		ID3D11RenderTargetView* RenderTargetView;
		ID3D11Texture2D* DepthStencil;
		ID3D11DepthStencilView* DepthStencilView;
		ID3D11ShaderResourceView* DepthShaderResourceView;

		ID3D11Texture2D* BackBuffer;


		int Width;
		int Height;
		D3D11_VIEWPORT* ViewPort;


		Math::Matrix Proj;
		Math::Matrix Ortho;

		DXUtility();
	public:
		static void CreateInstance();
		static void ReleaseInstance();
		static DXUtility* const GetInstance();	//주소를 변경하는것은 불법. 그러나 얻어서 Utility안의 값이 변경될 여지는 있다... 이건 코딩하면서 판단해보기

		HRESULT Init(HWND hWnd, int flags);
		void Release();

		void BeginScene();
		void EndScene();

		ID3D11Device* const GetDevice();	//Device로 얻어와서 Width를 셋팅하는게 맞나?device크기가 바뀌면
														//Texture의 크기를 다시만들어서 RenderTarget을 만들고 셋팅해야함
		ID3D11DeviceContext* const GetDeviceContext();	//?

		inline const Math::Matrix& GetProjectionMatrix() const
		{
			return Proj;
		}

		inline const Math::Matrix& GetOrthoMatrix() const
		{
			return Ortho;
		}

		const D3D_DRIVER_TYPE GetDriverType() const;	//단순한 Getter, 객체 내부의 값을 변경할 이유가 없음

		//void GetVideoCardInfo(char*, int&);

		//Test

		inline void GetDeviceWidthHeight(int& width, int& height) const
		{
			width = Width;
			height = Height;
		}
		void ResetRenderTarget();
		//void ResetViewPort();

		const ID3D11Texture2D* const GetBackBuffer() const; //BackBuffer는 단순히 얻어가기만 함. BackBuffer의 정보를 얻기만 하는 함수이기에 변경은 외부에서 할 수 없다
	};
}

