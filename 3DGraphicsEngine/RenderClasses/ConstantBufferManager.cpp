#include "EngineBaseHeader.h"
#include "ConstantBufferManager.h"
#include "Utility/Utility.h"
#include "DXUtility.h"

namespace Engine
{

	ConstantBufferManager::ConstantBufferManager()
	{
	}

	void ConstantBufferManager::CreateConstantBuffer(ConstBufferType type, UINT size)

	{		
		if (!ConstBuffer[(int)type])
		{
			Utility::CreateBufferFromData(&ConstBuffer[(int)type], D3D11_BIND_CONSTANT_BUFFER, nullptr,
				size, D3D11_USAGE_DYNAMIC, D3D11_CPU_ACCESS_WRITE);
		}
	}
	//D3D11_MAPPED_SUBRESOURCE* ConstantBufferManager::LockConstantBuffer(ConstBufferType type)
	//{		
	//	switch (type)
	//	{
	//	case ConstBufferType::perObject:				
	//		DXUtility::GetInstance()->GetDeviceContext()->Map(BuffPerObject, 0, D3D11_MAP_WRITE_DISCARD, 0, &MappedData);
	//		break;
	//	case ConstBufferType::perCamera:
	//		DXUtility::GetInstance()->GetDeviceContext()->Map(BuffPerCamera, 0, D3D11_MAP_WRITE_DISCARD, 0, &MappedData);
	//		break;
	//	case ConstBufferType::perMaterial:
	//		DXUtility::GetInstance()->GetDeviceContext()->Map(BuffPerMaterial[(int)ShaderType::Full], 0, D3D11_MAP_WRITE_DISCARD, 0, &MappedData);
	//		break;
	//	default:
	//		break;
	//	}
	//	
	//	return &MappedData;
	//}
	//void ConstantBufferManager::UnLockConstantBuffer(ConstBufferType type)
	//{
	//	switch (type)
	//	{
	//	case ConstBufferType::perObject:
	//		DXUtility::GetInstance()->GetDeviceContext()->Unmap(BuffPerObject, 0);
	//		break;
	//	case ConstBufferType::perCamera:
	//		DXUtility::GetInstance()->GetDeviceContext()->Unmap(BuffPerCamera, 0);
	//		break;
	//	case ConstBufferType::perMaterial:
	//		DXUtility::GetInstance()->GetDeviceContext()->Unmap(BuffPerMaterial[(int)ShaderType::Full], 0);
	//		break;
	//	default:
	//		break;
	//	}
	//}
	void ConstantBufferManager::UpdateConstantBuffer(ConstBufferType type, void* rowData, UINT size)
	{
		D3D11_MAPPED_SUBRESOURCE mappedData;

		//lock
		DXUtility::GetInstance()->GetDeviceContext()->Map(ConstBuffer[(int)type], 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedData);

		//Update
		memcpy(mappedData.pData, rowData, size);

		//unlock
		DXUtility::GetInstance()->GetDeviceContext()->Unmap(ConstBuffer[(int)type], 0);

	}
	ID3D11Buffer** ConstantBufferManager::GetBuffer(ConstBufferType type)
	{
		return &ConstBuffer[(int)type];
	}
	ConstantBufferManager& ConstantBufferManager::GetInstance()
	{
		static ConstantBufferManager instance;
		return instance;
	}

	void ConstantBufferManager::Release()
	{
		for(int i = 0 ; i < (int)ConstBufferType::bufferType_max; i++)
			Utility::Release(ConstBuffer[i]);
	}

}


