#include "UnCopyable.h"
#include "Engine.h"
namespace Engine
{
	class StateObjectManger :public UnCopyable
	{	
	public:
		enum class RasterizingType
		{
			Default,		//solid, cull_backface,
			Default_CullFront,
			Default_CullNone,
			Wire,	//cull_backface,
			Wire_CullFront,
			Wire_CullNone,
			Max_RasterzingType,
		};
	
		enum class SamplerType
		{
			Default,
			Anisotropic,
			Clamp,
			//wrap
			Max_SamplerType,
		};

		enum class BlendingType
		{
			Default,	//none
			AlphaBlend,
			Max_BlendingType,
		};

		enum class DepthStencilType
		{
			Default,
			NoWrite,
			Max_DepthStencilType
		};

	private:
		/*
		선택해야함 1.정해진 프리셋중 하나 2.매개변수 받아서.. 일단 정해진걸로 하자. 하다가 2번으로 넘어가기.
		2번은 좀더 고민해 보기.
		*/
						
		ID3D11SamplerState* SamplerState[D3D11_COMMONSHADER_SAMPLER_SLOT_COUNT];
		//std::vector<D3D11_RASTERIZER_DESC> RsDesces;
		//std::vector<ID3D11RasterizerState*> RsStates;		
		ID3D11RasterizerState* RasterizerStates[(int)RasterizingType::Max_RasterzingType];		
		ID3D11DepthStencilState* DepthStencilStates[(int)DepthStencilType::Max_DepthStencilType];
		ID3D11BlendState* BlendStates[(int)BlendingType::Max_BlendingType];



		//여러 셰이더객체.cpp에서 접근에서 사용해야 하기에 싱글톤으로!
		static StateObjectManger* Instance;

	private:
		StateObjectManger();		
	public:
		static StateObjectManger* const GetInstance();
		static void CreateInstance();
		static void ReleaseInstance();
		void Init();
		ID3D11SamplerState* const * const GetSamplerStates();
		ID3D11BlendState* const GetBlendState(StateObjectManger::BlendingType type);
		ID3D11RasterizerState* const GetRasterizerSate(StateObjectManger::RasterizingType type);
		ID3D11DepthStencilState* const GetDepthStencilState(StateObjectManger::DepthStencilType type);
		void Release();
		
		


	};
}