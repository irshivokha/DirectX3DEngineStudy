#include "EngineBaseHeader.h"
#include "DefaultRenderer.h"
#include "DXUtility.h"
#include "StateObjectManager.h"
#include "ConstantBufferManager.h"
#include "GameObject/GameObjectManager.h"
#include "GameObject/GameObject.h"

namespace Engine
{
	DefaultRenderer::DefaultRenderer()
	{
		for (int i = 0; i < PostProcessFlag::Max_Count; i++)
			PostProcessList[i] = false;

		DrawList.reserve(100);
		DrawList.assign(100, nullptr);
		//DrawList.clear();	//element는 없어져도 capacity는 남아있음
		DefaultCam.Init();
		//DefaultCam.Update();
		
	}

	DefaultRenderer::~DefaultRenderer()
	{
		ConstantBufferManager::GetInstance().Release();
	}

	void DefaultRenderer::Prepare()
	{
		ID3D11DeviceContext* context = DXUtility::GetInstance()->GetDeviceContext();
		if (context)
		{
			context->PSSetSamplers(0, 16, StateObjectManger::GetInstance()->GetSamplerStates());
			context->DSSetSamplers(0, 16, StateObjectManger::GetInstance()->GetSamplerStates());
			//object들은 단 하나의 const buffer를 공유해서 사용(World) 			
			ConstantBufferManager::GetInstance().CreateConstantBuffer(ConstantBufferManager::ConstBufferType::perObject, sizeof(ConstantBufferManager::ObjectConstBuffFormat));	
			ConstantBufferManager::GetInstance().CreateConstantBuffer(ConstantBufferManager::ConstBufferType::perCamera, sizeof(ConstantBufferManager::CameraConstBuffFormat));
			ConstantBufferManager::GetInstance().CreateConstantBuffer(ConstantBufferManager::ConstBufferType::perLight, sizeof(ConstantBufferManager::LightBuffFormat));


			DXUtility::GetInstance()->GetDeviceContext()->VSSetConstantBuffers(0, 1, ConstantBufferManager::GetInstance().GetBuffer(ConstantBufferManager::ConstBufferType::perObject));

			DXUtility::GetInstance()->GetDeviceContext()->HSSetConstantBuffers(0, 1, ConstantBufferManager::GetInstance().GetBuffer(ConstantBufferManager::ConstBufferType::perObject));
			DXUtility::GetInstance()->GetDeviceContext()->HSSetConstantBuffers(1, 1, ConstantBufferManager::GetInstance().GetBuffer(ConstantBufferManager::ConstBufferType::perCamera));

			DXUtility::GetInstance()->GetDeviceContext()->DSSetConstantBuffers(0, 1, ConstantBufferManager::GetInstance().GetBuffer(ConstantBufferManager::ConstBufferType::perObject));
			DXUtility::GetInstance()->GetDeviceContext()->DSSetConstantBuffers(1, 1, ConstantBufferManager::GetInstance().GetBuffer(ConstantBufferManager::ConstBufferType::perCamera));
			DXUtility::GetInstance()->GetDeviceContext()->DSSetConstantBuffers(2, 1, ConstantBufferManager::GetInstance().GetBuffer(ConstantBufferManager::ConstBufferType::perMaterial));
			
			//DXUtility::GetInstance()->GetDeviceContext()->


			//UINT psConstBuffcount = (int)ShaderType::Max_Type;
			context->PSSetConstantBuffers(0, 1, ConstantBufferManager::GetInstance().GetBuffer(ConstantBufferManager::ConstBufferType::perMaterial));
			DXUtility::GetInstance()->GetDeviceContext()->PSSetConstantBuffers(1, 1, ConstantBufferManager::GetInstance().GetBuffer(ConstantBufferManager::ConstBufferType::perLight));


		}
					
	}

	void DefaultRenderer::Render()
	{
		/*
		d3d11mappedptr ptr;	//local로 생성. sequential하게 복사되는 걸 확실하게 하기 위해.(순차적으로 cpy되는 게 확실하다고 여겨지면 구지 local로 안뺴도 될듯)
		ConstBffManager.Lock(&ptr);
		loop (index < objectList.count())			//object마다 가지고 있는 world값이 다르기에 각각 memcpy해주어야 함
		{
			memcpy(ptr, objectList[idx]->GetWorld());
			ObjectList[index]->Render();			
		}
		unLock();

		Material마다 가지고 있는 Shader에 따른 ShadingValue 값이 다름. 해당되는 값이 변경 됬을때에만 map 호출하여 바꾸기
		Object에 최초로 default Material의 상수버퍼 바인딩, 이후 UI를 통해서 다른 셰이더를 선택하는 순간에 해당하는 상수버퍼 바인딩.				
		View와 Proj역시 최초에 map으로 열어서 변경후 셋팅하고 변경될 시에만 map으로 열어서 변경
		*/

		//light update
		DefaultLight.Update();
		DefaultLight.UpdateConstBuff();

		SortDrawList();

		int camCnt = 1;

		DefaultCam.Update();
		
		for (int i = 0; i < camCnt; i++)
		{
			DefaultCam.UpdateConstBuff();

			for (size_t idx = 0; idx < DrawList.size(); idx++)
			{
				if (DrawList[idx])
				{					
					DrawList[idx]->Draw();
				}

			}


			/*
			//view, proj업데이트 매프레임 할것인가?
			1. 하나의 viewMatrix를 셰이더 안에서 사용한다면 돌아가면서 업데이트 해야함
			2. 여러개의 viewMatrix를 셰이더 안에서 사용한다면 각가 업데이트 됬을 시점에만 map 시켜도 됨
			일단 1번으로 테스트하고 잘되면 2번으로도 테스트해서 성능비교 해보기
			3. proj matrix 누구에게 종속적으로 할것인가? 윈도우 사이즈와 camera의 property 둘 다에 종속적이어 버림..
			윈도우 사이즈가 변하거나 camera의 near,Far가 변하면 projMatrix 그때 변형시켜버리기
			*/
		}

	}

	void DefaultRenderer::SortDrawList()
	{
 		std::map<UID, GameObjPtr>::iterator iter = GameObjectManager::GetInstance().GetBegin();
		UINT idx = 0;
		while(iter!=GameObjectManager::GetInstance().GetEnd())
		{
			//drawble인지 검사하고 집어넣기(후에 코드 추가)
			if (iter->second->IsDrawable())
			{
				DrawList[idx] = iter->second;

				idx++;				
			}
			iter++;

		}
	}
	
}


