#include "EngineBaseHeader.h"
#include "GameObjectManager.h"
#include "GameObject.h"
#include "DefineValues.h"

namespace Engine
{

	//GameObjectManager GameObjectManager::Instance;

	GameObjectManager::GameObjectManager()
	{
		ObjsMap.clear();
	}

	GameObjPtr GameObjectManager::GetGameObject(UID uId)
	{
		return ObjsMap.find(uId)->second;
	}

	GameObjPtr  GameObjectManager::CreateGameObject()
	{
		UID id = Randomize(0, 50000);
		
		std::map<UID, GameObjPtr>::iterator iter = ObjsMap.find(id);

		while(iter != ObjsMap.end())		
		{
			id = Randomize(0, 50000);
			iter = ObjsMap.find(id);
		}


		GameObjPtr obj(new GameObject());
		std::pair<UID, GameObjPtr> objPair;
		objPair.first = id;
		objPair.second = obj;
		ObjsMap.insert(objPair);
		
		return obj;
	}

	void GameObjectManager::DeleteGameObject(UID uId)
	{
		std::map<UID, GameObjPtr>::iterator itor;		
		itor = ObjsMap.find(uId);
		itor->second.reset();	//포인터의 참조 해제
		ObjsMap.erase(uId);
	}

	unsigned int GameObjectManager::GetGameObjectCount()
	{
		return ObjsMap.size();
	}

	//void GameObjectManager::ReleaseInstantce()
	//{
	//	Instance.Release();
	//}

	void GameObjectManager::Release()
	{
		//ObjsMap.clear();
	}

	std::map<UID, GameObjPtr>::iterator GameObjectManager::GetBegin()
	{
		return ObjsMap.begin();
	}

	std::map<UID, GameObjPtr>::iterator GameObjectManager::GetEnd()
	{
		return ObjsMap.end();
	}
 
}

