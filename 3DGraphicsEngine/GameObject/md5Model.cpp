#include "stdafx.h"

C_MD5MODEL::C_MD5MODEL()
{
}

void C_MD5MODEL::SetShader(Shader * pShader)
{
	Shader = pShader;
}

HRESULT C_MD5MODEL::Load(ID3D11Device * pD3dDevice, std::wstring  pSzModelName)
{
	HRESULT hr = S_OK;
	std::vector<std::wstring> textureNameArray;
	textureNameArray.reserve(10);
	//m_textureNameArray.reserve(10);
	textureNameArray.push_back(L"resource/boy/face.jpg");
	textureNameArray.push_back(L"resource/boy/arms.jpg");
	textureNameArray.push_back(L"resource/boy/shirt.jpg");
	textureNameArray.push_back(L"resource/boy/pants.jpg");
	textureNameArray.push_back(L"resource/boy/shoes.jpg");
	m_cMd5Loader.LoadMD5Model(pSzModelName, m_meshSRV, textureNameArray, pD3dDevice);
	m_cModel3D = m_cMd5Loader.GetModel3D();
	m_cMd5Loader.LoadMD5Anim(L"resource/boy/boy.md5anim", m_cModel3D);
	return hr;
}


void C_MD5MODEL::RenderSet(ID3D11DeviceContext *& pDeviceContext)
{
	XMMATRIX matWorld = XMMatrixIdentity();
	XMMATRIX Scale = XMMatrixScaling(0.02f, 0.02f, 0.02f);            // The model is a bit too large for our scene, so make it smaller
	XMMATRIX Translation = XMMatrixTranslation(0.0f, 2.0f, 0.0f);

	matWorld = Scale * Translation;

	UINT stride = sizeof(C_MD5LOADER::S_VERTEX);
	UINT offset = 0;
	for (int i = 0; i < m_cModel3D.numSubsets; ++i)
	{
		pDeviceContext->IASetVertexBuffers(0, 1, &m_cModel3D.subsets[i].pVertexBuffer, &stride, &offset);
		// Set index buffer
		pDeviceContext->IASetIndexBuffer(m_cModel3D.subsets[i].pIndexBuffer, DXGI_FORMAT_R32_UINT, 0);
		pDeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		Shader->SetShaderResource(0, 1, &m_meshSRV[m_cModel3D.subsets[i].texArrayIndex]);

		Shader->UpdateSubResource(matWorld);
		Shader->Render();
		pDeviceContext->DrawIndexed(m_cModel3D.subsets[i].indices.size(), 0, 0);

	}

}

void C_MD5MODEL::UpdateMD5Model(ID3D11DeviceContext *& pDeviceContext, float deltaTime, int animation)
{
	m_cModel3D.animations[animation].currAnimTime += deltaTime;			// Update the current animation time

	if (m_cModel3D.animations[animation].currAnimTime > m_cModel3D.animations[animation].totalAnimTime)
		m_cModel3D.animations[animation].currAnimTime = 0.0f;

	// Which frame are we on
	float currentFrame = m_cModel3D.animations[animation].currAnimTime * m_cModel3D.animations[animation].frameRate;
	int frame0 = floorf(currentFrame);
	int frame1 = frame0 + 1;

	// Make sure we don't go over the number of frames	
	if (frame0 == m_cModel3D.animations[animation].numFrames - 1)
		frame1 = 0;

	float interpolation = currentFrame - frame0;	// Get the remainder (in time) between frame0 and frame1 to use as interpolation factor

	std::vector<C_MD5LOADER::Joint> interpolatedSkeleton;		// Create a frame skeleton to store the interpolated skeletons in

													// Compute the interpolated skeleton
	for (int i = 0; i < m_cModel3D.animations[animation].numJoints; i++)
	{
		C_MD5LOADER::Joint tempJoint;
		C_MD5LOADER::Joint joint0 = m_cModel3D.animations[animation].frameSkeleton[frame0][i];		// Get the i'th joint of frame0's skeleton
		C_MD5LOADER::Joint joint1 = m_cModel3D.animations[animation].frameSkeleton[frame1][i];		// Get the i'th joint of frame1's skeleton

		tempJoint.parentID = joint0.parentID;											// Set the tempJoints parent id

																						// Turn the two quaternions into XMVECTORs for easy computations
		XMVECTOR joint0Orient = XMVectorSet(joint0.orientation.x, joint0.orientation.y, joint0.orientation.z, joint0.orientation.w);
		XMVECTOR joint1Orient = XMVectorSet(joint1.orientation.x, joint1.orientation.y, joint1.orientation.z, joint1.orientation.w);

		// Interpolate positions
		tempJoint.pos.x = joint0.pos.x + (interpolation * (joint1.pos.x - joint0.pos.x));
		tempJoint.pos.y = joint0.pos.y + (interpolation * (joint1.pos.y - joint0.pos.y));
		tempJoint.pos.z = joint0.pos.z + (interpolation * (joint1.pos.z - joint0.pos.z));

		// Interpolate orientations using spherical interpolation (Slerp)
		XMStoreFloat4(&tempJoint.orientation, XMQuaternionSlerp(joint0Orient, joint1Orient, interpolation));

		interpolatedSkeleton.push_back(tempJoint);		// Push the joint back into our interpolated skeleton
	}

	for (int k = 0; k < m_cModel3D.numSubsets; k++)
	{
		for (int i = 0; i < m_cModel3D.subsets[k].vertices.size(); ++i)
		{
			C_MD5LOADER::S_VERTEX tempVert = m_cModel3D.subsets[k].vertices[i];
			tempVert.Pos = XMFLOAT3(0, 0, 0);	// Make sure the vertex's pos is cleared first
			tempVert.Normal = XMFLOAT3(0, 0, 0);	// Clear vertices normal

													// Sum up the joints and weights information to get vertex's position and normal
			for (int j = 0; j < tempVert.WeightCount; ++j)
			{
				C_MD5LOADER::Weight tempWeight = m_cModel3D.subsets[k].weights[tempVert.StartWeight + j];
				C_MD5LOADER::Joint tempJoint = interpolatedSkeleton[tempWeight.jointID];

				// Convert joint orientation and weight pos to vectors for easier computation
				XMVECTOR tempJointOrientation = XMVectorSet(tempJoint.orientation.x, tempJoint.orientation.y, tempJoint.orientation.z, tempJoint.orientation.w);
				XMVECTOR tempWeightPos = XMVectorSet(tempWeight.pos.x, tempWeight.pos.y, tempWeight.pos.z, 0.0f);

				// We will need to use the conjugate of the joint orientation quaternion
				XMVECTOR tempJointOrientationConjugate = XMQuaternionInverse(tempJointOrientation);

				// Calculate vertex position (in joint space, eg. rotate the point around (0,0,0)) for this weight using the joint orientation quaternion and its conjugate
				// We can rotate a point using a quaternion with the equation "rotatedPoint = quaternion * point * quaternionConjugate"
				XMFLOAT3 rotatedPoint;
				XMStoreFloat3(&rotatedPoint, XMQuaternionMultiply(XMQuaternionMultiply(tempJointOrientation, tempWeightPos), tempJointOrientationConjugate));

				// Now move the verices position from joint space (0,0,0) to the joints position in world space, taking the weights bias into account
				tempVert.Pos.x += (tempJoint.pos.x + rotatedPoint.x) * tempWeight.bias;
				tempVert.Pos.y += (tempJoint.pos.y + rotatedPoint.y) * tempWeight.bias;
				tempVert.Pos.z += (tempJoint.pos.z + rotatedPoint.z) * tempWeight.bias;

				// Compute the normals for this frames skeleton using the weight normals from before
				// We can comput the normals the same way we compute the vertices position, only we don't have to translate them (just rotate)
				XMVECTOR tempWeightNormal = XMVectorSet(tempWeight.normal.x, tempWeight.normal.y, tempWeight.normal.z, 0.0f);

				// Rotate the normal
				XMStoreFloat3(&rotatedPoint, XMQuaternionMultiply(XMQuaternionMultiply(tempJointOrientation, tempWeightNormal), tempJointOrientationConjugate));

				// Add to vertices normal and ake weight bias into account
				tempVert.Normal.x -= rotatedPoint.x * tempWeight.bias;
				tempVert.Normal.y -= rotatedPoint.y * tempWeight.bias;
				tempVert.Normal.z -= rotatedPoint.z * tempWeight.bias;
			}

			m_cModel3D.subsets[k].positions[i] = tempVert.Pos;				// Store the vertices position in the position vector instead of straight into the vertex vector
			m_cModel3D.subsets[k].vertices[i].Normal = tempVert.Normal;		// Store the vertices normal
			XMStoreFloat3(&m_cModel3D.subsets[k].vertices[i].Normal, XMVector3Normalize(XMLoadFloat3(&m_cModel3D.subsets[k].vertices[i].Normal)));
		}

		// Put the positions into the vertices for this subset
		for (int i = 0; i < m_cModel3D.subsets[k].vertices.size(); i++)
		{
			m_cModel3D.subsets[k].vertices[i].Pos = m_cModel3D.subsets[k].positions[i];
		}

		// Update the subsets vertex buffer
		// First lock the buffer
		D3D11_MAPPED_SUBRESOURCE mappedVertBuff;
		pDeviceContext->Map(m_cModel3D.subsets[k].pVertexBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedVertBuff);
		// Copy the data into the vertex buffer.
		memcpy(mappedVertBuff.pData, &m_cModel3D.subsets[k].vertices[0], (sizeof(C_MD5LOADER::S_VERTEX) * m_cModel3D.subsets[k].vertices.size()));

		pDeviceContext->Unmap(m_cModel3D.subsets[k].pVertexBuffer, 0);

		// The line below is another way to update a buffer. You will use this when you want to update a buffer less
		// than once per frame, since the GPU reads will be faster (the buffer was created as a DEFAULT buffer instead
		// of a DYNAMIC buffer), and the CPU writes will be slower. You can try both methods to find out which one is faster
		// for you. if you want to use the line below, you will have to create the buffer with D3D11_USAGE_DEFAULT instead
		// of D3D11_USAGE_DYNAMIC
		//d3d11DevCon->UpdateSubresource( MD5Model.subsets[k].vertBuff, 0, nullptr, &MD5Model.subsets[k].vertices[0], 0, 0 );
	}
}


void C_MD5MODEL::Draw(ID3D11DeviceContext *& pDeviceContext, const XMMATRIX & matWorld)
{
	for (int i = 0; i <m_cModel3D.numSubsets; ++i)
	{

	}
}

void C_MD5MODEL::Release()
{

}
