#include "EngineBaseHeader.h"

#include "GameObject.h"
#include "RenderClasses/DXUtility.h"
#include "Resource/Material/Material.h"
#include "Resource/Model/Mesh.h"
#include "RenderClasses/ConstantBufferManager.h"
#include "Utility/Utility.h"
#include "Resource/Model/SkinnedPrimitiveMesh.h"
#include "Resource/Model/Terrain.h"

namespace Engine
{
	GameObject::GameObject() :
		CurrentMaterial(nullptr),
		ObjectMesh(nullptr),
		Name("GameObject"),
		ScriptComponent(nullptr),
		Drawable(false),
		Parent(nullptr),
		Trans(this)
	{		
		Children.clear();		
	}

	GameObject::GameObject(const GameObject& src)
		:CurrentMaterial(nullptr),
		ObjectMesh(nullptr),
		ScriptComponent(nullptr),
		Name(src.Name),
		Drawable(src.Drawable),
		Trans()
	{
		
		CurrentMaterial = new Material(*src.CurrentMaterial);
		ObjectMesh = new Mesh(*src.ObjectMesh);					
		Trans.SetOwer(this);
	}

	GameObject::~GameObject()
	{
	}

	GameObject& GameObject::operator=(const GameObject& rhs)
	{
		if (&rhs == this)
			return *this;

		*CurrentMaterial = *rhs.CurrentMaterial;
		*ObjectMesh = *rhs.ObjectMesh;
		Name = rhs.Name;

		return *this;

	}

	void GameObject::ImportMesh(const std::string& str, MeshModelType type)
	{
		if (!ObjectMesh)
		{
			if (type == MeshModelType::SkinnedPrimitive)
			{
				ObjectMesh = new SkinnedPrimitiveMesh();
			}
			else if (type == MeshModelType::Terrain)
				ObjectMesh = new Terrain();
			else
				ObjectMesh = new Mesh();

			GameObjPtr obj(this);
			ObjectMesh->SetOwner(obj);
			ObjectMesh->ImportMesh(str);
			Drawable = true;

			/*
			어떠한 오브젝트에 대해서 import라는 행위는 최초에 한번만 일어난다.
			이후에는 복사의 과정만 있을뿐
			따라서 오브젝트를 복사하면 머티리얼은 공유하게 된다.
			임포트라는 행위 자체는 새로운 머티리얼을 만드는 것.
			복사에 대해서 공유됨
			다른 머티리얼을 쓰던 오브젝트들이 같은 머티리얼을 쓰는 경우는 새로 만들어서 공유하는 수밖에 없음.
			인스턴스 머티리얼을 사용하는 경우는 인스턴스 머티리얼을 생성한 후 적용뒤 결정하는 것이다.
			*/

			//디폴트 머티리얼 생성후 머티리얼 값 지정
			CurrentMaterial = new Material();
			CurrentMaterial->SetShaderType(Engine::ShaderType::Full);

			if (type == MeshModelType::Terrain)
				CurrentMaterial->SetShaderType(Engine::ShaderType::Terrain);

			bool hasMaterial = true;
			if (hasMaterial)
			{
				/*
				Material에서 지정해준 값이 있다면
				CurrentMaterial->SetTexture();
				CurrentMaterial->SetProperty();
				*/
				
				
			}

		}
				
	}

	void GameObject::AddMesh(const Mesh& mesh)
	{		
		Meshes.push_back(mesh);
	}


	void GameObject::LinkResourceToPipeline()
	{
		if(ObjectMesh)
			ObjectMesh->LinkResourceToPipeline();
		if(CurrentMaterial)
			CurrentMaterial->LinkResourceToPipeline();
		/*
		PipleLine단계에 따라 셋팅후 렌더링
		0. IA단계(고정단계).
		1. proramable Shader단계 . 각 단계마다 필요한 상수버퍼를 업데이트 하고 파이프라인에 연결.
		2. OM단계(고정단계?)

		*******상수버퍼 업데이트에 관하여*****************
		Object마다 상수버퍼에 담고 있는 값이 다르므로 결국 파이프라인 마다 상수버퍼를 셋팅해주어야 한다.
		Object마다 가지고 있는 VertexBuffer값이 다르므로... VertexShader를 매번 파이프라인에 연결해야 함.
		그러나 상수버퍼의 내용과 vertexShader의 '내용'은 매번 갱신할 필요가 없다. 필요시에만 갱신.
		*/


	}

	void GameObject::UpdateConstBuff()
	{

		ConstantBufferManager::ObjectConstBuffFormat buffFormat;
		Math::Matrix world = GetTransform().GetWorldMat();
		buffFormat.World = Math::Matrix::MatrixTranspose(world);		
		buffFormat.InverseWorld = Math::Matrix::MatrixTranspose(world.Inverse());
		if (Drawable)
		{
			if (ObjectMesh)
				ObjectMesh->UpdateConstBuffer(&buffFormat);
			if (CurrentMaterial)
				CurrentMaterial->UpdateConstantBuffer();
		}
		
		ConstantBufferManager::GetInstance().UpdateConstantBuffer(ConstantBufferManager::ConstBufferType::perObject,&buffFormat,sizeof(ConstantBufferManager::ObjectConstBuffFormat));
	}

	void GameObject::Draw()
	{

		UpdateConstBuff();

		LinkResourceToPipeline();

		//상수버퍼 업데이트 . 로직 확인된 뒤에 밑으로 집어넣기
			
		//Draw방식에 따라 호출되는 함수가 다름

		//CurrentMaterial->Draw();
		//ObjectMesh->Draw();
		//draw함수는 다시 고민해보기					
		if(ObjectMesh)
			ObjectMesh->Draw();
	}

	void GameObject::Release()
	{
		if (ObjectMesh)
		{
			delete ObjectMesh;
			ObjectMesh = nullptr;
		}		
	}

	bool GameObject::IsDrawable()
	{
		return Drawable;
	}

	Mesh* GameObject::GetMesh()
	{
		return ObjectMesh;
	}

	void GameObject::SetChild(Engine::GameObjPtr child, Engine::GameObjPtr parent)
	{
		child->Parent = parent;		
		child->Trans.UpdateTransform();
		Children.push_back(child);
	}

	Engine::GameObjPtr GameObject::GetChild(UINT idx)
	{
		std::list<Engine::GameObjPtr>::iterator itor = Children.begin();
		for (size_t i = 0; i < Children.size(); i++)
		{
			if (idx == i)
				return *itor;
			itor++;
		}

		return nullptr;
	}

	void GameObject::DeleteChild(UINT idx)
	{
		std::list<Engine::GameObjPtr>::iterator itor = Children.begin();
		for (UINT i = 0; i < Children.size(); i++)
		{
			if (idx == i)
			{
				itor->reset();
				Children.erase(itor);

				return;
			}
			itor++;
		}
		
	}

	std::list<GameObjPtr>::iterator GameObject::GetChildrenBegin()
	{
		return Children.begin();
	}

	std::list<GameObjPtr>::iterator GameObject::GetChildrenEnd()
	{
		return Children.end();
	}

}

