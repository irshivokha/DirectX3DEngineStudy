#include "EngineBaseHeader.h"
#include "Transform.h"
#include "GameObject.h"
namespace Engine
{
	using namespace Math;

	Transform::Transform()
	{

	}
	Transform::Transform(GameObject* ownerPtr)
		: Position(), Rotation(), Scale(1, 1, 1), World(), Local(), Owner(ownerPtr)
	{
		
	}

	Transform::Transform(const Transform& src)
	{
		Position = src.Position; 
		Rotation = src.Rotation;
		Scale = src.Scale;
		World = src.World;
	}
	void Transform::UpdateTransform()
	{
		if (!Owner)
			return;
			
		Matrix scaleMat = Matrix::MatrixScaling(Scale);
		RotMat = Matrix::MatrixRotationPitchYawRollDegree(Rotation.x, Rotation.y, Rotation.z);
		Matrix transMat = Matrix::MatrixTranslation(Position);
		Local = scaleMat * RotMat * (transMat);

		if (Owner->GetParent())
		{
			Transform& parentTrans = Owner->GetParent()->GetTransform();						
			World = Local * parentTrans.World;
		}
		else		
			World = Local;					
		UpdateChildTransform();
		
			
	}
	void Transform::UpdateChildTransform()
	{
		if (!Owner)
			return;

		std::list<GameObjPtr>::iterator iter = Owner->GetChildrenBegin();
		while (iter != Owner->GetChildrenEnd())
		{
			(*iter)->GetTransform().UpdateTransform();
			iter++;
		}

	}
	const Math::Matrix& Transform::GetWorldMat() const
	{
		return World;
	}
	const Math::Vector3& Transform::GetPosition() const
	{
		return Position;
	}
	const Math::Vector3& Transform::GetScale() const
	{
		return Scale;
	}
	const Math::Vector3& Transform::GetRotation() const
	{
		return Rotation;
	}
	void Transform::SetPosition(const Math::Vector3& pos)
	{
		Position = pos;
		UpdateTransform();		
	}
	void Transform::SetPosition(float x, float y, float z)
	{
		Position.x = x;
		Position.y = y;
		Position.z = z;
		UpdateTransform();
	}
	void Transform::SetLocalPosition(float x, float y, float z)
	{
		if (Owner)
		{
			if(Owner->GetParent())
			{
				Transform parentTrans = Owner->GetParent()->GetTransform();				
				Local.m[3][0] = x;
				Local.m[3][1] = y;
				Local.m[3][2] = z;				
				World = Local * parentTrans.World;
				Position = Vector3(x, y, z);
			}
			else
			{	//parent가 world(root)가 된다. world의 값만 반영해주면됨.
				//Position = Vector3(x, y, z);				

				Matrix localTransMat = Matrix::MatrixTranslation(Vector3(x, y, z));
				Matrix scaleMat = Matrix::MatrixScaling(Scale);
				Local = localTransMat * (RotMat * scaleMat);
				World = Local;
				Position = Vector3(World.m[3][0], World.m[3][1], World.m[3][2]);	//parent(World의 원점(0,0,0)) 기준 상대적인 위치값
			}

			UpdateChildTransform();

		}
	}
	void Transform::SetRotation(const Math::Vector3& rot)
	{
		Rotation = rot;
		UpdateTransform();
	}
	void Transform::SetRotation(float x, float y, float z)
	{
		Rotation.x = x;
		Rotation.y = y;
		Rotation.z = z;
		UpdateTransform();
	}
	void Transform::SetLocalRotation(float x, float y, float z)
	{
		if (Owner)
		{
			if(Owner->GetParent())
			{
				Transform parentTrans = Owner->GetParent()->GetTransform();
				Matrix rot = Matrix::MatrixRotationPitchYawRollDegree(x, y, z);
				Matrix scale = Matrix::MatrixScaling(Scale);
				Matrix trans = Matrix::MatrixTranslation(Position);
				Local = scale * rot * trans;
				World = Local * parentTrans.GetWorldMat();
				Rotation = Vector3(x, y, z);

			}
			else
			{
				Matrix localRot = Matrix::MatrixRotationPitchYawRollDegree(x, y, z);

				Matrix rot = Matrix::MatrixRotationPitchYawRollDegree(Rotation.x, Rotation.y, Rotation.z);
				Matrix scale = Matrix::MatrixScaling(Scale);
				Matrix trans = Matrix::MatrixTranslation(Position);

				Local = localRot * (scale * RotMat * trans);	//or localRot * (worldScale * worldRot * worldTrans)				
				World = Local;
				Matrix::MatrixDecompose(nullptr, &Rotation, nullptr, World);				
			}

			UpdateChildTransform();

		}
	}

	void Transform::SetScale(const Math::Vector3& scale)
	{
		Scale = scale;
		UpdateTransform();
	}
	void Transform::SetScale(float x, float y, float z)
	{
		Scale.x = x;
		Scale.y = y;
		Scale.z = z;
		UpdateTransform();
	}

	void Transform::RotationX(float angle)
	{
		Rotation.x = angle * (180.0f / DirectX::XM_PI);
		XMMATRIX scaleMat = DirectX::XMMatrixScaling(Scale.x, Scale.y, Scale.z);
		XMMATRIX rotMat = DirectX::XMMatrixRotationX(angle);
		XMMATRIX transMat = DirectX::XMMatrixTranslation(Position.x, Position.y, Position.z);
		DirectX::XMStoreFloat4x4(&Local, scaleMat * rotMat * transMat);
		if (Owner->GetParent())
		{
			Transform& parentTrans = Owner->GetParent()->GetTransform();
			World = Local * parentTrans.World;
		}
		else
			World = Local;
		UpdateChildTransform();

	}
	void Transform::RotationY(float angle)
	{
		Rotation.y = angle * (180.0f / DirectX::XM_PI);
		XMMATRIX scaleMat = DirectX::XMMatrixScaling(Scale.x, Scale.y, Scale.z);
		XMMATRIX rotMat = DirectX::XMMatrixRotationY(angle);
		XMMATRIX transMat = DirectX::XMMatrixTranslation(Position.x, Position.y, Position.z);
		XMMATRIX test = scaleMat* rotMat;
		DirectX::XMStoreFloat4x4(&Local, scaleMat * rotMat * transMat);
		if (Owner->GetParent())
		{
			Transform& parentTrans = Owner->GetParent()->GetTransform();
			World = Local * parentTrans.World;
		}
		else
			World = Local;
		UpdateChildTransform();
	}
	void Transform::RotationZ(float angle)
	{
		Rotation.z = angle * (180.0f / DirectX::XM_PI);
		XMMATRIX scaleMat = DirectX::XMMatrixScaling(Scale.x, Scale.y, Scale.z);
		XMMATRIX rotMat = DirectX::XMMatrixRotationZ(angle);
		XMMATRIX transMat = DirectX::XMMatrixTranslation(Position.x, Position.y, Position.z);
		DirectX::XMStoreFloat4x4(&Local, scaleMat * rotMat * transMat);
		if (Owner->GetParent())
		{
			Transform& parentTrans = Owner->GetParent()->GetTransform();
			World = Local * parentTrans.World;
		}
		else
			World = Local;
		UpdateChildTransform();
	}
	void Transform::LocalRotationX(float angle)
	{
		if (!Owner)	//root
		{

		}
		XMMATRIX localRotMat = DirectX::XMMatrixRotationX(angle);
		DirectX::XMStoreFloat4x4(&Local, localRotMat);		
		World = Local * World;

	}
	void Transform::LocalRotationY(float angle)
	{
		if (Owner)
		{
			if (Owner->GetParent())
			{
				Transform& parentTrans = Owner->GetParent()->GetTransform();
				XMMATRIX localScaleMat = DirectX::XMMatrixScaling(Scale.x, Scale.y, Scale.z);
				XMMATRIX localRotMat = DirectX::XMMatrixRotationY(angle);
				XMMATRIX localTransMat = DirectX::XMMatrixTranslation(Position.x, Position.y, Position.z);
				DirectX::XMStoreFloat4x4(&Local, (localScaleMat * localRotMat * localTransMat));
				World = Local * parentTrans.GetWorldMat();
			}
			else
			{
				Rotation.y = angle * (180.0f / DirectX::XM_PI);
				XMMATRIX localRotMat = DirectX::XMMatrixRotationY(angle);

				XMMATRIX worldScaleMat = DirectX::XMMatrixScaling(Scale.x, Scale.y, Scale.z);
				float xrad = Rotation.x * (DirectX::XM_PI / 180.0f);
				float zrad = Rotation.z * (DirectX::XM_PI / 180.0f);
				XMMATRIX worldRotMat = DirectX::XMMatrixRotationRollPitchYaw(xrad, 0, zrad);
				XMMATRIX worldTransmat = DirectX::XMMatrixTranslation(Position.x, Position.y, Position.z);
				DirectX::XMStoreFloat4x4(&Local, localRotMat * (worldScaleMat * worldRotMat * worldTransmat));
				World = Local;
			}
		}


	}
	void Transform::LocalRotationZ(float angle)
	{
		XMMATRIX localRotMat = DirectX::XMMatrixRotationZ(angle);
		DirectX::XMStoreFloat4x4(&Local, localRotMat);
		World = Local * World;
	}
	void Transform::SetOwer(GameObject* pObj)
	{
		Owner = pObj;
	}
}


