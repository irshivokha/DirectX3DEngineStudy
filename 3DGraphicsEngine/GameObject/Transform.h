#pragma once
#include "DxMath/customMath.h"
#include "Engine.h"

namespace Engine
{
	class GameObject;

	class Transform
	{		
	private:
		Math::Vector3 Position;
		Math::Vector3 Rotation;
		Math::Vector3 Scale;
		Math::Matrix World;
		Math::Matrix Local;
		Math::Matrix RotMat;
		GameObject* Owner;

	public:
		explicit Transform();
		Transform(GameObject* ownerPtr);
		Transform(const Transform& src);
		void UpdateTransform();
		void UpdateChildTransform();

		const Math::Matrix& GetWorldMat() const;
		const Math::Vector3& GetPosition() const;
		const Math::Vector3& GetScale() const;
		const Math::Vector3& GetRotation() const;

		void SetPosition(const Math::Vector3& pos);
		void SetPosition(float x, float y, float z);
		void SetLocalPosition(float x, float y, float z);
		void Translate(float x, float y, float z);

		void SetRotation(const Math::Vector3& rot);
		void SetRotation(float x, float y, float z);
		void SetLocalRotation(float x, float y, float z);
		void Rotate();
		  
		void SetScale(const Math::Vector3& scale);
		void SetScale(float x, float y, float z);	

		////��������
		void RotationX(float angle);
		void RotationY(float angle);
		void RotationZ(float angle);
		void LocalRotationX(float angle);
		void LocalRotationY(float angle);
		void LocalRotationZ(float angle);

		void SetOwer(GameObject* pObj);
	};
}

