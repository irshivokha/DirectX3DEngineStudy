#pragma once
#include "UnCopyable.h"
#include <map>
#include <memory>
#include "Engine.h"
#include "DefineValues.h"

typedef unsigned long UID;


//후에 define.h로 옮기기
//EnginebaseHeader에 define도 추가하기!?
//Object를 생성/삭제 역할을 하는 클래스. Drawing은 하지 않는다. 
/*Singleton이 아니지만 피치 못하게 SingleTon으로 해야할 경우 SingleTon으로 하자.
여러군데에서 참조하는 공유형 자료의 성격이라고 봐야하는지를 아직 모르겠음*/

namespace Engine
{
	class GameObject;
	//class C_MD5MODEL;
	//class UIModel;
	//class Text;


	class GameObjectManager:public UnCopyable
	{
	private:
		std::map<UID, GameObjPtr> ObjsMap;		
		//static GameObjectManager Instance;
		explicit GameObjectManager();
	public:						
		GameObjPtr GetGameObject(UID uId);
		GameObjPtr CreateGameObject();
		void DeleteGameObject(UID uId);
		unsigned int GetGameObjectCount();
		static GameObjectManager& GetInstance()
		{
			static GameObjectManager objMgr;
			return objMgr;
		}
		//static void ReleaseInstance();
		void Release();
		std::map<UID, GameObjPtr>::iterator GetBegin();
		std::map<UID, GameObjPtr>::iterator GetEnd();

	};
}

