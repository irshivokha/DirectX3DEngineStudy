#pragma once
//Wrapper가 없으므로 Script에서 직접 참조해서 쓴다. 엔진이 형태가 갖춰져야 그다음 wrapper를 만들지..
//직접참조해서 쓰는 형태로 하고 그다음 스크립트 언어를 알아보자
#include "Engine.h"

namespace Engine
{
	namespace Script
	{	
		class IScript
		{
		public:
			IScript();
			virtual void Start();
			virtual void Update();
		};
	}
}

