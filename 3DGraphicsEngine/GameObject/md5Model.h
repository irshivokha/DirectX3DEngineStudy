#pragma once
#include <D3D11.h>
#include <vector>
#include "md5Loader.h"

class C_MD5MODEL
{
private:
	std::vector<ID3D11ShaderResourceView*> m_meshSRV;
	std::vector<std::wstring> m_textureNameArray;
	C_MD5LOADER::Model3D m_cModel3D;
	ID3D11Buffer* VertexBuffer;
	ID3D11Buffer* IndexBuffer;
	C_MD5LOADER m_cMd5Loader;
	Shader*							Shader;
public:
	C_MD5MODEL();
	void SetShader(Shader* pShader);
	HRESULT Load(ID3D11Device* pD3dDevice, std::wstring pSzModelName);
	void RenderSet(ID3D11DeviceContext*& pDeviceContext);
	void UpdateMD5Model(ID3D11DeviceContext *& pDeviceContext, float deltaTime, int animation);
	void Draw(ID3D11DeviceContext*& pDeviceContext, const XMMATRIX &matWorld);
	void Release();


};