#pragma once
#include "Transform.h"
#include <iostream>
#include "Engine.h"
#include "DefineValues.h"
#include "Animator.h"	

namespace Engine
{
	class ShaderObject;
	class Texture;
	class Material;
	class Mesh;

	namespace Script
	{
		class IScript;
	}
	

	class GameObject	//GameObject가 어떤 클래스의 멤버, 구현세부사항으로 들어가면 구현,정의 나누기. 그렇지 않으면 나누지 않기
	{									//objectManager는 pointer로 들고있으므로 Impl 구현 필요 x
	private:		
		//Mesh Render는 vertex,inderBuffer를 가지고 있음.상수버퍼도 필요하다면 가지고 있음.
		//유니티는 MeshRender를 따로 뺴서 노출시키지만 해당 기능에대해 알못이므로 일단 노출x
		//TransformGroup trans
		//Mesh_Renderer
		//importer는 다른곳에 있음. UI로 import 함.
		/*
		mesh import 시나리오
		0. import시 씬에 종속적이지 않은 가상의 gameObject 만듦. loader에 경로 넣어주어 rawVertexBuffer,indexBuffer get
		1. 이를 gameObject에 넘겨주어 v,ibuffer를 생성시켜주고 메시 생성, pipelineLink,draw;(애셋 윈도우에)
		2.마우스로 asset 윈도우에서 drag
		3. 씬 뷰에 lbuttonUp으로 갖다 놓으면 gameobject 생성, 애셋 윈도우에 있는 원본 object copy;
		*/
		
		std::string Name;
		Transform Trans;
		Script::IScript* ScriptComponent;
		bool Drawable;
		std::vector<Engine::GameObjPtr> Children;
		Engine::GameObjPtr Parent;

		//렌더링 관련 컴포넌트
		//Component로 세개다 묶기
		vector<Mesh> Meshes;	//drawble인 애들은 obj Mesh생성 그렇지 않은 애들은 nullptr로 들고있게 하려고.... 포인터로 만들었음.
		vector<Material> Material;
		Animator
		
	

	private:
		void LinkResourceToPipeline();	//prepareRender
		void UpdateConstBuff();
	public:
		GameObject();
		GameObject(const GameObject& src);
		virtual ~GameObject();
		GameObject& operator=(const GameObject& rhs);
		void ImportMesh(const std::string& str, MeshModelType type);
		void AddMesh(const Mesh& mesh);
		void Draw();
		void Release();
		bool IsDrawable();
		Mesh* GetMesh();

		void SetChild(Engine::GameObjPtr child, Engine::GameObjPtr parent);

		GameObjPtr GetChild(UINT idx);

		void DeleteChild(UINT idx);

		GameObjPtr GetParent()
		{
			return Parent;
		}

		Transform& GetTransform()
		{
			return Trans;
		}

		const std::string& GetName()
		{
			return Name;
		}

		void SetName(const std::string& name)
		{
			Name = name;
		}

		void SetScriptObj(Script::IScript* script)
		{
			ScriptComponent = script;
		}

		Script::IScript* const GetScript()
		{
			return ScriptComponent;
		}

		void SetChildCount(int cnt) { Children.reserve(cnt); }
		void AddChild(GameObjPtr childNodePtr) { Children.push_back(childNodePtr); }

		std::vector<GameObjPtr>::iterator GetChildrenBegin();
		std::vector<GameObjPtr>::iterator GetChildrenEnd();

	};

};

