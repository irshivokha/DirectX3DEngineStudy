#include "EngineBaseHeader.h"
#include "Utility.h"
#include "RenderClasses/DXUtility.h"

namespace Engine
{
	namespace Utility
	{
		HRESULT CreateBufferFromData(ID3D11Buffer** ppBuffer, D3D11_BIND_FLAG eBindFlag, void* pData, UINT nDataSize, D3D11_USAGE eUsage, UINT CPUAccessFlags)
		{

			ID3D11Device* device = DXUtility::GetInstance()->GetDevice();

			//후에 구조적	버퍼, 추가/ 소비 버퍼 혹은 바이트 버퍼를 사용하게 되면 miscflag와 structsize도 지정하도록 만들기. 
			D3D11_BUFFER_DESC bd = { nDataSize  , eUsage , (UINT)eBindFlag , CPUAccessFlags , 0 , 0 }; //2d,3d텍스쳐가 아니면 pitch값들은 의미 없음
			D3D11_SUBRESOURCE_DATA subResourceData = { pData , 0 ,0 };
			HRESULT hr = S_OK;
			D3D11_SUBRESOURCE_DATA* pSubResourceData = nullptr;

			if (pData)
				pSubResourceData = &subResourceData;

			if(device)
			hr = device->CreateBuffer(&bd, pSubResourceData, ppBuffer);
			return hr;
		}

//		HRESULT CompileShaderFromFile(WCHAR* szFileName, LPCSTR szEntryPoint, LPCSTR szShaderModel, ID3DBlob** ppBlobOut)
//		{
//			HRESULT hr = S_OK;
//
//			DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;
//#if defined( DEBUG ) || defined( _DEBUG )
//			// Set the D3DCOMPILE_DEBUG flag to embed debug information in the shaders.
//			// Setting this flag improves the shader debugging experience, but still allows 
//			// the shaders to be optimized and to run exactly the way they will run in 
//			// the release configuration of this program.
//			dwShaderFlags |= D3DCOMPILE_DEBUG;
//#endif
//
//			ID3DBlob* pErrorBlob;
//			hr = D3DX11CompileFromFile(szFileName, nullptr, nullptr, szEntryPoint, szShaderModel,
//				dwShaderFlags, 0, nullptr, ppBlobOut, &pErrorBlob, nullptr);
//			if (FAILED(hr))
//			{
//				if (pErrorBlob != nullptr)
//					OutputDebugStringA((char*)pErrorBlob->GetBufferPointer());
//				if (pErrorBlob) pErrorBlob->Release();
//				return hr;
//			}
//			if (pErrorBlob) pErrorBlob->Release();
//
//			return S_OK;
//		}

		void ReadFileBytes(const std::string& path, std::string& bytes)
		{
			std::ifstream in(path, std::ios::in | std::ios::binary);
			//std::ifstream in(path);
			std::string s;

			if (in.is_open())
			{
				in.seekg(0, std::ios::end);				
				int size = in.tellg();
				bytes.resize(size);
				in.seekg(0, std::ios::beg);
				in.read(&bytes[0], size);

			}
		}
		std::vector<std::string> StringSplit(const std::string str, char delimiter)
		{
			std::vector<std::string> internal;
			stringstream ss(str);
			std::string temp;

			while (getline(ss, temp, delimiter)) {
				internal.push_back(temp);
			}

			return internal;
		}
	}
}






