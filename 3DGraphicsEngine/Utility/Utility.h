#pragma once
#include "Engine.h"
#include "EngineBaseHeader.h"
namespace Engine
{
	using namespace std;
	namespace Utility
	{
		HRESULT CreateBufferFromData(ID3D11Buffer** ppBuffer, D3D11_BIND_FLAG eBindFlag,
			void* pData, UINT nDataSize, D3D11_USAGE eUsage, UINT CPUAccessFlags);


		//HRESULT CompileShaderFromFile(WCHAR* szFileName, LPCSTR szEntryPoint, LPCSTR szShaderModel, ID3DBlob** ppBlobOut);
	

		//https://stackoverflow.com/questions/28466722/unable-to-load-vertex-shader-from-cso-file-directx-c
		void ReadFileBytes(const std::string& path, std::string& bytes);

		template <class ID3D11Class>
		void Release(ID3D11Class& iclass)
		{
			if (iclass)
			{
				iclass->Release();
				iclass = nullptr;
			}
		}


		template <typename T>
		void Delete(T t)
		{
			if (t)
			{
				delete t;
				t = nullptr;
			}
		}

		template <typename T>
		void Delete(T t, unsigned int count)
		{
			if(count>0)
			if (t)
			{
				delete[] t;
				t = nullptr;
			}
		}

		std::vector<std::string> StringSplit(const std::string str, char delimiter);

		int byteBufferToInt(char* bytes)
		{

		}
		float byteBufferToFloat(char* bytes);
		double byteBufferToDouble(char* bytes);
			
	}
}
