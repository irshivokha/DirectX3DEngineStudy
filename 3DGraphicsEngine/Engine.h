#pragma once

#ifdef ENGINE_DLL

#define ENGINE_DECLSPEC  __declspec(dllexport)   

#else

#define ENGINE_DECLSPEC  __declspec(dllimport)   

#endif