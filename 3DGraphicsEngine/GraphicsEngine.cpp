#include "EngineBaseHeader.h"
#include "GraphicsEngine.h"
#include "RenderClasses/DXUtility.h"
//#include "Shader/shaderResourcePool.h"
#include "Shader/shaderManager.h"
#include "GameObject/GameObjectManager.h"
#include "RenderClasses/StateObjectManager.h"
#include "Input/directinput.h"
#include "GameObject/IScript.h"
#include "GameObject/GameObject.h"
#include "Resource/Model/ModelImporter/objLoader.h"
#include "System/Timer.h"


namespace Engine
{
	//void GraphicsEngine::UpdateSysInfo(GameMode::SysInfo* sInfo)
	//{

	//}

	GraphicsEngine::GraphicsEngine() :
		GameObjMgr(nullptr)
		//ShaderMgr(nullptr),
	{

	}

	GraphicsEngine::~GraphicsEngine()
	{
	}

	void GraphicsEngine::Init(HWND hWnd)
	{

		DXUtility::CreateInstance();
		DXUtility::GetInstance()->Init(hWnd, 1);		

		Timer::GetInstance().Init();
		InputCenter::CreateInstance();

		int screenWidth = 0;
		int screenHeight = 0;
		DXUtility::GetInstance()->GetDeviceWidthHeight(screenWidth, screenHeight);
		if (FAILED(InputCenter::GetInstance()->InitDirectInput(hWnd, screenWidth,screenHeight)))
			InputCenter::GetInstance()->CleanUpDirectInput();
		
		////ShaderResourcePool::CreateInstance(); <--- TextureResourcePool로 바꿔서 사용하자
		ShaderManager::CreateInstance();
		ShaderManager::GetInstance()->LoadShader();
		StateObjectManger::CreateInstance();
		StateObjectManger::GetInstance()->Init();
		Renderer.Prepare();
		ObjLoader::CreateInstance();

		//Importer load		

	}

	void GraphicsEngine::Release()
	{
		InputCenter::ReleaseInstance();
		DXUtility::GetInstance()->Release();
		DXUtility::ReleaseInstance();
		//ShaderResourcePool::ReleaseInstance();	<--- TextureResourcePool로 바꿔서 사용하자		
		ShaderManager::ReleaseInstance();		
		StateObjectManger::ReleaseInstance();
		GameObjectManager::GetInstance().Release();
		ObjLoader::GetInstance()->Release();
		//ObjLoader::ReleaseInstance();
	}

	void GraphicsEngine::UpdateScene()	//GameMode::SysInfo* sInfo
	{		
		//나중에는 SwapChian별로 Rendering 할수 있게 지정하기!
		DXUtility::GetInstance()->BeginScene();

		//UpdateSysInfo(sInfo);
		InputCenter::GetInstance()->UpdateInputState();
		Timer::GetInstance().Frame();
		UpdateScripts();
		Renderer.Render();

		DXUtility::GetInstance()->EndScene();
	}

	bool GraphicsEngine::IsExit()
	{
		if (InputCenter::GetInstance()->IsKeyDown(DIK_ESCAPE))
			return true;
		return false;
	}

	void GraphicsEngine::StartScripts()
	{
		std::map<UID, std::shared_ptr<Engine::GameObject>>::iterator iter = Engine::GameObjectManager::GetInstance().GetBegin();
		while (iter != Engine::GameObjectManager::GetInstance().GetEnd())
		{
			if (iter->second.get())
			{								
				Script::IScript* scriptComponent = iter->second.get()->GetScript();
				if (scriptComponent)
					scriptComponent->Start();
			}
			iter++;
		}
	}

	void GraphicsEngine::UpdateScripts()
	{
		std::map<UID, std::shared_ptr<Engine::GameObject>>::iterator iter = Engine::GameObjectManager::GetInstance().GetBegin();
		while (iter != Engine::GameObjectManager::GetInstance().GetEnd())
		{
			if (iter->second.get())
			{
				Engine::Script::IScript* scriptComponent = iter->second.get()->GetScript();
				if (scriptComponent)
					scriptComponent->Update();
			}
			iter++;
		}
	}

}

