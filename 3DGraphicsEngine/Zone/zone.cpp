#include "stdafx.h"


C_ZONE::C_ZONE()
	:Camera(nullptr),
	SkyDome(nullptr),
	Terrain(nullptr),
	m_bWireframe(false)
{
}

void C_ZONE::Init()
{

	SkyDome = new C_SKYDOME;
	Camera = new C_CAMERA;
	Terrain = new C_TERRAIN;


}

void C_ZONE::LoadData(ID3D11Device * pD3dDevice, ShaderMGR* pShaderMgr)
{

	int nLightCount = 4;
	DirectLight = new DirectionalLight[nLightCount];

	SkyDome->Load(pD3dDevice, "skysphere");
	SkyDome->SetShader(pShaderMgr->GetShader(ShaderMGR::E_DEFAULT_SKYDOME));

	Terrain->Load(pD3dDevice,"terrain");
	Terrain->SetShader(pShaderMgr->GetShader(ShaderMGR::E_DEFAULT));

}

void C_ZONE::Frame(C_DIRECTINPUT * pDirectInput, C_DIRECT3D* pDirect3D, float fElapsedTime, LightMgr* pLightMgr)
{
	//updateShaderResourece();
	render(pDirect3D, pLightMgr);

}

void C_ZONE::UpdateLight(C_DIRECT3D * pDirect3D, LightMgr * pLightMgr)
{
	std::map<int, LightMgr::S_DATA>::iterator itor = pLightMgr->GetBegin();
	int nLightIndex = -1;

	while (itor != pLightMgr->GetEnd())
	{
		if (itor->first != nLightIndex)								//같은 모델은 모델인덱스가 같다. 따라서 같은 모델일 경우에는 렌더셋을 하지않고 바로그린다. 이에따라
																	//래퍼클래스나 인터페이스 부분에서 모델을 추가할때는 같은 종류의 모델인지 아닌지 따져봐야 한다
		{
			nLightIndex = itor->first;
			DirectLight[nLightIndex].SetLightMatrix(itor->second.matWorld);
			DirectLight[nLightIndex].SetLightDir(itor->second.LightDir);

		}

		//ModelData[nModelIndex].Draw(pDeviceContext, itor->second.matWorld);

		++itor;
	}



	// Update our time
	static float t = 0.0f;
	if (pDirect3D->GetDriverType() == D3D_DRIVER_TYPE_REFERENCE)
	{
		t += (float)XI * 0.0125f;
	}
	else
	{
		static DWORD dwTimeStart = 0;
		DWORD dwTimeCur = GetTickCount();
		if (dwTimeStart == 0)
			dwTimeStart = dwTimeCur;
		t = (dwTimeCur - dwTimeStart) / 3000.0f;
	}

	// Rotate the second light around the origin
	XMVECTOR vLightDir = XMLoadFloat4(&DirectLight[0].GetLightDir());
	XMMATRIX mRotate = XMMatrixRotationY(-2.0f * t);
	//vLightDir = XMVector3Transform(vLightDir, mRotate);
	XMFLOAT4 lightPos;
	lightPos.x = DirectLight[0].GetLightPos().x;
	lightPos.y = DirectLight[0].GetLightPos().y;
	lightPos.z = DirectLight[0].GetLightPos().z;
	lightPos.w = DirectLight[0].GetLightPos().w;

	DirectLight[0].SetLookAt(0, 0, 0);
	DirectLight[0].GenerateViewMatrix();
	DirectLight[0].GenerateProjMatrix();


	ShaderRESOURCE::GetInstance()->SetLightDir(&vLightDir);
	ShaderRESOURCE::GetInstance()->SetLightPos(&lightPos);
	ShaderRESOURCE::GetInstance()->SetLightViewMatrix(&DirectLight[0].GetLightViewMat());
	ShaderRESOURCE::GetInstance()->SetLightProjMatrix(&DirectLight[0].GetLightProjMat());
}

void C_ZONE::render(C_DIRECT3D* pDirect3D, LightMgr* pLightMgr)
{
	XMFLOAT4 cameraPos = *(Camera->GetPos());
	XMMATRIX matCameraPos = XMMatrixTranslation(cameraPos.x, cameraPos.y, cameraPos.z);
	XMMATRIX worldMatrix = XMMatrixIdentity();

	//ShaderRESOURCE::GetInstance()->SetProjectionMatrix(&pDirect3D->GetProjectionMatrix());
	//ShaderRESOURCE::GetInstance()->SetOrthoMatrix(&pDirect3D->GetOrthoMatrix());
	//ShaderRESOURCE::GetInstance()->UpdateVO();
	//ShaderRESOURCE::GetInstance()->UpdateVP();

	ID3D11DeviceContext* pDeviceContext = pDirect3D->GetDeviceContext();

	

	pDirect3D->TurnOffCulling();
	pDirect3D->TurnZBufferOff();
	SkyDome->RenderSet(pDeviceContext);
	SkyDome->Draw(pDeviceContext, matCameraPos);

	pDirect3D->TurnZBufferOn();
	pDirect3D->TurnOnCulling();
	if (m_bWireframe)
		pDirect3D->EnableWireframe();

	//worldMatrix = XMMatrixScaling(2.0f, 1.0f, 2.0f);
	//Terrain->RenderSet(pDeviceContext);
	//Terrain->Draw(pDeviceContext, worldMatrix);

}

void C_ZONE::HandleCameraAndStateByInput(C_DIRECTINPUT* pDirectInput, float fElapsedTime)
{
	BYTE Lbutton = 0;
	BYTE Rbutton = 1;
	BYTE MButton = 2;
	
	if (pDirectInput->IsButtonDown(Rbutton))
	{
		if (pDirectInput->IsKeyDown(DIK_W))
			Camera->MoveF(fElapsedTime);
		if (pDirectInput->IsKeyDown(DIK_S))
			Camera->MoveB(fElapsedTime);
		if (pDirectInput->IsKeyDown(DIK_A))
			Camera->Rotate(-0.3f, 0.0f, 0.0f, fElapsedTime);
		if (pDirectInput->IsKeyDown(DIK_D))
			Camera->Rotate(0.3f, 0.0f, 0.0f, fElapsedTime);
	}

	if (pDirectInput->IsKeyDown(DIK_F1))
		m_bWireframe = true;
	if (pDirectInput->IsKeyDown(DIK_F2))
		m_bWireframe = false;

	if (pDirectInput->IsButtonDown(MButton))
	{
		int nX = pDirectInput->GetMouseX();
		int nY = pDirectInput->GetMouseY();
		float fYaw = (float)nX * 0.3f;
		float fPitch = (float)nY * 0.3f;

		Camera->Rotate(fYaw, fPitch, 0.0f, fElapsedTime);
	}

	Camera->Update();
	Camera->RenderReflection(-2);

	
}

void C_ZONE::UpdateShaderResourece(C_DIRECT3D* pDirect3D)
{
	ShaderRESOURCE::GetInstance()->SetCameraDir(Camera->GetDir());
	ShaderRESOURCE::GetInstance()->SetCameraPos(Camera->GetPos());
	ShaderRESOURCE::GetInstance()->SetViewMatrix(&Camera->GetMatrix());
	ShaderRESOURCE::GetInstance()->SetReflectionMatrix(&Camera->GetReflectionMat());
	ShaderRESOURCE::GetInstance()->SetProjectionMatrix(&pDirect3D->GetProjectionMatrix());
	ShaderRESOURCE::GetInstance()->SetOrthoMatrix(&pDirect3D->GetOrthoMatrix());
	ShaderRESOURCE::GetInstance()->UpdateVO();
	ShaderRESOURCE::GetInstance()->UpdateVP();
	XMVECTOR vLightDir = XMLoadFloat4(&DirectLight[0].GetLightDir());
	ShaderRESOURCE::GetInstance()->SetLightDir(&vLightDir);

}



void C_ZONE::ReleaseData()
{
	if(Camera)
	{
		delete Camera;
		Camera = nullptr;
	}

	if(SkyDome)
	{
		delete SkyDome;
		SkyDome = nullptr;
	}

}

ID3D11ShaderResourceView * C_ZONE::GetTerrainTexture()
{
	return Terrain->GetTexture();
}

void C_ZONE::RenderTerrain(C_DIRECT3D* pDirect3D)
{

	ID3D11DeviceContext* pDeviceContext = pDirect3D->GetDeviceContext();


	XMMATRIX worldMatrix = XMMatrixIdentity();
	worldMatrix = XMMatrixScaling(2.0f, 1.0f, 2.0f);
	worldMatrix = XMMatrixTranslation(0, -2, 0);
	Terrain->RenderSet(pDeviceContext);
	Terrain->Draw(pDeviceContext, worldMatrix);
}

void C_ZONE::SetTerrainTexture(ID3D11ShaderResourceView * pView, int idx)
{
	Terrain->SetShaderResource(pView, idx);
}
