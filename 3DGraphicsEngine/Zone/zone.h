#pragma once
#include <D3D11.h>

class C_SKYDOME;
class C_CAMERA;
class C_TERRAIN;
class DirectionalLight;

class C_ZONE
{
private:
	C_SKYDOME* SkyDome;
	C_CAMERA* Camera;
	C_TERRAIN* Terrain;
	DirectionalLight* DirectLight;
	bool m_bWireframe;

private:
	
public:
	C_ZONE();
	void Init();
	void LoadData(ID3D11Device* pD3dDevice, ShaderMGR* pShaderMgr);
	void Frame(C_DIRECTINPUT* pDirectInput, C_DIRECT3D* pDirect3D, float fElapsedTime, LightMgr* pLightMgr);
	void UpdateLight(C_DIRECT3D* pDirect3D, LightMgr* pLightMgr);
	void render(C_DIRECT3D* pDirect3D, LightMgr* pLightMgr);
	void ReleaseData();
	void HandleCameraAndStateByInput(C_DIRECTINPUT* pDirectInput, float fElapsedTime);
	void UpdateShaderResourece(C_DIRECT3D* pDirect3D);
	ID3D11ShaderResourceView* GetTerrainTexture();
	void RenderTerrain(C_DIRECT3D* pDirect3D);		//for test...
	void SetTerrainTexture(ID3D11ShaderResourceView* pView, int idx);
};