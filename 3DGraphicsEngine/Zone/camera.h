#pragma once

#include <D3DX11.h>

class C_CAMERA
{
private:
	XMMATRIX	m_matView;
	XMMATRIX	m_matReflectView;
	XMVECTOR	m_vZ;

	XMVECTOR    m_vPos;
	XMVECTOR    m_vDir;

	XMMATRIX	m_matRot;
	XMFLOAT4	m_f4Pos;
	XMFLOAT4	m_f4ReflectedPos;

	XMFLOAT3	m_f3Dir;

	float		m_fYaw;
	float		m_fPitch;
	float		m_fRoll;
private:
	
	void		adjustRid(float &fRid);

public:
	static void * operator new (size_t nSize);
	static void operator delete (void *p);
	C_CAMERA();
	const XMMATRIX & GetMatrix();
	const XMMATRIX & GetReflectionMat();
	void MoveF(float fElapsedTime);
	void MoveB(float fElapsedTime);
	void Rotate(float fYaw, float fPitch, float fRoll , float fElapsedTime);
	void Update();
	void RenderReflection(float height);
	const XMFLOAT4* GetPos();
	const XMFLOAT4* GetReflectedPos();
	const XMVECTOR* GetDir();
};