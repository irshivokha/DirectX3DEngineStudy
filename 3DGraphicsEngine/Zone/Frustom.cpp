#include "stdafx.h"


Frustum::Frustum()
{
}


Frustum::Frustum(const Frustum& other)
{
}


Frustum::~Frustum()
{
}

//ConstructFrustum is called every frame by the GraphicsClass.
//It passes in the the depth of the screen, the projection matrix, and the view matrix.
//We then use these input variables to calculate the matrix of the view frustum at that frame.
//With the new frustum matrix we then calculate the six planes that form the view frustum.

void Frustum::ConstructFrustum(float screenDepth, XMMATRIX projectionMatrix, XMMATRIX viewMatrix)
{
	float zMinimum, r;
	XMMATRIX matrix;
	matrix = XMMatrixIdentity();

	// Calculate the minimum Z distance in the frustum.
	zMinimum = -projectionMatrix._43 / projectionMatrix._33;
	r = screenDepth / (screenDepth - zMinimum);
	projectionMatrix._33 = r;
	projectionMatrix._43 = -r * zMinimum;

	// Create the frustum matrix from the view matrix and updated projection matrix.
	matrix = XMMatrixMultiply(matrix, viewMatrix);
	matrix = XMMatrixMultiply(matrix, projectionMatrix);

	// Calculate near plane of frustum.
	lanes[0].x = matrix._14 + matrix._13;
	lanes[0].y = matrix._24 + matrix._23;
	lanes[0].z = matrix._34 + matrix._33;
	lanes[0].w = matrix._44 + matrix._43;
	lanes[0] = XMPlaneNormalize(lanes[0]);

	// Calculate far plane of frustum.
	lanes[1].x = matrix._14 - matrix._13;
	lanes[1].y = matrix._24 - matrix._23;
	lanes[1].z = matrix._34 - matrix._33;
	lanes[1].w = matrix._44 - matrix._43;
	lanes[1] = XMPlaneNormalize(lanes[1]);

	// Calculate left plane of frustum.
	lanes[2].x = matrix._14 + matrix._11;
	lanes[2].y = matrix._24 + matrix._21;
	lanes[2].z = matrix._34 + matrix._31;
	lanes[2].w = matrix._44 + matrix._41;
	lanes[2] = XMPlaneNormalize(lanes[2]);

	// Calculate right plane of frustum.
	lanes[3].x = matrix._14 - matrix._11;
	lanes[3].y = matrix._24 - matrix._21;
	lanes[3].z = matrix._34 - matrix._31;
	lanes[3].w = matrix._44 - matrix._41;
	lanes[3] = XMPlaneNormalize(lanes[3]);

	// Calculate top plane of frustum.
	lanes[4].x = matrix._14 - matrix._12;
	lanes[4].y = matrix._24 - matrix._22;
	lanes[4].z = matrix._34 - matrix._32;
	lanes[4].w = matrix._44 - matrix._42;
	lanes[4] = XMPlaneNormalize(lanes[4]);

	// Calculate bottom plane of frustum.
	lanes[5].x = matrix._14 + matrix._12;
	lanes[5].y = matrix._24 + matrix._22;
	lanes[5].z = matrix._34 + matrix._32;
	lanes[5].w = matrix._44 + matrix._42;
	lanes[5] = XMPlaneNormalize(lanes[5]);

	return;
}
