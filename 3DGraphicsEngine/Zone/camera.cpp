#include "stdafx.h"

C_CAMERA::C_CAMERA() :
	m_f4Pos(0.0f, 0.0f, 0.0f,0.0f),
	m_f3Dir(0.0f, 0.0f, 0.0f),
	m_fYaw(0.0f),
	m_fPitch(0.0f),
	m_fRoll(0.0f)
{	
	m_matRot = XMMatrixIdentity();
	m_vPos = XMVectorSet(0.0f, 0.0f, -10.0f, 0.0f);
	m_vZ = XMVectorSet(0.0f ,0.0f ,1.0f ,0.0f);
	m_vDir = m_vZ;
	m_matView = XMMatrixIdentity();
}

const XMMATRIX & C_CAMERA::GetMatrix()
{
	return m_matView;
}

const XMMATRIX & C_CAMERA::GetReflectionMat()
{
	return m_matReflectView;
}

void C_CAMERA::MoveF(float fElapsedTime)
{
 	m_vPos += m_vDir * fElapsedTime * 10.0f;
}

void C_CAMERA::MoveB(float fElapsedTime)
{
	m_vPos -= m_vDir * fElapsedTime * 10.0f;
}

void C_CAMERA::Rotate(float fYaw, float fPitch, float fRoll, float fElapsedTime)
{
	m_fYaw += fYaw * fElapsedTime;
	adjustRid(m_fYaw);
	m_fPitch += fPitch * fElapsedTime;
	adjustRid(m_fPitch);
	m_fRoll += fRoll * fElapsedTime;
	adjustRid(m_fRoll);

	m_matRot = XMMatrixRotationRollPitchYaw(m_fPitch,m_fYaw, m_fRoll);
	
	m_vDir = XMVector3TransformCoord(m_vZ, m_matRot);
}

void C_CAMERA::Update()
{
	XMFLOAT3 f3Pos;
	XMStoreFloat3(&f3Pos, m_vPos);

	XMVECTOR vDeterminant;
	XMMATRIX matInv;

	matInv = m_matRot;
	matInv._41 = f3Pos.x;
	matInv._42 = f3Pos.y;
	matInv._43 = f3Pos.z;

	m_matView = XMMatrixInverse(&vDeterminant, matInv);

	m_f4Pos = XMFLOAT4(f3Pos.x, f3Pos.y, f3Pos.z,1.0f);
}

void C_CAMERA::RenderReflection(float height)
{
	XMFLOAT3 f3Pos;
	XMFLOAT3 f3Up;
	XMFLOAT3 LookAt;
	XMStoreFloat3(&f3Pos, m_vPos);

	XMVECTOR vDeterminant;
	XMMATRIX matInv;

	matInv = m_matRot;
	matInv._41 = f3Pos.x;
	matInv._42 = f3Pos.y;
	matInv._43 = f3Pos.z;

	matInv._42 = -f3Pos.y + (height * 2.0f);

	m_matReflectView = XMMatrixInverse(&vDeterminant, matInv);

	m_f4ReflectedPos = XMFLOAT4(f3Pos.x, -f3Pos.y + (height * 2.0f), f3Pos.z, 1.0f);

	
}

void C_CAMERA::adjustRid(float & fRid)
{
	if (fRid < 0.0f)
		fRid += XM_2PI;
	else if (fRid > XM_2PI)
		fRid -= XM_2PI;
}

const XMFLOAT4 * C_CAMERA::GetPos()
{
	return &m_f4Pos;
}

const XMFLOAT4 * C_CAMERA::GetReflectedPos()
{
	return &m_f4ReflectedPos;
}

const XMVECTOR * C_CAMERA::GetDir()
{
	return &m_vDir;
}

void * C_CAMERA::operator new(size_t nSize)
{
#ifdef	_DECLSPEC_ALIGN_16_
	return _aligned_malloc(nSize , 16);
#else
	return malloc(nSize);
#endif
}

void C_CAMERA::operator delete(void * p)
{
#ifdef	_DECLSPEC_ALIGN_16_
	return _aligned_free(p);
#else
	return free(p);
#endif
}
