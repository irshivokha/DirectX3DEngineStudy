#include "stdafx.h"

UIMgr::UIMgr()
	:ModelCount(0),
	Text(nullptr),
	UIModel(nullptr)
{

}

void UIMgr::LoadData(ID3D11Device * pD3dDevice, ShaderMGR * pShaderMgr, C_DIRECT3D * pDirect3D)
{


	ModelCount = 1;


	UIModel = new UIModel[ModelCount];

	int screenWidth = 0;
	int screenHeight = 0;
	pDirect3D->GetWidthHeight(screenWidth, screenHeight);

	const char* texNameArray[3];

	texNameArray[0] = "resource/mousePointer/default.png";
	texNameArray[1] = "resource/mousePointer/move.png";
	texNameArray[2] = "resource/mousePointer/rotate.png";


	UIModel[0].Load(pD3dDevice, "cube", screenWidth, screenHeight, 30, 30,texNameArray,3);
	UIModel[0].SetShader(pShaderMgr->GetShader(ShaderMGR::E_UI));

	Text = new Text;
	Text->Load(pD3dDevice, screenWidth, screenHeight,2);
	Text->SetShader(pShaderMgr->GetShader(ShaderMGR::E_FONT));

}

void UIMgr::Frame(C_WORLDMATRIXMGR * pWorldMatrixMgr, C_DIRECT3D * pDirect3D, 
	C_DIRECTINPUT * pDirectInput, C_GAME::SysInfo* sInfo)
{
	ID3D11DeviceContext* pDeviceContext = pDirect3D->GetDeviceContext();

	int mouseX = 0;
	int mouseY = 0;
	BYTE iconDefault = 0;
	BYTE iconMove = 1;
	BYTE iconRotate = 2;
	int iconShape = 0;
	pDirectInput->GetMoustLocation(mouseX, mouseY);

	pDirect3D->TurnZBufferOff();
	XMMATRIX matrix = XMMatrixIdentity();
	if (pDirectInput->IsButtonDown(iconDefault))
		iconShape = 0;	//default
	else if (pDirectInput->IsButtonDown(iconMove))
		iconShape = 1;	//moveShape
	else if (pDirectInput->IsButtonDown(iconRotate))
		iconShape = 2;
	UIModel[0].Draw(pDeviceContext, matrix, mouseX, mouseY, iconShape);


	Text->SetCPU(sInfo->CpuRate, pDeviceContext);
	Text->SetFPS(sInfo->Fps, pDeviceContext);
	Text->Draw(pDeviceContext, matrix, sInfo);

	pDirect3D->TurnZBufferOn();

	//XMMATRIX matrix = XMMatrixIdentity();
	//Md5Model->RenderSet(pDeviceContext);
	//Md5Model->Draw(pDeviceContext, matrix);
}

void UIMgr::handleModelStateByInput(ID3D11DeviceContext * pDeviceContext, C_DIRECTINPUT * pDirectInput, float fElapsedTime)
{

}

void UIMgr::ReleaseData()
{
	for (int i = 0; i < ModelCount; i++)
	{
		UIModel[i].Release();
	}
	delete[] UIModel;
	UIModel = nullptr;

	if (Text)
	{
		Text->Shutdown();
		delete Text;
		Text = nullptr;
	}
}
