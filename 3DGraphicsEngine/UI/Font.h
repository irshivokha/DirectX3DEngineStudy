////////////////////////////////////////////////////////////////////////////////
// Filename: fontclass.h
////////////////////////////////////////////////////////////////////////////////
#pragma once


#include <d3d11.h>
#include <fstream>
using namespace std;

///////////////////////
// MY CLASS INCLUDES //
///////////////////////
//#include "textureclass.h"


////////////////////////////////////////////////////////////////////////////////
// Class name: FontClass
////////////////////////////////////////////////////////////////////////////////
class Font
{
private:

	struct FontType
	{
		float left, right;
		int size;
	};


	struct VertexType
	{
		XMFLOAT4 position;
		XMFLOAT2 texcoord;
	};

public:
	Font();
	Font(const Font&);
	~Font();

	bool Init(ID3D11Device*, char*, WCHAR** textureFileArray, int TexCount);
	void Shutdown();

	ID3D11ShaderResourceView** GetTexture();
	ID3D11ShaderResourceView* m_ShaderResourceView;
	
	void BuildVertexArray(void*, char*, float, float);

private:
	bool LoadFontData(char*);
	void ReleaseFontData();
	bool LoadTexture(ID3D11Device*, WCHAR** TexArray, int TexCount);
	void ReleaseTexture();

private:
	FontType* m_Font;
	Texture* m_Texture;
};



