#pragma once

#include "../Main/game.h"
class UIModel;
class Text;

class UIMgr
{
private:

	UIModel * UIModel;
	Text* Text;
	int		ModelCount;

public:
	UIMgr();
	void LoadData(ID3D11Device* pD3dDevice, ShaderMGR* pShaderMgr, C_DIRECT3D* pDirect3D);
	void Frame(C_WORLDMATRIXMGR* pWorldMatrixMgr, C_DIRECT3D* pDirect3D, C_DIRECTINPUT* pDirectInput, C_GAME::SysInfo* sInfo);
	void handleModelStateByInput(ID3D11DeviceContext* pDeviceContext, C_DIRECTINPUT* pDirectInput, float fElapsedTime);
	void ReleaseData();
};