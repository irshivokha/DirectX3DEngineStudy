//The Text handles all the 2D text drawing that the application will need.
//It renders 2D text to the screen and uses the FontClass and FontShaderClass to assist it in doing so.

////////////////////////////////////////////////////////////////////////////////
// Filename: Text.h
////////////////////////////////////////////////////////////////////////////////
#pragma once

#include "stdafx.h"

////////////////////////////////////////////////////////////////////////////////
// Class name: Text
////////////////////////////////////////////////////////////////////////////////
class Text
{
private:
	//SentenceType is the structure that holds the rendering information for each text sentence.

	struct SentenceType
	{
		ID3D11Buffer *vertexBuffer, *indexBuffer;
		int vertexCount, indexCount, maxLength;
		float red, green, blue;
	};

	//The VertexType must match the one in the FontClass.
	struct VertexType
	{
		XMFLOAT4 position;
		XMFLOAT2 texcoord;
	};

public:
	Text();
	Text(const Text&);
	~Text();

	bool Load(ID3D11Device* pDevice, int screenWidth, int screenHeight,int SentenceCount);
	void Shutdown();
	bool Draw(ID3D11DeviceContext* deviceContext, XMMATRIX& worldMatrix, C_GAME::SysInfo* sInfo);
	void SetShader(Shader* pShader);
	bool SetMousePosition(int mouseX, int mouseY, ID3D11DeviceContext* deviceContext);
	bool SetFPS(float FPS, ID3D11DeviceContext* deviceContext);
	bool SetCPU(int CPU, ID3D11DeviceContext* deviceContext);
private:
	bool InitializeSentence(SentenceType**, int, ID3D11Device*);
	bool UpdateSentence(SentenceType*, char*, int, int, float, float, float, ID3D11DeviceContext*);
	void ReleaseSentence(SentenceType**);
	void SetBuffer(ID3D11DeviceContext* pDeviceContext, SentenceType*);
	bool RenderSentence(ID3D11DeviceContext*, XMMATRIX&, SentenceType* sentence);

private:
	Font* m_Font;
	Shader* m_FontShader;
	int m_screenWidth, m_screenHeight;
	XMMATRIX m_baseViewMatrix;
	SentenceType** m_sentence;
	int m_SenteceCount;
};

