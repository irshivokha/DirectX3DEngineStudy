#include "stdafx.h"

UIModel::UIModel() :
	IndexBuffer(nullptr),
	VertexBuffer(nullptr),
	Shader(nullptr),
	IndexLength(0),
	m_arTexture(),
	TextureArray(nullptr),
	UseAlphablend(false),
	m_screenWidth(0),
	m_screenHeight(0),
	m_bitmapWidth(0),
	m_bitmapHeight(0),
	reviousPosX(0),
	reviousPosY(0)
{

}

void UIModel::SetShader(Shader* pShader)
{
	Shader = pShader;
}


HRESULT UIModel::Load(ID3D11Device* pD3dDevice, const char *pSzModelName, int screenWidth, int screenHeight, int bitmapWidth, int bitmapHeight,
					const char** texNameArray, int texCount)
{
	bool bLoad = false;
	OBJLoader *pObjLoader = OBJLoader::GetInstance();


	bLoad = pObjLoader->LoadFromObjFile(pSzModelName);


	if (!bLoad)
	{
		MessageBox(nullptr,
			L"model load error", L"Error", MB_OK);

		return E_FAIL;
	}

	HRESULT hr = S_OK;


	{
		m_screenWidth = screenWidth;
		m_screenHeight = screenHeight;
		m_bitmapWidth = bitmapWidth;
		m_bitmapHeight = bitmapHeight;

		reviousPosX = -1;
		reviousPosY = -1;


		VertexType* vertices;
		unsigned long* indices;
		D3D11_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
		D3D11_SUBRESOURCE_DATA vertexData, indexData;
		HRESULT result;
		int i;

		VertexCount = 6;
		IndexCount = 6;

		vertices = new VertexType[VertexCount];
		if (!vertices)
		{
			return false;
		}

		// Create the index array.
		indices = new unsigned long[IndexCount];
		if (!indices)
		{
			return false;
		}

		// Initialize vertex array to zeros at first.
		memset(vertices, 0, (sizeof(VertexType) * VertexCount));

		// Load the index array with data.
		for (i = 0; i < IndexCount; i++)
		{
			indices[i] = i;
		}


		hr = RENDER_UTILFUNC::CreateBufferFromData(pD3dDevice, &VertexBuffer, D3D11_BIND_VERTEX_BUFFER,
			vertices,
			sizeof(VertexType)* VertexCount, D3D11_USAGE_DYNAMIC, D3D11_CPU_ACCESS_WRITE);

		if (FAILED(hr))
		{
			pObjLoader->Release();
			return hr;
		}

		hr = RENDER_UTILFUNC::CreateBufferFromData(pD3dDevice, &IndexBuffer, D3D11_BIND_INDEX_BUFFER,
			indices, sizeof(unsigned long) * IndexCount, D3D11_USAGE_DEFAULT, 0);
	}


	pObjLoader->Release();

	loadTexture(pD3dDevice, texNameArray, texCount);

	return hr;

}


void UIModel::Release()
{
	if (IndexBuffer)
		IndexBuffer->Release();
	if (VertexBuffer)
		VertexBuffer->Release();

	for (int i = 0; i < E_MAX; ++i)
	{
		if (m_arTexture[i])
			m_arTexture[i]->Release();
	}
}

HRESULT UIModel::loadTexture(ID3D11Device * pD3dDevice, const char **pSzModelNameArray, int texCount)
{
	HRESULT hr = S_OK;

	//pObjLoader->GetDiffuseTextureName(szTextureName, MAX_PATH, pSzModelName);

	for (int i = 0; i < texCount; i++)
		hr = D3DX11CreateShaderResourceViewFromFileA(pD3dDevice, pSzModelNameArray[i], nullptr, nullptr, &m_arTexture[i], nullptr);
	if (FAILED(hr))
		return hr;

	return S_OK;
}

void UIModel::UpdateBuffer(float positionX, float positionY, ID3D11DeviceContext* pDeviceContext)
{
	float left, right, top, bottom;
	VertexType* vertices;
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	VertexType* verticesPtr;
	HRESULT result;

	//We check if the position to render this image has changed.If it hasn't changed then we just exit since the vertex buffer doesn't need any changes for this frame.This check can save us a lot of processing.

	// If the position we are rendering this bitmap to has not changed then don't update the vertex buffer since it
	// currently has the correct parameters.
	if ((positionX == reviousPosX) && (positionY == reviousPosY))
	{
		return;
	}

	//If the position to render this image has changed then we record the new location for the next time we come through this function.

	// If it has changed then update the position it is being rendered to.
	reviousPosX = positionX;
	reviousPosY = positionY;

	//The four sides of the image need to be calculated.See the diagram at the top of the tutorial for a complete explaination.

	// Calculate the screen coordinates of the left side of the bitmap.
	left = (float)((m_screenWidth / 2) * -1) + (float)positionX;

	// Calculate the screen coordinates of the right side of the bitmap.
	right = left + (float)m_bitmapWidth;

	// Calculate the screen coordinates of the top of the bitmap.
	top = (float)(m_screenHeight / 2) - (float)positionY;

	// Calculate the screen coordinates of the bottom of the bitmap.
	bottom = top - (float)m_bitmapHeight;

	//Now that the coordinates are calculated create a temporary vertex array and fill it with the new six vertex points.

	// Create the vertex array.
	vertices = new VertexType[VertexCount];
	if (!vertices)
	{
		return;
	}

	//Load the vertex array with data.
	//First triangle.
	vertices[0].Position = XMFLOAT4(left, top, 0.0f, 1.0f);  // Top left.
	vertices[0].Texcoord = XMFLOAT2(0.0f, 0.0f);

	vertices[1].Position = XMFLOAT4(right, bottom, 0.0f, 1.0f);  // Bottom right.
	vertices[1].Texcoord = XMFLOAT2(1.0f, 1.0f);

	vertices[2].Position = XMFLOAT4(left, bottom, 0.0f, 1.0f);  // Bottom left.
	vertices[2].Texcoord = XMFLOAT2(0.0f, 1.0f);

	// Second triangle.
	vertices[3].Position = XMFLOAT4(left, top, 0.0f, 1.0f);  // Top left.
	vertices[3].Texcoord = XMFLOAT2(0.0f, 0.0f);

	vertices[4].Position = XMFLOAT4(right, top, 0.0f, 1.0f);  // Top right.
	vertices[4].Texcoord = XMFLOAT2(1.0f, 0.0f);

	vertices[5].Position = XMFLOAT4(right, bottom, 0.0f, 1.0f);  // Bottom right.
	vertices[5].Texcoord = XMFLOAT2(1.0f, 1.0f);

	//Now copy the contents of the vertex array into the vertex buffer using the Map and memcpy functions.

	// Lock the vertex buffer so it can be written to.
	result = pDeviceContext->Map(VertexBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	if (FAILED(result))
	{
		return;
	}

	// Get a pointer to the data in the vertex buffer.
	verticesPtr = (VertexType*)mappedResource.pData;

	// Copy the data into the vertex buffer.
	memcpy(verticesPtr, (void*)vertices, (sizeof(VertexType) * VertexCount));

	// Unlock the vertex buffer.
	pDeviceContext->Unmap(VertexBuffer, 0);

	// Release the vertex array as it is no longer needed.
	delete[] vertices;
	vertices = 0;
}

void UIModel::SetBuffer(ID3D11DeviceContext* pDeviceContext)
{
	UINT stride = sizeof(VertexType);
	UINT offset = 0;
	pDeviceContext->IASetVertexBuffers(0, 1, &VertexBuffer, &stride, &offset);
	// Set index buffer
	pDeviceContext->IASetIndexBuffer(IndexBuffer, DXGI_FORMAT_R32_UINT, 0);

	pDeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

}

void UIModel::RenderBuffer(const XMMATRIX& matWorld, ID3D11DeviceContext* pDeviceContext)
{
	Shader->UpdateMappedResource(matWorld, false, pDeviceContext);
	Shader->UpdateMappedResource(matWorld, pDeviceContext, XMFLOAT4(1, 1, 1, 1));
	//Shader->UpdateSubResource(matWorld);
	Shader->Render();

	//pDeviceContext->DrawIndexed(IndexLength, 0, 0);
	pDeviceContext->DrawIndexed(IndexCount, 0, 0);
}



void UIModel::Draw(ID3D11DeviceContext*& pDeviceContext, const XMMATRIX &matWorld, float positionX, float positionY, int texIdx)
{

	UpdateBuffer(positionX, positionY, pDeviceContext);
	SetBuffer(pDeviceContext);
	Shader->SetShaderResource(0, 1, &m_arTexture[texIdx]);
	RenderBuffer(matWorld, pDeviceContext);

	//Shader->UpdateSubResource(matWorld);

	//Shader->Render();
	//pDeviceContext->DrawIndexed(IndexLength, 0, 0);


	return;
}

