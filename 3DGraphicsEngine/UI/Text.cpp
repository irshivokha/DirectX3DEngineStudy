#include "stdafx.h"

//The class constructor initializes the private member variables to nullptr.

Text::Text()
{
	m_Font = 0;
	m_FontShader = 0;
	m_SenteceCount = 0;
	m_sentence = nullptr;
}


Text::Text(const Text& other)
{
}


Text::~Text()
{
}


bool Text::Load(ID3D11Device* device, int screenWidth, int screenHeight, int SentenceCount)
{
	bool result;


	m_SenteceCount = SentenceCount;
	//후에 여기에 기본 뷰매트릭스 저장하는 루틴 넣자. 쉐이더별로 지정해주는거 너무 불편..

	// Store the screen width and height.
	m_screenWidth = screenWidth;
	m_screenHeight = screenHeight;

	// Store the base view matrix.
	//Create and initialize the font object.

	// Create the font object.
	m_Font = new Font;
	if (!m_Font)
	{
		return false;
	}

	// Initialize the font object.

	WCHAR* temp = L"resource/text/font.dds";
	result = m_Font->Init(device, "resource/text/fontdata.txt", &temp,1);
	if (!result)
	{
		return false;
	}

	//Create and initialize the two strings that will be used for this tutorial.
	//One string says Hello in white at 100, 100 and the other says Goodbye in yellow at 100, 200. 
	//The UpdateSentence function can be called to change the contents, location, and color of the strings at any time.
	// Initialize the first sentence.

	m_sentence = new SentenceType*[m_SenteceCount];

	for(int i = 0 ; i < m_SenteceCount; i++)
	{
		result = InitializeSentence(&m_sentence[i], 16, device);
		if (!result)
		{
			return false;
		}
	}
	


	return true;
}

//The Shutdown function will release the two sentences, the font object, and the font shader object.

void Text::Shutdown()
{
	for (int i = 0; i < m_SenteceCount; i++)
	{
		ReleaseSentence(&m_sentence[i]);
	}

	// Release the font shader object.
	if (m_FontShader)
	{
		m_FontShader = nullptr;
	}

	// Release the font object.
	if (m_Font)
	{
		m_Font->Shutdown();
		delete m_Font;
		m_Font = 0;
	}

	return;
}

//Render will draw the two sentences to the screen.

bool Text::Draw(ID3D11DeviceContext* deviceContext, XMMATRIX& worldMatrix, C_GAME::SysInfo* sInfo)
{
	bool result;

	//// Now update the sentence vertex buffer with the new string information.
	//result = UpdateSentence(m_sentence1, "Hello", 100, 100, 1.0f, 1.0f, 1.0f, deviceContext);
	//if (!result)
	//{
	//	return false;
	//}

	//SetBuffer(deviceContext, m_sentence1);

	//// Draw the first sentence.
	//result = RenderSentence(deviceContext, worldMatrix, m_sentence1);
	//if (!result)
	//{
	//	return false;
	//}

	//// Now update the sentence vertex buffer with the new string information.
	//result = UpdateSentence(m_sentence2, "Goodbye", 100, 200, 1.0f, 1.0f, 0.0f, deviceContext);
	//if (!result)
	//{
	//	return false;
	//}

	//SetBuffer(deviceContext, m_sentence2);

	//// Draw the second sentence.
	//result = RenderSentence(deviceContext, worldMatrix, m_sentence2);
	//if (!result)
	//{
	//	return false;
	//}


	//SetMousePosition(mouseX, mouseY, deviceContext);
	for (int i = 0; i < m_SenteceCount; i++)
	{
		SetBuffer(deviceContext, m_sentence[i]);
		RenderSentence(deviceContext, worldMatrix, m_sentence[i]);
	}

	return true;
}

void Text::SetShader(Shader * FontShader)
{
	m_FontShader = FontShader;
}

//The InitializeSentence function creates a SentenceType with an empty vertex buffer which will be used to store and render sentences.The maxLength input parameter determines how large the vertex buffer will be.All sentences have a vertex and index buffer associated with them which is initialized first in this function.

bool Text::InitializeSentence(SentenceType** sentence, int maxLength, ID3D11Device* device)
{

	VertexType* vertices;
	unsigned long* indices;
	D3D11_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
	D3D11_SUBRESOURCE_DATA vertexData, indexData;
	HRESULT result;
	int i;


	// Create a new sentence object.
	*sentence = new SentenceType;
	if (!*sentence)
	{
		return false;
	}

	// Initialize the sentence buffers to nullptr.
	(*sentence)->vertexBuffer = 0;
	(*sentence)->indexBuffer = 0;

	// Set the maximum length of the sentence.
	(*sentence)->maxLength = maxLength;

	// Set the number of vertices in the vertex array.
	(*sentence)->vertexCount = 6 * maxLength;

	// Set the number of indexes in the index array.
	(*sentence)->indexCount = (*sentence)->vertexCount;

	// Create the vertex array.
	vertices = new VertexType[(*sentence)->vertexCount];
	if (!vertices)
	{
		return false;
	}

	// Create the index array.
	indices = new unsigned long[(*sentence)->indexCount];
	if (!indices)
	{
		return false;
	}

	// Initialize vertex array to zeros at first.
	memset(vertices, 0, (sizeof(VertexType) * (*sentence)->vertexCount));

	// Initialize the index array.
	for (i = 0; i<(*sentence)->indexCount; i++)
	{
		indices[i] = i;
	}

	//During the creation of the vertex buffer description for the sentence we set the Usage type to dynamic as we may want to change the contents of the sentence at any time.

	RENDER_UTILFUNC::CreateBufferFromData(device, &(*sentence)->vertexBuffer, D3D11_BIND_VERTEX_BUFFER,
		vertices, sizeof(VertexType)* (*sentence)->vertexCount, 
		D3D11_USAGE_DYNAMIC, D3D11_CPU_ACCESS_WRITE);

	//The index buffer is setup as a normal static buffer since the contents will never need to change.
	// Set up the description of the static index buffer.
	RENDER_UTILFUNC::CreateBufferFromData(device, &(*sentence)->indexBuffer, D3D11_BIND_INDEX_BUFFER,
		indices, sizeof(unsigned long) * (*sentence)->indexCount, D3D11_USAGE_DEFAULT, 0);

	// Release the vertex array as it is no longer needed.
	delete[] vertices;
	vertices = 0;

	// Release the index array as it is no longer needed.
	delete[] indices;
	indices = 0;

	return true;
}

//UpdateSentence changes the contents of the vertex buffer for the input sentence.It uses the Map and Unmap functions along with memcpy to update the contents of the vertex buffer.

bool Text::UpdateSentence(SentenceType* sentence, char* text, int positionX, int positionY, float red, float green, float blue,
	ID3D11DeviceContext* deviceContext)
{
	int numLetters;
	VertexType* vertices;
	float drawX, drawY;
	HRESULT result;
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	VertexType* verticesPtr;

	//Set the color and size of the sentence.

		// Store the color of the sentence.
		sentence->red = red;
	sentence->green = green;
	sentence->blue = blue;

	// Get the number of letters in the sentence.
	numLetters = (int)strlen(text);

	// Check for possible buffer overflow.
	if (numLetters > sentence->maxLength)
	{
		return false;
	}

	// Create the vertex array.
	vertices = new VertexType[sentence->vertexCount];
	if (!vertices)
	{
		return false;
	}

	// Initialize vertex array to zeros at first.
	memset(vertices, 0, (sizeof(VertexType) * sentence->vertexCount));

	//Calculate the starting location to draw the sentence on the screen at.

		// Calculate the X and Y pixel position on the screen to start drawing to.
		drawX = (float)(((m_screenWidth / 2) * -1) + positionX);
	drawY = (float)((m_screenHeight / 2) - positionY);

	//Build the vertex array using the FontClass and the sentence information.

		// Use the font class to build the vertex array from the sentence text and sentence draw location.
		m_Font->BuildVertexArray((void*)vertices, text, drawX, drawY);

	//Copy the vertex array information into the sentence vertex buffer.

		// Lock the vertex buffer so it can be written to.
		result = deviceContext->Map(sentence->vertexBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	if (FAILED(result))
	{
		return false;
	}

	// Get a pointer to the data in the vertex buffer.
	verticesPtr = (VertexType*)mappedResource.pData;

	// Copy the data into the vertex buffer.
	memcpy(verticesPtr, (void*)vertices, (sizeof(VertexType) * sentence->vertexCount));

	// Unlock the vertex buffer.
	deviceContext->Unmap(sentence->vertexBuffer, 0);

	// Release the vertex array as it is no longer needed.
	delete[] vertices;
	vertices = 0;

	return true;
}

//ReleaseSentence is used to release the sentence vertex and index buffer as well as the sentence itself.

void Text::ReleaseSentence(SentenceType** sentence)
{
	if (*sentence)
	{
		// Release the sentence vertex buffer.
		if ((*sentence)->vertexBuffer)
		{
			(*sentence)->vertexBuffer->Release();
			(*sentence)->vertexBuffer = 0;
		}

		// Release the sentence index buffer.
		if ((*sentence)->indexBuffer)
		{
			(*sentence)->indexBuffer->Release();
			(*sentence)->indexBuffer = 0;
		}

		// Release the sentence.
		delete *sentence;
		*sentence = 0;
	}

	return;
}

void Text::SetBuffer(ID3D11DeviceContext * pDeviceContext, SentenceType* sentence)
{
	unsigned int stride, offset;
	XMFLOAT4 pixelColor;
	// Set vertex buffer stride and offset.
	stride = sizeof(VertexType);
	offset = 0;

	// Set the vertex buffer to active in the input assembler so it can be rendered.
	pDeviceContext->IASetVertexBuffers(0, 1, &sentence->vertexBuffer, &stride, &offset);

	// Set the index buffer to active in the input assembler so it can be rendered.
	pDeviceContext->IASetIndexBuffer(sentence->indexBuffer, DXGI_FORMAT_R32_UINT, 0);

	// Set the type of primitive that should be rendered from this vertex buffer, in this case triangles.
	pDeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	
	ID3D11ShaderResourceView** Temp = m_Font->GetTexture();
	for (int i = 0; i < 1; ++i)
	{
		m_FontShader->SetShaderResource(i, 1, &Temp[0]);
	}
}

//The RenderSentence function puts the sentence vertex and index buffer on the input assembler and then calls the FontShaderClass object to draw the sentence that was given as input to this function.Notice that we use the m_baseViewMatrix instead of the current view matrix.This allows us to draw text to the same location on the screen each frame regardless of where the current view may be.Likewise we use the orthoMatrix instead of the regular projection matrix since this should be drawn using 2D coordinates.

bool Text::RenderSentence(ID3D11DeviceContext* pContext, XMMATRIX& mWorld, SentenceType* sentence)
{
	XMFLOAT4 color = XMFLOAT4(sentence->red, sentence->green, sentence->blue, 1);
	m_FontShader->UpdateMappedResource(mWorld, pContext, color);
	// Render the text using the font shader.
	m_FontShader->Render();

	pContext->DrawIndexed(sentence->indexCount, 0, 0);

	return true;
}

bool Text::SetMousePosition(int mouseX, int mouseY, ID3D11DeviceContext* deviceContext)
{
	char tempString[16]; 
	char mouseString[16]; 
	bool result; 
	// Convert the mouseX integer to string format. 
	_itoa_s(mouseX, tempString, 10); 
	// Setup the mouseX string. 
	strcpy_s(mouseString, "Mouse X: "); 
	strcat_s(mouseString, tempString); 
	// Update the sentence vertex buffer with the new string information. 
	result = UpdateSentence(m_sentence[0], mouseString, 20, 20, 1.0f, 1.0f, 1.0f, deviceContext); 
	if(!result) { return false; } 
	// Convert the mouseY integer to string format. 
	_itoa_s(mouseY, tempString, 10); 
	// Setup the mouseY string. 
	strcpy_s(mouseString, "Mouse Y: "); 
	strcat_s(mouseString, tempString); 
	// Update the sentence vertex buffer with the new string information. 
	result = UpdateSentence(m_sentence[1], mouseString, 20, 40, 1.0f, 1.0f, 1.0f, deviceContext); 
	if(!result) { return false; } 
	return true; 
}

bool Text::SetFPS(float FPS, ID3D11DeviceContext * deviceContext)
{
	char tempString[16];
	char fpsString[16];
	float red, green, blue;
	bool result;


	// Truncate the fps to below 10,000.
	if (FPS > 9999)
	{
		FPS = 9999;
	}

	// Convert the fps integer to string format.
	//_itoa_s(FPS, tempString, 10);
	sprintf_s(tempString, sizeof(tempString),"%.2f", FPS);

	// Setup the fps string.
	strcpy_s(fpsString, "Fps: ");
	strcat_s(fpsString, tempString);

	// If fps is 60 or above set the fps color to green.
	if (FPS >= 60)
	{
		red = 0.0f;
		green = 1.0f;
		blue = 0.0f;
	}

	// If fps is below 60 set the fps color to yellow.
	if (FPS < 60)
	{
		red = 1.0f;
		green = 1.0f;
		blue = 0.0f;
	}

	// If fps is below 30 set the fps color to red.
	if (FPS < 30)
	{
		red = 1.0f;
		green = 0.0f;
		blue = 0.0f;
	}

	// Update the sentence vertex buffer with the new string information.
	result = UpdateSentence(m_sentence[0], fpsString, 20, 20, red, green, blue, deviceContext);
	if (!result)
	{
		return false;
	}

	return true;
}

bool Text::SetCPU(int CPU, ID3D11DeviceContext * deviceContext)
{
	char tempString[16];
	char cpuString[16];
	bool result;


	// Convert the cpu integer to string format.
	_itoa_s(CPU, tempString, 10);

	// Setup the cpu string.
	strcpy_s(cpuString, "Cpu: ");
	strcat_s(cpuString, tempString);
	strcat_s(cpuString, "%");

	// Update the sentence vertex buffer with the new string information.
	result = UpdateSentence(m_sentence[1], cpuString, 20, 40, 0.0f, 1.0f, 0.0f, deviceContext);
	if (!result)
	{
		return false;
	}

	return true;
}

