#pragma once
#include <D3D11.h>

class Shader;
class TextureArray;
class UIModel
{
public:
	enum E_TEXTURETYPE
	{
		E_DEFAULT,
		E_SECOND,
		E_MAX,
	};

	struct VertexType
	{
		XMFLOAT4 Position;
		XMFLOAT2 Texcoord;
	};
private:
	ID3D11Buffer*                       VertexBuffer;
	ID3D11Buffer*                       IndexBuffer;
	Shader*							Shader;
	int										 IndexLength;

	ID3D11ShaderResourceView	*		m_arTexture[16];
	TextureArray* TextureArray;
	bool UseAlphablend;
	int m_screenWidth, m_screenHeight;
	int m_bitmapWidth, m_bitmapHeight;
	float reviousPosX, reviousPosY;
	int VertexCount;
	int IndexCount;
private:
	HRESULT loadTexture(ID3D11Device* pD3dDevice, const char **pSzModelNameArray, int texCount);
	void UpdateBuffer(float PositionX, float PositionY, ID3D11DeviceContext* pDeviceContext);
	void SetBuffer(ID3D11DeviceContext* pDeviceContext);
	void RenderBuffer(const XMMATRIX& matWorld, ID3D11DeviceContext* pDeviceContext);
public:
	UIModel();
	void SetShader(Shader* pShader);
	HRESULT Load(ID3D11Device* pD3dDevice, const char *pSzModelName, int screenWidth, int screenHeight, int bitmapWidth, int bitmapHeight,
		const char** texNameArray, int texCount);
	void RenderSet(ID3D11DeviceContext*& pDeviceContext);
	void Draw(ID3D11DeviceContext*& pDeviceContext, const XMMATRIX &matWorld, float positionX, float positonY, int texIdx);
	void Release();
}; 

#pragma once
