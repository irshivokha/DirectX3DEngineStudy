#pragma once
#include <DirectXMath.h>
#include "Engine.h"
//#include <xnamath.h>

using namespace DirectX;

namespace Engine
{
	namespace Math
	{				
		struct Vector2 :public XMFLOAT2
		{
		public:

			explicit Vector2() = default;
			~Vector2()
			{

			}
			Vector2(const Vector2& source) = default;

			Vector2(float x, float y)
				:XMFLOAT2(x, y)
			{

			}

			//Vector3& operator=(const Vector3& rhs)
			//{
			//	XMFLOAT3::operator=(rhs);
			//	x = rhs.x;
			//	y = rhs.y;
			//	z = rhs.z;

			//	return *this;
			//}

			inline const Vector2 operator+(const Vector2& rhs)
			{
				x += rhs.x;
				y += rhs.y;				
				return Vector2(x + rhs.x, y + rhs.y);
			}
		};

		struct Vector3 :public XMFLOAT3
		{
		public:
			
			explicit Vector3() = default;
			~Vector3()
			{

			}
			Vector3(const Vector3& source) = default;

			Vector3(float x, float y, float z)
				:XMFLOAT3(x,y,z)
			{

			}

			//Vector3& operator=(const Vector3& rhs)
			//{
			//	XMFLOAT3::operator=(rhs);
			//	x = rhs.x;
			//	y = rhs.y;
			//	z = rhs.z;

			//	return *this;
			//}

			inline const Vector3 operator+(const Vector3& rhs)
			{
				return Vector3(x + rhs.x, y + rhs.y, z + rhs.z);
			}

			inline const Vector3 operator-(const Vector3& rhs)
			{
				return Vector3(x - rhs.x, y - rhs.y, z - rhs.z);
			}

			void Normalize();


			friend Vector3 operator*(float lhs, const Vector3& rhs);
			friend Vector3 operator+(const Vector3& lhs, const Vector3& rhs);
		};

		//Vector3 관련 전역 연산자 오버로딩 함수들
		Vector3 operator*(float lhs, const Vector3& rhs);
		Vector3 operator+(const Vector3& lhs, const Vector3& rhs);

		Vector3 Vector3Cross(const Vector3& lhs, const Vector3& rhs);
		float Vector3Dot(const Vector3& lhs, const Vector3& rhs);

		struct Vector4 :public XMFLOAT4
		{
		public:
			explicit Vector4() = default;
			~Vector4()
			{

			}
			Vector4(const Vector4& source) = default;

			Vector4(float x, float y, float z, float w) :
				XMFLOAT4(x, y, z, w)
			{

			}

			//Vector3& operator=(const Vector3& rhs)
			//{
			//	XMFLOAT3::operator=(rhs);
			//	x = rhs.x;
			//	y = rhs.y;
			//	z = rhs.z;

			//	return *this;
			//}

			inline const Vector4 operator+(const Vector4& rhs)
			{
				return Vector4(x + rhs.x, y + rhs.y, z + rhs.z, w + rhs.w);
			}

			//Vector4& operator*(const Matrix& mat);
		};

		class Matrix :public XMFLOAT4X4
		{
		public:
			Matrix():XMFLOAT4X4(){}
			Matrix(float m00, float m01, float m02, float m03,
				float m10, float m11, float m12, float m13,
				float m20, float m21, float m22, float m23,
				float m30, float m31, float m32, float m33)
				:XMFLOAT4X4(m00, m01, m02, m03,
							m10, m11, m12, m13,
							m20, m21, m22, m23,
							m30, m31, m32, m33)
			{
			}
			Matrix(float* elementArr)
				:XMFLOAT4X4(elementArr)
			{
			}
			Matrix(const Matrix& src) = default;
			Matrix operator*(const Matrix& rhs);

			//static func
			static Matrix Identity();
			static Matrix MatrixLookAtLH(const Vector3& eye, const Vector3& at, const Vector3& up);
			static Matrix MatrixPerpectiveFovLH(float FovAngleY, float aspectRatio, float nearZ, float farZ);
			static Matrix MatrixOrthographicLH(float width, float height, float nearZ, float farZ);
			static Matrix MatrixTranspose(const Matrix& mat);
			static Matrix MatrixScaling(const Vector3& scale);
			static Matrix MatrixTranslation(const Vector3& offset);
			static Matrix MatrixRotationPitchYawRoll(float xrad, float yrad , float zrad);	//unit : radian
			static Matrix MatrixRotationPitchYawRollDegree(float xDegree, float yDegree, float ZDegree);
			static void MatrixDecompose(Vector3* outScale, Vector3* outRot, Vector3* outTrans, const Matrix& inMat);
			static Matrix MatrixRotationAxis(Vector3 axis, float angle);
			static Matrix MatrixRotationY(float angle);
			static Vector3 Transform(const Vector3& src, const Matrix& mat);
			static Vector3 TransformNormal(const Vector3& src, const Matrix& mat);
			static Vector3 TransformCoord(const Vector3& src, const Matrix& mat);
			const Matrix& Inverse();


		};

		Matrix operator*(const Matrix& lhs, const Matrix& rhs);

		struct UInt4 :public XMINT4
		{
		public:
			explicit UInt4() = default;
			~UInt4()
			{

			}
			UInt4(const UInt4& source) = default;

			UInt4(UINT x, UINT y, UINT z, UINT w) :
				XMINT4(x, y, z, w)
			{

			}

			//Vector3& operator=(const Vector3& rhs)
			//{
			//	XMFLOAT3::operator=(rhs);
			//	x = rhs.x;
			//	y = rhs.y;
			//	z = rhs.z;

			//	return *this;
			//}

			UInt4& operator+(const UInt4& rhs)
			{
				x += rhs.x;
				y += rhs.y;
				z += rhs.z;
				w += rhs.w;
				return *this;				
			}

			//Vector4& operator*(const Matrix& mat);
		};

		//struct Byte4 :public XMBYTE4
		//{
		//public:
		//	explicit Byte4() = default;
		//	~Byte4()
		//	{

		//	}
		//	Byte4(const Byte4& source) = default;


		//	//Vector3& operator=(const Vector3& rhs)
		//	//{
		//	//	XMFLOAT3::operator=(rhs);
		//	//	x = rhs.x;
		//	//	y = rhs.y;
		//	//	z = rhs.z;

		//	//	return *this;
		//	//}

		//	//Vector4& operator*(const Matrix& mat);
		//};
	}

	using namespace Math;
}

