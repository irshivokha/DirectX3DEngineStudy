#include "EngineBaseHeader.h"
#include "customMath.h"
namespace Engine
{
	namespace Math
	{
		Matrix Matrix::Identity()
		{
			return Matrix(1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1);
		}
		Matrix Matrix::MatrixLookAtLH(const Vector3& eye, const Vector3& at, const Vector3& up)
		{			
			Matrix resultMat = Matrix::Identity();			
			XMStoreFloat4x4(&resultMat, DirectX::XMMatrixLookAtLH(XMLoadFloat3(&eye), XMLoadFloat3(&at), XMLoadFloat3(&up)));

			return resultMat;
		}
		Matrix Matrix::MatrixPerpectiveFovLH(float FovAngleY, float aspectRatio, float nearZ, float farZ)
		{
			Matrix resultMat = Matrix::Identity();
			XMStoreFloat4x4(&resultMat, DirectX::XMMatrixPerspectiveFovLH(FovAngleY, aspectRatio, nearZ, farZ));

			return resultMat;
		}
		Matrix Matrix::MatrixOrthographicLH(float width, float height, float nearZ, float farZ)
		{
			Matrix resultMat = Matrix::Identity();
			XMStoreFloat4x4(&resultMat, DirectX::XMMatrixOrthographicLH(width, height, nearZ, farZ));

			return resultMat;
		}
		Matrix Matrix::MatrixTranspose(const Matrix& mat)
		{			
			Matrix resultMat = Matrix::Identity();
			XMStoreFloat4x4(&resultMat, DirectX::XMMatrixTranspose(XMLoadFloat4x4(&mat)));
			return resultMat;
		}
		Matrix Matrix::MatrixScaling(const Vector3& scale)
		{
			Matrix mat;
			XMStoreFloat4x4(&mat,DirectX::XMMatrixScaling(scale.x, scale.y, scale.z));
			return mat;
		}
		Matrix Matrix::MatrixTranslation(const Vector3& offset)
		{
			Matrix mat = Matrix::Identity();
			mat.m[3][0] = offset.x;
			mat.m[3][1] = offset.y;
			mat.m[3][2] = offset.z;
			return mat;
		}
		Matrix Matrix::MatrixRotationPitchYawRoll(float xrad, float yrad, float zrad)
		{
			Matrix mat;
			XMStoreFloat4x4(&mat, DirectX::XMMatrixRotationRollPitchYaw(xrad, yrad, zrad));

			return mat;
			
		}
		Matrix Matrix::MatrixRotationPitchYawRollDegree(float xDegree, float yDegree, float zDegree)
		{
			Matrix mat;

			float xrad = xDegree * (DirectX::XM_PI / 180.0f);
			float yrad = yDegree * (DirectX::XM_PI / 180.0f);
			float zrad = zDegree * (DirectX::XM_PI / 180.0f);
			XMStoreFloat4x4(&mat, DirectX::XMMatrixRotationRollPitchYaw(xrad, yrad, zrad));
			
			return mat;			
		}

		void Matrix::MatrixDecompose(Vector3* outScale, Vector3* outRot, Vector3* outTrans, const Matrix& inMat)
		{
			XMVECTOR scale;
			XMVECTOR rot;
			XMVECTOR trans;
			XMMATRIX mat;
			mat = XMLoadFloat4x4(&inMat);
			DirectX::XMMatrixDecompose(&scale, &rot, &trans, mat);			
			XMStoreFloat3(outRot, rot);
			XMStoreFloat3(outScale, scale);
			XMStoreFloat3(outTrans, trans);

			return ;
		}

		Matrix Matrix::MatrixRotationAxis(Vector3 axis, float angle)
		{
			Matrix result;

			XMStoreFloat4x4(&result, XMMatrixRotationAxis(XMLoadFloat3(&axis), angle));

			return result;
		}

		Matrix Matrix::MatrixRotationY(float angle)
		{
			Matrix result; 
			XMStoreFloat4x4(&result, XMMatrixRotationY(angle));
			return result;
		}

		Vector3 Matrix::Transform(const Vector3& src, const Matrix& mat)
		{
			Vector3 result;

			XMStoreFloat3(&result, XMVector3Transform(XMLoadFloat3(&src), XMLoadFloat4x4(&mat)));
			return result;
		}

		Vector3 Matrix::TransformNormal(const Vector3& src, const Matrix& mat)
		{
			Vector3 result;

			XMStoreFloat3(&result,XMVector3TransformNormal(XMLoadFloat3(&src), XMLoadFloat4x4(&mat)));
			return result;
		}

		Vector3 Matrix::TransformCoord(const Vector3& src, const Matrix& mat)
		{
			Vector3 result;

			XMStoreFloat3(&result, XMVector3TransformCoord(XMLoadFloat3(&src), XMLoadFloat4x4(&mat)));
			return result;
		}



		const Matrix& Matrix::Inverse()
		{			
			XMMATRIX mat = XMLoadFloat4x4(this);
			XMMATRIX result;
			result = DirectX::XMMatrixInverse(nullptr, mat);

			XMStoreFloat4x4(this, result);
			return *this;
		}

		Matrix Matrix::operator*(const Matrix& rhs)
		{
			Matrix result = Matrix::Identity();
			result.m[0][0] = (m[0][0] * rhs.m[0][0]) + (m[0][1] * rhs.m[1][0]) + (m[0][2] * rhs.m[2][0]) + (m[0][3] * rhs.m[3][0]);
			result.m[0][1] = (m[0][0] * rhs.m[0][1]) + (m[0][1] * rhs.m[1][1]) + (m[0][2] * rhs.m[2][1]) + (m[0][3] * rhs.m[3][1]);
			result.m[0][2] = (m[0][0] * rhs.m[0][2]) + (m[0][1] * rhs.m[1][2]) + (m[0][2] * rhs.m[2][2]) + (m[0][3] * rhs.m[3][2]);
			result.m[0][3] = (m[0][0] * rhs.m[0][3]) + (m[0][1] * rhs.m[1][3]) + (m[0][2] * rhs.m[2][3]) + (m[0][3] * rhs.m[3][3]);
					  												  	  												  
			result.m[1][0] = (m[1][0] * rhs.m[0][0]) + (m[1][1] * rhs.m[1][0]) + (m[1][2] * rhs.m[2][0]) + (m[1][3] * rhs.m[3][0]);
			result.m[1][1] = (m[1][0] * rhs.m[0][1]) + (m[1][1] * rhs.m[1][1]) + (m[1][2] * rhs.m[2][1]) + (m[1][3] * rhs.m[3][1]);
			result.m[1][2] = (m[1][0] * rhs.m[0][2]) + (m[1][1] * rhs.m[1][2]) + (m[1][2] * rhs.m[2][2]) + (m[1][3] * rhs.m[3][2]);
			result.m[1][3] = (m[1][0] * rhs.m[0][3]) + (m[1][1] * rhs.m[1][3]) + (m[1][2] * rhs.m[2][3]) + (m[1][3] * rhs.m[3][3]);
					  												  	  												  
			result.m[2][0] = (m[2][0] * rhs.m[0][0]) + (m[2][1] * rhs.m[1][0]) + (m[2][2] * rhs.m[2][0]) + (m[2][3] * rhs.m[3][0]);
			result.m[2][1] = (m[2][0] * rhs.m[0][1]) + (m[2][1] * rhs.m[1][1]) + (m[2][2] * rhs.m[2][1]) + (m[2][3] * rhs.m[3][1]);
			result.m[2][2] = (m[2][0] * rhs.m[0][2]) + (m[2][1] * rhs.m[1][2]) + (m[2][2] * rhs.m[2][2]) + (m[2][3] * rhs.m[3][2]);
			result.m[2][3] = (m[2][0] * rhs.m[0][3]) + (m[2][1] * rhs.m[1][3]) + (m[2][2] * rhs.m[2][3]) + (m[2][3] * rhs.m[3][3]);
					  												  	  												  
			result.m[3][0] = (m[3][0] * rhs.m[0][0]) + (m[3][1] * rhs.m[1][0]) + (m[3][2] * rhs.m[2][0]) + (m[3][3] * rhs.m[3][0]);
			result.m[3][1] = (m[3][0] * rhs.m[0][1]) + (m[3][1] * rhs.m[1][1]) + (m[3][2] * rhs.m[2][1]) + (m[3][3] * rhs.m[3][1]);
			result.m[3][2] = (m[3][0] * rhs.m[0][2]) + (m[3][1] * rhs.m[1][2]) + (m[3][2] * rhs.m[2][2]) + (m[3][3] * rhs.m[3][2]);
			result.m[3][3] = (m[3][0] * rhs.m[0][3]) + (m[3][1] * rhs.m[1][3]) + (m[3][2] * rhs.m[2][3]) + (m[3][3] * rhs.m[3][3]);
								
			return result;
		}

        Vector3 operator*(float lhs, const Vector3& rhs)
        {
			Vector3 result;
			result.x = rhs.x * lhs;
			result.y = rhs.y * lhs;
			result.z = rhs.z * lhs;
			return result;
        }

		Vector3 operator+(const Vector3& lhs, const Vector3& rhs)
		{
			Vector3 result;
			result.x = lhs.x + rhs.x;
			result.y = lhs.y + rhs.y;
			result.z = lhs.z + rhs.z;

			return result;
		}

		Vector3 Vector3Cross(const Vector3& lhs, const Vector3& rhs)
		{
			Vector3 result;
			XMStoreFloat3(&result, XMVector3Cross(XMLoadFloat3(&lhs), XMLoadFloat3(&rhs)));
			return result;
		}

		float Vector3Dot(const Vector3& lhs, const Vector3& rhs)
		{
			float result = 0;
			result = XMVectorGetX(XMVector3Dot(XMLoadFloat3(&lhs), XMLoadFloat3(&rhs)));
			return result;
		}

        Matrix operator*(const Matrix& lhs, const Matrix& rhs)
		{
			Matrix result = Matrix::Identity();
			result.m[0][0] = (lhs.m[0][0] * rhs.m[0][0]) + (lhs.m[0][1] * rhs.m[1][0]) + (lhs.m[0][2] * rhs.m[2][0]) + (lhs.m[0][3] * rhs.m[3][0]);
			result.m[0][1] = (lhs.m[0][0] * rhs.m[0][1]) + (lhs.m[0][1] * rhs.m[1][1]) + (lhs.m[0][2] * rhs.m[2][1]) + (lhs.m[0][3] * rhs.m[3][1]);
			result.m[0][2] = (lhs.m[0][0] * rhs.m[0][2]) + (lhs.m[0][1] * rhs.m[1][2]) + (lhs.m[0][2] * rhs.m[2][2]) + (lhs.m[0][3] * rhs.m[3][2]);
			result.m[0][3] = (lhs.m[0][0] * rhs.m[0][3]) + (lhs.m[0][1] * rhs.m[1][3]) + (lhs.m[0][2] * rhs.m[2][3]) + (lhs.m[0][3] * rhs.m[3][3]);

			result.m[1][0] = (lhs.m[1][0] * rhs.m[0][0]) + (lhs.m[1][1] * rhs.m[1][0]) + (lhs.m[1][2] * rhs.m[2][0]) + (lhs.m[1][3] * rhs.m[3][0]);
			result.m[1][1] = (lhs.m[1][0] * rhs.m[0][1]) + (lhs.m[1][1] * rhs.m[1][1]) + (lhs.m[1][2] * rhs.m[2][1]) + (lhs.m[1][3] * rhs.m[3][1]);
			result.m[1][2] = (lhs.m[1][0] * rhs.m[0][2]) + (lhs.m[1][1] * rhs.m[1][2]) + (lhs.m[1][2] * rhs.m[2][2]) + (lhs.m[1][3] * rhs.m[3][2]);
			result.m[1][3] = (lhs.m[1][0] * rhs.m[0][3]) + (lhs.m[1][1] * rhs.m[1][3]) + (lhs.m[1][2] * rhs.m[2][3]) + (lhs.m[1][3] * rhs.m[3][3]);

			result.m[2][0] = (lhs.m[2][0] * rhs.m[0][0]) + (lhs.m[2][1] * rhs.m[1][0]) + (lhs.m[2][2] * rhs.m[2][0]) + (lhs.m[2][3] * rhs.m[3][0]);
			result.m[2][1] = (lhs.m[2][0] * rhs.m[0][1]) + (lhs.m[2][1] * rhs.m[1][1]) + (lhs.m[2][2] * rhs.m[2][1]) + (lhs.m[2][3] * rhs.m[3][1]);
			result.m[2][2] = (lhs.m[2][0] * rhs.m[0][2]) + (lhs.m[2][1] * rhs.m[1][2]) + (lhs.m[2][2] * rhs.m[2][2]) + (lhs.m[2][3] * rhs.m[3][2]);
			result.m[2][3] = (lhs.m[2][0] * rhs.m[0][3]) + (lhs.m[2][1] * rhs.m[1][3]) + (lhs.m[2][2] * rhs.m[2][3]) + (lhs.m[2][3] * rhs.m[3][3]);

			result.m[3][0] = (lhs.m[3][0] * rhs.m[0][0]) + (lhs.m[3][1] * rhs.m[1][0]) + (lhs.m[3][2] * rhs.m[2][0]) + (lhs.m[3][3] * rhs.m[3][0]);
			result.m[3][1] = (lhs.m[3][0] * rhs.m[0][1]) + (lhs.m[3][1] * rhs.m[1][1]) + (lhs.m[3][2] * rhs.m[2][1]) + (lhs.m[3][3] * rhs.m[3][1]);
			result.m[3][2] = (lhs.m[3][0] * rhs.m[0][2]) + (lhs.m[3][1] * rhs.m[1][2]) + (lhs.m[3][2] * rhs.m[2][2]) + (lhs.m[3][3] * rhs.m[3][2]);
			result.m[3][3] = (lhs.m[3][0] * rhs.m[0][3]) + (lhs.m[3][1] * rhs.m[1][3]) + (lhs.m[3][2] * rhs.m[2][3]) + (lhs.m[3][3] * rhs.m[3][3]);

			return result;
		}

        void Vector3::Normalize()
        {
			XMVECTOR vec = DirectX::XMLoadFloat3(this);
			vec = DirectX::XMVector3Normalize(vec);
			DirectX::XMStoreFloat3(this, vec);

			return;
        }

        //Vector4& Vector4::operator*(const Matrix& mat)
		//{
		//	x = x * mat.m[0][0] + y * mat.m[1][0] + z * mat.m[2][0] + w * mat.m[3][0];
		//	y = y * mat.m[0][1] + y * mat.m[1][1] + z * mat.m[2][1] + w * mat.m[3][1];
		//	z = z * mat.m[0][2] + y * mat.m[1][2] + z * mat.m[2][2] + w * mat.m[3][2];
		//	w = x * mat.m[0][3] + y * mat.m[1][3] + z * mat.m[2][3] + w * mat.m[3][3];
		//}
	}

}


