#pragma once
//#include "../Main/gameMode.h"
#include <memory>
#include "RenderClasses/DefaultRenderer.h"

class GameMode;

namespace Engine
{
	class GameObjectManager;
	class ShaderManager;		
	
	class GraphicsEngine
	{
	private:
		//ShaderManager* ShaderMgr;
		std::shared_ptr<GameObjectManager> GameObjMgr;	//통일성을 위해서 얘도 singleTon으로 해야하나?
		//C_ZONE* Zone;
		//UIMgr* UIMgr;
		//C_MD5LOADER* Md5Loader;
		DefaultRenderer Renderer;

		//void UpdateSysInfo(GameMode::SysInfo* sInfo);

	public:
		GraphicsEngine();
		~GraphicsEngine();
		void Init(HWND hWnd);
		void Release();
		void UpdateScene();		//GameMode::SysInfo* sInfo
		bool IsExit();
		void StartScripts();
		void UpdateScripts();
	};
}

