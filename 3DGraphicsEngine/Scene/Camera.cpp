#include "EngineBaseHeader.h"
#include "Camera.h"
#include "RenderClasses/ConstantBufferManager.h"
#include "RenderClasses/DXUtility.h"
#include "Input/directinput.h"
#include "EngineNamespace.h"
#include "System/Timer.h"

namespace Engine
{      
	Camera::Camera()
		:preMouseX(0),
		preMouseY(0),
		Pos(0,0.1,-3),
		Right(1,0,0),
		Up(0,1,0),
		Look(0,0,1),
		cameraMoveLock(false)
	{
		ViewMat = Math::Matrix::Identity();
	}

	void Camera::Init()
	{
		Update();		
	}

	const Math::Matrix& Camera::GetViewMatrix()
	{
		return ViewMat;
	}

	void Camera::Update()
	{
		if (InputCenter::GetInstance())
		{

			//마우스 우클릭시
			if (InputCenter::GetInstance()->IsButtonDown(MOUSE_RIGHT))
			{			
				cameraMoveLock = true;
			}
			if (InputCenter::GetInstance()->IsMouseMove())
			{
				if (cameraMoveLock)
				{
					Math::Vector3 mouseDelta = InputCenter::GetInstance()->GetMouseMove();
					int screenWidth = 0;
					int screenHeight = 0;
					DXUtility::GetInstance()->GetDeviceWidthHeight(screenWidth, screenHeight);
					

					float deltaX = mouseDelta.x / (float)screenWidth;
					float deltaY = mouseDelta.y / (float)screenHeight;

					//float deltaX = mouseDelta.x * 0.001f;
					//float deltaY = mouseDelta.y * 0.001f;


					//Matrix rMat = Matrix::MatrixRotationAxis(Right, deltaY * 0.4f);
					//Look = Matrix::TransformCoord(Vector3(0, 0, 1), rMat);
					//Look.Normalize();

					//Matrix yRotMat = Matrix::MatrixRotationAxis(Vector3(0, 1, 0), deltaX);
					//Matrix yRotMat = Matrix::MatrixRotationY(m_fYaw);
					//Right = Matrix::TransformCoord(Vector3(1, 0, 0), yRotMat);
					//Up = Matrix::TransformCoord(Up, yRotMat);
					//Up.Normalize();


					//Pos += moveLeftRight * camRight;
					//camPosition += moveBackForward * camForward;

					//Foward = Matrix::TransformCoord(Vector3(0, 0, 1), yRotMat);

					//x축 회전

					if (deltaY != 0)
					{
						Matrix xRotMat = Matrix::MatrixRotationAxis(Right, deltaY);
						Up = Matrix::TransformNormal(Up, xRotMat);
						Look = Matrix::TransformNormal(Look, xRotMat);
					}

					//y축 회전
					if (deltaX != 0)
					{
						Matrix yRotMat = Matrix::MatrixRotationY(deltaX);

						Right = Matrix::TransformNormal(Right, yRotMat);
						Up = Matrix::TransformNormal(Up, yRotMat);
						Look = Matrix::TransformNormal(Look, yRotMat);
					}					


				}


			}
			if (InputCenter::GetInstance()->IsButtonUp(MOUSE_RIGHT))
			{
				cameraMoveLock = false;
			}

			if (InputCenter::GetInstance()->IsKeyDown(DIK_W))
			{
				if (cameraMoveLock)
				{
					double dt = Timer::GetInstance().GetElapsedTime();
					
					Pos = Pos + ((float)dt * Look);
				}
			}

			if (InputCenter::GetInstance()->IsKeyDown(DIK_S))
			{
				if (cameraMoveLock)
				{
					double dt = Timer::GetInstance().GetElapsedTime();

					Pos = Pos - ((float)dt * Look);
				}
			}

			if (InputCenter::GetInstance()->IsKeyDown(DIK_A))
			{
				if (cameraMoveLock)
				{
					double dt = Timer::GetInstance().GetElapsedTime();
					//float delta = 0.001f;

					Pos = Pos - ((float)dt * Right);
				}
			}

			if (InputCenter::GetInstance()->IsKeyDown(DIK_D))
			{
				if (cameraMoveLock)
				{
					double dt = Timer::GetInstance().GetElapsedTime();
					//float delta = 0.001f;

					Pos = Pos + ((float)dt * Right);
				}
			}

			UpdateViewMatrix();
		}
				
	}

	void Camera::UpdateViewMatrix()
	{
		Look.Normalize();

		//보정된 새 상향벡터를 계산해서 정규화.
		Up = Vector3Cross(Look, Right);
		Up.Normalize();

		//보정된 오른쪽 벡터를 계산한다. 상향/시선벡터가 정규직교이므로 외적 다시 정규화필요 x
		Right = Vector3Cross(Up, Look);
		//Right.Normalize();

		float x = -Math::Vector3Dot(Pos, Right);
		float y = -Math::Vector3Dot(Pos, Up);
		float z = -Math::Vector3Dot(Pos, Look);
		ViewMat(0, 0) = Right.x;
		ViewMat(1, 0) = Right.y;
		ViewMat(2, 0) = Right.z;
		ViewMat(3, 0) = x;

		ViewMat(0, 1) = Up.x;
		ViewMat(1, 1) = Up.y;
		ViewMat(2, 1) = Up.z;
		ViewMat(3, 1) = y;

		ViewMat(0, 2) = Look.x;
		ViewMat(1, 2) = Look.y;
		ViewMat(2, 2) = Look.z;
		ViewMat(3, 2) = z;

		ViewMat(0, 3) = 0.0f;
		ViewMat(1, 3) = 0.0f;
		ViewMat(2, 3) = 0.0f;
		ViewMat(3, 3) = 1.0;
	}

	void Camera::UpdateConstBuff()
	{
		//D3D11_MAPPED_SUBRESOURCE* camMapeedResource;
		//camMapeedResource = ConstantBufferManager::GetInstance().LockConstantBuffer(ConstantBufferManager::ConstBufferType::perCamera);
		ID3D11Buffer* cameraBuff;
		cameraBuff = *ConstantBufferManager::GetInstance().GetBuffer(ConstantBufferManager::ConstBufferType::perCamera);		
		ConstantBufferManager::CameraConstBuffFormat camBuffFormat;
		camBuffFormat.View = Math::Matrix::MatrixTranspose(ViewMat);
		camBuffFormat.Proj = Math::Matrix::MatrixTranspose(DXUtility::GetInstance()->GetProjectionMatrix());
		camBuffFormat.CamPos = Math::Vector4(Pos.x, Pos.y, Pos.z, 0);
		camBuffFormat.minMaxDistance = Math::Vector4(0.01, 4.1, 0, 0);
		camBuffFormat.minMaxLOD = Math::Vector4(1, 6, 0, 0);
		//cam
		D3D11_MAPPED_SUBRESOURCE camMappedResource;
		DXUtility::GetInstance()->GetDeviceContext()->Map(cameraBuff, 0, D3D11_MAP_WRITE_DISCARD, 0, &camMappedResource);
		memcpy(camMappedResource.pData, &camBuffFormat, sizeof(ConstantBufferManager::CameraConstBuffFormat));
		DXUtility::GetInstance()->GetDeviceContext()->Unmap(cameraBuff, 0);


		//ConstantBufferManager::CameraConstBuffFormat camBuffFormat;
		//camBuffFormat.View = Math::Matrix::MatrixTranspose(ViewMat);
		//camBuffFormat.Proj = Math::Matrix::MatrixTranspose(DXUtility::GetInstance()->GetProjectionMatrix());
		//memcpy(&camMapeedResource->pData, &camBuffFormat, sizeof(ConstantBufferManager::CameraConstBuffFormat));
		//ConstantBufferManager::GetInstance().UnLockConstantBuffer(ConstantBufferManager::ConstBufferType::perCamera);


	}

}



//const XMMATRIX& C_CAMERA::GetReflectionMat()
//{
//	return m_matReflectView;
//}
//
//void C_CAMERA::MoveF(float fElapsedTime)
//{
//	m_vPos += m_vDir * fElapsedTime * 10.0f;
//}
//
//void C_CAMERA::MoveB(float fElapsedTime)
//{
//	m_vPos -= m_vDir * fElapsedTime * 10.0f;
//}
//
//void C_CAMERA::Rotate(float fYaw, float fPitch, float fRoll, float fElapsedTime)
//{
//	m_fYaw += fYaw * fElapsedTime;
//	adjustRid(m_fYaw);
//	m_fPitch += fPitch * fElapsedTime;
//	adjustRid(m_fPitch);
//	m_fRoll += fRoll * fElapsedTime;
//	adjustRid(m_fRoll);
//
//	m_matRot = XMMatrixRotationRollPitchYaw(m_fPitch, m_fYaw, m_fRoll);
//
//	m_vDir = XMVector3TransformCoord(m_vZ, m_matRot);
//}
//

//
//void C_CAMERA::RenderReflection(float height)
//{
//	XMFLOAT3 f3Pos;
//	XMFLOAT3 f3Up;
//	XMFLOAT3 LookAt;
//	XMStoreFloat3(&f3Pos, m_vPos);
//
//	XMVECTOR vDeterminant;
//	XMMATRIX matInv;
//
//	matInv = m_matRot;
//	matInv._41 = f3Pos.x;
//	matInv._42 = f3Pos.y;
//	matInv._43 = f3Pos.z;
//
//	matInv._42 = -f3Pos.y + (height * 2.0f);
//
//	m_matReflectView = XMMatrixInverse(&vDeterminant, matInv);
//
//	m_f4ReflectedPos = XMFLOAT4(f3Pos.x, -f3Pos.y + (height * 2.0f), f3Pos.z, 1.0f);
//
//
//}
//
//void C_CAMERA::adjustRid(float& fRid)
//{
//	if (fRid < 0.0f)
//		fRid += XM_2PI;
//	else if (fRid > XM_2PI)
//		fRid -= XM_2PI;
//}
//
//const XMFLOAT4* C_CAMERA::GetPos()
//{
//	return &m_f4Pos;
//}
//
//const XMFLOAT4* C_CAMERA::GetReflectedPos()
//{
//	return &m_f4ReflectedPos;
//}
//
//const XMVECTOR* C_CAMERA::GetDir()
//{
//	return &m_vDir;
//}
//
//void* C_CAMERA::operator new(size_t nSize)
//{
//#ifdef	_DECLSPEC_ALIGN_16_
//	return _aligned_malloc(nSize, 16);
//#else
//	return malloc(nSize);
//#endif
//}
//
//void C_CAMERA::operator delete(void* p)
//{
//#ifdef	_DECLSPEC_ALIGN_16_
//	return _aligned_free(p);
//#else
//	return free(p);
//#endif
//}
