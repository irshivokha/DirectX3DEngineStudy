#pragma once
#include "DxMath/customMath.h"
#include "Engine.h"
namespace Engine
{
	class Light
	{
	private:
		Math::Vector3 LightPos[100];
		Math::Vector3 LightDir[100];
		Math::Vector4 LightColor[100];
		unsigned int GlobalLightCount;
		static const int MaxGloablLightCount;
		//XMMATRIX	m_matReflectView;
		//XMVECTOR	m_vZ;

		//Vector4 
		//XMVECTOR    m_vPos;
		//XMVECTOR    m_vDir;

		//XMMATRIX	m_matRot;
		//XMFLOAT4	m_f4Pos;
		//XMFLOAT4	m_f4ReflectedPos;

		//XMFLOAT3	m_f3Dir;
	private:

		//void		adjustRid(float& fRid);

	public:
		static void* operator new (size_t nSize);
		static void operator delete (void* p);
		Light();
		void Init();		
		//const XMMATRIX& GetReflectionMat();
		//void MoveF(float fElapsedTime);
		//void MoveB(float fElapsedTime);
		//void Rotate(float fYaw, float fPitch, float fRoll, float fElapsedTime);
		//Camera가 여러개 물려있을수 있기에 카메라 갯수만큼 반복하면서 object를 그리고 const buffer를 업데이트 하기.
		void Update();
		void UpdateConstBuff();
		//void RenderReflection(float height);
		//const XMFLOAT4* GetPos();
		//const XMFLOAT4* GetReflectedPos();
		//const XMVECTOR* GetDir();
	};
}
