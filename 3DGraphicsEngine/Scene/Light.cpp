#include "EngineBaseHeader.h"
#include "Light.h"
#include "RenderClasses/ConstantBufferManager.h"
#include "RenderClasses/DXUtility.h"
#include "DxMath/customMath.h"


namespace Engine
{
	using namespace Math;
	const int Light::MaxGloablLightCount = 100;
	Light::Light():
		GlobalLightCount(0)
	{		
	}

	void Light::Update()
	{
		//rotation을 어떻게 적용할 것인가? 예제 찾아보기..
		LightPos[0] = Vector3(5, 5, 0);
		LightDir[0] = Vector3(-1, -1, 0);	//test code
		LightColor[0] = Vector4(1, 0, 0, 1);
		GlobalLightCount = 1;
	}

	void Light::UpdateConstBuff()
	{
		//D3D11_MAPPED_SUBRESOURCE* camMapeedResource;
		//camMapeedResource = ConstantBufferManager::GetInstance().LockConstantBuffer(ConstantBufferManager::ConstBufferType::perCamera);
		ID3D11Buffer* lightBuff;
		lightBuff = *ConstantBufferManager::GetInstance().GetBuffer(ConstantBufferManager::ConstBufferType::perLight);
		ConstantBufferManager::LightBuffFormat lightBuffFormat;
		for (UINT i = 0; i < GlobalLightCount; i++)
		{
			lightBuffFormat.LightPos[i] = Vector4(LightPos[i].x, LightPos[i].y, LightPos[i].z,0);
			lightBuffFormat.LightDir[i] = Vector4(LightDir[i].x, LightDir[i].y, LightDir[i].z,0);
			lightBuffFormat.LightColor[i] = LightColor[i];			
			
		}
		lightBuffFormat.LightCount = UInt4(GlobalLightCount,0,0,0);
		D3D11_MAPPED_SUBRESOURCE lightMappedResource;
		DXUtility::GetInstance()->GetDeviceContext()->Map(lightBuff, 0, D3D11_MAP_WRITE_DISCARD, 0, &lightMappedResource);
		memcpy(lightMappedResource.pData, &lightBuffFormat, sizeof(ConstantBufferManager::LightBuffFormat));
		DXUtility::GetInstance()->GetDeviceContext()->Unmap(lightBuff, 0);

	}

}



