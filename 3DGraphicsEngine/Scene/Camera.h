#pragma once
#include "DxMath/customMath.h"
#include "Engine.h"
namespace Engine
{
	class Camera
	{
	private:
		Math::Matrix ViewMat;
		Math::Vector3 Pos;
		Math::Vector3 Rot;		
		Math::Vector3 Dir;
		//XMMATRIX	m_matReflectView;
		//XMVECTOR	m_vZ;

		//Vector4 
		//XMVECTOR    m_vPos;
		//XMVECTOR    m_vDir;

		//XMMATRIX	m_matRot;
		//XMFLOAT4	m_f4Pos;
		//XMFLOAT4	m_f4ReflectedPos;

		//XMFLOAT3	m_f3Dir;

		float		m_fYaw;
		float		m_fPitch;
		float		m_fRoll;
		float		preMouseX;
		float		preMouseY;

		Math::Vector3 Right;
		Math::Vector3 Up;
		Math::Vector3 Look;
		bool cameraMoveLock;
	private:

		//void		adjustRid(float& fRid);

	public:
		static void* operator new (size_t nSize);
		static void operator delete (void* p);
		Camera();
		void Init();
		const Math::Matrix& GetViewMatrix();
		//const XMMATRIX& GetReflectionMat();
		//void MoveF(float fElapsedTime);
		//void MoveB(float fElapsedTime);
		//void Rotate(float fYaw, float fPitch, float fRoll, float fElapsedTime);
		//Camera가 여러개 물려있을수 있기에 카메라 갯수만큼 반복하면서 object를 그리고 const buffer를 업데이트 하기.
		void Update();
		void UpdateViewMatrix();
		void UpdateConstBuff();
		//void RenderReflection(float height);
		//const XMFLOAT4* GetPos();
		//const XMFLOAT4* GetReflectedPos();
		//const XMVECTOR* GetDir();
	};
}
