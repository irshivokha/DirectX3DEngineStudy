#pragma once

#define DIRECTINPUT_VERSION 0x0800
//static const int MOUSE_LEFT = 0;
//static const int MOUSE_RIGHT = 1;
#define MOUSE_LEFT 0
#define MOUSE_RIGHT 1

#include "UnCopyable.h"
#include <dinput.h>
#include "Engine.h"
#include "DxMath/customMath.h"

namespace Engine
{
	class InputCenter : public UnCopyable
	{

	private:
		LPDIRECTINPUT8          DI; // DirectInput interface       
		LPDIRECTINPUTDEVICE8    Mouse; // Device interface
		LPDIRECTINPUTDEVICE8    Keyboard;

		BYTE					KeyboardState[256];
		DIMOUSESTATE			MouseState;

	private:
		int ScreenWidth, ScreenHeight;
		int MouseX, MouseY;

		HRESULT CreateMouse(HWND hWnd);
		HRESULT CreateKeyboard(HWND hWnd);
		HRESULT	UpdateMouse();
		HRESULT UpdateKeyboard();
		void ProcessInput();
		static InputCenter* Instance;


		InputCenter();
		~InputCenter();

	public:
		static void CreateInstance();
		static InputCenter* const GetInstance();
		static void ReleaseInstance();
		HRESULT InitDirectInput(HWND hWnd, int ScreenWidth, int ScreenHeight);
		HRESULT UpdateInputState();
		VOID CleanUpDirectInput();
		BOOL IsKeyDown(BYTE byDIK);
		BOOL IsKeyUp(BYTE byDIK);
		BOOL IsButtonDown(BYTE byDIK);
		BOOL IsButtonUp(BYTE byDIK);
		Vector3 GetMouseMove();
		BOOL IsMouseMove();
		LONG const GetMouseXDelta() const;
		LONG const GetMouseYDelta() const;
		void GetMoustLocation(int& mouseX, int& mouseY) const;
	};
}

