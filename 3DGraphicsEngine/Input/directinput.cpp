#include "EngineBaseHeader.h"
#include "directinput.h"

namespace Engine
{
	InputCenter::InputCenter() :
		DI(nullptr),
		Mouse(nullptr),
		Keyboard(nullptr),
		KeyboardState(),
		MouseState(),
		MouseX(0),
		MouseY(0),
		ScreenWidth(0),
		ScreenHeight(0)
	{
	}

	InputCenter* InputCenter::Instance = nullptr;

	InputCenter::~InputCenter()
	{
	}

	HRESULT InputCenter::InitDirectInput(HWND hWnd, int screenWidth, int screenHeight)
	{
		ScreenWidth = screenWidth;
		ScreenHeight = screenHeight;

		if (FAILED(DirectInput8Create(GetModuleHandle(nullptr), DIRECTINPUT_VERSION,
			IID_IDirectInput8, (VOID**)&DI, nullptr)))
			return E_FAIL;

		if (FAILED(CreateMouse(hWnd)))
			return E_FAIL;
		if (FAILED(CreateKeyboard(hWnd)))
			return E_FAIL;

		return S_OK;
	}

	HRESULT InputCenter::CreateMouse(HWND hWnd)
	{
		if (FAILED(DI->CreateDevice(GUID_SysMouse, &Mouse, nullptr)))
			return E_FAIL;

		if (FAILED(Mouse->SetDataFormat(&c_dfDIMouse)))
			return E_FAIL;

		if (FAILED(Mouse->SetCooperativeLevel(hWnd, DISCL_FOREGROUND |
			DISCL_EXCLUSIVE)))
			return E_FAIL;

		return S_OK;
	}

	HRESULT InputCenter::CreateKeyboard(HWND hWnd)
	{
		if (FAILED(DI->CreateDevice(GUID_SysKeyboard, &Keyboard, nullptr)))
			return E_FAIL;

		if (FAILED(Keyboard->SetDataFormat(&c_dfDIKeyboard)))
			return E_FAIL;

		if (FAILED(Keyboard->SetCooperativeLevel(hWnd, DISCL_FOREGROUND | DISCL_EXCLUSIVE)))
			return E_FAIL;

		return S_OK;
	}

	HRESULT InputCenter::UpdateMouse()
	{
		HRESULT hr = Mouse->GetDeviceState(sizeof(DIMOUSESTATE), (LPVOID)&MouseState);
		if ((hr != DI_OK))
		{
			if ((hr == DIERR_INPUTLOST) || (hr == DIERR_NOTACQUIRED))
				Mouse->Acquire();
		}

		return hr;
	}

	HRESULT InputCenter::UpdateKeyboard()
	{
		HRESULT hr = Keyboard->GetDeviceState(sizeof(BYTE) * 256, (LPVOID)&KeyboardState);
		if ((hr != DI_OK))
		{
			if ((hr == DIERR_INPUTLOST) || (hr == DIERR_NOTACQUIRED))
				Keyboard->Acquire();
		}

		return hr;
	}

	void InputCenter::ProcessInput()
	{
		// Update the location of the mouse cursor based on the change of the mouse location during the frame. 
		MouseX += MouseState.lX; MouseY += MouseState.lY;
		// Ensure the mouse location doesn't exceed the screen width or height. 
		if (MouseX < 0) { MouseX = 0; }
		if (MouseY < 0) { MouseY = 0; }
		if (MouseX + 30 >= ScreenWidth) { MouseX = ScreenWidth - 30; }
		if (MouseY + 35 >= ScreenHeight) { MouseY = ScreenHeight - 35; }
		return;

	}

	HRESULT InputCenter::UpdateInputState()
	{
		UpdateMouse();
		UpdateKeyboard();
		ProcessInput();

		return S_OK;
	}

	VOID InputCenter::CleanUpDirectInput()
	{
		if (Mouse)
			Mouse->Unacquire();
		if (Keyboard)
			Keyboard->Unacquire();

		if (Mouse)
			Mouse->Release();
		if (Keyboard)
			Keyboard->Release();
		if (DI)
			DI->Release();
	}

	BOOL InputCenter::IsKeyDown(BYTE byDIK)
	{
		if (KeyboardState[byDIK] & 0x80)
			return TRUE;

		return FALSE;
	}

	BOOL InputCenter::IsKeyUp(BYTE byDIK)
	{
		if (!(KeyboardState[byDIK] & 0x80))
			return TRUE;

		return FALSE;
	}

	BOOL InputCenter::IsButtonDown(BYTE byDIK)
	{
		if (MouseState.rgbButtons[byDIK] & 0x80)
			return TRUE;
		return FALSE;
	}

	BOOL InputCenter::IsButtonUp(BYTE byDIK)
	{
		if (!(MouseState.rgbButtons[byDIK] & 0x80))
			return TRUE;
		return FALSE;
	}

	Vector3 InputCenter::GetMouseMove()
	{
		return Vector3((float)MouseState.lX, (float)MouseState.lY, (float)MouseState.lZ);
	}

	BOOL InputCenter::IsMouseMove()
	{
		DIMOUSESTATE currstate;

		Mouse->GetDeviceState(sizeof(DIMOUSESTATE), (LPVOID)&currstate);

		if (currstate.lX != MouseState.lX ||
			currstate.lY != MouseState.lY)
		{
			return true;
		}
			
		return false;
	}

	LONG const InputCenter::GetMouseXDelta() const
	{
		return MouseState.lX;
	}

	LONG const InputCenter::GetMouseYDelta() const
	{
		return MouseState.lY;
	}

	void InputCenter::GetMoustLocation(int& mouseX, int& mouseY) const
	{
		mouseX = MouseX;
		mouseY = MouseY;

		return;
	}

	void InputCenter::CreateInstance()
	{
		if (!Instance)
			Instance = new InputCenter();
	}

	InputCenter* const InputCenter::GetInstance()
	{
		return Instance;
	}

	void InputCenter::ReleaseInstance()
	{
		if (Instance)
		{
			delete Instance;
			Instance = nullptr;
		}

	}

}

