#pragma once
#include "EngineBaseHeader.h"
#include "DefineValues.h"

#include "Mesh.h"
#include "Resource/Material/Material.h"

namespace Engine
{  
		
	class ModelImporter
	{	
	private:
		/*
			메시와 머티리얼은 1대1관계가 아니다. 메시 여러개가 하나의 머티리얼을 참조하는 경우도 있다. 따라서 메시가 머티리얼을
			가지고 있게 하지말고 scene에서 읽어서 메시와 머티리얼을 매핑시켜야한다.
		*/
		MeshModelType ModelType;
		string Directory;
		aiScene* AssetScene;
	public:
		ModelImporter();		
		~ModelImporter();		

		//Model 로딩에 관련된 함수들
		GameObjPtr ImportMesh(const std::string& modelFileName);	//rootNode 반환
		GameObjPtr processNode(aiNode* node, const aiScene* scene);
		Mesh processMesh(aiMesh* mesh, const aiScene* scene);
		string processMaterial( const aiScene* scene, aiMesh* mesh);
		void loadMaterialTextures( shared_ptr<Material> engineMat, aiMaterial* mat, aiTextureType type, TextureType texType, const aiScene* scene);
		string determineTextureType(const aiScene* scene, aiMaterial* mat);
		int getTextureIndex(aiString* str);
		ID3D11ShaderResourceView* getTextureFromModel(const aiScene* scene, int textureindex);

	};
}

