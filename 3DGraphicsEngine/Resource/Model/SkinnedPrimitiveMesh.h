#pragma once
#include <vector>
#include <iostream>
#include "DefineValues.h"
#include "DxMath/customMath.h"
#include "Mesh.h"

namespace Engine
{
	struct KeyFrame
	{
		KeyFrame();

	public:
		float TimePos;
		Math::Vector3 Position;
		Math::Vector3 Scale;
		Math::Vector3 Rotation;
	};


	//시간에 따라 두 keyFrame을 보간한다.
	struct BoneAnimation
	{
		float GetStartTime() const;
		float GetEndTime() const;
		void Interpolate(float time, BonePtr bone, bool rootBone);

		std::vector<KeyFrame> KeyFrames;
	};

	struct AnimationClip
	{
	private:
		std::string ClipName;
		float StartTime;
		float EndTime;
		float PlayTime;
		std::vector<BoneAnimation> BoneAnimations;	//Bone마다 애니메이션이 있음		
		//std::vector<BonePtr> BoneTrans;
	public:
		AnimationClip();
		const float GetStartTime() const;
		const float GetEndTime() const;
		//void Interpolate(float time, BonePtr bone);
		void Update(std::vector<BonePtr>* bones);
		void SetClipName(const std::string& clipName);
		std::string& GetClipName();
		void AddBoneAnimation(const BoneAnimation& boneAnim);
		void UpdateStartEndTime();
		const float GetPlayTime();
		void SetPlayTime(float playTime);

	};

	class SkinnedPrimitiveMesh : public Mesh
	{
	private:
		std::vector<int> BoneHierarchy;
		//BonePtr RootBone;	//std::vector<XMFLOAT4X4> BoneOffsets;
		std::vector<BonePtr> Bones;
		std::map<std::string, AnimationClip> Animations;
		std::string ActiveAnimClip;
		AnimationClip* ActiveAnimation;
		bool IdleState;	//추후에는 AnimationState들을 enum같은 걸로 구분하여 구현하기

	public:
		SkinnedPrimitiveMesh();
		void Update();
		void SetBindPose();
		virtual bool ImportMesh(const std::string& modelFileName);
		void LoadAnimation(const std::string& modelFileName);
		virtual void UpdateConstBuffer(void* constBuffPtr) override;
	};
}

