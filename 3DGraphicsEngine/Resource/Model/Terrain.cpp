#include "EngineBaseHeader.h"
#include "Terrain.h"
#include "GeometryGenerator.h"
#include "Utility/Utility.h"


namespace Engine
{
	Terrain::Terrain()
	{

	}

	bool Terrain::ImportMesh(const std::string& modelFileName)
	{

		//지형 모델 생성
		ModelType = MeshModelType::Terrain;
		{
			std::vector<GeometryGenerator::VertexFormat> vertices;
			std::vector<unsigned int> indices;
			if (modelFileName == "Terrain")
			{
				GeometryGenerator::GenerateTerrain(&vertices, &indices, 32, 32,
					4,4, VertexCount, IndexCount);				
			}
			

			VertexData = vertices.data();
			IndexData = indices.data();

			int size = sizeof(GeometryGenerator::VertexFormat);
			//option, 사용 목적에 따라 flag들이 달라질것이다. defaultFormat를 두는 것도 나쁘지 않을듯
			Utility::CreateBufferFromData(&VertexBuffer, D3D11_BIND_VERTEX_BUFFER, VertexData,
				size * VertexCount, D3D11_USAGE_DEFAULT, 0);

			Utility::CreateBufferFromData(&IndexBuffer, D3D11_BIND_INDEX_BUFFER, IndexData,
				sizeof(UINT) * IndexCount, D3D11_USAGE_DEFAULT, 0);

			VertexData = nullptr;
			IndexData = nullptr;
		}

		//높이맵 로딩은 스크립트로?

		return true;
	}

	//void Terrain::UpdateConstBuffer(void* constBuffPtr)
	//{

	//}
}





