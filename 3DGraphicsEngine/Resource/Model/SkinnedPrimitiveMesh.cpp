#include "EngineBaseHeader.h"
#include "SkinnedPrimitiveMesh.h"
#include "Bone.h"
#include "DxMath/customMath.h"
#include "RenderClasses/ConstantBufferManager.h"
#include "System/Timer.h"
#include "GeometryGenerator.h"
#include "Utility/Utility.h"
#include <filesystem>
#include "GameObject/GameObject.h"



namespace Engine
{
	using namespace Math;

	KeyFrame::KeyFrame()
		:TimePos(0.0f),
		Position(0,0,0),
		Scale(0,0,0),
		Rotation(0,0,0)
	{

	}
	float BoneAnimation::GetStartTime() const
	{
		return KeyFrames.front().TimePos;
	}
	float BoneAnimation::GetEndTime() const
	{
		return KeyFrames.back().TimePos;
	}

	void BoneAnimation::Interpolate(float time, BonePtr bone, bool rootBone)
	{		
		Vector3 bindPos = bone->GetBindPosition();
		Vector3 bindRot = bone->GetBindRotation();
		Vector3 bindScale = bone->GetBindScale();
		Vector3 currentBonePos = bone->GetPosition();

		if (!bone->GetOwner())
			return;

		Vector3 ownerPos = bone->GetOwner()->GetTransform().GetPosition();
		Vector3 ownerRot = bone->GetOwner()->GetTransform().GetRotation();
		Vector3 ownerScale = bone->GetOwner()->GetTransform().GetScale();

		if (time < KeyFrames.front().TimePos)
		{						
			Vector3 newPos = bindPos + KeyFrames.front().Position;			
			Vector3 newScale;
			newScale.x = bindScale.x * KeyFrames.front().Scale.x;
			newScale.y = bindScale.y * KeyFrames.front().Scale.y;
			newScale.z = bindScale.z * KeyFrames.front().Scale.z;
			Vector3 newRot = bindRot + KeyFrames.front().Rotation;
			
			if (rootBone)
			{
				newPos = ownerPos + newPos;
				newScale.x = ownerScale.x * newScale.x;
				newScale.y = ownerScale.y * newScale.y;
				newScale.z = ownerScale.z * newScale.z;
				newRot = ownerRot + newRot;
			}

			Matrix scaleMat = Matrix::MatrixScaling(newScale);
			Matrix rotMat = Matrix::MatrixRotationPitchYawRollDegree(newRot.x, newRot.y, newRot.z);
			Matrix transMat = Matrix::MatrixTranslation(newPos);
			
			bone->SetLaterUpdate(true);
			bone->SetPosition(newPos);
			bone->SetScale(newScale);
			bone->SetRotation(newRot);
			bone->SetLaterUpdate(false);

			bone->SetTransform(scaleMat * rotMat * transMat);
		}
		else if (time > KeyFrames.back().TimePos)
		{			
			Vector3 newPos = bindPos + KeyFrames.back().Position;			
			Vector3 newScale;
			newScale.x = bindScale.x * KeyFrames.back().Scale.x;
			newScale.y = bindScale.y * KeyFrames.back().Scale.y;
			newScale.z = bindScale.z * KeyFrames.back().Scale.z;

			Vector3 newRot = bindRot + KeyFrames.back().Rotation;

			if (rootBone)
			{
				newPos = ownerPos + newPos;
				newScale.x *= ownerScale.x;
				newScale.y *= ownerScale.y;
				newScale.z *= ownerScale.z;
				newRot = ownerRot + newRot;
			}

			Matrix scaleMat = Matrix::MatrixScaling(newScale);
			Matrix rotMat = Matrix::MatrixRotationPitchYawRollDegree(newRot.x, newRot.y, newRot.z);
			Matrix transMat = Matrix::MatrixTranslation(newPos);

			

			bone->SetLaterUpdate(true);
			bone->SetPosition(newPos);
			bone->SetScale(newScale);
			bone->SetRotation(newRot);
			bone->SetLaterUpdate(false);

			bone->SetTransform(scaleMat * rotMat * transMat);
		}
		else
		{
			for (UINT i = 0; i < KeyFrames.size(); i++)
			{
				if (time >= KeyFrames[i].TimePos && time <= KeyFrames[i + 1].TimePos)
				{
					float lerpRate = (time - KeyFrames[i].TimePos) / (KeyFrames[i + 1].TimePos - KeyFrames[i].TimePos);

					Vector3 s0 = KeyFrames[i].Scale;
					Vector3 s1 = KeyFrames[i + 1].Scale;

					Vector3 r0 = KeyFrames[i].Rotation;
					Vector3 r1 = KeyFrames[i + 1].Rotation;

					Vector3 p0 = KeyFrames[i].Position;
					Vector3 p1 = KeyFrames[i + 1].Position;

					//보간
					Vector3 finalScale = (1.0f - lerpRate) * s0 + lerpRate * s1;
					Vector3 finalRotation = (1.0f - lerpRate) * r0 + lerpRate * r1;
					Vector3 finalPosition = (1.0f - lerpRate) * p0 + lerpRate * p1;
					
					finalScale.x = finalScale.x * bindScale.x;
					finalScale.y = finalScale.y * bindScale.y;
					finalScale.z = finalScale.z * bindScale.z;

					finalRotation = finalRotation + bindRot;
					finalPosition = finalPosition + bindPos;

					if (rootBone)
					{
						finalScale.x = finalScale.x * ownerScale.x;
						finalScale.y = finalScale.y * ownerScale.y;
						finalScale.z = finalScale.z * ownerScale.z;

						finalRotation = finalRotation + ownerRot;
						finalPosition = finalPosition + ownerPos;
					}

					
					Matrix scaleMat = Matrix::MatrixScaling(finalScale);
					Matrix rotMat = Matrix::MatrixRotationPitchYawRollDegree(finalRotation.x, finalRotation.y, finalRotation.z);
					Matrix transMat = Matrix::MatrixTranslation(finalPosition);
					bone->SetLaterUpdate(true);					
					bone->SetScale(finalScale);
					bone->SetRotation(finalRotation);
					bone->SetPosition(finalPosition);
					bone->SetLaterUpdate(false);
					bone->SetTransform(scaleMat * rotMat * transMat);

				}
			}
		}
	}

	AnimationClip::AnimationClip()
	{
		StartTime = 0;
		EndTime = 0;
		PlayTime = 0;
	}

	const float AnimationClip::GetStartTime() const
	{
		return StartTime;
	}

	const float AnimationClip::GetEndTime() const
	{
		return EndTime;
	}

	void AnimationClip::Update(std::vector<BonePtr>* bones)
	{
		for (UINT i = 0; i < BoneAnimations.size(); i++)
		{
			if(i==0)
				BoneAnimations[i].Interpolate(PlayTime, (*bones)[i],true);
			else
				BoneAnimations[i].Interpolate(PlayTime, (*bones)[i], false);
		}
	}

	void AnimationClip::SetClipName(const std::string& clipName)
	{
		ClipName = clipName;
	}

	std::string& AnimationClip::GetClipName()
	{
		return ClipName;
	}

	void AnimationClip::AddBoneAnimation(const BoneAnimation& boneAnim)
	{
		BoneAnimations.push_back(boneAnim);
	}

	void AnimationClip::UpdateStartEndTime()
	{
		StartTime = 0.0f;

		//CalEndTime
		EndTime = BoneAnimations[0].GetEndTime();
		for (UINT i = 1; i < BoneAnimations.size(); i++)
		{
			if (BoneAnimations[i].GetEndTime() > EndTime)
				EndTime = BoneAnimations[i].GetEndTime();
		}
		
	}

	const float AnimationClip::GetPlayTime()
	{
		return PlayTime;
	}

	void AnimationClip::SetPlayTime(float time)
	{
		PlayTime = time;
	}

	SkinnedPrimitiveMesh::SkinnedPrimitiveMesh()
		:ActiveAnimClip("")
	{
		BoneHierarchy.clear();
		Bones.clear();
		Animations.clear();
		Skinned = true;
		IdleState = false;
	}

	void SkinnedPrimitiveMesh::Update()
	{
		
		if (ActiveAnimation)
		{
			float elapsedtime = static_cast<float>(Timer::GetInstance().GetElapsedTime());
			float animPlayTime = ActiveAnimation->GetPlayTime();
			//IdleState = false;
			if (IdleState)
			{
				
				if (animPlayTime < ActiveAnimation->GetEndTime())
				{
					ActiveAnimation->SetPlayTime(animPlayTime + elapsedtime);
				}
				else
					ActiveAnimation->SetPlayTime(0.0f);
			}
			ActiveAnimation->Update(&Bones);
		}
			
	}

	void SkinnedPrimitiveMesh::SetBindPose()
	{
		for (UINT i = 0; i < Bones.size(); i++)
			Bones[i]->SetBindPose();
	}

	bool SkinnedPrimitiveMesh::ImportMesh(const std::string& modelFileName)
	{
		ModelType = MeshModelType::SkinnedPrimitive;
		{
			std::vector<GeometryGenerator::VertexFormat> vertices;
			std::vector<unsigned int> indices;
			if (modelFileName == "SkinnedCone")
			{
				GeometryGenerator::GenerateWeightedSkinnedCone(&vertices, &indices, 12, 4, 0.5,
					1, VertexCount, IndexCount, 4, &Bones,Owner);

				//AnimationData를 셋팅한다
			}
			SetBindPose();
			LoadAnimation(modelFileName);

			std::map<string, AnimationClip>::iterator itor = Animations.find("idle");
			if (itor != Animations.end())
			{
				ActiveAnimation = &itor->second;
				IdleState = true;
			}
				

			VertexData = vertices.data();
			IndexData = indices.data();

			int size = sizeof(GeometryGenerator::VertexFormat);
			//option, 사용 목적에 따라 flag들이 달라질것이다. defaultFormat를 두는 것도 나쁘지 않을듯
			Utility::CreateBufferFromData(&VertexBuffer, D3D11_BIND_VERTEX_BUFFER, VertexData,
				size * VertexCount, D3D11_USAGE_DEFAULT, 0);

			Utility::CreateBufferFromData(&IndexBuffer, D3D11_BIND_INDEX_BUFFER, IndexData,
				sizeof(UINT) * IndexCount, D3D11_USAGE_DEFAULT, 0);

			VertexData = nullptr;
			IndexData = nullptr;
		}

		return true;
	}

	void SkinnedPrimitiveMesh::LoadAnimation(const std::string& modelFileName)
	{
		std::ifstream fin;
		std::string input;
		fin.open("resource/PrimitiveMeshData/SkinnedPrimitiveAnimation.anim");

		if (fin.fail() == true)
			return;
		
		while (fin)
		{
			//fin >> input;
			std::getline(fin, input);
			if (input.find(modelFileName, 0) != std::string::npos)
			{
				std::getline(fin, input);				
				std::getline(fin, input); //for '{' or '}'
				if (input.find("Animations"))
				{
					std::getline(fin, input);
					std::getline(fin, input);	//for '{' or '}'
					if (input.find("AnimationClip"))
					{
						AnimationClip clip;
						std::getline(fin, input);
						std::getline(fin, input);
						//clip의 이름을 얻어온다
						std::vector<std::string> splitArr = Utility::StringSplit(input, '=');						
						int length = splitArr[1].length();
						clip.SetClipName(splitArr[1].substr(2, length - 3));

						for (UINT i = 0; i < Bones.size(); i++)
						{
							std::getline(fin, input);
							splitArr = Utility::StringSplit(input, '_');
							if (splitArr[0].find("BoneAnimation"))
							{
								BoneAnimation boneAnim;
								std::getline(fin, input);
								std::getline(fin, input);
								splitArr = Utility::StringSplit(input, '=');
								int numKeyFrame = atoi(splitArr[1].c_str());
								for (int j = 0; j < numKeyFrame; j++)
								{
									std::getline(fin, input);
									std::vector<std::string> keyFrameData;
									keyFrameData = Utility::StringSplit(input, ' ');
									if (keyFrameData[0].find("keyFrame"))
									{
										KeyFrame key;
										key.TimePos = static_cast<float>(atof(keyFrameData[1].c_str()));

										//set Pos
										std::vector<std::string> temp;
										temp = Utility::StringSplit(keyFrameData[2], ',');
										key.Rotation.x = static_cast<float>(atof(temp[0].c_str()));
										key.Rotation.y = static_cast<float>(atof(temp[1].c_str()));
										key.Rotation.z = static_cast<float>(atof(temp[2].c_str()));

										temp = Utility::StringSplit(keyFrameData[3], ',');
										key.Scale.x = static_cast<float>(atof(temp[0].c_str()));
										key.Scale.y = static_cast<float>(atof(temp[1].c_str()));
										key.Scale.z = static_cast<float>(atof(temp[2].c_str()));

										boneAnim.KeyFrames.push_back(key);
									}
								}
								std::getline(fin, input);

								clip.AddBoneAnimation(boneAnim);
							}
						}
						clip.UpdateStartEndTime();
						std::pair<string, AnimationClip> animPair;
						animPair.first = clip.GetClipName();
						animPair.second = clip;
						Animations.insert(animPair);

						
					}						
					
				}

				return;
			}
		}
	}

	void SkinnedPrimitiveMesh::UpdateConstBuffer(void* constBuffPtr)
    {
		Update();

		ConstantBufferManager::ObjectConstBuffFormat* buffFormat = static_cast<ConstantBufferManager::ObjectConstBuffFormat*>(constBuffPtr);
		if (!buffFormat)
			return;
		buffFormat->TessellationFactor = Math::Vector4(TesselationFactor, TesselationFactor, TesselationFactor, TesselationFactor);
		buffFormat->IsSkinned = UInt4(Skinned, 0, 0, 0);		
		for (UINT i = 0; i < Bones.size(); i++)
		{
			Matrix mat = Bones[i]->GetBindInverse() * Bones[i]->GetWorldMat();
			mat = Matrix::MatrixTranspose(mat);
			buffFormat->SkinMatrix[i] = mat;
			Matrix inverseMat = mat.Inverse();
			buffFormat->NormalMatrix[i] = Matrix::MatrixTranspose(inverseMat);
			//buffFormat->SkinMatrix[i] = Matrix::MatrixTranspose(Bones[i]->GetWorldMat());
			//buffFormat->World = Matrix::Identity();			
		}
    }

}


