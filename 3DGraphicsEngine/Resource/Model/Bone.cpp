#include "EngineBaseHeader.h"
#include "Bone.h"



namespace Engine
{
	using namespace Math;

	Bone::Bone()
		:Name(""),
		Local(Matrix::Identity()),
		World(Matrix::Identity()),
		NormalMat(Matrix::Identity()),
		Scale(1,1,1),
		Rotation(0,0,0),
		Position(0,0,0),
		Parent(nullptr),
		LaterUpdate(false)
	{
		Children.clear();
	}

	const Math::Matrix& Bone::GetWorldMat() const
	{
		return World;
	}

	const Math::Vector3& Bone::GetPosition() const
	{
		return Position;
	}

	const Math::Vector3& Bone::GetScale() const
	{
		return Scale;
	}

	const Math::Vector3& Bone::GetRotation() const
	{
		return Rotation;
	}

	void Bone::SetPosition(const Math::Vector3& pos)
	{
		Position = pos;
		UpdateTransform();
	}

	void Bone::SetPosition(float x, float y, float z)
	{
		Position.x = x;
		Position.y = y;
		Position.z = z;
		if(!LaterUpdate)
			UpdateTransform();

	}

	void Bone::SetRotation(const Math::Vector3& rot)
	{
		Rotation = rot;
		if (!LaterUpdate)
			UpdateTransform();
	}

	void Bone::SetRotation(float x, float y, float z)
	{
		Rotation.x = x;
		Rotation.y = y;
		Rotation.z = z;
		if (!LaterUpdate)
			UpdateTransform();
	}

	void Bone::SetScale(const Math::Vector3& scale)
	{
		Scale = scale;
		UpdateTransform();
	}

	void Bone::SetScale(float x, float y, float z)
	{
		Scale.x = x;
		Scale.y = y;
		Scale.z = z;
		if (!LaterUpdate)
			UpdateTransform();
	}

	void Bone::UpdateTransform()
	{

		Matrix scaleMat = Matrix::MatrixScaling(Scale);
		Matrix rotMat = Matrix::MatrixRotationPitchYawRollDegree(Rotation.x, Rotation.y, Rotation.z);
		Matrix transMat = Matrix::MatrixTranslation(Position);
		Local = scaleMat * rotMat * (transMat);

		if (Parent)
			World = Local * Parent->GetWorldMat();
		else
			World = Local;
		UpdateChildTransform();
	}

	void Bone::UpdateChildTransform()
	{
		for (UINT i = 0; i < Children.size(); i++)
		{
			if (Children[i])
				Children[i]->UpdateTransform();
		}
	}

	void Bone::SetChild(Engine::BonePtr child)
	{
		child->Parent = this;
		//child->Trans.UpdateTransform();
		Children.push_back(child);
	}

	BonePtr Bone::GetChild(UINT idx)
	{
		return Children[idx];
	}
	void Bone::DeleteChild(UINT idx)
	{
		//Children.erase()
		//std::list<Engine::BonePtr>::iterator itor = Children.begin();
	}

	std::vector<BonePtr>::iterator Bone::GetChildrenBegin()
	{
		return Children.begin();
	}

	std::vector<BonePtr>::iterator Bone::GetChildrenEnd()
	{
		return Children.end();
	}

	void Bone::SetBindPose()
	{
		//???어떻게 구함?
		BindInverse = GetWorldMat();
		BindInverse.Inverse();
	}
	void Bone::AnimateBone()
	{

	}
	void Bone::SetName(const std::string& name)
	{
		Name = name;
	}
	const std::string& Bone::GetName()
	{
		return Name;
	}

	void Engine::Bone::SetTransform(const Math::Matrix& mat)
	{
		Local = mat;
		if (Parent)
			World = Local * Parent->GetWorldMat();
		else
			World = Local;
		UpdateChildTransform();
	}

	void Bone::SetBindPosition(const Vector3& bindPos)
	{
		BindPosition = bindPos;
	}

	void Bone::SetBindRotation(const Vector3& bindRot)
	{
		BindRotation = bindRot;
	}

	void Bone::SetBindScale(const Vector3& bindScale)
	{
		BindScale = bindScale;
	}

	const Vector3& Bone::GetBindPosition()
	{
		return BindPosition;
	}

	const Vector3& Bone::GetBindRotation()
	{
		return BindRotation;
	}

	const Vector3& Bone::GetBindScale()
	{
		return BindScale;
	}
    const Math::Matrix& Bone::GetBindInverse() const
    {
		return BindInverse;
    }
	void Bone::SetLaterUpdate(bool flag)
	{
		LaterUpdate = flag;
	}
}