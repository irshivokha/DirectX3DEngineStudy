#include "EngineBaseHeader.h"
#include "GeometryGenerator.h"
#include "DxMath/customMath.h"
#include "Mesh.h"
#include "Utility/Utility.h"
#include "DefineValues.h"
#include "Bone.h"


namespace Engine
{
	using namespace Math;
	//void GeometryGenerator::GenerateCube(std::vector<GeometryGenerator::VertexFormat>* vertexData, std::vector<UINT>* indexData, unsigned int width, unsigned int height
	//	, unsigned int& vertexCount, unsigned int& indexCount)
	//{
	//	//unsigned int vertexNum = 6;			
	//}

	void GeometryGenerator::GenerateCone(std::vector<GeometryGenerator::VertexFormat>* vertexData, std::vector<UINT>* indexData, unsigned int vRes, 
		unsigned int uRes, float radius, float height, unsigned int& vertexCount, unsigned int& indexCount)
	{
		unsigned int vertexNum = (vRes + 1) * (uRes + 1);	//원의 정점 개수 + 원뿔의 꼭지점 
		vertexCount = vertexNum;
		//Vector3* pos = new Vector3[vertexNum];		

		//pos[0] = Vector3(0, 0, 0);

		if (vertexData)
		{
			vertexData->reserve(vertexNum + 1);
			vertexData->resize(vertexNum);

			float uvRadius = 0.5f;
			unsigned int circleOrigin = 0;
			float heightUnit = 1.0f / uRes * height;
			float currHeight = 0.0f;
			currHeight = -((float)uRes / 2.0f) * heightUnit;

			float currentRadius = radius;
			for (UINT i = 0; i < uRes + 1; i++)
			{

				currentRadius *= 0.8f;
				unsigned int circleVerIndex = 0;
				circleOrigin = (vRes + 1) * i;
				(*vertexData)[circleOrigin].Pos = Vector3(0, currHeight, 0);
				(*vertexData)[circleOrigin].Normal = Vector3(0, -1, 0);
				(*vertexData)[circleOrigin].UV = Vector2(0.5, 0.5);

				for (unsigned int j = circleOrigin + 1; j < circleOrigin + 1 + vRes; j++)
				{
					float angle = 2.0f * DirectX::XM_PI * ((float)circleVerIndex / (float)vRes);		//unit : theta;
					(*vertexData)[j].Pos.x = currentRadius * cosf(angle);
					(*vertexData)[j].Pos.y = currHeight;
					(*vertexData)[j].Pos.z = currentRadius * sinf(angle);
					//set normal
					(*vertexData)[j].Normal = Vector3(0, -1, 0);

					//set uv
					(*vertexData)[j].UV.x = uvRadius * cosf(angle) + 0.5f;
					(*vertexData)[j].UV.y = (-uvRadius * sinf(angle)) + 0.5f;					

					circleVerIndex++;
				}

				currHeight += heightUnit;

			}	
		}			


		unsigned int circleIndicesCount = (vRes * 3 * (uRes + 1));
		unsigned int sideIndicesCount = (6 * vRes * uRes);
		unsigned int indicesCount = circleIndicesCount + sideIndicesCount;
		indexCount = indicesCount;
		if(indexData)
		{			
			indexData->reserve(indicesCount + 1);
			indexData->resize(indicesCount);
			//원 부분
			unsigned int idx = 0;
			for (unsigned int i = 0; i < uRes + 1; i++)
			{
				unsigned int indicesFactor = 2;
				for (unsigned int j = 0; j < vRes * 3; j = j + 3)
				{
					unsigned int circleOrigin = (vRes + 1) * i;
					(*indexData)[idx + 0] = circleOrigin;
					if (i == uRes)
					{
						if (j == (vRes * 3 - 3))
						{
							(*indexData)[idx + 1] = circleOrigin + 1;
							(*indexData)[idx + 2] = circleOrigin + vRes;
						}
						else
						{
							(*indexData)[idx + 1] = circleOrigin + indicesFactor;
							(*indexData)[idx + 2] = (*indexData)[idx + 1] - 1;
						}
					}
					else
					{
						if (j == (vRes * 3 - 3))
						{
							(*indexData)[idx + 1] = circleOrigin + 1;
							(*indexData)[idx + 2] = circleOrigin + vRes;							
						}
						else
						{
							(*indexData)[idx + 1] = circleOrigin + indicesFactor;
							(*indexData)[idx + 2] = (*indexData)[idx + 1] - 1;
						}

					}

					idx = idx + 3;
					indicesFactor++;
				}
			}

			
			//사이드 부분
			unsigned int startPoint = (vRes + 1) + 1;	//circleOrigin의 오른쪽 정점
			unsigned int sideIdx = circleIndicesCount;
			for (unsigned int i = 0; i < uRes; i++)
			{
				startPoint = (vRes + 1) * (i + 1) + 1;
				for (unsigned int j = 0; j < vRes * 6; j = j + 6)
				{							
					if (j == vRes * 6 - 6)
					{
						(*indexData)[sideIdx + 0] = startPoint;
						(*indexData)[sideIdx + 1] = startPoint - (vRes - 1);
						(*indexData)[sideIdx + 2] = (*indexData)[sideIdx + 0] - (vRes + 1);
						(*indexData)[sideIdx + 3] = startPoint - (vRes - 1);
						(*indexData)[sideIdx + 4] = (*indexData)[sideIdx + 3] - (vRes + 1);
						(*indexData)[sideIdx + 5] = startPoint - (vRes + 1);
					}
					else
					{
						(*indexData)[sideIdx + 0] = startPoint;
						(*indexData)[sideIdx + 1] = startPoint + 1;
						(*indexData)[sideIdx + 2] = (*indexData)[sideIdx + 0] - (vRes + 1);
						(*indexData)[sideIdx + 3] = startPoint + 1;
						(*indexData)[sideIdx + 4] = (*indexData)[sideIdx + 3] - (vRes + 1);
						(*indexData)[sideIdx + 5] = (*indexData)[sideIdx + 4] - 1;
					}


					startPoint++;
					sideIdx = sideIdx + 6;
				}				
			}

		}
	}
	void GeometryGenerator::GeneratePlane(std::vector<GeometryGenerator::VertexFormat>* vertexData, std::vector<UINT>* indexData, float width, float height,
		unsigned int& vertexCount, unsigned int& indexCount)
	{
		unsigned int vertexNum = 4;	//원의 정점 개수 + 원뿔의 꼭지점 
		vertexCount = vertexNum;

		float halfWidth = width / 2.0f;
		float halfHeight = height / 2.0f;
		
		if (vertexData)
		{
			vertexData->reserve(vertexNum + 1);
			vertexData->resize(vertexNum);

			(*vertexData)[0].Pos = Vector3(-halfWidth, 0.0f, halfHeight);
			(*vertexData)[0].UV = Vector2(0.0f, 0.0f);
			(*vertexData)[0].Normal = Vector3(0.0f, 1.0f, 0.0f);

			(*vertexData)[1].Pos = Vector3(+halfWidth, 0.0f, halfHeight);
			(*vertexData)[1].UV = Vector2(1.0f, 0.0f);
			(*vertexData)[1].Normal = Vector3(0.0f, 1.0f, 0.0f);

			(*vertexData)[2].Pos = Vector3(-halfWidth, 0.0f, -halfHeight);
			(*vertexData)[2].UV = Vector2(0.0f, 1.0f);
			(*vertexData)[2].Normal = Vector3(0.0f, 1.0f, 0.0f);

			(*vertexData)[3].Pos = Vector3(+halfWidth, 0.0f, -halfHeight);
			(*vertexData)[3].UV = Vector2(1.0f, 1.0f);
			(*vertexData)[3].Normal = Vector3(0.0f, 1.0f, 0.0f);
		}



		unsigned int indicesCount = 6;
		indexCount = indicesCount;
		if (indexData)
		{
			indexData->reserve(indicesCount + 1);
			indexData->resize(indicesCount);

			(*indexData)[0] = 1;
			(*indexData)[1] = 3;
			(*indexData)[2] = 0;
			(*indexData)[3] = 3;
			(*indexData)[4] = 2;
			(*indexData)[5] = 0;

		}

													
	}
	void GeometryGenerator::GenerateWeightedSkinnedCone(std::vector<GeometryGenerator::VertexFormat>* vertexData, std::vector<UINT>* indexData, unsigned int vRes,
		unsigned int uRes, float radius, float height, unsigned int& vertexCount, 
		unsigned int& indexCount, unsigned int numBones, std::vector<BonePtr>* boneData, GameObjPtr owner)
	{
		unsigned int vertexNum = (vRes + 1) * (uRes + 1);	//원의 정점 개수 + 원뿔의 꼭지점 
		vertexCount = vertexNum;		
		//Vector3* pos = new Vector3[vertexNum];		

		//pos[0] = Vector3(0, 0, 0);

		//Bone은 Map으로 관리하는게 나을것 같지만 일단은 Vector

		if (vertexData)
		{
			vertexData->reserve(vertexNum + 1);
			vertexData->resize(vertexNum);

			Vector2 uvUnit = Vector2(4.0 /(float)vRes,1);
			unsigned int circleOrigin = 0;
			float heightUnit = 1.0f / uRes * height;
			float currHeight = 0.0f;
			currHeight = -(heightUnit * 2.0f);
			Vector2 currUV = Vector2(0, uRes);

			float currentRadius = radius;
			for (UINT i = 0; i < uRes + 1; i++)
			{

				currentRadius *= 0.8f;
				unsigned int circleVerIndex = 0;
				circleOrigin = (vRes + 1) * i;
				(*vertexData)[circleOrigin].Pos = Vector3(0, currHeight, 0);
				(*vertexData)[circleOrigin].Normal = Vector3(0, 0, 0);
				(*vertexData)[circleOrigin].UV = Vector2(0.5, 0.5);				

				if (i == uRes)
				{
					(*vertexData)[circleOrigin].BoneIDs = UInt4(i - 1, 0, 0, 0);
					(*vertexData)[circleOrigin].BoneWeights = Vector4(1, 0, 0, 0);
				}
				else
				{
					(*vertexData)[circleOrigin].BoneIDs = UInt4(i, 0, 0, 0);
					(*vertexData)[circleOrigin].BoneWeights = Vector4(1, 0, 0, 0);
				}

				currUV.x = 0;
				currUV.y -= uvUnit.y;
				for (unsigned int j = circleOrigin + 1; j < circleOrigin + 1 + vRes; j++)
				{
					float angle = 2.0f * DirectX::XM_PI * ((float)circleVerIndex / (float)vRes);		//unit : theta;

					float x = currentRadius * cosf(angle);
					float z = currentRadius * sinf(angle);
					(*vertexData)[j].Pos.x = x;
					(*vertexData)[j].Pos.y = currHeight;
					(*vertexData)[j].Pos.z = z;
					//set normal
					Vector3 normal = Vector3(x, 0, z);
					normal.Normalize();
					(*vertexData)[j].Normal.x = normal.x;
					(*vertexData)[j].Normal.y = 0;
					(*vertexData)[j].Normal.z = normal.z;

					//set uv
					(*vertexData)[j].UV.x = currUV.x;
					(*vertexData)[j].UV.y = currUV.y;					

					if (i == uRes)
					{
						(*vertexData)[j].BoneIDs = UInt4(i - 1, 0, 0, 0);
						(*vertexData)[j].BoneWeights = Vector4(1, 0, 0, 0);
					}
					else
					{
						(*vertexData)[j].BoneIDs = UInt4(i, 0, 0, 0);
						(*vertexData)[j].BoneWeights = Vector4(1, 0, 0, 0);
					}
					
					circleVerIndex++;
					currUV.x += uvUnit.x;
				}

				currHeight += heightUnit;
			}
		}
		
		//bone Making	
		//std::vector<BonePtr> bones;
		//bones.reserve(uRes);
		//bones.resize(uRes);


		float heightUnit = 1.0f / uRes * height;
		float boneHeight = -(heightUnit * 2.0f);

		for (UINT i = 0; i < numBones; i++)
		{
			//float currHeight = 0.0f;
			//float heightUnit = 1.0f / uRes * height;
			//currHeight = -((float)uRes / 2.0f)  + (heightUnit/2.0f);			
			Bone* currBone = new Bone();			
			if (i == 0)
			{
				currBone->SetPosition(0, boneHeight, 0);
				currBone->SetBindPosition(Vector3(0, boneHeight,0));
				currBone->SetBindRotation(Vector3(0, 0, 0));
				currBone->SetBindScale(Vector3(1, 1, 1));
				currBone->SetOwner(owner);
			}
				
			else if (i!=0)
			{
				(*boneData)[i - 1]->SetChild(currBone);
				currBone->SetPosition(0, heightUnit, 0);
				currBone->SetBindPosition(Vector3(0, heightUnit, 0));
				currBone->SetBindRotation(Vector3(0, 0, 0));
				currBone->SetBindScale(Vector3(1, 1, 1));
				currBone->SetOwner(owner);

			}
			(*boneData).push_back(currBone);


			//BoneData의 preset

		}

		

		
		unsigned int circleIndicesCount = (vRes * 3 * (uRes + 1));
		unsigned int sideIndicesCount = (6 * vRes * uRes);
		unsigned int indicesCount = circleIndicesCount + sideIndicesCount;
		indexCount = indicesCount;
		if (indexData)
		{
			indexData->reserve(indicesCount + 1);
			indexData->resize(indicesCount);
			//원 부분
			unsigned int idx = 0;
			for (unsigned int i = 0; i < uRes + 1; i++)
			{
				unsigned int indicesFactor = 2;
				for (unsigned int j = 0; j < vRes * 3; j = j + 3)
				{
					unsigned int circleOrigin = (vRes + 1) * i;
					(*indexData)[idx + 0] = circleOrigin;
					if (i == uRes)
					{
						if (j == (vRes * 3 - 3))
						{
							(*indexData)[idx + 1] = circleOrigin + 1;
							(*indexData)[idx + 2] = circleOrigin + vRes;
						}
						else
						{
							(*indexData)[idx + 1] = circleOrigin + indicesFactor;
							(*indexData)[idx + 2] = (*indexData)[idx + 1] - 1;
						}
					}
					else
					{
						if (j == (vRes * 3 - 3))
						{
							(*indexData)[idx + 1] = circleOrigin + 1;
							(*indexData)[idx + 2] = circleOrigin + vRes;
						}
						else
						{
							(*indexData)[idx + 1] = circleOrigin + indicesFactor;
							(*indexData)[idx + 2] = (*indexData)[idx + 1] - 1;
						}

					}

					idx = idx + 3;
					indicesFactor++;
				}
			}


			//사이드 부분
			unsigned int startPoint = (vRes + 1) + 1;	//circleOrigin의 오른쪽 정점
			unsigned int sideIdx = circleIndicesCount;
			for (unsigned int i = 0; i < uRes; i++)
			{
				startPoint = (vRes + 1) * (i + 1) + 1;
				for (unsigned int j = 0; j < vRes * 6; j = j + 6)
				{
					if (j == vRes * 6 - 6)
					{
						(*indexData)[sideIdx + 0] = startPoint;
						(*indexData)[sideIdx + 1] = startPoint - (vRes - 1);
						(*indexData)[sideIdx + 2] = (*indexData)[sideIdx + 0] - (vRes + 1);
						(*indexData)[sideIdx + 3] = startPoint - (vRes - 1);
						(*indexData)[sideIdx + 4] = (*indexData)[sideIdx + 3] - (vRes + 1);
						(*indexData)[sideIdx + 5] = startPoint - (vRes + 1);
					}
					else
					{
						(*indexData)[sideIdx + 0] = startPoint;
						(*indexData)[sideIdx + 1] = startPoint + 1;
						(*indexData)[sideIdx + 2] = (*indexData)[sideIdx + 0] - (vRes + 1);
						(*indexData)[sideIdx + 3] = startPoint + 1;
						(*indexData)[sideIdx + 4] = (*indexData)[sideIdx + 3] - (vRes + 1);
						(*indexData)[sideIdx + 5] = (*indexData)[sideIdx + 4] - 1;
					}


					startPoint++;
					sideIdx = sideIdx + 6;
				}
			}

		}
	}
	void GeometryGenerator::GenerateTerrain(std::vector<GeometryGenerator::VertexFormat>* vertexData, std::vector<UINT>* indexData, int terrXLen, int terrZlen,
		int terrainWidth, int terrainDepth, unsigned int& vertexCount, unsigned int& indexCount)
	{



		//ex) 3x3격자일 경우 4x4개의 정점 필요
		vertexCount = (terrXLen + 1) * (terrZlen + 1);

		if (vertexData)
		{
			vertexData->reserve((terrXLen + 1) * (terrZlen + 1));
			vertexData->resize((terrXLen + 1) * (terrZlen + 1));

			float fWidth = static_cast<float>(terrXLen);
			float fHeight = static_cast<float>(terrZlen);


			float halfWidth = (float)terrainWidth / 2.0f;
			float halfDepth = (float)terrainDepth / 2.0f;
			for (UINT x = 0; x < terrXLen + 1; x++)
			{
				for (int z = 0; z < terrZlen + 1; z++)
				{
					float fX = (float)terrainWidth * (static_cast<float>(x) / fWidth - 0.5f);
					float fZ = (float)terrainDepth * (static_cast<float>(z) / fHeight - 0.5f);
					(*vertexData)[x + z * (terrXLen + 1)].Pos = Vector3(fX, 0, fZ);

					//uv를 0~1범위로 정하기 위한 연산
					float u = (fX - (-halfWidth)) / terrainWidth;
					float v = (fZ - (-halfDepth)) / terrainDepth;
					(*vertexData)[x + z * (terrXLen + 1)].UV = Vector2(u, v);
					//(*vertexData)[x + z * (terrain_x_len + 1)].Normal = Vector3(fX, 0, fZ);
				}
			}
			
		}


		for (int x = 0; x < terrXLen; x++)
		{
			for (int z = 0; z < terrZlen; z++)
			{
				//지형 타일당 12개의 제어점 정의

				//0부터 3까지는 실제 사각형 정점
				indexData->push_back((z + 0) + (x + 0) * (terrXLen + 1));
				indexData->push_back((z + 1) + (x + 0) * (terrXLen + 1));
				indexData->push_back((z + 0) + (x + 1) * (terrXLen + 1));
				indexData->push_back((z + 1) + (x + 1) * (terrXLen + 1));

				//4와 5는 +x 방향 이웃
				indexData->push_back(clamp(z + 0, 0, terrZlen) + clamp(x + 2, 0, terrXLen) * (terrXLen + 1));
				indexData->push_back(clamp(z + 1, 0, terrZlen) + clamp(x + 2, 0, terrXLen) * (terrXLen + 1));

				//6과 7은 +z 방향 이웃
				indexData->push_back(clamp(z + 2, 0, terrZlen) + clamp(x + 0, 0, terrXLen) * (terrXLen + 1));
				indexData->push_back(clamp(z + 2, 0, terrZlen) + clamp(x + 1, 0, terrXLen) * (terrXLen + 1));

				//8과 9는 -x 방향 이웃
				indexData->push_back(clamp(z + 0, 0, terrZlen) + clamp(x - 1, 0, terrXLen) * (terrXLen + 1));
				indexData->push_back(clamp(z + 1, 0, terrZlen) + clamp(x - 1, 0, terrXLen) * (terrXLen + 1));
				
				//10과 11은 -z 방향 이웃
				indexData->push_back(clamp(z - 1, 0, terrZlen) + clamp(x + 0, 0, terrXLen) * (terrXLen + 1));
				indexData->push_back(clamp(z - 1, 0, terrZlen) + clamp(x + 1, 0, terrXLen) * (terrXLen + 1));
			}
		}

		indexCount = indexData->size();

	}
}


