#pragma once
#include "DefineValues.h"
#include "DxMath/customMath.h"
#include "GameObject/Transform.h"

namespace Engine
{	
	class Bone
	{
	private:
		std::string Name;

		Math::Matrix BindInverse;
		Math::Matrix Local;
		Math::Matrix World;
		Math::Matrix NormalMat;
		Math::Vector3 Position;
		Math::Vector3 Rotation;
		Math::Vector3 Scale;

		//Bind Value
		Math::Vector3 BindPosition;
		Math::Vector3 BindRotation;
		Math::Vector3 BindScale;

		std::vector<Engine::BonePtr> Children;
		Engine::BonePtr Parent;
		bool LaterUpdate;
		Engine::GameObjPtr Owner;		

	public:
		Bone();
		const Math::Matrix& GetWorldMat() const;
		const Math::Vector3& GetPosition() const;
		const Math::Vector3& GetScale() const;
		const Math::Vector3& GetRotation() const;
		

		//pos
		void SetPosition(const Math::Vector3& pos);
		void SetPosition(float x, float y, float z);
		void SetLocalPosition(float x, float y, float z);
		void Translate(float x, float y, float z);

		//rot
		void SetRotation(const Math::Vector3& rot);
		void SetRotation(float x, float y, float z);
		void SetLocalRotation(float x, float y, float z);
		void Rotate();


		//scale
		void SetScale(const Math::Vector3& scale);
		void SetScale(float x, float y, float z);

		void UpdateTransform();
		void UpdateChildTransform();


		void SetChild(Engine::BonePtr child);
		BonePtr GetChild(UINT idx);
		void DeleteChild(UINT idx);

		void SetOwner(GameObjPtr owner)
		{
			Owner = owner;
		}

		GameObjPtr GetOwner()
		{
			return Owner;
		}

		BonePtr GetParent()
		{
			return Parent;
		}

		std::vector<BonePtr>::iterator GetChildrenBegin();
		std::vector<BonePtr>::iterator GetChildrenEnd();

		void SetBindPose();



		void AnimateBone();

		void SetName(const std::string& name);

		const std::string& GetName();

		void SetTransform(const Math::Matrix& mat);


		void SetBindPosition(const Math::Vector3& bindPos);
		void SetBindRotation(const Math::Vector3& bindRot);
		void SetBindScale(const Math::Vector3& bindScale);

		const Math::Vector3& GetBindPosition();
		const Math::Vector3& GetBindRotation();
		const Math::Vector3& GetBindScale();

		const Math::Matrix& GetBindInverse() const;
		void SetLaterUpdate(bool flag);


	};
}

