#pragma once

#include "DefineValues.h"
#include "RenderClasses/DXUtility.h"

namespace Engine
{
	struct VERTEX {
		XMFLOAT3 Position;
		XMFLOAT3 Texcoord;
		XMFLOAT3 Normal;
	};

	class Texture;

	class Mesh
	{
	public:
		vector<VERTEX> Vertices;
		vector<UINT> Indices;
		vector<Texture> Textures;
		
		Mesh(vector<VERTEX> vertices, vector<UINT> indices, vector<Texture> textures)
		{
			this->Vertices = vertices;
			this->Indices = indices;
			this->Textures = textures;
			
			//this->setupMesh(dev);
		}

		//렌더링 관련 함수들
		void LinkResourceToPipeline(MeshModelType modelType, UINT vertexSize)
		{		
			if (!DXUtility::GetInstance())
				return;

			ID3D11DeviceContext* immContext = DXUtility::GetInstance()->GetDeviceContext();
			if (!immContext)
				return;			
						
			UINT offset = 0;

			immContext->IASetVertexBuffers(0, 1, &VertexBuffer, &vertexSize, &offset);
			immContext->IASetIndexBuffer(IndexBuffer, DXGI_FORMAT_R32_UINT, offset);
			if (modelType == MeshModelType::Terrain)
				immContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY::D3D11_PRIMITIVE_TOPOLOGY_12_CONTROL_POINT_PATCHLIST);
			else
				immContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY::D3D11_PRIMITIVE_TOPOLOGY_3_CONTROL_POINT_PATCHLIST);
			/*context->IASetInputLayout()  //vertexShader를 컴파일한후에 layOut을 만들 수 있음
			shader는 material에 종속적...material은 각각에 맞는 shader를 shadermanager로부터 요청해서 포인터로 가지고있음*/
			//머티리얼 링크걸고. 머티리얼이 이미 링크가 걸려있으면 링크 걸지 말기
		}

		void Draw()
		{
			ID3D11DeviceContext* context = DXUtility::GetInstance()->GetDeviceContext();
			if (context)
				context->DrawIndexed(Indices.size(), 0, 0);
		}


	private:
		/*  Render data  */
		ID3D11Buffer* VertexBuffer;
		ID3D11Buffer* IndexBuffer;
		string materialKey;


		/*  Functions    */
		// Initializes all the buffer objects/arrays
		bool setupMesh(ID3D11Device* dev)
		{
			HRESULT hr;

			D3D11_BUFFER_DESC vbd;
			vbd.Usage = D3D11_USAGE_IMMUTABLE;
			vbd.ByteWidth = sizeof(VERTEX) * Vertices.size();
			vbd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
			vbd.CPUAccessFlags = 0;
			vbd.MiscFlags = 0;

			D3D11_SUBRESOURCE_DATA initData;
			initData.pSysMem = &Vertices[0];

			hr = dev->CreateBuffer(&vbd, &initData, &VertexBuffer);
			if (FAILED(hr))
				return false;

			D3D11_BUFFER_DESC ibd;
			ibd.Usage = D3D11_USAGE_IMMUTABLE;
			ibd.ByteWidth = sizeof(UINT) * Indices.size();
			ibd.BindFlags = D3D11_BIND_INDEX_BUFFER;
			ibd.CPUAccessFlags = 0;
			ibd.MiscFlags = 0;

			initData.pSysMem = &Indices[0];

			hr = dev->CreateBuffer(&ibd, &initData, &IndexBuffer);
			if (FAILED(hr))
				return false;
		}
	};
}