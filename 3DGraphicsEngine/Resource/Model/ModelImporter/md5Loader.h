#pragma once

#define D_RESOURCE "resource"
#define D_RESOURCE_OBJKEY ".obj"
#define D_RESOURCE_DIFFUSE "_D.dds"
#define D_RESOURCE_NORMAL "_N.dds"
#define D_RESOURCE_SPECULAR "_S.dds"
#include <vector>
#include <fstream>
#include <istream>
#include <sstream>
#include <dwrite.h>

class C_MD5LOADER
{
public:
	struct S_VERTEX
	{
		XMFLOAT3 Pos;
		XMFLOAT2 Tex;
		XMFLOAT3 Normal;
		XMFLOAT3 Tan;
		XMFLOAT3 biTangent;
		int StartWeight;
		int WeightCount;
	};
	struct Joint
	{
		std::wstring name;
		int parentID;

		XMFLOAT3 pos;
		XMFLOAT4 orientation;
	};
	struct Weight
	{
		int jointID;
		float bias;
		XMFLOAT3 pos;
		XMFLOAT3 normal;
	};
	struct BoundingBox
	{
		XMFLOAT3 min;
		XMFLOAT3 max;
	};

	struct FrameData
	{
		int frameID;
		std::vector<float> frameData;
	};
	struct AnimJointInfo
	{
		std::wstring name;
		int parentID;

		int flags;
		int startIndex;
	};

	struct ModelAnimation
	{
		int numFrames;
		int numJoints;
		int frameRate;
		int numAnimatedComponents;

		float frameTime;
		float totalAnimTime;
		float currAnimTime;

		std::vector<AnimJointInfo> jointInfo;
		std::vector<BoundingBox> frameBounds;
		std::vector<Joint>	baseFrameJoints;
		std::vector<FrameData>	frameData;
		std::vector<std::vector<Joint>> frameSkeleton;
	};
private:
	struct ModelSubset
	{
		int texArrayIndex;
		int numTriangles;

		std::vector<S_VERTEX> vertices;
		std::vector<DWORD> indices;
		std::vector<Weight> weights;

		std::vector<XMFLOAT3> positions;

		ID3D11Buffer* pVertexBuffer;
		ID3D11Buffer* pIndexBuffer;
	};
public:
	struct Model3D
	{
		int numSubsets;
		int numJoints;

		std::vector<Joint> joints;
		std::vector<ModelSubset> subsets;
		std::vector<ModelAnimation> animations;
	};
private:
	Model3D m_model3D;
public:
	C_MD5LOADER();
	void Release();
	bool LoadMD5Model(std::wstring filename,
		std::vector<ID3D11ShaderResourceView*>& shaderResourceViewArray,
		std::vector<std::wstring> texFileNameArray, ID3D11Device* pDevice);
	bool LoadMD5Anim(std::wstring filename, Model3D& MD5Model);
	Model3D& GetModel3D();

};