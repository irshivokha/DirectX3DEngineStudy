#include "EngineBaseHeader.h"
#include "ObjLoader.h"
//#include <Importer.hpp>
using namespace std;

namespace Engine
{
		ObjLoader* ObjLoader::Instance = nullptr;

		ObjLoader::ObjLoader()
			:FaceCount(0),
			IndicesLength(0),
			VerticesLength(0),
			NormalCount(0),
			TextureCount(0),
			VertexCount(0),
			Positions(nullptr),
			Normals(nullptr),
			Texcoords(nullptr),
			Faces(nullptr),
			Vertices(nullptr),
			Indices(nullptr)			
		{			
		}

		ObjLoader::~ObjLoader()
		{
		}

		ObjLoader*  ObjLoader::GetInstance()
		{
			return Instance;
		}

		void ObjLoader::CreateInstance()
		{
			if (!Instance)
				Instance = new ObjLoader();
		}

		void ObjLoader::ReleaseInstance()
		{
			if (Instance)
			{
				delete Instance;
				Instance = nullptr;
			}
		}

		bool ObjLoader::ReadFileCounts(const char* filename)
		{
			ifstream fin;
			char input;

			// Initialize the counts.
			VertexCount = 0;
			TextureCount = 0;
			NormalCount = 0;
			FaceCount = 0;

			// Open the file.
			fin.open(filename);

			// Check if it was successful in opening the file.
			if (fin.fail() == true)
				return false;

			// Read from the file and continue to read until the end of the file is reached.
			fin.get(input);
			while (!fin.eof())
			{
				// If the line starts with 'v' then count either the vertex, the texture coordinates, or the normal vector.
				if (input == 'v')
				{
					fin.get(input);
					if (input == ' ') { VertexCount++; }
					if (input == 't') { TextureCount++; }
					if (input == 'n') { NormalCount++; }
				}

				// If the line starts with 'f' then increment the face count.
				if (input == 'f')
				{
					fin.get(input);
					if (input == ' ') { FaceCount++; }
				}

				// Otherwise read in the remainder of the line.
				while (input != '\n')
				{
					fin.get(input);
				}

				// Start reading the beginning of the next line.
				fin.get(input);
			}

			// Close the file.
			fin.close();

			return true;
		}

		bool ObjLoader::LoadDataStructures(const char* filename)
		{
			ifstream fin;
			int vertexIndex, texcoordIndex, normalIndex, faceIndex;
			char input, input2;

			if (VertexCount <= 0 || TextureCount <= 0 || NormalCount <= 0 || FaceCount <= 0)
				return false;

			Positions = new Vector3[VertexCount];
			Texcoords = new Vector3[TextureCount];
			Normals = new Vector3[NormalCount];
			Faces = new FaceType[FaceCount];

			// Initialize the indexes.
			vertexIndex = 0;
			texcoordIndex = 0;
			normalIndex = 0;
			faceIndex = 0;

			// Open the file.
			fin.open(filename);

			// Check if it was successful in opening the file.
			if (fin.fail() == true)
				return false;

			// Read in the Vertices, texture coordinates, and Normals into the data structures.
			// Important: Also convert to left hand coordinate system since Maya uses right hand coordinate system.
			fin.get(input);
			while (!fin.eof())
			{
				if (input == 'v')
				{
					fin.get(input);

					// Read in the Vertices.
					if (input == ' ')
					{
						fin >> Positions[vertexIndex].x >> Positions[vertexIndex].y >> Positions[vertexIndex].z;

						// Invert the Z vertex to change to left hand system.
						Positions[vertexIndex].z = Positions[vertexIndex].z * -1.0f;
						vertexIndex++;
					}

					// Read in the texture uv coordinates.
					if (input == 't')
					{
						fin >> Texcoords[texcoordIndex].x >> Texcoords[texcoordIndex].y;

						// Invert the V texture coordinates to left hand system.
						Texcoords[texcoordIndex].y = 1.0f - Texcoords[texcoordIndex].y;
						texcoordIndex++;
					}

					// Read in the Normals.
					if (input == 'n')
					{
						fin >> Normals[normalIndex].x >> Normals[normalIndex].y >> Normals[normalIndex].z;

						// Invert the Z normal to change to left hand system.
						Normals[normalIndex].z = Normals[normalIndex].z * -1.0f;
						normalIndex++;
					}
				}

				// Read in the Faces.
				if (input == 'f')
				{
					fin.get(input);
					if (input == ' ')
					{
						// Read the face data in backwards to convert it to a left hand system from right hand system.
						fin >> Faces[faceIndex].vIndex3 >> input2 >> Faces[faceIndex].tIndex3 >> input2 >> Faces[faceIndex].nIndex3
							>> Faces[faceIndex].vIndex2 >> input2 >> Faces[faceIndex].tIndex2 >> input2 >> Faces[faceIndex].nIndex2
							>> Faces[faceIndex].vIndex1 >> input2 >> Faces[faceIndex].tIndex1 >> input2 >> Faces[faceIndex].nIndex1;
						faceIndex++;
					}
				}

				// Read in the remainder of the line.
				while (input != '\n')
				{
					fin.get(input);
				}

				// Start reading the beginning of the next line.
				fin.get(input);
			}

			// Close the file.
			fin.close();

			VerticesLength = FaceCount * 3;
			 
			Vertices = new VertexFormat[VerticesLength]();

			for (int i = 0; i < faceIndex; i++)
			{
				MakeVertex(Faces[i].vIndex1, Faces[i].tIndex1, Faces[i].nIndex1, &Vertices[i * 3]);
				MakeVertex(Faces[i].vIndex2, Faces[i].tIndex2, Faces[i].nIndex2, &Vertices[i * 3 + 1]);
				MakeVertex(Faces[i].vIndex3, Faces[i].tIndex3, Faces[i].nIndex3, &Vertices[i * 3 + 2]);
			}

			IndicesLength = faceIndex * 3;
			Indices = new UINT[IndicesLength]();

			for (int i = 0; i < IndicesLength; i++)
			{
				Indices[i] = (UINT)i;
			}

			//Vector3* pSdir = new Vector3[VerticesLength]();
			//Vector3* pTdir = new Vector3[VerticesLength]();

			//for (int i = 0; i < IndicesLength / 3; i++)
			//{
			//	Vector3 sDir;
			//	Vector3 tDir;
			//	CalculateTangentBinormal(
			//		Vertices[Indices[i * 3]],
			//		Vertices[Indices[i * 3 + 1]],
			//		Vertices[Indices[i * 3 + 2]], sDir, tDir);


			//	for (int j = 0; j < 3; j++)
			//	{
			//		pSdir[Indices[i * 3 + j]] + sDir;
			//		pSdir[Indices[i * 3 + j]] + sDir;
			//		pTdir[Indices[i * 3 + j]] + tDir;
			//	}
			//}

			//for (int i = 0; i < IndicesLength; i++)
			//{
			//	MakeTangentBinormal(Vertices[i], pSdir[i], pTdir[i]);
			//}

			//delete[] pSdir;
			//delete[] pTdir;
			//pSdir = nullptr;
			//pTdir = nullptr;


			return true;
		}

		void ObjLoader::Release()
		{
			// Release the four data structures.
			if (Vertices)
			{
				delete[] Vertices;
				Vertices = nullptr;
			}
			if (Texcoords)
			{
				delete[] Texcoords;
				Texcoords = nullptr;
			}
			if (Normals)
			{
				delete[] Normals;
				Normals = nullptr;
			}
			if (Faces)
			{
				delete[] Faces;
				Faces = nullptr;
			}
		}


		void ObjLoader::MakeVertex(int nVindex, int nTindex, int nNindex, VertexFormat* pOutVertex)
		{
			nVindex--;
			nTindex--;
			nNindex--;

			memset(pOutVertex, 0, sizeof(VertexFormat));

			pOutVertex->Pos.x = Positions[nVindex].x;
			pOutVertex->Pos.y = Positions[nVindex].y;
			pOutVertex->Pos.z = Positions[nVindex].z;

			pOutVertex->Tex.x = Texcoords[nTindex].x;
			pOutVertex->Tex.y = Texcoords[nTindex].y;

			pOutVertex->Normal.x = Normals[nNindex].x;
			pOutVertex->Normal.y = Normals[nNindex].y;
			pOutVertex->Normal.z = Normals[nNindex].z;

		}

		void ObjLoader::CalculateTangentBinormal(const VertexFormat& vertex1,
			const VertexFormat& vertex2, const
			VertexFormat& vertex3,
			XMFLOAT3 & sdir, XMFLOAT3 & tdir)
		{
			float x1 = vertex2.Pos.x - vertex1.Pos.x;
			float x2 = vertex3.Pos.x - vertex1.Pos.x;
			float y1 = vertex2.Pos.y - vertex1.Pos.y;
			float y2 = vertex3.Pos.y - vertex1.Pos.y;
			float z1 = vertex2.Pos.z - vertex1.Pos.z;
			float z2 = vertex3.Pos.z - vertex1.Pos.z;

			float s1 = vertex2.Tex.x - vertex1.Tex.x;
			float s2 = vertex3.Tex.x - vertex1.Tex.x;
			float t1 = vertex2.Tex.y - vertex1.Tex.y;
			float t2 = vertex3.Tex.y - vertex1.Tex.y;

			float r = 1.0F / (s1 * t2 - s2 * t1);
			sdir = XMFLOAT3((t2 * x1 - t1 * x2) * r, (t2 * y1 - t1 * y2) * r,
				(t2 * z1 - t1 * z2) * r);
			tdir = XMFLOAT3((s1 * x2 - s2 * x1) * r, (s1 * y2 - s2 * y1) * r,
				(s1 * z2 - s2 * z1) * r);
		}

		void ObjLoader::MakeTangentBinormal(VertexFormat& vertex, XMFLOAT3 & sdir, XMFLOAT3 & tdir)
		{
			//XMVECTOR vNormal = XMLoadFloat3(&vertex.Normal);
			//XMVECTOR vSdir = XMLoadFloat3(&sdir);
			//XMVECTOR vTdir = XMLoadFloat3(&tdir);
			//XMVECTOR vTan;
			//XMVECTOR vBinormal;

			//// Gram-Schmidt orthogonalize
			//vTan = XMVector3Normalize(vSdir - vNormal * XMVector3Dot(vNormal, vSdir));

			//// Calculate handedness
			//XMVector3Dot(XMVector3Cross(vNormal, vSdir), vTdir);

			//XMStoreFloat3(&vertex.Tan, vTan);
			//vBinormal = XMVector3Cross(vNormal, vTan);
			//XMStoreFloat3(&vertex.biNormal, vBinormal);

			//tangent[a].w = (Dot(Cross(n, t), tan2[a]) < 0.0F) ? -1.0F : 1.0F;
			//delete[] tan1;
		}


		ObjLoader::VertexFormat* ObjLoader::GetVertices()
		{
			return Vertices;
		}

		UINT* ObjLoader::GetIndices()
		{
			return Indices;
		}

		const int ObjLoader::GetVerticesLength()
		{
			return VerticesLength;
		}

		const int ObjLoader::GetIndicesLength()
		{
			return IndicesLength;
		}

		bool ObjLoader::LoadFromObjFile(const string& pfileName)
		{
			char szObjName[MAX_PATH] = "";
			//MakeReourceName(szObjName, MAX_PATH, pfileName, D_RESOURCE_OBJKEY);
			ReadFileCounts(pfileName.c_str());
			LoadDataStructures(pfileName.c_str());
 			return true;
		}

		void ObjLoader::MakeReourceName(char* pResName, int nResLength, const char* pModelName, const char* pKey) const
		{
			sprintf_s(pResName, nResLength, "%s/%s/%s%s", D_RESOURCE, pModelName, pModelName, pKey);
		}

		void ObjLoader::GetDiffuseTextureName(char* szTextureName, int nTextureLength, const char* szModelName) const
		{
			MakeReourceName(szTextureName, nTextureLength, szModelName, D_RESOURCE_DIFFUSE);
		}

		void ObjLoader::GetNormalTextureName(char* szTextureName, int nTextureLength, const char* szModelName) const
		{

			MakeReourceName(szTextureName, nTextureLength, szModelName, D_RESOURCE_NORMAL);
		}

		void ObjLoader::GetSpecularTextureName(char* szTextureName, int nTextureLength, const char* szModelName) const
		{
			MakeReourceName(szTextureName, nTextureLength, szModelName, D_RESOURCE_SPECULAR);
		}
	
}


