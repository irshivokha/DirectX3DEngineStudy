#pragma once
#include "UnCopyable.h"
#include "DxMath/customMath.h"
#include "Engine.h"
#define D_RESOURCE "resource"
#define D_RESOURCE_OBJKEY ".obj"
#define D_RESOURCE_DIFFUSE "_D.dds"
#define D_RESOURCE_NORMAL "_N.dds"
#define D_RESOURCE_SPECULAR "_S.dds"

/*Vector3같은 class들은 한번 기능이 정해지면 바뀌지 않으므로 최후에는 미리컴파일된 헤더에 넣어놓기.
그러나 지금은 아직 구현단계이므로 최대한 정방선언해서 사용하고 cpp에서 인클루드하기*/


namespace Engine
{	
	using namespace Math;

		class ObjLoader:public UnCopyable
		{
		public:
			struct VertexFormat
			{
				Vector3 Pos;
				Vector3 Tex;
				Vector3 Normal;
				Vector3 Tan;
				Vector3 biNormal;
			};

		private:
			struct S_VERTEXTYPE
			{
				float x, y, z;
			};
			struct FaceType
			{
				unsigned int vIndex1, vIndex2, vIndex3;
				unsigned int tIndex1, tIndex2, tIndex3;
				unsigned int nIndex1, nIndex2, nIndex3;
			};
		private:
			int VertexCount;
			int TextureCount;
			int NormalCount;
			int FaceCount;

			int VerticesLength;		
			int IndicesLength;

			Vector3* Positions;
			Vector3* Texcoords;
			Vector3* Normals;
			FaceType* Faces;

			VertexFormat* Vertices;
			UINT* Indices;

			static ObjLoader* Instance;

		private:
			bool ReadFileCounts(const char* filename);
			void MakeVertex(int nVindex, int nTindex, int nNindex, VertexFormat* pOutVertex);
			bool LoadDataStructures(const char* pfilename);
			void CalculateTangentBinormal(const VertexFormat& vertex1,
				const VertexFormat& vertex2, const
				VertexFormat& vertex3,
				XMFLOAT3& sdir, XMFLOAT3& tdir);
			void MakeTangentBinormal(VertexFormat& vertex, XMFLOAT3& sdir, XMFLOAT3& tdir);

			ObjLoader();
			~ObjLoader();
			void MakeReourceName(char* pResName, int nResLength, const char* pModelName, const char* pKey) const;
		public:

			static ObjLoader* GetInstance();
			static void CreateInstance();
			static void ReleaseInstance();

			bool LoadFromObjFile(const std::string& pfileName);
			void Release();

			VertexFormat* GetVertices();
			UINT* GetIndices();
			const int GetVerticesLength();
			const int GetIndicesLength();
			void GetDiffuseTextureName(char* szTextureName, int nTextureLength, const char* szModelName) const;
			void  GetNormalTextureName(char* szTextureName, int nTextureLength, const char* szModelName) const;
			void GetSpecularTextureName(char* szTextureName, int nTextureLength, const char* szModelName) const;
		};
	
}

