#pragma once
#include "UnCopyable.h"
#include "DxMath/customMath.h"
#include "DefineValues.h"

namespace Engine
{
	class Mesh;

	using namespace Math;
	class GeometryGenerator :public UnCopyable
	{		
	public:
		struct VertexFormat
		{
			Vector3 Pos;						
			UInt4 BoneIDs;
			Vector4 BoneWeights;			
			Vector3 Normal;			
			Vector2 UV;

			//Vector3 Pos;
			//Vector3 UV;
			//Vector3 Normal;
			//Vector3 Tan;
			//Vector3 biNormal;
		};

		static void GenerateCube(std::vector<GeometryGenerator::VertexFormat>* vertexData, std::vector<UINT>* indexData, unsigned int width, unsigned int height
		, unsigned int& vertexCount, unsigned int& indexCount);
		static void GenerateCone(std::vector<GeometryGenerator::VertexFormat>* vertexData, std::vector<UINT>* indexData, unsigned int vRes, 
			unsigned int uRes,  float radius, float height, unsigned int& vertexCount, unsigned int& indexCount);
		static void GeneratePlane(std::vector<GeometryGenerator::VertexFormat>* vertexData, std::vector<UINT>* indexData, float width, float height,
			unsigned int& vertexCount, unsigned int& indexCount);
		//generateSphere
		//genereateCylinder
		static void GenerateWeightedSkinnedCone(std::vector<GeometryGenerator::VertexFormat>* vertexData, std::vector<UINT>* indexData, unsigned int vRes,
			unsigned int uRes, float radius, float height, unsigned int& vertexCount, unsigned int& indexCount, unsigned int numBones,
			std::vector<BonePtr>* boneData,GameObjPtr owner);

		static void GeometryGenerator::GenerateTerrain(std::vector<GeometryGenerator::VertexFormat>* vertexData, std::vector<UINT>* indexData, int terrXLen, int terrZLen,
			int terrainWidth, int terrainDepth, unsigned int& vertexCount, unsigned int& indexCount);

	private:
		GeometryGenerator();
	};
}

