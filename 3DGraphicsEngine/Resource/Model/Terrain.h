#pragma once
#include <vector>
#include <iostream>
#include "DefineValues.h"
#include "DxMath/customMath.h"
#include "Mesh.h"

namespace Engine
{
	class Terrain : public Mesh
	{
	public:
		Terrain();		
		virtual bool ImportMesh(const std::string& modelFileName);		
		//virtual void UpdateConstBuffer(void* constBuffPtr) override;
	};
}
