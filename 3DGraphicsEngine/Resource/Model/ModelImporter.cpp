#include "EngineBaseHeader.h"
#include "ModelImporter.h"
#include "Mesh.h"
#include "RenderClasses/DXUtility.h"
#include "ModelImporter/objLoader.h"
#include "Utility/Utility.h"
#include "GeometryGenerator.h"
#include "RenderClasses/ConstantBufferManager.h"
#include "Resource/Texture/TextureManager.h"
#include "Resource/Material/MaterialManager.h"
#include "GameObject/GameObjectManager.h"
#include "GameObject/GameObject.h"
#include "Resource/Model/Bone.h"
std::string projectDir = "../Projects/FirstGame/";
string textype;

namespace Engine
{

	ModelImporter::ModelImporter()
		:ModelType(MeshModelType::Primitive),
		AssetScene(nullptr),
		Directory()
	{
	}
	
	ModelImporter::~ModelImporter()
	{
	}

	GameObjPtr ModelImporter::ImportMesh(const std::string& modelFileName)
	{		
		this->Directory = modelFileName.substr(0, modelFileName.find_last_of('/'));
		std::string::size_type dot = modelFileName.find_last_of('.') + 1;
		std::string ext = modelFileName.substr(dot, modelFileName.length() - dot);		

		if (ext == "X")
		{
			ModelType = MeshModelType::obj;
			//
			//ObjLoader::GetInstance()->LoadFromObjFile(modelFileName);
			//VertexCount = ObjLoader::GetInstance()->GetVerticesLength();
			//VertexData = ObjLoader::GetInstance()->GetVertices();
			//IndexCount = ObjLoader::GetInstance()->GetIndicesLength();
			//IndexData = ObjLoader::GetInstance()->GetIndices();

			////option, 사용 목적에 따라 flag들이 달라질것이다. defaultFormat를 두는 것도 나쁘지 않을듯
			//Utility::CreateBufferFromData(&VertexBuffer, D3D11_BIND_VERTEX_BUFFER, VertexData,
			//	sizeof(ObjLoader::VertexFormat) * VertexCount, D3D11_USAGE_DEFAULT, 0);

			//Utility::CreateBufferFromData(&IndexBuffer, D3D11_BIND_INDEX_BUFFER, IndexData,
			//	sizeof(UINT) * IndexCount, D3D11_USAGE_DEFAULT, 0);
			//Utility::Delete((ObjLoader::VertexFormat*)VertexData, VertexCount);
			//Utility::Delete(IndexData, IndexCount);

			Assimp::Importer importer;

			const aiScene* pScene = importer.ReadFile(modelFileName,
				aiProcess_Triangulate |
				aiProcess_ConvertToLeftHanded);

			if (pScene == NULL)
				return false;
			
			processNode(pScene->mRootNode, pScene);

			return nullptr;

		}			
		else if(ext =="md5")
			ModelType = MeshModelType::md5;
		else if(ext =="fbx")
			ModelType = MeshModelType::fbx;
		else if (ext == "stl")
			ModelType = MeshModelType::stl;
		else
		{
			ModelType = MeshModelType::Primitive;
			{				
				//프리미티브 메쉬 따로 로드하는 코드 만들기.
				//std::vector<GeometryGenerator::VertexFormat> vertices;
				//std::vector<unsigned int> indices;
				//if (modelFileName == "Cone")														
				//	GeometryGenerator::GenerateCone(&vertices, &indices, 12, 4, 0.5, 1, VertexCount, IndexCount);
				//else if(modelFileName =="Plane")
				//	GeometryGenerator::GeneratePlane(&vertices, &indices, 1, 1, VertexCount, IndexCount);
				//VertexData = vertices.data();
				//IndexData = indices.data();

				//int size = sizeof(GeometryGenerator::VertexFormat);
				////option, 사용 목적에 따라 flag들이 달라질것이다. defaultFormat를 두는 것도 나쁘지 않을듯
				//Utility::CreateBufferFromData(&VertexBuffer, D3D11_BIND_VERTEX_BUFFER, VertexData,
				//	size * VertexCount, D3D11_USAGE_DEFAULT, 0);

				//Utility::CreateBufferFromData(&IndexBuffer, D3D11_BIND_INDEX_BUFFER, IndexData,
				//	sizeof(UINT) * IndexCount, D3D11_USAGE_DEFAULT, 0);

				//VertexData = nullptr;
				//IndexData = nullptr;
			}
		}
			
		return nullptr;
	}

	GameObjPtr ModelImporter::processNode(aiNode* node, const aiScene* scene)
	{
		GameObjPtr newNode = nullptr;
		for (UINT i = 0; i < node->mNumMeshes; i++)
		{	
			newNode = GameObjectManager::GetInstance().CreateGameObject();
			newNode->SetName(node->mName.C_Str());			
			aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];			
			newNode->AddMesh(this->processMesh(mesh, scene));			
		}

		if(newNode)
		{
			newNode->SetChildCount(node->mNumChildren);

			for (UINT i = 0; i < node->mNumChildren; i++)
			{				
				newNode->AddChild( processNode(node->mChildren[i], scene) );			
			}
		}

		return newNode;
	}

	Mesh ModelImporter::processMesh(aiMesh* mesh, const aiScene* scene)
	{
		// Data to fill
		vector<VERTEX> vertices;
		vector<UINT> indices;
		vector<Texture> textures;
		
		// 정점 채우기
		for (UINT i = 0; i < mesh->mNumVertices; i++)
		{
			VERTEX vertex;
			aiAnimation* test;
			aiNode* test2;
			
			vertex.Position = XMFLOAT3(mesh->mVertices[i].x, mesh->mVertices[i].y, mesh->mVertices[i].z);			
			for (int uvChannel = 0; uvChannel < mesh->GetNumUVChannels(); uvChannel++)
			{
				if (mesh->mTextureCoords[uvChannel])
					vertex.Texcoord = XMFLOAT3((float)mesh->mTextureCoords[uvChannel][i].x, (float)mesh->mTextureCoords[uvChannel][i].y,
					(float)mesh->mTextureCoords[uvChannel][i].z);
			}

			if (mesh->HasNormals())
				vertex.Normal = XMFLOAT3(mesh->mNormals[i].x, mesh->mNormals[i].y, mesh->mNormals[i].z);
						
			vertices.push_back(vertex);
		}

		// 인덱스 버퍼 채우기
		for (UINT i = 0; i < mesh->mNumFaces; i++)
		{
			aiFace face = mesh->mFaces[i];

			for (UINT j = 0; j < face.mNumIndices; j++)
				indices.push_back(face.mIndices[j]);
		}

		// 본 구조 만들기
		for (UINT i = 0; i < mesh->mNumBones; i++)
		{
			Bone* currBone = new Bone();						
			currBone->SetTransform(Math::Matrix(&mesh->mBones[0]->mOffsetMatrix.a1));			
		}
		
		//머티리얼 만들기
		string materialKey = processMaterial(scene, mesh);

		return Mesh(vertices, indices, textures);
		
	}

	string ModelImporter::processMaterial(const aiScene* scene, aiMesh* mesh)
	{
		string materialkey;
		if (mesh->mMaterialIndex >= 0)
		{			
			aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];
			if (!material)
				return;
									
			textype = determineTextureType(scene, material);
			materialkey = material->GetName().C_Str();
			auto engineMatPtr = MaterialManager::GetInstance().CreateMaterial(material->GetName().C_Str(), ShaderType::Default);

			//머티리얼 속성 별로 부여하기

			aiColor3D color;
			if (material->Get(AI_MATKEY_COLOR_DIFFUSE, color) == AI_SUCCESS)
			{
				TypeVector3* vDiffuse = new TypeVector3(color.r, color.g, color.b);
				MaterialProperty propDiffuse(MaterialKey::DiffuseColor, 0, vDiffuse);
				engineMatPtr->SetProperty(propDiffuse);
			}

			if (material->Get(AI_MATKEY_COLOR_SPECULAR, color) == AI_SUCCESS)
			{
				TypeVector3* vSpecular = new TypeVector3(color.r, color.g, color.b);
				MaterialProperty propSpecular(MaterialKey::SpecularColor, 0, vSpecular);
				engineMatPtr->SetProperty(propSpecular);
			}

			if (material->Get(AI_MATKEY_COLOR_AMBIENT, color) == AI_SUCCESS)
			{
				string key(AI_MATKEY_COLOR_DIFFUSE);
				TypeVector3* vAmbient = new TypeVector3(color.r, color.g, color.b);
				MaterialProperty propAmbient(MaterialKey::AmbientColor, 0, vAmbient);
				engineMatPtr->SetProperty(propAmbient);
			}


			//종류별로 텍스쳐 만들기
			this->loadMaterialTextures(engineMatPtr, material, aiTextureType_DIFFUSE, TextureType::DiffuseMap, scene);
			this->loadMaterialTextures(engineMatPtr, material, aiTextureType_NORMALS, TextureType::NormalMap, scene);
			this->loadMaterialTextures(engineMatPtr, material, aiTextureType_SPECULAR, TextureType::SpecularMap, scene);

			
		}

		return materialkey;
	}



	void ModelImporter::loadMaterialTextures(shared_ptr<Material> engineMat, aiMaterial* mat, aiTextureType type, TextureType texType, const aiScene* scene)
	{
		if (!engineMat || !mat )
			return;

		if (scene && mat)
		{			
			for (UINT i = 0; i < mat->GetTextureCount(type); i++)
			{
				aiString str;
				mat->GetTexture(type, i, &str);
				// Check if texture was loaded before and if so, continue to next iteration: skip loading a new texture
				bool skip = false;

				//if (!skip)
				{   // If texture hasn't been loaded already, load it
					HRESULT hr;
					Texture texture;
					string filename;
					if (textype == "embedded compressed texture")
					{
						int textureindex = getTextureIndex(&str);
						//std::shared_ptr<Texture> texture = Engine::TextureManager::GetInstance().LoadTexture(projectDir + "Assets/Textures/EyeOfHorus_128_Blurred.png");
						filename = scene->mTextures[textureindex]->mFilename.C_Str();
						std::shared_ptr<Texture> texture = Engine::TextureManager::GetInstance().LoadTexture(filename);
						//texture.texture = getTextureFromModel(scene, textureindex);
					}
					else
					{
						filename = string(str.C_Str());
						filename = Directory + '/' + filename;						
						std::shared_ptr<Texture> texture = Engine::TextureManager::GetInstance().LoadTexture(filename);
						//hr = CreateWICTextureFromFile(dev, devcon, filenamews.c_str(), nullptr, &texture.texture);
						//if (FAILED(hr))
						//	MessageBox(hwnd, "Texture couldn't be loaded", "Error!", MB_ICONERROR | MB_OK);
					}
					
					texture.SetTexType(texType);
					texture.SetTexPath(str.C_Str());

					TypeTexture* textureType = new TypeTexture(filename);
					MaterialProperty propTex(MaterialKey::DiffuseMap, i, textureType);
					engineMat->SetProperty(propTex);
				}
			}
		}		
	}

	string ModelImporter::determineTextureType(const aiScene* scene, aiMaterial* mat)
	{
		aiString textypeStr;
		mat->GetTexture(aiTextureType_DIFFUSE, 0, &textypeStr);		
		string textypeteststr = textypeStr.C_Str();
		if (textypeteststr == "*0" || textypeteststr == "*1" || textypeteststr == "*2" || textypeteststr == "*3" || textypeteststr == "*4" || textypeteststr == "*5")
		{
			if (scene->mTextures[0]->mHeight == 0)
			{
				return "embedded compressed texture";
			}
			else
			{
				return "embedded non-compressed texture";
			}
		}
		if (textypeteststr.find('.') != string::npos)
		{
			return "textures are on disk";
		}
	}

	int ModelImporter::getTextureIndex(aiString* str)
	{
		string tistr;
		tistr = str->C_Str();
		tistr = tistr.substr(1);
		return stoi(tistr);		
	}

	void ModelImporter::Draw()
	{

	/*
	PipleLine단계에 따라 셋팅후 렌더링
	0. IA단계(고정단계).
	1. proramable Shader단계 . 각 단계마다 필요한 상수버퍼를 업데이트 하고 파이프라인에 연결.
	2. OM단계(고정단계?)

	*******상수버퍼와 vertexbuffer,indexbuffer 업데이트에 관하여*****************
	Object마다 상수버퍼에 담고 있는 값이 다르므로 결국 파이프라인 마다 상수버퍼를 셋팅해주어야 한다.
	Object마다 가지고 있는 VertexBuffer값이 다르므로... VertexShader를 매번 파이프라인에 연결해야 함.
	그러나 상수버퍼의 내용과 vertexbuffer,indexbuffer의 '내용'은 매번 갱신할 필요가 없다. 필요시에만 갱신.
	*/

		UINT vertexSize = 0;
		if (ModelType == MeshModelType::obj)
			vertexSize = sizeof(ObjLoader::VertexFormat);
		else if (ModelType == MeshModelType::Primitive ||
			ModelType == MeshModelType::SkinnedPrimitive ||
			ModelType == MeshModelType::Terrain)
			vertexSize = sizeof(GeometryGenerator::VertexFormat);

		//메시에 따라서 머티리얼달라짐.텍스처와 파라메터들
		//렌더링 할떄 메시가 가진 고유한 머티리얼값 업데이트. 이미 파이프라인에 할당된 머티리얼의 키값이 중복되는거면,인스턴스드 머티리얼이 아니면 업데이트. 
		for (int i = 0; i < Meshes.size(); i++)
		{
			Meshes[i].LinkResourceToPipeline(ModelType, vertexSize);			
		}

			
		for (int i = 0; i < Meshes.size(); i++)
			Meshes[i].Draw();
	}

	void ModelImporter::UpdateConstBuffer(void* constBuffPtr)
	{		
		ConstantBufferManager::ObjectConstBuffFormat* buffFormat = static_cast<ConstantBufferManager::ObjectConstBuffFormat*>(constBuffPtr);
		if (!buffFormat)
			return; 
		buffFormat->TessellationFactor = Math::Vector4(TesselationFactor, TesselationFactor, TesselationFactor, TesselationFactor);
		buffFormat->IsSkinned = UInt4(Skinned,0,0,0);
		
		
	}

}


