#pragma once
#include "Engine.h"
#include "DefineValues.h"
#include "Type\MaterialProperty.h"
#

namespace Engine
{

	class ShaderObject;
	class Texture;
	class MaterialImpl;

	//MaterialInterface class	
	class Material
	{
	private:
		MaterialImpl* Impl;
	public:
		Material();
		Material(const Material& src);		//구현 클래스의 복사 생성자는 어떻게?
		~Material();
		Material& operator=(const Material& rhs);

		

		//추후 어떤 셰이더에 적용할지 필요한 옵션이 생길수도 있으나 일단 그건 나중에
		//getter, setter

		void SetShaderType(Engine::ShaderType type);
		void SetTexture(const std::string& texturePath, Engine::TextureType type);
		void SetProperty(const MaterialProperty& value);
		void LinkResourceToPipeline();
		void UpdateConstantBuffer();
		void SetInstanced(bool flag);
		bool IsInstanced();
	};
}