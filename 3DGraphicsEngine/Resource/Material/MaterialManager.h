#pragma once
#include "UnCopyable.h"
#include <map>
#include <string>
#include <memory>



namespace Engine
{
	class Material;

	class MaterialManager : public UnCopyable
	{
	private:		
		std::map<std::string, std::shared_ptr<Material>> MaterialMap;
		MaterialManager();
	public:
		std::shared_ptr<Material> CreateMaterial(const std::string& materialkey, ShaderType type = ShaderType::Default );
		static MaterialManager& GetInstance()
		{
			static MaterialManager instance;
			return instance;
		}

	};
}

