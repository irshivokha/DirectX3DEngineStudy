#include "EngineBaseHeader.h"
#include "MaterialManager.h"
#include "Material.h"

namespace Engine
{
	MaterialManager::MaterialManager()
	{
		MaterialMap.clear();
	}

	std::shared_ptr<Material> Engine::MaterialManager::CreateMaterial(const std::string& materialkey)
	{
		if (materialkey.empty())
			return nullptr;

		std::map<std::string, std::shared_ptr<Material>>::iterator iter = MaterialMap.find(materialkey);
		if (iter != MaterialMap.end())
			return iter->second;
		else
		{
			Material* mat = new Material();
			//mat->LoadTexture(path, texType, arraySize);
			std::shared_ptr<Material> newMaterial(mat);
			std::pair<std::string, std::shared_ptr<Material>> pair;
			pair.first = materialkey;
			pair.second = newMaterial;
			MaterialMap.insert(pair);

			return newMaterial;
		}
	}
}

