#pragma once

#include "EngineBaseHeader.h"
#include "DefineValues.h"
#include "Type/MaterialProperty.h"


namespace Engine
{
	class ShaderObject;
	enum ShaderType;
	
	//1) Material에선 Surface에 관한 value 값들만 Shader로 전달한다. 이 값들을 위한 ConstBuffer만을 가지고 있다.
	//2) 전역 ConstBuffer. Light나 View,ProjMatrix 같이 공유해서 사용하는 값들은 특정 클래스들에서 접근하여 업데이트하고 가지고 와서 사용해야한다.
	//3) Object 또는 Mesh에 한정된 ConstBuffer. StaticMesh는 World를 매프레임 업데이트 하지 않고 애니메이션 이 있는 오브젝트는 매프레임 한다.
	class MaterialImpl
	{	
	private:					
		std::shared_ptr<ShaderObject> ShaderInstance;	//공개할 필요가 있나?? 셰이더의 이름정도는 공개해도 될듯
		Engine::ShaderType ShaderType;
		bool Instanced;
		MaterialPropertyMap Properties;
	public:
		MaterialImpl();
		MaterialImpl(const MaterialImpl& src);		//구현 클래스의 복사 생성자는 어떻게?
		~MaterialImpl();

		MaterialImpl& operator=(const MaterialImpl& rhs);

		//getter, setter
		void SetShaderType(Engine::ShaderType type );
		void SetTexture(const std::string& texturePath, Engine::TextureType type);
		void SetProperty(const MaterialProperty& aiProp);
		void LinkResourceToPipeline();
		void UpdateConstantBuffer();		
		void SetInstanced(bool flag) { Instanced = flag; }
		//getter
		bool IsInstanced() { return Instanced; }

		//void GetProperty();

	};
}

