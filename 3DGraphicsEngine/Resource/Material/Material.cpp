#include "EngineBaseHeader.h"
#include "Material.h"
#include "Shader/shaderManager.h"
#include "Shader/ShaderObject.h"
#include "RenderClasses/DXUtility.h"
#include "Resource/Texture/Texture.h"
#include "MaterialImpl.h"

namespace Engine
{
	Material::Material()
		:Impl(nullptr)
	{
	}

	Material::Material(const Material& src)		
	{		
	}

	Material::~Material()
	{
	}

	Material& Material::operator=(const Material& rhs)
	{		
		return *this;
	}

	void Material::SetShaderType(Engine::ShaderType type)
	{		
		if (Impl)
			Impl->SetShaderType(type);
	}

	void Material::SetTexture(const std::string& texturePath, Engine::TextureType type)
	{
	}

	void Material::SetProperty(const MaterialProperty& value)
	{
		if (Impl)
			Impl->SetProperty(value);
	}

	void Material::UpdateConstantBuffer()
	{			
		if (Impl)
			Impl->UpdateConstantBuffer();
	}

	void Material::LinkResourceToPipeline()
	{
		if (Impl)
			Impl->LinkResourceToPipeline();
		//UseTexture();
	}

	bool Material::IsInstanced()
	{
		if (Impl)
			return Impl->IsInstanced();
		
		return false;
	}

	void Material::SetInstanced(bool flag)
	{
		if (Impl)
			Impl->SetInstanced(flag);
	}
	
}


