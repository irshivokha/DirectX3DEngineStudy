#include "MaterialImpl.h"
#include "RenderClasses/DXUtility.h"
#include "Shader/ShaderObject.h"
#include "RenderClasses/ConstantBufferManager.h"

namespace Engine
{
	MaterialImpl::MaterialImpl()
		:ShaderInstance(nullptr)
	{
	}

	MaterialImpl::MaterialImpl(const MaterialImpl& src)
		:ShaderInstance(src.ShaderInstance)
	{
	}

	MaterialImpl::~MaterialImpl()
	{
	}

	MaterialImpl& MaterialImpl::operator=(const MaterialImpl& rhs)
	{
		ShaderInstance = rhs.ShaderInstance;
		return *this;
	}

	void MaterialImpl::SetShaderType(Engine::ShaderType type)
	{
		//Shadermanager에 요청해서 Shader를 가져오는 코드 필요
	}

	void MaterialImpl::SetTexture(const std::string& texturePath, TextureType type)
	{
	}
	
	void MaterialImpl::SetProperty(const MaterialProperty& prop)
	{
		
		auto iter = Properties.find(prop.Name);
		if (iter != Properties.end())
		{	
			iter->second.Value = prop.Value;		
		}
		else
		{			
			Properties.insert(make_pair(prop.Name, prop));
		}
	}

	void MaterialImpl::LinkResourceToPipeline()
	{
		ShaderInstance->LinkToPipeline();
	}

	void MaterialImpl::UpdateConstantBuffer()
	{
		if (ShaderInstance)
		{					
			//properties를 넘기기.
			ShaderInstance->UpdateConstantBuffer(Properties);
		}
	}


}




