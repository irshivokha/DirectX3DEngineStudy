#pragma once
#include "UnCopyable.h"
#include "Texture.h"


namespace Engine
{
	class Texture;

	class TextureManager : public UnCopyable
	{		
	private:
		std::map<std::string, std::shared_ptr<Texture>> TextureMap;
		TextureManager();
	public:		
		std::shared_ptr<Texture> LoadTexture(const std::string& path, Texture::TextureType texType = Texture::TextureType::tex2D, UINT arraySize = 1, 
			float customWidth = 0.0f, float customHeight = 0.0f );
		static TextureManager& GetInstance()
		{
			static TextureManager instance;
			return instance;
		}

	};
}
