#pragma once
//복사가 허용되어야 하나? 고민.....

namespace Engine
{
	class Texture
	{
	private:
		ID3D11Texture2D* Texture2D;
		ID3D11ShaderResourceView* Texture2DResourceView;
		int Width;
		int Height;
		int Depth;
		byte* RawDataPtr;
		TextureType Type;
		string Path;
		void CreateTexture2D(UINT arraysize, DXGI_FORMAT format, UINT mipLevels, UINT bindFlag = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET);

	public:
		enum class TextureResourceType
		{			
			tex1D,
			tex2D,
			tex2DUnordered,
			tex3D			
		};

		Texture();
		//Texture(const Texture&);
		~Texture();
		void LoadTexture(const std::string& path, TextureType texType = TextureType::tex2D, UINT arraySize = 1, float customWidth = 0.0f, float customHeight = 0.0f);
		ID3D11ShaderResourceView* Get2DShaderResourceView() { return Texture2DResourceView; };
		void SetTexType(TextureType texType) { Type = texType; }
		void SetTexPath(const string& path) { Path = path; }
						
	};
}


