#include "EngineBaseHeader.h"
#include <iostream>
#include "PngParser.h"
#include "lodepng.h"

#define PNGSIGSIZE 8

void decodeOneStep(const char* filename, std::vector<unsigned char>& imageOut, int& width, int& height) {
	//std::vector<unsigned char> image; //the raw pixels
	unsigned w, h;

	//decode
	unsigned error = lodepng::decode(imageOut, w, h, filename);
	int size = imageOut.size();
	width = w;
	height = h;
	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++)
		{
			int r = (int)imageOut[4 * (i * width + j) + 0];
			int g = (int)imageOut[4 * (i * width + j) + 1];
			int b = (int)imageOut[4 * (i * width + j) + 2];
			int a = (int)imageOut[4 * (i * width + j) + 3];
			if (g >200)
				int k = 5;
		}
	}
	//if there's an error, display it
	if (error) std::cout << "decoder error " << error << ": " << lodepng_error_text(error) << std::endl;

	//the pixels are now in the vector "image", 4 bytes per pixel, ordered RGBARGBA..., use it as texture, draw it, ...
}

//Example 2
//Load PNG file from disk to memory first, then decode to raw pixels in memory.
void decodeTwoSteps(const char* filename) {
	std::vector<unsigned char> png;
	std::vector<unsigned char> image; //the raw pixels
	unsigned width, height;

	//load and decode
	unsigned error = lodepng::load_file(png, filename);
	if (!error) error = lodepng::decode(image, width, height, png);

	//if there's an error, display it
	if (error) std::cout << "decoder error " << error << ": " << lodepng_error_text(error) << std::endl;

	//the pixels are now in the vector "image", 4 bytes per pixel, ordered RGBARGBA..., use it as texture, draw it, ...
}

//Example 3
//Load PNG file from disk using a State, normally needed for more advanced usage.
void decodeWithState(const char* filename) {
	std::vector<unsigned char> png;
	std::vector<unsigned char> image; //the raw pixels
	unsigned width, height;
	lodepng::State state; //optionally customize this one

	unsigned error = lodepng::load_file(png, filename); //load the image file with given filename
	if (!error) error = lodepng::decode(image, width, height, state, png);

	//if there's an error, display it
	if (error) std::cout << "decoder error " << error << ": " << lodepng_error_text(error) << std::endl;

	//the pixels are now in the vector "image", 4 bytes per pixel, ordered RGBARGBA..., use it as texture, draw it, ...
	//State state contains extra information about the PNG such as text chunks, ...
}

void ReadPng(const char* path, int& width, int& height, std::vector<unsigned char>& imageOut)
{
	decodeOneStep(path, imageOut,width,height);

}

