#include "EngineBaseHeader.h"
#include "Texture.h"
#include "ImageParser/PngParser.h"
#include "RenderClasses/DXUtility.h"

namespace Engine
{
	Texture::Texture()
		:Texture2D(nullptr),
		Texture2DResourceView(nullptr),
		RawDataPtr(nullptr),
		Width(0),
		Height(0),
		Depth(0),
		Type(),
		Path()
	{

	}


	Texture::~Texture()
	{

	}

	void Texture::LoadTexture(const std::string& path, TextureType texType, UINT arraySize, float customWidth, float customHeight)
	{
		switch (texType)
		{
		case TextureType::tex2D:
		{
			std::vector<unsigned char> image;

			std::string ext = path.substr(path.find_last_of(".") + 1);
			if (ext == "png")
			{
				ReadPng(path.c_str(), Width, Height, image);				
			}

			CreateTexture2D(arraySize, DXGI_FORMAT_R8G8B8A8_UNORM, 0);
			DXUtility::GetInstance()->GetDeviceContext()->UpdateSubresource(Texture2D, 0, nullptr, image.data(),
				Width * sizeof(byte) * 4, Height);
			DXUtility::GetInstance()->GetDeviceContext()->GenerateMips(Texture2DResourceView);
		}
		break;
		case TextureType::tex2DUnordered:
		{
			Width = (int)customWidth;
			Height = (int)customHeight;
			CreateTexture2D(arraySize, DXGI_FORMAT_R8G8B8A8_UNORM, 0, D3D11_BIND_UNORDERED_ACCESS | D3D11_BIND_SHADER_RESOURCE);
		}
		break;

		}
		
	}

	void Texture::CreateTexture2D(UINT arraysize, DXGI_FORMAT format, UINT mipLevels, UINT bindFlags)
	{
		D3D11_TEXTURE2D_DESC desc;
		desc.Width = Width;
		desc.Height = Height;
		desc.MipLevels = mipLevels;
		desc.ArraySize = arraysize;
		desc.Format = format;
		desc.SampleDesc.Count = 1;
		desc.SampleDesc.Quality = 0;
		desc.BindFlags = bindFlags;
		desc.Usage = D3D11_USAGE_DEFAULT;
		desc.CPUAccessFlags = 0;
		desc.MiscFlags = D3D11_RESOURCE_MISC_GENERATE_MIPS;


		//D3D11_SUBRESOURCE_DATA data;	<--initialData 넣는법좀 알아야 할듯	
		DXUtility::GetInstance()->GetDevice()->CreateTexture2D(&desc, nullptr, &Texture2D);	//Resource 하나가 Array를 담을수 있는가? Resource하나당 

		D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;

		srvDesc.Format = format;
		srvDesc.Texture2D.MipLevels = -1;
		srvDesc.Texture2D.MostDetailedMip = 0;
		srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;		
		if(Texture2D)
			DXUtility::GetInstance()->GetDevice()->CreateShaderResourceView(Texture2D, &srvDesc, &Texture2DResourceView);
	}
}




