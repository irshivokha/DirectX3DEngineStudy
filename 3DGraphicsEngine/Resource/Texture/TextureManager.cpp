#include "EngineBaseHeader.h"
#include "TextureManager.h"

namespace Engine
{
	TextureManager::TextureManager()
	{
		TextureMap.clear();
	}
	std::shared_ptr<Texture> TextureManager::LoadTexture(const std::string& path, Texture::TextureType texType, UINT arraySize, float customWidth, float customHeight)
	{

		if (path.empty())
			return nullptr;
		
		std::map<std::string, std::shared_ptr<Texture>>::iterator iter = TextureMap.find(path);
		if (iter != TextureMap.end())
			return iter->second;
		else
		{
			Texture* tex = new Texture();
			tex->LoadTexture(path,texType,arraySize);
			std::shared_ptr<Texture> newTexture(tex);
			std::pair<std::string, std::shared_ptr<Texture>> pair;
			pair.first = path;
			pair.second = newTexture;
			TextureMap.insert(pair);
			return newTexture;
		}
	}
}


