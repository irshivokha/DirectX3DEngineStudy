#pragma once
#include <time.h>
#include "EngineBaseHeader.h"
#include "Type/MaterialProperty.h"
#include <memory>
namespace Engine
{
	class GameObject;
	class Bone;

	template<typename T>
	inline T Randomize(const T& min, const T& max)
	{

		srand((unsigned)time(nullptr));
		T minVal;
		T maxVal;
		if (max > 500000)
			maxVal = 500000;
				
		if (min < 0)					
			minVal = 0;
		
			
		T returnVal;
		returnVal = (((unsigned long)rand() << 15) | rand()) % max + min;

		return returnVal;
	}
	
	//inline int Randomize(const UINT min, const UINT max)
	//{

	//	srand((unsigned)time(nullptr));
	//	UINT minVal;
	//	UINT maxVal;
	//	if (max > 500000)
	//		maxVal = 500000;

	//	if (min < 0)
	//		minVal = 0;


	//	UINT returnVal;
	//	//returnVal = (((unsigned long)rand() << 15) | rand()) % max + min;
	//	returnVal = rand();

	//	return returnVal;
	//}

	//실수일 경우와 고려 안되있음. 최대값 최소값은 사용하는 쪽에서 제한을 두는게 맞음 아님 여기서?


	//Shader종류 

	enum class ShaderType
	{
		Default,
		Full,
		Lite,
		Cartoon,
		Terrain,
		//E_BUMPSPEC,
		//E_SKYDOME,
		//E_DEFAULT_SKYDOME,
		//E_TERRAIN_DEFAULT,
		//E_HORIZONTAL,
		//E_VERTICAL,
		//E_MD5,
		//E_ALPHA,
		//E_BLEND,
		//E_ExtractDepth,
		//E_CompositDOF,
		//E_DIFFUSELIGHT,
		//E_UI,
		//E_FONT,
		//E_LIGHTMAP,
		//E_FOG,
		//E_ClipPlane,
		//E_Reflection,
		//E_Fade,
		//E_Shadow,
		Max_Type,
	};

	enum class MeshModelType
	{
		Primitive,
		SkinnedPrimitive,
		Terrain,
		obj,
		md5,
		fbx,
		stl,
	};

	enum class PropertyType
	{
		DiffuseMap,		//naming은 다시..
		NormalMap,
		SpecularMap,
		Diffuse,
		Ambient,
		Specular,
		HeightmapInfo,
		Type_Max,
	};
	
	namespace MaterialKey
	{
		const std::string DiffuseColor("DiffuseColor");
		const std::string SpecularColor("SpecularColor");
		const std::string AmbientColor("AmbientColor");
		const std::string DiffuseMap("DiffuseMap");
		const std::string NormalMap("NormalMap");
		const std::string SpecularMap("SpecularMap");
	}

	enum class TextureType
	{
		DiffuseMap,
		NormalMap,
		SpecularMap,
		Type_Max
	};

	typedef XMFLOAT2 Float2;
	typedef XMFLOAT3 Float3;
	typedef XMFLOAT4 Float4;	

	typedef Bone* BonePtr;
	typedef std::shared_ptr<GameObject> GameObjPtr;

	typedef map<string, MaterialProperty> MaterialPropertyMap;
}

#define clamp(value, minimum, maximum) (max(min((value),(maximum)),(minimum)))


