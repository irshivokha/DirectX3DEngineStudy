#pragma once

#include "Engine.h"

class UnCopyable
{
protected:
	UnCopyable() {};
	virtual ~UnCopyable() {};
private:
	UnCopyable(const UnCopyable&);
	UnCopyable& operator=(const UnCopyable&);
};