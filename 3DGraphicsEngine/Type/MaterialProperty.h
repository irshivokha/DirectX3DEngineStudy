#pragma once

#include "EngineBaseHeader.h"
#include "DefineValues.h"
#include "DxMath/customMath.h"

//template <typename T>
//class Type
//{
//private:	
//	T Value;
//public:	
//	Type() {};
//	Type(const Type& src) { Value = src.Value; };
//	Type& operator=(const Type& rhs) { Value = rhs.Value; };
//};
//
//template <typename T>
//class Property			//일단은 셰이더 상수버퍼 전달용으로만 값을 설정했고, 이후 더 옵션이 늘어나면 형태를 확장해보기
//{
//private:
//	std::string Name;
//	Type<T> Value;
//public:
//	Property() {};
//	Property(const Property& src) { Value = src.Value; };
//	Property& operator=(const Property& rhs) { Name = rhs.Name; Value = rhs.Value; };
//
//	const std::string& GetName() { return Name; };
//	void SeValue(T value) { Value = value; };
//};


namespace Engine
{
	//typeBase는 load,save, 타입정보등을 가지고있음
	class TypeBase
	{
	public:
		enum class EnumType
		{
			TypeNone,
			TypeInt,
			TypeFloat,
			TypeBool,
			TypeString,
			TypeVector2,
			TypeVector3,
			TypeVector4,
			TypeTexture,

		};
	private:
		EnumType Type;
	public:
		TypeBase():Type(EnumType::TypeNone)
		{}
		TypeBase(const TypeBase& src) :Type(src.Type)
		{}
		TypeBase(EnumType type) :Type(type)
		{}
		virtual ~TypeBase() {};
		TypeBase& operator=(const TypeBase& rhs)
		{
			Type = rhs.Type;
			return *this;
		}
	};

	class TypeInt : public TypeBase
	{
	public:
		TypeInt(int val): TypeBase(EnumType::TypeInt),
			Value(val) 
		{}

		TypeInt(const TypeInt& src)
			: TypeBase(src),
			Value(src.Value)
		{
		}

		TypeInt& operator=(const TypeInt& src)
		{
			TypeBase::operator=(src);
			Value = src.Value;
			return *this;
		}

		int GetValue() { return Value; }
		void SetValue(int value) { Value = value; }
	private:
		int Value;
	};

	class TypeFloat : public TypeBase
	{
	public:
		TypeFloat(float val) :TypeBase(EnumType::TypeFloat),
			Value(val)
		{}

		TypeFloat(const TypeFloat& src)
			: TypeBase(src),
			Value(src.Value)
		{
		}

		TypeFloat& operator=(const TypeFloat& src)
		{
			TypeBase::operator=(src);
			Value = src.Value;
			return *this;
		}

		float GetValue() { return Value; }
		void SetValue(float value) { Value = value; }
	private:
		float Value;
	};

	class TypeBool : public TypeBase
	{
	public:
		TypeBool(bool val) :TypeBase(EnumType::TypeBool),
			Value(val)
		{}

		TypeBool(const TypeBool &src)
			: TypeBase(src),
			Value(src.Value)
		{}

		TypeBool& operator=(const TypeBool& src)
		{
			TypeBase::operator=(src);
			Value = src.Value;
			return *this;
		}

		float GetValue() { return Value; }
		void SetValue(float value) { Value = value; }
	private:
		bool Value;
	};

	class TypeVector2 : public TypeBase
	{
	public:
		TypeVector2(int x, int y) :TypeBase(EnumType::TypeVector2),
			Value(x, y)
		{}

		TypeVector2(Vector2 src) :TypeBase(EnumType::TypeVector2),
			Value(src)
		{}

		TypeVector2(const TypeVector2& src)
			: TypeBase(src),
			Value(src.Value)
		{}

		TypeVector2& operator=(const TypeVector2& src)
		{
			TypeBase::operator=(src);
			Value = src.Value;
			return *this;
		}

		Vector2 GetValue() { return Value; }
		void SetValue(Vector2 value) { Value = value; }
	private:
		Vector2 Value;
	};

	class TypeVector3 : public TypeBase
	{
	public:
		TypeVector3( int x, int y, int z):TypeBase(EnumType::TypeVector3),
			Value(x, y, z)
		{}

		TypeVector3(Vector3 src) :TypeBase(EnumType::TypeVector3),
			Value(src)
		{}

		TypeVector3(const TypeVector3& src)
			: TypeBase(src),
			Value(src.Value)
		{}

		TypeVector3& operator=(const TypeVector3& src)
		{
			TypeBase::operator=(src);
			Value = src.Value;
			return *this;
		}

		Vector3 GetValue() { return Value; }
		void SetValue(Vector3 value) { Value = value; }
	private:
		Vector3 Value;
	};

	class TypeVector4 : public TypeBase
	{
	public:		
		TypeVector4( int x, int y, int z, int w) :TypeBase(EnumType::TypeVector4),
			Value( x, y, z, w )
		{
		}

		TypeVector4(Vector4 src) :TypeBase(EnumType::TypeVector4),
			Value(src)
		{
		}

		TypeVector4(const TypeVector4& src)
			: TypeBase(src),
			Value(src.Value)
		{
		}

		TypeVector4& operator=(const TypeVector4& src)
		{
			TypeBase::operator=(src);
			Value = src.Value;
			return *this;
		}
		Vector4 GetValue() { return Value; }
		void SetValue(Vector4 value) { Value = value; }
	private:
		Vector4 Value;
	};

	class TypeTexture : public TypeBase
	{
	public:
		TypeTexture(string texPath) :TypeBase(EnumType::TypeTexture),
			Value(texPath)
		{}

		TypeTexture(const TypeTexture& src)
			:TypeBase(src),
			Value(src.Value)
		{}

		TypeTexture& operator=(const TypeTexture& src)
		{
			TypeTexture::operator=(src);
			Value = src.Value;
			return *this;
		}

		string GetValue() { return Value; }
		void SetValue(string value) { Value = value; }
	private:
		string Value;

	};




	//property는 UI쪽에서 표시하거나, propertyGroup으로 묶었을때 저장하기 위한 
	struct MaterialProperty
	{	
		string MatKey;
		int Index;		
		TypeBase* Value;	
		MaterialProperty::MaterialProperty(string matkey, int index, TypeBase* value)
			:MatKey(matkey),Index(index),Value(value)
		{
		}
		//Property(const Property& src) { Value = src.Value; };
		//Property& operator=(const Property& rhs) { Name = rhs.Name; Value = rhs.Value; };

		//const std::string& GetName() { return Name; };
		//void SeValue(T value) { Value = value; };
	};

}


