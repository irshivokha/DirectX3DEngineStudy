#include "GameObject/IScript.h"
//#include "GameObject/GameObjectManager.h"
//#include "GameObject/GameObject.h"

#ifdef DLL_EXPORT

#define DLL_DECLSPEC  __declspec(dllexport)   

#else

#define DLL_DECLSPEC  __declspec(dllimport)   

#endif


namespace Engine
{
	namespace Script
	{
		
		class MainScript :public IScript
		{		
		public:
			MainScript();

			virtual void Start() override;
			
			virtual void Update() override;

			void Test(void (*func)());
		};
	}
}