#include "GameObject/GameObjectManager.h"
#include "GameObject/GameObject.h"
#include "MainScript.h"
#include "System/Timer.h"
#include "EngineBaseHeader.h"
#include "Resource/Texture/TextureManager.h"
#include "Resource/Material/Material.h"
#include "DefineValues.h"
#include "Scene/Camera.h"
#include "Input/directinput.h"
//#include "DefineValues.h"



namespace Engine
{
	using namespace Math;
	namespace Script
	{
		GameObjPtr CatObject = nullptr;
		GameObjPtr CubeObject = nullptr;		
		GameObjPtr ConeObject0 = nullptr;
		GameObjPtr ConeObject1 = nullptr;
		GameObjPtr ConeObject2 = nullptr;
		GameObjPtr PlaneObject = nullptr;
		GameObjPtr SkinnedConeObj = nullptr;
		GameObjPtr TerrainObj = nullptr;

		std::string projectDir = "../Projects/FirstGame/" ;
		float Angle = 0.0;
		float LocalPosFactor = 0.0f;
		float tesselFactor = 1.0f;

		MainScript::MainScript()
		{
		}

		void MainScript::Start()
		{
			CatObject = GameObjectManager::GetInstance().CreateGameObject();
			//CubeObject = GameObjectManager::GetInstance().CreateGameObject();
			//PlaneObject = GameObjectManager::GetInstance().CreateGameObject();
			//ConeObject0 = GameObjectManager::GetInstance().CreateGameObject();
			//ConeObject1 = GameObjectManager::GetInstance().CreateGameObject();
			//ConeObject2 = GameObjectManager::GetInstance().CreateGameObject();

			SkinnedConeObj = GameObjectManager::GetInstance().CreateGameObject();

			//	
			CatObject->ImportMesh(projectDir + "Assets/Models/GUST_NPC_TOWN_A11_QUEST_YANG_SUYEON/GUST_NPC_TOWN_A11_QUEST_YANG_SUYEON.X", MeshModelType::obj);
			////CubeObject->ImportMesh(projectDir + "Assets/Models/cube/cube.obj");
			////ConeObject0->ImportMesh("Cone");
			////ConeObject1->ImportMesh("Cone");
			////ConeObject2->ImportMesh("Cone");
			////PlaneObject->ImportMesh("Plane");
			SkinnedConeObj->ImportMesh("SkinnedCone",MeshModelType::SkinnedPrimitive);
			//

			CatObject->SetName("cat");
			////CubeObject->SetName("cube");
			////ConeObject->SetName("Cone");
			////PlaneObject->SetName("Plane");
			//SkinnedConeObj->SetName("SkinnedCone");

			Engine::Transform& catTrans = CatObject->GetTransform();
			catTrans.SetPosition(-0.5, 0, -2.5);
			////Engine::Transform& cubeTrans = CubeObject->GetTransform();			
			////cubeTrans.SetPosition(1, 0, 0);			

			////Engine::Transform& planeTrans = PlaneObject->GetTransform();
			////planeTrans.SetPosition(0, -1, 0);
			////planeTrans.SetRotation(0, 0, 75);

			////Engine::Transform& coneTrans0 = ConeObject0->GetTransform();
			////coneTrans0.SetPosition(0, 1.5, -2);			
			////coneTrans0.SetRotation(0, 0, 0);			
			////Engine::Transform& coneTrans1 = ConeObject1->GetTransform();
			////coneTrans1.SetPosition(0, 1, 0);
			////coneTrans1.SetRotation(0, 0, 15);
			////ConeObject1->SetChild(ConeObject2, ConeObject1);
			////Engine::Transform& coneTrans2 = ConeObject2->GetTransform();
			////coneTrans2.SetPosition(0, 1, 0);
			////coneTrans2.SetRotation(0, 0, 0);

			Engine::Transform& skinnedConeTrans = SkinnedConeObj->GetTransform();
			skinnedConeTrans.SetPosition(0, 0.5, -2);
			skinnedConeTrans.SetScale(0.4, 0.4, 0.4);


			//int test = PlaneObject.use_count();

			//PlaneObject->SetChild(ConeObject, PlaneObject);

			//int test2 = PlaneObject.use_count();

			TerrainObj = GameObjectManager::GetInstance().CreateGameObject();
			TerrainObj->ImportMesh("Terrain", MeshModelType::Terrain);

			TerrainObj->SetName("Land");

			Engine::Transform& TerrainTrans = TerrainObj->GetTransform();
			TerrainTrans.SetPosition(0, 0.6, -3);


			//텍스쳐도 파일에서 파싱해서 가져오기.
			std::shared_ptr<Texture> heightTex = Engine::TextureManager::GetInstance().LoadTexture(projectDir + "Assets/Textures/TerrainHeightMap.png");
			TerrainObj->GetMaterial()->SetHeightTexture(heightTex.get());
			//std::shared_ptr<Texture> lookUpTex = Engine::TextureManager::GetInstance().LoadTexture(projectDir + "Assets/Textures/");

			std::shared_ptr<Texture> computeLookupTex = Engine::TextureManager::GetInstance().LoadTexture(projectDir, Engine::Texture::TextureType::tex2DUnordered,
			0, 512, 512);
			TerrainObj->GetMaterial()->SetComputeLookupTexture(computeLookupTex.get());

			std::shared_ptr<Texture> coneColorTex = Engine::TextureManager::GetInstance().LoadTexture(projectDir + "Assets/Textures/EyeOfHorus_128_Blurred.png");
			std::shared_ptr<Texture> coneHeightTex = Engine::TextureManager::GetInstance().LoadTexture(projectDir + "Assets/Textures/EyeOfHorus.png");
			SkinnedConeObj->GetMaterial()->SetColorTexture(coneColorTex.get());
			SkinnedConeObj->GetMaterial()->SetHeightTexture(coneHeightTex.get());
		}

		void MainScript::Update()
		{
			double elapsedTime = Timer::GetInstance().GetElapsedTime();
			Angle += 30 * (float)elapsedTime;
			LocalPosFactor += (0.1f * elapsedTime);
			if (Angle > 360.0f)
				Angle -= 360.0f;


			//if (Angle >= 2.0f * DirectX::XM_PI)
			//	Angle -= 2.0f * DirectX::XM_PI;
			//Engine::Transform& cubeTrans = CubeObject->GetTransform();			
			//cubeTrans.RotateY(angle);
			//Engine::Transform& catTrans = CatObject->GetTransform();
			//catTrans.RotateY(angle);

			//Engine::Mesh* cubeMesh = CubeObject->GetMesh();
			//Engine::Mesh* catMesh = CatObject->GetMesh();
			//Engine::Mesh* conMesh = ConeObject->GetMesh();
			//Engine::Transform& coneTrans = ConeObject->GetTransform();
			//Engine::Transform& planeTrans = PlaneObject->GetTransform();
			//
			//Vector3 currPos = PlaneObject->GetTransform().GetPosition();
			////Vector3 newPos = currPos + Vector3(0.01, 0, 0);
			//Vector3 newPos = currPos + Vector3(0.1f * elapsedTime, 0, 0);
			////planeTrans.SetPosition(newPos);
			////planeTrans.RotationX(Angle);
			//float currScaleFactor = PlaneObject->GetTransform().GetScale().z;
			////float newScale = currScale + Vector3(0.1f * elapsedTime, 0, 0);
			////planeTrans.SetScale(1, 1, currScaleFactor + (0.1f * elapsedTime));
			//Vector3 currConPos = coneTrans.GetPosition();
			//coneTrans.SetPosition(currConPos.x, LocalPosFactor, currConPos.z);
			//planeTrans.RotationY((DirectX::XM_PI * 1.0 / 3.0f));
			//planeTrans.SetLocalRotation(0, Angle, 0);
			//coneTrans.SetRotation(0, 0, Angle * 10);

			Engine::Transform& skinnedConeTrans = SkinnedConeObj->GetTransform();
			skinnedConeTrans.SetRotation(0, Angle, 0);
			//
			////planeTrans.SetPosition(
			////coneTrans.LocalRotationY(angle);
			////coneTrans.RotationY(angle);

			//tesselFactor += (float)elapsedTime;
			////if (cubeMesh)
			////	cubeMesh->SetTesselationFactor((sinf(tesselFactor*0.5f) * 3.0f) + 3.1f);		//sinf(tesselFactor) + 1.0f

			////if (catMesh)
			////	catMesh->SetTesselationFactor((sinf(tesselFactor * 0.5f) * 1.2f) + 1.4f);

			////if (conMesh)
			////	conMesh->SetTesselationFactor((sinf(tesselFactor * 0.5f) * 1.2f) + 1.4f);


			////ConeObject->GetMaterial()->SetTexture(coneTex.get());
			////PlaneObject->GetMaterial()->SetTexture(coneTex.get());


			

			
			//Math::Vector3 newPos = TerrainTrans.GetPosition();
			//newPos.y -= 0.00001f;
			//TerrainTrans.SetPosition(newPos);
			//cubeTrans.SetRotation()
		}

		void MainScript::Test(void(*func)())
		{
			func();
		}

	}
}



