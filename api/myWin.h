#pragma once

#include<Windows.h>

class C_MYWIN
{
private:
	HINSTANCE	m_hInstance;
	HWND		m_hWnd;
public:
	C_MYWIN();
	BOOL Init(HINSTANCE hInstance);
	void UpdateMsg(bool (*pUpdateFunc)());
	LRESULT CALLBACK MyProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	static LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	HWND GetHwnd();
};