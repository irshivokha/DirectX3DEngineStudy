#include "stdafx.h"

C_MYWIN::C_MYWIN() :
	m_hInstance(nullptr),
	m_hWnd(nullptr)
{

}

BOOL C_MYWIN::Init(HINSTANCE hInstance)
{
	WNDCLASSEXW wcex;
	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = 0;
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = 0;
	wcex.lpszClassName = L"class";
	wcex.hIconSm = 0;
	RegisterClassExW(&wcex);

	m_hWnd = CreateWindowW(L"class", nullptr, WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

	if (!m_hWnd)
	{
		return FALSE;
	}
	m_hInstance = hInstance;

	SetWindowLong(m_hWnd, GWL_USERDATA, (LONG)this);

	ShowWindow(m_hWnd, SW_SHOW);
	UpdateWindow(m_hWnd);

	return TRUE;
}

void C_MYWIN::UpdateMsg(bool(*pUpdateFunc)())
{
	MSG msg = { 0 };
	bool result = false;
	while (WM_QUIT != msg.message)
	{
		if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
		{
			if (pUpdateFunc)
				result = pUpdateFunc();

			if (result == false)
				return ;
		}
	}
}

LRESULT C_MYWIN::MyProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_SIZE:

		break;
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);
		// TODO: 여기에 hdc를 사용하는 그리기 코드를 추가합니다.
		EndPaint(hWnd, &ps);
	}
	break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

LRESULT CALLBACK C_MYWIN::WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	C_MYWIN *pWin = (C_MYWIN *)GetWindowLong(hWnd, GWL_USERDATA);
	if (pWin)
		return pWin->MyProc(hWnd, message, wParam, lParam);

	return DefWindowProc(hWnd, message, wParam, lParam);
}

HWND C_MYWIN::GetHwnd()
{
	return m_hWnd;
}

